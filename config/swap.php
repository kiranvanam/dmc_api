<?php

/*
 * This file is part of Laravel Swap.
 *
 * (c) Florian Voutzinos <florian@voutzinos.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

return [

    /*
    |--------------------------------------------------------------------------
    | Options.
    |--------------------------------------------------------------------------
    |
    | The options to pass to Swap amongst:
    |
    | * cache_ttl: The cache ttl in seconds.
    */
    'options' => [],

    /*
    |--------------------------------------------------------------------------
    | Services
    |--------------------------------------------------------------------------
    |
    | This option specifies the services to use with their name as key and
    | their config as value.
    |
    | Here is the config spec for each service:
    |
    | * "central_bank_of_czech_republic", "central_bank_of_republic_turkey", "european_central_bank", "fixer"
    |   "google", "national_bank_of_romania", "webservicex", "yahoo" can be enabled with "true" as value.
    |
    | * 'currency_layer' => [
    |       'access_key' => 'secret', // Your app id
    |       'enterprise' => true, // True if your AppId is an enterprise one
    |   ]
    |
    | * 'open_exchange_rates' => [
    |       'app_id' => 'secret', // Your app id
    |       'enterprise' => true, // True if your AppId is an enterprise one
    |   ]
    |
    | * 'xignite' => [
    |       'token' => 'secret', // The API token
    |   ]
    |
    */

    'services' => [
        'fixer' => [ 'access_key' => '78ebe146e175a055e505128a2d73292c', 'enterprise' => false],
        'currency_layer' => [ 'access_key' => 'd62d08ef4c6a99421e74e0a62292656f', 'enterprise' => false ],
        "currency_converter_api"=> [ 'api_key' => "ed87bb0190a993d72076", "access_key" => "ed87bb0190a993d72076"],
        'yahoo' => true,
        'forge' => [ 'api_key'  => 'jGxXqScFqTt057yBo5L4fwv9qpGZ10oy'],
        'google' => true,
    ],

    /*
    |--------------------------------------------------------------------------
    | Cache
    |--------------------------------------------------------------------------
    |
    | This option specifies the Laravel cache store to use.
    |
    | 'cache' => 'file'
    */
    'cache' => null,

    /*
    |--------------------------------------------------------------------------
    | Http Client.
    |--------------------------------------------------------------------------
    |
    | The HTTP client service name to use.
    */
    'http_client' => null,

    /*
    |--------------------------------------------------------------------------
    | Request Factory.
    |--------------------------------------------------------------------------
    |
    | The Request Factory service name to use.
    */
    'request_factory' => null,

    /*
    |--------------------------------------------------------------------------
    | Cache Item Pool.
    |--------------------------------------------------------------------------
    |
    | The Cache Item Pool service name to use.
    */
    'cache_item_pool' => null,
];
