<?php

return [ 
    "resource" => [ "name" => "Bookings", "slug" => "bookings"],

    "permissions" => [],
    
    "sub" => [        
        [ 
            "resource" => [ "name" => "Accomodation", "slug" => "accomodation"],

            "permissions" => [
                ["name" => "View All", "slug" => "list"],
                ["name" => "View Own Agents bookings", "slug" => "agents-list"],
                ["name" => "View Own bookings", "slug" => "own-list"],
                ["name" => "Book", "slug" => "book"],
                ["name" => "Vochuer",  "slug" => "voucher"],
                ["name" => "Cancel",  "slug" => "cancel"],                
              ]
        ],
        [ 
            "resource" => [ "name" => "Tours", "slug" => "tours"],

            "permissions" => [
                ["name" => "View All", "slug" => "list"],
                ["name" => "View Own Agents bookings", "slug" => "agents-list"],
                ["name" => "View Own bookings", "slug" => "own-list"],
                ["name" => "Book", "slug" => "book"],
                ["name" => "Vochuer",  "slug" => "voucher"],
                ["name" => "Cancel",  "slug" => "cancel"],                
              ]
        ],
        [ 
            "resource" => [ "name" => "Transfers", "slug" => "transfers"],

            "permissions" => [ 
                ["name" => "View All", "slug" => "list"],
                ["name" => "View Own Agents bookings", "slug" => "agents-list"],
                ["name" => "View Own bookings", "slug" => "own-list"],
                ["name" => "Book", "slug" => "book"],
                ["name" => "Vochuer",  "slug" => "voucher"],
                ["name" => "Cancel",  "slug" => "cancel"],                
              ]
        ],
        [ 
            "resource" => [ "name" => "Meals", "slug" => "meals"],

            "permissions" => [
                ["name" => "View All", "slug" => "list"],
                ["name" => "View Own Agents bookings", "slug" => "agents-list"],
                ["name" => "View Own bookings", "slug" => "own-list"],
                ["name" => "Book", "slug" => "book"],
                ["name" => "Vochuer",  "slug" => "voucher"],
                ["name" => "Cancel",  "slug" => "cancel"],                
              ]
        ],
        [ 
            "resource" => [ "name" => "Guides", "slug" => "guides"],

            "permissions" => [
                ["name" => "View All", "slug" => "list"],
                ["name" => "View Own Agents bookings", "slug" => "agents-list"],
                ["name" => "View Own bookings", "slug" => "own-list"], 
                ["name" => "Book", "slug" => "book"],
                ["name" => "Vochuer",  "slug" => "voucher"],
                ["name" => "Cancel",  "slug" => "cancel"],                
              ]
        ],
        [ 
            "resource" => [ "name" => "Visas", "slug" => "visas"],

            "permissions" => [
                ["name" => "View All", "slug" => "list"],
                ["name" => "View Own Agents bookings", "slug" => "agents-list"],
                ["name" => "View Own bookings", "slug" => "own-list"],
                ["name" => "Book", "slug" => "book"],
                ["name" => "Vochuer",  "slug" => "voucher"],
                ["name" => "Cancel",  "slug" => "cancel"],                
              ]
        ],
        [ 
            "resource" => [ "name" => "Visas", "slug" => "visas"],

            "permissions" => [
                ["name" => "View All", "slug" => "list"],
                ["name" => "View Own Agents bookings", "slug" => "agents-list"],
                ["name" => "View Own bookings", "slug" => "own-list"],
                ["name" => "Book", "slug" => "book"],
                ["name" => "Vochuer",  "slug" => "voucher"],
                ["name" => "Cancel",  "slug" => "cancel"],                
              ]
        ],
    ]
];