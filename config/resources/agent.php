<?php

return [ 
    "resource" => [ "name" => "Agents", "slug" => "agents"],
    "permissions" => [ 
        ["name" => "List View", "slug" => "list"], 
        ["name" => "View details", "slug" => "details"], 
        ["name" => "Edit",  "slug" => "edit"], 
        ["name" => "Create", "slug" => "create"], 
        ["name" => "Delete", "slug" => "delete"],
        ["name" => "Approve", "slug" => "approve"],
        ["name" => "Can Edit Credit limit", "slug" => "credit-limit"],
      ],
    "sub" => []
];