<?php

return [ 
    "resource" => [ "name" => "Roles", "slug" => "roles"],

    "permissions" => [ 
        ["name" => "List View", "slug" => "list"],
        ['name' => 'Own List View', 'slug' => "own-list" ],
        ["name" => "View details", "slug" => "details"], 
        ["name" => "Edit",  "slug" => "edit"], 
        ["name" => "Create", "slug" => "create"], 
        ["name" => "Delete", "slug" => "delete"]
      ],
    
    "sub" => []
];