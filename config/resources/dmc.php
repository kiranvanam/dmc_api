<?php

return [
    "resource" => [ "name" => "Dmc", "slug" => "dmc"],
    "permissions" => [ 
        ["name" => "List View", "slug" => "list"], 
        ["name" => "View details", "slug" => "details"], 
        ["name" => "Edit",  "slug" => "edit"], 
        ["name" => "Create", "slug" => "create"], 
        ["name" => "Delete", "slug" => "delete"]
      ],
    "sub" => []
];