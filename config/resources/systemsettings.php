<?php

return [ 
    "resource" => [ "name" => "System settings", "slug" => "system-settings"],

    "permissions" => [],
    
    "sub" => [        
        [ 
            "resource" => [ "name" => "Currencies", "slug" => "currencies"],

            "permissions" => [ 
                ["name" => "View Only", "slug" => "view"],
                ["name" => "Edit",  "slug" => "edit"]                
              ]
        ],
        [ 
            "resource" => [ "name" => "Code Generation", "slug" => "code-generation"],

            "permissions" => [ 
                ["name" => "View Only", "slug" => "view"],
                ["name" => "Edit",  "slug" => "edit"]
              ]
        ],
        [ 
            "resource" => [ "name" => "Email Sending Settings", "slug" => "emails"],

            "permissions" => [ 
                ["name" => "View Only", "slug" => "view"],
                ["name" => "Create",  "slug" => "create"],
                ["name" => "Edit",  "slug" => "edit"],
                ["name" => "Delete",  "slug" => "delete"]
              ]
        ],
        [ 
            "resource" => [ "name" => "DMC Bank Accounts", "slug" => "dmc-bank-accounts"],

            "permissions" => [ 
                ["name" => "View Only", "slug" => "view"],
                ["name" => "Create",  "slug" => "create"],
                ["name" => "Edit",  "slug" => "edit"],
                ["name" => "Delete",  "slug" => "delete"]
              ]
        ],
        [ 
            "resource" => [ "name" => "Markups", "slug" => "markups"],

            "permissions" => [ 
                ["name" => "View Only", "slug" => "view"],
                ["name" => "Create",  "slug" => "create"],
                ["name" => "Edit",  "slug" => "edit"],
                ["name" => "Delete",  "slug" => "delete"]
              ]
        ],
        [ 
            "resource" => [ "name" => "Taxes", "slug" => "taxes"],

            "permissions" => [ 
                ["name" => "View Only", "slug" => "view"],
                ["name" => "Create",  "slug" => "create"],
                ["name" => "Edit",  "slug" => "edit"],
                ["name" => "Delete",  "slug" => "delete"]
              ]
        ],
        [ 
            "resource" => [ "name" => "Invoice Settings", "slug" => "invoice-settings"],

            "permissions" => [ 
                ["name" => "View Only", "slug" => "view"],
                ["name" => "Create",  "slug" => "create"],
                ["name" => "Edit",  "slug" => "edit"],
                ["name" => "Delete",  "slug" => "delete"]
              ]
        ]        
        
    ]
];