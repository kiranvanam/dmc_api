<?php


return [
  "resource" => ["name" => "Vendor", "slug" => "vendors"],
  "permissions" => [] ,
  "sub" =>  [
          [
            "resource" => ["name" => "Contracts", "slug" => "contracts"],
            "permissions" => [],
            "sub" => [
                [
                    "resource" => ["name" => "Hotels", "slug" => "hotels"],
                    
                    "permissions" => [
                        ["name" => "List View" , "slug" => "list"],
                        ["name" => "View Details", "slug" => "details"],
                        ["name" => "Edit",  "slug" => "edit"], 
                        ["name" => "Create", "slug" => "create"], 
                        ["name" => "Delete", "slug" => "delete"]
                      ],
                      
                    "sub" => []
                ],
                [
                    "resource" => ["name" => "Tours", "slug" => "tours"],
                    "permissions" => [
                        ["name" => "List View" , "slug" => "list" ],
                        ["name" => "View Details", "slug" => "details"],
                        ["name" => "Edit",  "slug" => "edit"], 
                        ["name" => "Create", "slug" => "create"], 
                        ["name" => "Delete", "slug" => "delete"]
                      ],
                    "sub" => []
                ],
                [
                    "resource" => ["name" => "Transfers", "slug" => "transfers"],
                    
                    "permissions" => [
                        ["name" => "List View" , "slug" => "list"],
                        ["name" => "View Details", "slug" => "details"],
                        ["name" => "Edit",  "slug" => "edit"], 
                        ["name" => "Create", "slug" => "create"], 
                        ["name" => "Delete", "slug" => "delete"]
                      ],

                    "sub" => []
                ],
                [
                    "resource" => ["name" => "Meals", "slug" => "meals"],
                    
                    "permissions" => [
                        ["name" => "List View" , "slug" => "list"],
                        ["name" => "View Details", "slug" => "details"],
                        ["name" => "Edit",  "slug" => "edit"], 
                        ["name" => "Create", "slug" => "create"], 
                        ["name" => "Delete", "slug" => "delete"]
                      ],

                    "sub" => []
                ],

                [
                    "resource" => ["name" => "Visas", "slug" => "visas"],

                    "permissions" => [
                        ["name" => "List View" , "slug" => "list"],
                        ["name" => "View Details", "slug" => "details"],
                        ["name" => "Edit",  "slug" => "edit"], 
                        ["name" => "Create", "slug" => "create"],
                        ["name" => "Delete", "slug" => "delete"]
                      ],

                    "sub" => []
                ],
/* 
                [
                    "resource" => ["name" => "Packages", "slug" => "packages"],

                    "permissions" => [
                        ["name" => "List View" , "slug" => "list"],
                        ["name" => "View Details", "slug" => "details"],
                        ["name" => "Edit",  "slug" => "edit"], 
                        ["name" => "Create", "slug" => "create"],
                        ["name" => "Delete", "slug" => "delete"]
                      ],

                    "sub" => []
                ], */

              ]
              
          ],
          [
              "resource" => ["name" => "Api Points", "slug" => "api"],
              "permissions" => [
                          ["name" => "List View" , "slug" => "list" ],
                          ["name" => "View Hotel Details", "slug" => "details"],
                          ["name" => "Edit",  "slug" => "edit"], 
                          ["name" => "Create", "slug" => "create"], 
                          ["name" => "Delete", "slug" => "delete"]
                      ],
              "sub" => []
          ]
      ]
];
