<?php

return [ 
    "resource" => [ "name" => "Locations", "slug" => "locations"],
    "permissions" => [],
    "sub" => [
      [
        "resource" => [ "name" => "Countries", "slug" => "countries"],
        "permissions" => [ 
            ["name" => "List View", "slug" => "list"], 
            ["name" => "View details", "slug" => "details"], 
            ["name" => "Edit",  "slug" => "edit"], 
            ["name" => "Create", "slug" => "create"], 
            ["name" => "Delete", "slug" => "delete"]
          ],
        "sub" => []
      ],
      [
        "resource" => [ "name" => "Regions", "slug" => "regions"],
        "permissions" => [ 
            ["name" => "List View", "slug" => "list"], 
            ["name" => "View details", "slug" => "details"], 
            ["name" => "Edit",  "slug" => "edit"], 
            ["name" => "Create", "slug" => "create"], 
            ["name" => "Delete", "slug" => "delete"]
          ],
        "sub" => []
      ],
      [
        "resource" => [ "name" => "Cities", "slug" => "cities"],
        "permissions" => [ 
            ["name" => "List View", "slug" => "list"], 
            ["name" => "View details", "slug" => "details"], 
            ["name" => "Edit",  "slug" => "edit"], 
            ["name" => "Create", "slug" => "create"], 
            ["name" => "Delete", "slug" => "delete"]
          ],
        "sub" => []
      ],
      [
        "resource" => [ "name" => "Areas", "slug" => "areas"],
        "permissions" => [ 
            ["name" => "List View", "slug" => "list"], 
            ["name" => "View details", "slug" => "details"], 
            ["name" => "Edit",  "slug" => "edit"], 
            ["name" => "Create", "slug" => "create"], 
            ["name" => "Delete", "slug" => "delete"]
          ],
        "sub" => []
      ],
      [
        "resource" => [ "name" => "Market Places", "slug" => "market-places"],
        "permissions" => [ 
            ["name" => "List View", "slug" => "list"], 
            ["name" => "View details", "slug" => "details"], 
            ["name" => "Edit",  "slug" => "edit"], 
            ["name" => "Create", "slug" => "create"], 
            ["name" => "Delete", "slug" => "delete"]
          ],
        "sub" => []
      ]
    ]
];