<?php

return [
    "resource" => ["name" => "Meals", "slug" => "meals"],
    
    "permissions" => [
        ["name" => "List View" , "slug" => "list"],
        ["name" => "View Details", "slug" => "details"],
        ["name" => "Edit",  "slug" => "edit"], 
        ["name" => "Create", "slug" => "create"], 
        ["name" => "Delete", "slug" => "delete"]
      ],

    "sub" => []
];  