<?php

return [
    "resource" => ["name" => "Transfers", "slug" => "transfers"],
    
    "permissions" => [
        ["name" => "List View" , "slug" => "list"],
        ["name" => "View Details", "slug" => "details"],
        ["name" => "Edit",  "slug" => "edit"], 
        ["name" => "Create", "slug" => "create"], 
        ["name" => "Delete", "slug" => "delete"]
      ],

    "sub" => []
];  