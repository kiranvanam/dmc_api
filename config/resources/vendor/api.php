<?php

return [
    "resource" => ["name" => "Api Points", "slug" => "api"],
    "permissions" => [
                ["name" => "List View" , "slug" => "list" ],
                ["name" => "View Hotel Details", "slug" => "details"],
                ["name" => "Edit",  "slug" => "edit"], 
                ["name" => "Create", "slug" => "create"], 
                ["name" => "Delete", "slug" => "delete"]
            ],
    "sub" => []
];  