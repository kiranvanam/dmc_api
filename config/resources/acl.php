<?php

return [ 
    "resource" => [ "name" => "Acl", "slug" => "acl"],

    "permissions" => [],
    
    "sub" => [
        [ 
            "resource" => [ "name" => "Roles", "slug" => "roles"],

            "permissions" => [ 
                ["name" => "List View", "slug" => "list"],
                ['name' => 'Own List View', 'slug' => "own-list" ],
                ["name" => "View details", "slug" => "details"], 
                ["name" => "Edit",  "slug" => "edit"], 
                ["name" => "Create", "slug" => "create"], 
                ["name" => "Delete", "slug" => "delete"]
              ],
            
            "sub" => []
        ],
        [ 
            "resource" => [ "name" => "Teams", "slug" => "teams"],

            "permissions" => [ 
                ["name" => "List View", "slug" => "list"], 
                ["name" => "View details", "slug" => "details"], 
                ["name" => "Edit",  "slug" => "edit"], 
                ["name" => "Create", "slug" => "create"], 
                ["name" => "Delete", "slug" => "delete"]
              ],
            
            "sub" => []
        ],

        [
            "resource" => [ "name" => "Users", "slug" => "users"],

            "permissions" => [ 
                ["name" => "List View", "slug" => "list"], 
                ["name" => "View details", "slug" => "details"], 
                ["name" => "Edit",  "slug" => "edit"], 
                ["name" => "Create", "slug" => "create"], 
                ["name" => "Delete", "slug" => "delete"]
              ],
                 
            "sub" => [
                [
                  "resource" => [ "name" => "Own Profile", "slug" => "self"],
                  "permissions" => [
                        ["name" => "View Own Profile", "slug" => "view"], 
                        ["name" => "Edit", "slug" => "edit"] 
                      ],
                  "sub" => []
                ]
            ]
        ]
        
    ]
];