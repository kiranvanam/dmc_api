<tr class="details">
    <td>
        {{ $booking->visa->name }} <br>
        {{ $booking->service_start_date }}   
    </td>
    <td>
        Validity: {{ array_get($booking->visa['visa_details'], 'validity', "NaN") }} -
                {{ array_get($booking->visa['visa_details'], 'validity_type', 'NaN') }} 
    </td>
    <td>
        <table class="table table-sm">
            <thead class="thead-default">
                <tr class="heading">
                    <th> Pax </th>
                    <th> price </th>
                    <th>Qty.</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>AD</td>
                    <td> {{ array_get($booking->visa,'unit_pricing.adult_price') }}
                            {{ $booking->currency }}
                    </td>
                    <td> 
                        {{ array_get( $booking->visa,'no_of_adults') }}
                    </td>
                </tr>
                <tr style="padding:0px">
                    <td>CH</td>
                    <td style="padding:0px">
                        {{ array_get($booking->visa,'unit_pricing.child_price') }} 
                        {{ $booking->currency }}
                    </td>
                    <td style="padding:0px"> {{ array_get( $booking->visa,'no_of_children') }} </td>
                </tr>
                <tr style="padding:0px">
                    <td style="">INF</td>
                    <td style="padding:0px"> 
                        {{ array_get($booking->visa,'unit_pricing.infant_price') }} 
                        {{ $booking->currency }}
                    </td>
                    <td style="padding:0px"> {{ array_get( $booking->visa,'no_of_infants') }} </td>
                </tr>
            </tbody>                            
        </table>
    </td>
    <td>
        <!-- check if markup & tax is included -->
        {{ array_get($booking->visa, 'total_price') }}
        {{ $booking->currency }}
    </td>
</tr>