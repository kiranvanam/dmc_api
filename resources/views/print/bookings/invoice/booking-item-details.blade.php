<table class="table table-sm">
    <thead class="thead-default">
        <tr>
            <th style="width:30%"> Service Details </th>
            <th style="width:28%">  Other details</th>
            <th style="width:30%"> Unit Pricing </th>
            <th> Total </th>
        </tr>
    </thead>
    <tbody>
    @foreach ($booking->bookingItems  as $bookingItem)
        <?php $bookingItem->setBooking($booking);  ?>
        <tr class="details">
            <td class="text-left pl-2 pt-2">

                <?php $tourTileDetails = $bookingItem->privateTourTitleDetails() ?>

                @if(!empty($tourTileDetails) )
                <span class="text-info">{{ array_get($tourTileDetails, 'name') }} -
                        {{  array_get($tourTileDetails, 'code') }} </span>
                @endif

                {{ $bookingItem->service_name }} <br>
                (
                @if ($bookingItem->service_code)
                    {{ $bookingItem->service_code }}/
                @endif
                @if ($bookingItem->vendor_code)
                    {{ $bookingItem->vendor_code }}
                @endif
                )
            </td>
            <td class="text-left">
                <strong> City: </strong> {{ $bookingItem->city_name }} <br>
                @if($bookingItem->isHotel())
                    <strong>Check-in</strong>: {!! $bookingItem->hotelCheckIn() !!} <br>
                    <strong>Check-out</strong>: {!! $bookingItem->hotelCheckOut() !!} <br>
                @else
                    <strong>Travel Date :</strong> {{ $bookingItem->travelDate() }} <br>
                @endif
                @if ($bookingItem->isTransfer())
                    <strong>Pick-up Time:</strong> {{ $bookingItem->pickUpTime() }} <br>
                    <strong> Pick up: </strong>: <br>
                        {{ $bookingItem->pickUpPointDetails() }} <br>
                    <strong>Drop-off:</strong> <br>
                        {{ $bookingItem->dropOffPointDetails() }}
                @endif
            </td>
            <td>
                @if ($bookingItem->isPricingPerPax())
                    <table class="table table-sm table-bordered">
                        <thead class="thead-default p-0">
                            <tr class="heading">
                                <th class="p-0">Pax</th>
                                <th class="p-0">Price</th>
                                <th class="p-0">Qty</th>
                            </tr>
                        </thead>
                        <tbody>
                                <tr class="item">
                                    <td class="p-0"> AD </td>
                                    <td class="p-0"> {{ $bookingItem->adultUnitPrice() }} {{ $booking->currency() }} </td>
                                    <td class="p-0">  {{ $bookingItem->noOfAdults() }} </td>
                                </tr>
                                <tr class="item">
                                    <td class="p-0"> CH </td>
                                    <td class="p-0"> {{ $bookingItem->childUnitPrice() }} {{ $booking->currency() }} </td>
                                    <td class="p-0"> {{ $bookingItem->noOfChildren() }} </td>
                                </tr>
                                <tr class="item">
                                    <td class="p-0"> INF </td>
                                    <td class="p-0"> {{ $bookingItem->infantUnitPrice() }}  {{ $booking->currency() }} </td>
                                    <td class="p-0">  {{ $bookingItem->noOfInfants() }} </td>
                                </tr>
                        </tbody>
                    </table>
                @elseif($bookingItem->isPricingHotelGRN())

                    @include('emails.bookings.booking.grn-pricing', ['bookingItem'=>$bookingItem])

                @elseif($bookingItem->isPricingSingleUnitType())
                    <table class="table table-sm table-bordered font-size-12">
                        <thead class="thead-default p-0">
                            <tr class="heading">
                                <th class="p-0">Title/Name</th>
                                <th class="p-0">Price</th>
                                <th clss="p-0">Qty</th>
                            </tr>
                        </thead>
                        <tbody>
                                <tr class="item">
                                    <td class="p-0"> {{ $bookingItem->service_name  }} </td>
                                    <td class="p-0"> {{ $bookingItem->unitPrice() }} {{ $booking->currency() }} </td>
                                    <td class="p-0">  {{ $bookingItem->noOfUnits() }} </td>
                                </tr>
                        </tbody>
                    </table>
                @endif
            </td>
            <td>
                {{ $bookingItem->totalPrice() }}  {{ $booking->currency() }}
            </td>
        </tr>
        @endforeach

        <tr class="item">
            <td></td>
            <td colspan="2" align="right"> <strong> Net Total </strong> </td>
            <td> {{ $booking->netTotal() }}  {{ $booking->currency() }} </td>
        </tr>

        @if ($booking->isMarkupDisplayedSeperatly())
        <?php $markupDetails = $booking->markupDetails() ?>
        <tr class="item">
            <td></td>
            <td colspan="2" align="right"> <strong> {{ array_get( $markupDetails, 'name') }} </strong> </td>
            <td colspan="2"> {{ array_get( $markupDetails, 'value') }}  {{ $booking->currency  }}  </td>
        </tr>
        @endif

        @if ($booking->areTaxesDisplayedSeperatly())
        <?php $taxDetails = $booking->taxDetails() ?>
        @foreach ($taxDetails as $tax)
        <tr class="item">
            <td></td>
            <td colspan="2" align="right"> <strong> {{ array_get( $tax, 'name') }} </strong> </td>
            <td colspan="2"> {{ array_get( $tax, 'value') }}  {{ $booking->currency  }} </td>
        </tr>
        @endforeach
        @endif

        <tr class="item">
            <td></td>
            <td colspan="2" align="right"> <strong> Gross Total </strong> </td>
            <td> {{ $booking->grossTotal() }} {{ $booking->currency }} </td>
        </tr>
    </tbody>
</table>