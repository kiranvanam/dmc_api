@extends('print.bookings.base-layout')

@section('content')
    <div class="container mt-4">
        <div class="form-group row">
            <div class="col-md-12">
                <div style="text-align:center">
                    <h3> Invoice/ Booking Ref.# {{ $booking->code }} </h3>
                </div>
            </div>
        </div>
        <div class="form-group row">
            <div class="col-md-7">
                <img src="{{ $booking->logoUrl() }}" style="width:200px; width:120px;">
                <br>
                <div style="font-size:14px">
                    {{ $booking->leadPassengerName() }}-({{ $booking->leadPassengerNationality() }}) <br>
                    {{-- Email: {{ $booking->leadPassengerEmail() }} <br> --}}
                    Phone: {{ $booking->leadPassengerContactNumber() }}
                </div>

            </div>
            <div class="col-md-4">
                {{ $booking->dmcAddress()  }} <br>
                {{  $booking->dmcCodeName() }} : {{   $booking->dmcCodeNumber() }}
            </div>
        </div>
        <div class="row ">
            <div class="col-md-12">
                <table class="table table-sm">
                    <thead class="thead-default">
                        <tr>
                            <th class="text-left"> Agency Details </th>
                            <th class="text-left"> Invoice Details </th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td class="text-left">
                                {{ $booking->agencyName() }}<br>
                                {{ $booking->agencyAddress() }} <br>
                                Handled By:{{ $booking->agencyUserBookedBy()  }}
                            </td>
                            <td class="text-left">
                                Invoice #: {{ $booking->invoiceCode() }}<br>
                                GSTN/VAT: {{ $booking->agencyCode() }} <br>
                                Dated: {{ $booking->invoiceDate() }}
                            </td>
                        </tr>
                    </tbody>
                </table>

            </div>
        </div>

        <div class="form-gorup row">
            <div class="col-md-12">

                <strong> Lead passenger : </strong>
                {{  $booking->leadPassengerName() }}
                    (
                    Nationality: {{ $booking->leadPassengerNationality() }}
                    )
                <br>

                @include('print.bookings.invoice.booking-item-details')


                <div class="form-group row">
                    <div class="col-md-3">
                        <h4> Invoice Notes </h4>
                    </div>
                    <div class="col-md-8">
                        {!! $booking->invoiceNotes()  !!}
                    </div>
                </div>


                <div class="form-group row">
                    <div class="col-md-12">
                        <h4> Bank details </h4>
                        <div class="form-group row">
                            <div class="col-md-9" v-for="bankAccount in bankDetails">
                                <table class="table table-sm table-bordered">
                                    @foreach($booking->dmcInvoiceBankAccountDetails()  as $bankAccount)

                                    <tr>
                                        <th> Country </th>
                                        <td class="text-left">
                                            {{ $booking->countryNameById( array_get($bankAccount, 'country_id') ) }}
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>Name</th>
                                        <td class="text-left"> {{ array_get( $bankAccount, 'bank_name') }} </td>
                                    </tr>
                                    <tr>
                                        <th> In favour Of </th>
                                        <td class="text-left"> {{ array_get( $bankAccount, 'in_favour_off') }}  </td>
                                    </tr>
                                    <tr>
                                        <th> Currency </th>
                                        <td class="text-left"> {{ array_get( $bankAccount, 'currency') }} </td>
                                    </tr>
                                    @if( array_has( $bankAccount, 'swift_code') )
                                    <tr>
                                        <th> SWIFT code </th>
                                        <td class="text-left">  {{ array_get( $bankAccount, 'swift_code') }}  </td>
                                    </tr>
                                    @endif
                                    @if( array_has( $bankAccount, 'iban_code') )
                                    <tr>
                                        <th> IBAN Code </th>
                                        <td class="text-left"> {{ array_get( $bankAccount, 'iban_code') }} </td>
                                    </tr>
                                    @endif
                                    @if( array_has( $bankAccount, 'ifsc_code') )
                                    <tr>
                                        <th> IFSC code </th>
                                        <td class="text-left"> {{ array_get( $bankAccount, 'ifsc_code') }} </td>
                                    </tr>
                                    @endif
                                    <tr>
                                        <th> Address </th>
                                        <td class="text-left">
                                            Branch: {{ array_get( $bankAccount, 'branch_name') }} <br>
                                            {{ array_get( $bankAccount, 'city') }} <br>
                                            {{ array_get( $bankAccount, 'address') }}
                                        </td>
                                    </tr>
                                    @endforeach
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</body>
</html>