
<h3 class="text-info" style="color:blue;background-color:#eee"> Visa Details </h3>


<table class="table table-sm table-striped">
    <tr>
        <td class="text-right" style="width:20%">Name</td>
        <td> {{ array_get($booking->visa,'name') }} </td>
    </tr>
    <tr>
        <td class="text-right"> Country </td>
        <td> {{ $booking->visa->country_name }} </td>
    </tr>
    <tr class="item">
        <th> Validity </th>
        <th> {{ array_get($booking->visa,'visa_details.validity') }} {{ array_get($booking->visa,'visa_details.validity_type')
            }}
        </th>
    </tr>
    <tr class="item">
        <th>Pax</th>
        <td> AD - {{ array_get( $booking->visa, 'no_of_adults' ) }}
            <br> CH - {{ array_get( $booking->visa, 'no_of_children' ) }}
            <br> INF - {{ array_get( $booking->visa, 'no_of_infants' ) }}
        </td>
    </tr>
    <tr class="item">
        <th> Travelling Date </th>
        <td> {{ $booking->service_start_date }} </td>
    </tr>
</table>

<h3 class="text-info" style="color:blue;background-color:#eee"> Pax Details </h3>
<br>
<table class="table table-sm" style="width:60%;margin-left:10%">
    <thead class="thead-default">
        <tr class="heading" style="background-color:#eee">
            <td> Name </td>
            <td> Type </td>
            <td>Age </td>
            <td> Passport Number</td>
        </tr>
    </thead>
    <tbody>
        @if( array_get( $booking->visa, 'pax_details', false) ) @foreach( array_get( $booking->visa, 'pax_details') as $paxDetail
        )
        <tr class="item">
            <td> {{ array_get($paxDetail,'title') . " " . array_get($paxDetail,'name') . " " . array_get($paxDetail,'surname')}}
                </td>
            <td> {{ array_get($paxDetail,'type') }} </td>
            <td> {{ array_get($paxDetail, 'age') }} </td>
            <td> {{ array_get($paxDetail,'passport_number') }} </td>
        </tr>
        @endforeach @endif
    </tbody>
</table>


<h3 style="color:blue;background-color:#eee"> Terms &amp; Conditions </h3>

<div>
    {!! array_get( $booking->visa, 'visa_details.terms_and_conditions', '') !!}
</div>


<h3 class="text-info" style="color:blue;background-color:#eee"> Remarks </h3>

<div>
    {!! array_get( $booking->visa, 'visa_details.remarks', '') !!}
</div>