<h5 style="color:blue;"> service Description </h5>                
@if ($bookingItem->titleDetails())
    {!! $bookingItem->titleDescription()  !!}               
@endif
<br>
@if($bookingItem->description())    
    {!! $bookingItem->description() !!}
@endif

@if ($bookingItem->inclusions())
    <h5 style="color:blue;font-size-14"> Inclusions </h5>                

    <div>
        {!! $bookingItem->inclusions() !!}
    </div>
@endif

@if ($bookingItem->exclusions())    
    <h5 style="color:blue;font-size-14"> Exclusions </h5>                

    <div>
        {!! $bookingItem->exclusions() !!}
    </div>
@endif

@if($bookingItem->isVisa())
    <h5 style="color:blue;font-size-14"> Docs Required </h5>

    <div>
        {!! $bookingItem->docsRequired() !!}
    </div>
@endif

@if($bookingItem->termsAndConditions())    
    <h5 style="color:blue;font-size-14"> Terms & Conditions </h5>                
    <div>
        {!! $bookingItem->termsAndConditions() !!}
    </div>
@endif

@if ($bookingItem->remarks())    
    <h5 style="color:blue;font-size-14"> Remarks </h5>
    <div>
        {!! $bookingItem->remarks() !!}
    </div>
@endif