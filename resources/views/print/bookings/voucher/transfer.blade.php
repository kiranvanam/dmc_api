<h3 class="text-info" style="color:blue;"> Visa Details </h3>

<table class="table table-sm" cellspacing="0" cellpadding="0" style="font-size:10px">
    <thead class="thead-default">
        <tr class="heading">
            <th> Service Details </th>
            <th>  Other details</th>
            <th> Vehicle Details </th>
            <th> Pax Details </th>
        </tr>
    </thead>
    <tbody>
        
        @foreach ($booking->transfers as $transferDetails)                            
            <tr class="details">
                <td class="text-left">
                    {{ array_get($transferDetails, 'transfer_service_name') }} <br> 
                    {{  array_get($transferDetails, 'direction') }} 
                     - {{ array_get($transferDetails, 'vendor_code') }} </span> <br> 
                    
                    Travel Date:{{ array_get($transferDetails, 'travelling_date') }}
                </td>
                <td class="text-left">
                    
                    Pick-up: <br>
                        City: {{ array_get($transferDetails, 'pick_up_city_name') }} - {{ array_get($transferDetails, 'pick_up_area_name') }} <br>
                        Details: {{ array_get($transferDetails, 'pick_up_point_details') }} <br>
                        Time: {{ array_get($transferDetails, 'pick_up_time') }}
                    <br>
                    Drop-off: <br>
                        City: {{ array_get($transferDetails, 'drop_off_city_name') }} - {{ array_get($transferDetails, 'drop_off_area_name') }} <br>
                        Details: {{ array_get($transferDetails, 'drop_off_point_details') }} <br>
                                                
                </td> 
                <td>
                    @if( array_get($transferDetails,'transfer_type') == 'private')
                        <table cellspacing="0" cellpadding="0">
                            <tr class="heading">
                                <td style="padding:0px">Vehicle</td>
                                <td style="padding:0px">Qty</td>
                            </tr>
                            <tr class="details">
                                <td> 
                                    {{ array_get( $transferDetails, 'vehicle_details.name' ) }} ( {{ array_get( $transferDetails, 'vehicle_details.seater' ) }} -Seater)                                
                                </td>
                                <td>  {{ array_get( $transferDetails, 'vehicle_details.no_of_booked_vehicles', 1) }}   </td>
                            </tr>
                        </table>
                    @else 
                        <strong> SIC Transfers </strong>
                    @endif
                </td>                 
                <td>
                    Adults :  {{ array_get($transferDetails, 'no_of_adults', 0) }}  <br>
                    Children: {{ array_get($transferDetails, 'no_of_children', 0) }}
                </td>
            </tr>
            <tr class="details">

                <td colspan="4">
                    <div>
                        <h4> Description </h4>
                        <div>
                            {!! array_get( $transferDetails->service_details, 'description') !!}
                        </div>
                    </div>
                    <div>
                        <h4> Inclusions </h4>
                        <div>
                            {!! array_get( $transferDetails->service_details, 'inclusions') !!}
                        </div>
                    </div>
                    <div>
                        <h4> Exclusions </h4>
                        <div>
                            {!! array_get( $transferDetails->service_details, 'exclusions') !!}
                        </div>
                    </div>                    
                    <div>
                        <h4> Terms & Conditions </h4>
                        <div>
                            {!! array_get( $transferDetails->service_details, 'terms_and_conditions') !!}
                        </div>
                    </div> 
                    <div>
                        <h4> Remarks </h4>
                        <div>
                            {!! array_get( $transferDetails->service_details, 'remarks') !!}
                        </div>
                    </div> 
                </td>
            </tr>
        @endforeach
    </tbody>
</table>

