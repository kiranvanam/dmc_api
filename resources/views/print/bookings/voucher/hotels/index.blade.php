<table class="table table-border">
    <tbody>
        <tr>
            <td style="width:15%;;" class="align-middle" align="right">
                <strong>Service </strong>
            </td>
            <td align="left;" style="padding-left:15px">
                {{ $bookingItem->service_name }} {!! $bookingItem->grnStarCategory() !!}
                <br>
                Address: {{ $bookingItem->grnHotelAddress()}}
            </td>
        </tr>
        <tr>
            <td style="width:15%;;" class="align-middle" align="right">
                <strong>Supplier Reference </strong>
            </td>
            <td align="left;" style="padding-left:15px">
                 {{ $bookingItem->grnHotelSupplierReference()}}
            </td>
        </tr>
        <tr>
            <td style="width:15%;" align="right" class="align-middle">
                <strong>Address </strong>
            </td>
            <td align="left;" style="padding-left:15px">
                 {{ $bookingItem->grnHotelAddress()}} <br>
                 Email: {{ $bookingItem->grnHotelEmail() }} <br>
                 Contact: {{ $bookingItem->grnHotelContact()  }}
            </td>
        </tr>
        <tr>
            <td style="width:15%;" align="right" class="align-middle">
                <strong> Travel Dates </strong>
            </td>
            <td align="left;" style="padding-left:15px" >
                <strong>Check-in</strong>: {!! $bookingItem->grnCheckIn() !!} <br>
                <strong>Check-out</strong>: {!! $bookingItem->grnCheckOut() !!} <br>
            </td>
        </tr>


        {{-- <tr>
            <td style="width:15%;" align="right" class="align-middle">
                <strong> Room Details </strong>
            </td>
            <td align="left;" style="padding-left:15px" >
                @foreach ($bookingItem->grnRooms()  as $room)
                    {{ $bookingItem->grnRoomName($room) }}({{ $bookingItem->grnTotalForRoom($room) }}) <br>
                @endforeach
            </td>
        </tr> --}}
    </tbody>
</table>

<div>
    {!! $bookingItem->grnHotelDescription()  !!}
</div>

<div class="form-group row">
    <div class="col-md-10">
        <h5 class="text-info"> Confirmation Details </h5>
        <table class="table table-sm">
            <thead class="thead-default">
                <tr>
                    <th style="width:35%"> Room Details </th>
                    <th> Meal </th>
                    <th> No.Of Rooms </th>
                    <th> Adults</th>
                    <th>Children</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($bookingItem->grnVoucherBookingItems()  as $booking_item)
                    @foreach ($bookingItem->grnRoomsForBookingItem($booking_item)  as $room)
                        <tr>
                            <td>
                                {{ array_get($room, 'room_type') }}
                                @if ($bookingItem->grnRoomReferenceFor($room))
                                <label style="font-size:10px">
                                    ( ref:  {{ $bookingItem->grnRoomReferenceFor($room)  }})
                                </label>
                                @endif
                            </td>
                            <td>
                                @foreach ($bookingItem->grnRoomInclusitonsForRoom($room) as $inclusion)
                                    {{ $inclusion }} <br>
                                @endforeach
                            </td>
                            <td> {{ array_get($room, "no_of_rooms", 1) }} </td>
                            <td>
                                {{ array_get($room,'no_of_adults') }}
                            </td>
                            <td>
                                {{ array_get($room,'no_of_children') }}
                            </td>
                        </tr>
                        <tr>
                            <td colspan="5">
                                @foreach (array_get($booking_item, 'rate_comments')  as $key => $comments)
                                   <strong style="text-transform: capitalize;">
                                        {{ str_replace( "_", " ", $key) }} :
                                    </strong>
                                    {{ empty($comments) ? " No ". $key : $comments }}   <br>

                                @endforeach

                            </td>
                        </tr>
                    @endforeach
                @endforeach
                </template>
            </tbody>
        </table>
    </div>
</div>

<div class="form-group row">
    <div class="col-md-7">
        {{-- <h5 class="text-info"> Holde Details </h5>
        <strong> Name: </strong> {{ $bookingItem->grnHolderName() }} <br>
        <strong> Email: </strong> {{ $bookingItem->grnHolderEmail() }} <br>
        <strong> Contact: </strong> {{ $bookingItem->grnHolderContact() }} <br>
        <br> --}}
        <h5 class="text-info"> Guest Details </h5>
        <table class="table table-sm" style="font-size:12px">
            <thead class="thead-default">
                <tr>
                    <th style="width:60%"> Name </th>
                    <th> Type </th>
                    <th> Age </th>
                </tr>
            </thead>
            <tbody>
                @foreach ($bookingItem->grnVoucherGuestDetails() as $pax)
                    <tr>
                        <td> {{ $bookingItem->grnUserName($pax) }} </td>
                        <td> {{ array_get($pax, 'type', '') }} </td>
                        <td> {{ array_get($pax, 'age', '') }} </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
