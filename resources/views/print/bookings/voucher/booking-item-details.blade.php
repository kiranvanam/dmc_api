<table class="table table-sm">
    <tbody>
        <tr class="item">
            <td style="width:15%;padding:0px" align="right">
                <strong>Service </strong>
            </td>
            <td align="left;" >
                {{ $bookingItem->service_name }}
                &nbsp; {{ $bookingItem->productCode() }}
            </td>
        </tr>
        <tr class="item">
            <td style="width:15%;padding:0px" align="right">
                <strong>Travel Date </strong>
            </td>
            <td align="left;" >
                {{ $bookingItem->travelDate() }}
            </td>
        </tr>

        @if($bookingItem->isPricingPerPax())

        <tr class="item">
            <td style="width:15%;padding:0px" align="right">
                <strong> Pax details </strong>
            </td>
            <td align="left;" >
                <strong> Adults </strong> : {{ $bookingItem->noOfAdults() }} <br>
                <strong> Children </strong> : {{ $bookingItem->noOfChildren() }} <br>
                <strong> Infants </strong> : {{ $bookingItem->noOfInfants() }} <br>
            </td>
        </tr>
        @else
        <tr class="item">
            <td style="width:15%;padding:0px" align="right">
                <strong> Title Details </strong>
            </td>
            <td align="left;" >
                {{ $bookingItem->service_name }} (  Qty - {{ $bookingItem->noOfUnits() }} ) <br>
            </td>
        </tr>
        @endif
        <tr class="item">
            <td style="width:25%;padding:0px" align="right">
                <strong> Pick-up & Drop-off </strong>
            </td>
            <td align="left;" style="padding:0px;padding-left:1em">
                @if ($bookingItem->pickUpTime())
                    <strong>Time: </strong>  {{ $bookingItem->pickUpTime() }} <br>
                @endif

                @if ($bookingItem->pickUpPointDetails())
                    <strong> Pick-up: </strong> {!! $bookingItem->pickUpPointDetails() !!} <br>
                @endif

                @if ($bookingItem->dropOffPointDetails())
                    <strong> Drop-off: </strong> {!! $bookingItem->dropOffPointDetails() !!} <br>
                @endif
            </td>
        </tr>
    </tbody>
</table>

@include('print.bookings.voucher.general-info', ['bookingItem' => $bookingItem ])
