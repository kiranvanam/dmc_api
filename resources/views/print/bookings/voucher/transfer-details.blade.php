<table class="table table-sm">
    <tbody>
        <tr>
            <th style="width:20%;padding:0px" align="right">
                @if($bookingItem->isTransferService())
                    <strong> Transfer </strong>
                @else
                    <strong> Vehicle </strong>
                @endif
            </th>
            <th align="left;" style="padding:0px;padding-left:1em">
                {{ $bookingItem->service_name }}
            </th>
        </tr>
        <tr class="item">
            <td style="width:20%;padding:0px" align="right">
                <strong> Pick-up </strong>
            </td>
            <td align="left;" style="padding:0px;padding-left:1em">
                Time: {{  $bookingItem->pickUpTime() }} <br>
                <div>  {!! $bookingItem->pickUpPointDetails() !!} </div>
            </td>
        </tr>
        <tr class="item">
            <td style="width:20%;padding:0px" align="right">
                <strong> Drop-off: </strong>
            </td>
            <td align="left;" style="padding:0px;padding-left:1em">
                {!! $bookingItem->dropOffPointDetails() !!}
            </td>
        </tr>
        @if($bookingItem->vehicleDetails())
        <tr class="item">
            <td  style="width:20%;padding:0px" align="right">
                <strong> Other Info </strong>
            </td>
            <td align="left;" style="padding:0px;padding-left:1em">
                {{ $bookingItem->vehicleName() }} ( Qty- {{ $bookingItem->noOfUnits() }}) <br>

                <strong> Passengers only </strong> <br>
                    <i class="fa fa-users"></i> {{ $bookingItem->passengersOnly() }}  Persons <br>
                </template>
                <strong> Passengers with Luggage: </strong> <br>
                <i class="fa fa-users"></i>  {{ $bookingItem->passengersWithLuggage() }} Persons    <br>
                <span v-if="vehicleDetails.small_bags">
                    <i class="fa fa-suitcase font-size-8"></i> - {{ $bookingItem->noOfSmallBags() }}
                            ( {{ $bookingItem->smallBagWeight() }} ) Small Bags
                </span> <br>
                <span v-if="vehicleDetails.large_bags">
                    <i class="fa fa-suitcase font-size-12"></i> -
                    {{ $bookingItem->noOfLargeBags() }} ( {{$bookingItem->largeBagWeight() }} ) Large Bags
                </span> <br>
            </td>
        </tr>
        @elseif($bookingItem->isPricingPerPax())
        <tr class="item">
            <td style="width:20%;padding:0px" align="right">
                <strong> Pax </strong>
            </td>
            <td align="left;" style="padding:0px;padding-left:1em">
                <div style="width:60%">
                        <strong> SIC Transfers </strong>
                        Adults :  {{ $bookingItem->noOfAdults() }}  <br>
                        Children: {{ $bookingItem->noOfChildren() }}
                        Infants : {{ $bookingItem->noOfInfats() }}
                </div>
            </td>
        </tr>
        @endif
    </tbody>
</table>

@if($bookingItem->vehicleDetails())
    <h5 style="colot:blue"> Vehicle Description </h5>
    {!! $bookingItem->vehicleDescription() !!}
@endif

@if($bookingItem->isTransferService())
    @include('emails.bookings.voucher.general-info', ['bookingItem' => $bookingItem ])
@endif
