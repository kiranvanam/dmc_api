@extends('print.bookings.base-layout')

@section('content')
    <div class="container">
        <div class="form-group row">
            <div class="col-md-12">
                <div style="text-align:center">
                    <h5> Service Voucher/ Booking Ref.# {{ $booking->code }} </h5>
                </div>
            </div>
        </div>
        <div class="form-group row">
            <div class="col-md-4">
                <img src="{{ $booking->logoUrl() }}" style="width:200px; width:120px;">
            </div>
            <div class="col-md-6">
                <h3 class="text-center"> <strong> Service Voucher </strong> </h3>
            </div>
        </div>

        <div class="form-group row">
            <div class="col-md-12">

                <h5 class="text-info" style="color:blue;background-color:#eee"> Lead Passenger Details </h5>

                <strong> Name:</strong>
                {{ $booking->leadPassengerName() }}
                <br>

                <strong> Nationality: </strong> {{ $booking->leadPassengerNationality()}}
                <br>
                <strong> Contact </strong> {{ $booking->leadPassengerContactNumber() }}
                <br>
                <strong>Email</strong> {{ $booking->leadPassengerEmail() }}
            </div>
        </div>

        <div class="form-group row">
            <div class="col-md-12">

                <h4 class="text-info" style="color:blue;background-color:#eee">  Booking Ref: {{ $booking->code }} </h4>

                {{-- display tour title description for private tours --}}

                @foreach ($booking->bookingItems as $index => $bookingItem)
                    @if( $booking->isFirstItemForPrivateTour($bookingItem,$index) )
                        <?php $tourTileDetails = $bookingItem->privateTourTitleDetails() ?>
                        <div>
                            <h5 class="text-info">{{ array_get($tourTileDetails, 'name') }} -
                                    {{  array_get($tourTileDetails, 'code') }} </h5>

                            <strong> Tour type: </strong> Private <br>

                            {!! $bookingItem->slotDetails() !!}  <br>

                            {!!  array_get($tourTileDetails, 'description') !!}
                        </div>
                    @endif
                    <div>
                        <span style="color:blue"> Service: </span>
                        <span style="text-transform: uppercase; font-size:16px;">
                            {{ $bookingItem->service }}
                            @if($bookingItem->isTour())
                                (SIC)
                            @endif
                        </span>
                    </div>
                    @if ($bookingItem->isTransfer() ||  $bookingItem->isVehicle())
                        @include('print.bookings.voucher.transfer-details', ['bookingItem' => $bookingItem ])
                    @elseif($bookingItem->isHotel())
                        @include('print.bookings.voucher.hotels.index', ["bookingItem" => $bookingItem])
                    @else
                        @include('print.bookings.voucher.booking-item-details', ['bookingItem' => $bookingItem ])
                    @endif
                @endforeach
            </div>
        </div>

        <div class="form-group row">
            <div class="col-md-12">
                <h5 class="text-info" style="color:blue;background-color:#eee"> Emergency Contact </h5>
                <strong> Agent Contact Number: </strong> +9714 2865955
                <br>
                <strong> Service Operator Number: </strong> +91 - 9910411117 (24X7 India Helpline Number)
            </div>
        </div>

    </div>
@endsection