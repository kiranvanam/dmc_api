<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title> {{ $data['dmc'] }} DMC- United Arab Emirates - Destination Management Company </title>
    <meta name="robots" content="index, follow"/>
    <link rel="shortcut icon" type="image/x-icon" href="/images/favicon/go-dubai.ico">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <link rel="stylesheet" type="text/css" href="/dist/vendor/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="/dist/vendor/jscrollpane/style/jquery.jscrollpane.css">
    {{-- <link rel="stylesheet" type="text/css" href="/dist/vendor/ladda/dist/ladda-themeless.min.css">
    <link rel="stylesheet" type="text/css" href="/dist/vendor/select2/dist/css/select2.min.css">
    <link rel="stylesheet" type="text/css" href="/dist/vendor/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css">
    <link rel="stylesheet" type="text/css" href="/dist/vendor/fullcalendar/dist/fullcalendar.min.css">
     --}}
    <link rel="stylesheet" type="text/css" href="/dist/vendor/bootstrap-sweetalert/dist/sweetalert.css">
    <link rel="stylesheet" type="text/css" href="/dist/vendor/owl.carousel/dist/assets/owl.carousel.min.css">
    {{-- <link rel="stylesheet" type="text/css" href="/dist/vendor/ionrangeslider/css/ion.rangeSlider.css">
    <link rel="stylesheet" type="text/css" href="/dist/vendor/datatables/media/css/dataTables.bootstrap4.min.css"> --}}
    <!-- v.1.4.0 -->
    <link rel="stylesheet" type="text/css" href="/dist/vendor/nprogress/nprogress.css">
    <link rel="stylesheet" type="text/css" href="/dist/vendor/jquery-steps/demo/css/jquery.steps.css">
    <!-- v.1.4.2 -->
    <link rel="stylesheet" type="text/css" href="/dist/vendor/bootstrap-select/dist/css/bootstrap-select.min.css">
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto">
    <!-- Clean UI Styles -->
    <!--<link rel="stylesheet" type="text/css" href="/css/cleanui/main.css">-->
    <link rel="stylesheet" type="text/css" href="/dist/css/index.css">

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
    <link rel="stylesheet" type="text/css" href="/dist/css/wizard.css">

    <!-- clock -->
    <link rel="stylesheet" type="text/css" href="/dist/css/jClocksGMT.css">

  </head>
  <body class="theme-default menu-top menu-fixed mode-material mode-squared colorful-enabled">

    <div id="app"></div>


    <script src="/dist/vendor/jquery/dist/jquery.min.js"></script>
    <script src="/dist/vendor/tether/dist/js/tether.min.js"></script>
    <script src="/dist/vendor/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="/dist/vendor/jscrollpane/script/jquery.jscrollpane.min.js"></script>
    <script src="/dist/vendor/jquery-mask-plugin/dist/jquery.mask.min.js"></script>
    <script src="/dist/vendor/autosize/dist/autosize.min.js"></script>
    <script src="/dist/vendor/bootstrap-show-password/bootstrap-show-password.min.js"></script>
    <script src="/dist/vendor/bootstrap-sweetalert/dist/sweetalert.min.js"></script>
    <script src="/dist/vendor/remarkable-bootstrap-notify/dist/bootstrap-notify.min.js"></script>

    <!-- Clean UI Scripts -->
    <script src="/dist/js/cleanui/common.js"></script>
    <script src="/dist/vendor/moment/min/moment.min.js"></script>
    <script src="https://momentjs.com/downloads/moment-timezone-with-data.js"></script>
    <script src="/dist/js/jquery.smartWizard.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/mathjs/3.17.0/math.min.js"></script>

    <script src="http://maps.googleapis.com/maps/api/js?key=AIzaSyCUwVkizMlyqVMIc0wtDN3FE0QxPw7IIXo&sensor=false&amp;libraries=places" type="text/javascript"></script>
    <script src="/dist/js/jClocksGMT.js"></script>
    <script src="/dist/js/jquery.rotate.js"></script>
    <script src="/dist/build.js"></script>
  </body>
</html>
