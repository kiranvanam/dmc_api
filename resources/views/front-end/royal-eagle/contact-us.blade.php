@extends("front-end.".$data['dmc'].".master")


@section('banner')
    @include("front-end.".$data['dmc'].".non-home-banner")
@endsection

@section('content')
    
    <div class="contact-top">
        <!-- container -->
        <div class="container">
            <div class="contact-info wow fadeInDown animated animated" data-wow-delay=".5s">
                <h2>Contact</h2>
            </div>
          
            <div class="mail-grids">
                <div class="col-md-6 mail-grid-left wow fadeInLeft animated animated" data-wow-delay=".5s">
                    <h3>Address</h3>
                    <p style="font-weight:bold;color:black">
                        301, Saif Al Ashram Building,<br> 
                        Near Clock Tower, <br>
                        Deira | Dubai | United Arab Emirates | P.O. Box No. 181367 <br>
                        Tele : +971 4 299 9499  <br>
                        Mobile: +97150 3935881 <br>
                        Email: info@godubaitourism.com
                    </p>                                        
                </div>
                <div class="col-md-6 contact-form wow fadeInRight animated animated" data-wow-delay=".5s" id="contact-us">
                    <form @submit.prevent>
                        <input type="text" placeholder="Name" v-model="contactUs.name" required="">
                        <input type="text" placeholder="Email" v-model="contactUs.email" required="">
                        <label v-if="contactUs.email && !validateEmail(contactUs.email)" class="text-danger"> Enter a valid email id</label>
                        <input type="text" placeholder="Subject" v-model="contactUs.subject" required="">
                        <textarea placeholder="Message" required="" v-model="contactUs.message"></textarea>
                        <p v-if="!isAllDataFilled" class="text-danger"> Please Make sure to fill all mandatory fields before submitting </p>
                        <input type="submit"  @click="submit" :disabled="!isAllDataFilled || isForSubmitted" value="SEND">
                    </form>

                    <hr>
                    <h5> Need Assistance? </h5>
                    <table class="table">
                        <tr>
                            <th class="font-weight-bold text-right" style="width:50%">Sales Related Queries </th>
                            <td class="text-info text-left"> sales@godubaitourism.com </td>
                        </tr>
                        <tr>
                            <th class="font-weight-bold text-right">If you are Interested to Become our Distributor / Agent / Franchisee</th>
                            <td class="text-info text-left"> info@godubaitourism.com</td> 
                        </tr>
                        <tr>
                            <th class="font-weight-bold text-right"> M.I.C.E.</th>
                            <td class="text-info text-left"> info@godubaitourism.com </td>
                        </tr>
                        <tr>
                            <th class="font-weight-bold text-right"> Groups related queries</th>
                            <td class="text-info text-left"> info@godubaitourism.com </td>
                        </tr>
                        <tr>
                            <th class="font-weight-bold text-right">Complaints / Feedback </th>
                            <td class="text-info text-left"> info@godubaitourism.com </td>
                        </tr>
                    </table>
                </div>
                <div class="clearfix"> </div>
            </div>
        </div>
        <!-- //container -->
    </div>


<script>
    new Vue({
      el: '#contact-us',      
      data: {
            contactUs : { name:'', email:'', subject:'', message:'' },
            isForSubmitted:false
        },
        computed:{
            isAllDataFilled() {
                return this.contactUs.email && this.validateEmail(this.contactUs.email)  && this.contactUs.subject && 
                        this.contactUs.name && this.contactUs.message
            }
        },
        methods:{
            disableSubitmitButton() {
                this.isForSubmitted= true
            },
            enableSubitmitButton() {
                this.isForSubmitted= false
            },
            submit() {
                this.disableSubitmitButton()
                axios.post("/contact-us", this.contactUs)
                    .then( response => {
                        alert("Submitted successfully")
                        this.enableSubitmitButton()
                        this.contactUs = { name:'', email:'', subject:'', message:'' }
                    }).catch( response => {
                        if(response.body.email)
                            alert("Please enter valid email")
                        this.enableSubitmitButton()
                    } )
            },
            
            validateEmail(email) {
                var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                return re.test(email);
            }
        }
    })
</script>
    
@endsection