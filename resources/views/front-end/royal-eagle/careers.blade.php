@extends("front-end.".$data['dmc'].".master")


@section('banner')
    @include("front-end.".$data['dmc'].".non-home-banner")
@endsection

@section('content')
    
    <div class="about-top">
        <!-- container -->
        <div class="container">
            <div class="about-info wow fadeInDown animated animated" data-wow-delay=".5s">
                <h2> Careers</h2>
            </div>
            <div class="about-top-grids">
                <div class="col-md-8 about-top-grid">
                    
                    <strong> royaleagledubai.com </strong> is the wholesale travel company dedicated to selling purely to the global travel trade.
                    
                    <p>
                    The company keenly focus in promoting Dubai Tourism since 1999 with a dedicated team of varied nationalities to manage and develop the business. Each office has a highly experienced team of sales managers, contractors, operations experts, IT and finance staff to service and develop the needs of the travel trade in their respective regions.
                    </p>

                    <p>
                    We are committed to investing in our people and our partners to continue our expansion and provide a competitive service across the globe.
                    </p>

                    <p>
                        Our Mission is to continue establishing regional offices in Europe, Asia Pacific, Africa and the Americas as well as develop the most competitive and comprehensive range of products and services with sufficient allocations to meet the requirements of partner offices and travel professionals globally.
                    </p>

                    <p>
                        Our Vision is to be the undisputed leader in Dubai Destination Management Company in the global wholesale travel business.
                    </p>
                    <p>
                        We have a number of varied career opportunities for ambitious, qualified professionals wishing to join a professional organisation and further their career in a very challenging and rewarding environment.
                    </p>
                    <p>
                       <strong> To apply please mail to : </strong>  <span class="text-info"> hr@royaleagledubai.com </span>
                    </p>
                    
                    <hr>
                    <h5>  Careers at royaleagledubai </h5>
                    <hr>
                    <div class="row no-gutters">
                        <div class="col-12">
                            <table class="table">
                                <thead class="thead-default">
                                    <tr>
                                        <th>Job Title</th>
                                        <th>  Reference   </th>
                                        <th> Job Location </th>
                                        <th> Department </th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 about-top-grid wow fadeInRight animated animated" data-wow-delay=".5s">
                    <img src="https://cdn.laimoon.com/profileimages/thumbnail/5948d074-02f4-4c0e-bc41-4b450a00004d_xpress-English-Center.png" alt="" />
                </div>
                <div class="clearfix"> </div>
            </div>
        </div>
        <!-- //container -->
    </div>

@endsection