
<!DOCTYPE html>
<html>
<head>
<title> {{ $data['title'] }} DMC- United Arab Emirates - Destination Management Company </title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="shortcut icon" type="image/x-icon" href="/images/favicon/{{$data['dmc']}}.png">
<meta name="keywords" content="Dubai Tourism, Uinted Arab Emirates, UAE, Burj Khalifa, Abu Dhabi, UAE DMC, Destination Management Compant " />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<!-- bootstrap-css -->
<link href="/front-end/css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
<!--// bootstrap-css -->
<!-- css -->
    
<link rel="stylesheet" href="/front-end/css/royal-eagle-style.css" type="text/css" media="all" />
<!--// css -->

<!-- font -->
<link href='//fonts.googleapis.com/css?family=Parisienne' rel='stylesheet' type='text/css'>
<link href='//fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
<link href='//fonts.googleapis.com/css?family=Cinzel+Decorative:400,700,900' rel='stylesheet' type='text/css'>
<!-- //font -->
<script src="/front-end/js/jquery-1.11.1.min.js"></script>
<script src="/front-end/js/bootstrap.js"></script>
<script type="text/javascript" src="/front-end/js/move-top.js"></script>
<script type="text/javascript" src="/front-end/js/easing.js"></script>
<script type="text/javascript">
    jQuery(document).ready(function($) {
        $(".scroll").click(function(event){     
            event.preventDefault();
            $('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
        });
    });
</script>
<script src="https://unpkg.com/vue"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.16.2/axios.min.js"></script>   
<!--animate-->
<link href="/front-end/css/animate.css" rel="stylesheet" type="text/css" media="all">
<script src="/front-end/js/wow.min.js"></script>
    <script>
         new WOW().init();
    </script>
<!--//end-animate-->
</head>
<body>
    <!-- banner-top -->
    <div class="banner-top">
        <div class="container">
            <div class="banner-top-left wow fadeInLeft animated animated" data-wow-delay=".5s">
                <p>Email : <a href="mailto:sales@godubaitourism.com">sales@godubaitourism.com</a></p>
            </div>
            <div class="banner-top-right wow fadeInRight animated animated" data-wow-delay=".5s">
                <p> Tel/WhatsApp: +971503935881 </p>
            </div>
            <div class="clearfix"> </div>
        </div>
    </div>
    <!-- banner-top -->
    <!-- banner -->
    <div class="banner">
        <div class="container">
            @yield('banner')
        </div>

        <div class="top-navigation">
            <div class="container">
                <div class="top-nav">
                    <nav class="navbar navbar-default">
                        <div class="container">
                            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">Menu                       
                            </button>
                        </div>
                        <!-- Collect the nav links, forms, and other content for toggling -->
                        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                            <ul class="nav navbar-nav">
                                <li><a href="/" class="<?php  if(isset($data['home'])) echo $data['home']; ?>">Home</a></li>
                                <li><a href="<?php echo url('/about'); ?>" class="<?php  if(isset($data['about'])) echo $data['about']; ?>">About</a></li>
                                <li><a href="<?php echo url('/services'); ?>" class="<?php  if(isset($data['services'])) echo $data['services']; ?>">Services</a></li>
                                   
                                <li><a href="<?php echo url('/careers'); ?>" class="<?php  if(isset($data['careers'])) echo $data['careers']; ?>">Careers</a></li>
                                <li><a href="<?php echo url('/e-brochures'); ?>" class="<?php  if(isset($data['e-brochures'])) echo $data['e-brochures']; ?>">E-Brochure</a></li>
                                <li><a href="<?php echo url('/contact'); ?>" class="<?php  if(isset($data['contact'])) echo $data['contact']; ?>">Contact</a></li>
                                <li><a href="#" class="dropdown-toggle hvr-bounce-to-bottom" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Agents<span class="caret"></span></a>
                                    <ul class="dropdown-menu">
                                        <li><a class="hvr-bounce-to-bottom" href="<?php echo $data['agentLoginUrl']; ?>" target="_blank">Sign-in</a></li>
                                        <li><a class="hvr-bounce-to-bottom" href="<?php echo $data['agentRegisterUrl']; ?>" target="_blank" > Register</a></li>                                                   
                                    </ul>
                                </li>
                            </ul>   
                            <div class="clearfix"> </div>
                        </div>  
                    </nav>      
                </div>
            </div>
        </div>
    </div>
    <!-- //banner -->
    @yield('content')
    <!-- footer -->
    <div class="footer">
        <div class="container">
            <div class="footer-grids">
                <div class="footer-heading wow fadeInUp animated animated" data-wow-delay=".5s">
                    <h3>Get in touch with us</h3>
                </div>
                <div class="footer-icons wow fadeInUp animated animated" data-wow-delay=".5s">
                    <ul>
                        <li><a href="https://www.facebook.com/RoyalEagleTourismDubai/?modal=admin_todo_tour" target="_blank" class="twitter facebook"></a><span>Facebook</span></li>
                        <!-- <li><a href="#" class="twitter chrome"></a><span>Google +</span></li>
                        <li><a href="#" class="twitter dribbble"></a><span>Dribbble</span></li> -->
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="footer-bottom">
        <div class="container">
            <div class="footer-nav wow fadeInRight animated animated" data-wow-delay=".5s">                
                <ul>
                    <li><a href="/" class="active">Home</a></li>
                    <li><a href="<?php echo url('/about'); ?>">About</a></li>
                    <li><a href="<?php echo url('/services'); ?>">Services</a></li>
                    <li><a href="<?php echo url('/careers'); ?>">Careers</a></li>
                    <li><a href="<?php echo url('/e-brochures'); ?>">E-Brochure</a></li>
                    <li><a href="<?php echo url('/contact'); ?>">Contact</a></li>                    
                </ul>
            </div>
            <div class="copyright wow fadeInLeft animated animated" data-wow-delay=".5s">
                <p>© 2018 Royal Eagle Dubai . All Rights Reserved.</p>
            </div>
        </div>
    </div>
    <!-- //footer -->
</body> 
</html>