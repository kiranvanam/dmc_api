@extends("front-end.".$data['dmc'].".master")

@section('banner')
    <div class="banner-grids">
        <div class="t-logo wow zoomIn animated animated" data-wow-delay=".5s">
            <div class="banner-left">
                <div class="banner-logo">
                    <h1>
                        <a href="/">
                             <img src="images/go-dubai/logo.png" width="300px" height="120px">
                        </a>
                    </h1>

                </div>
            </div>
        </div>
        <div class="banner-right">
            <div class="banner-top-right-text wow fadeInUp animated animated" data-wow-delay=".5s">
                <h2> Since Year 2003</h2>
            </div>
            <div class="banner-right-top">
                <script src="/front-end/js/responsiveslides.min.js"></script>
                <script>
                    // You can also use "$(window).load(function() {"
                    $(function () {
                      // Slideshow 4
                      $("#slider3").responsiveSlides({
                        auto: true,
                        pager: false,
                        nav: false,
                        speed: 500,
                        namespace: "callbacks",
                        before: function () {
                          $('.events').append("<li>before event fired.</li>");
                        },
                        after: function () {
                          $('.events').append("<li>after event fired.</li>");
                        }
                      });

                    });
                </script>
                <div id="top" class="callbacks_container">
                    <ul class="rslides callbacks callbacks1" id="slider3">
                        <li id="callbacks1_s0" class="callbacks1_on" style="display: block; float: left; position: relative; opacity: 1; z-index: 2; transition: opacity 500ms ease-in-out;">
                            <div class="banner-right-info">
                                <h3 class="wow fadeInUp animated animated" data-wow-delay=".5s">"Destination Management Specialist" in Dubai </h3>
                                <p class="wow fadeInUp animated animated" data-wow-delay=".5s">We are equipped with a real-time and user-friendly B2B online reservation system for Dubai and UAE that is seamlessly connected with our Travel Professionals worldwide.
                                 Become Go Dubai Travel Partner!. <br>
                                 Apply for your Travel Agency account</p>
                                <div class="more-button wow fadeInUp animated animated" data-wow-delay=".5s">
                                    <a href="{{$data['agentLoginUrl']}}" target="_blank">Click Here </a>
                                </div>
                            </div>
                        </li>
                        <li id="callbacks1_s1" class="" style="display: block; float: none; position: absolute; opacity: 0; z-index: 1; transition: opacity 500ms ease-in-out;">
                            <div class="banner-right-info">
                                <h3>Offering Best of Services at Un-beatable Price and Quality</h3>
                                <p>We are equipped with a real-time and user-friendly B2B online reservation system for Dubai and UAE that is seamlessly connected with our Travel Professionals worldwide, through which they receive instant confirmation for all services. Become Go Dubai Travel Partner!.
                                Apply for your Travel Agency account....</p>
                                <div class="more-button">
                                    <a href="<?php echo $data['agentLoginUrl']; ?>" target="_blank">Click Here    </a>
                                </div>
                            </div>
                        </li>
                    </ul><ul class="callbacks_tabs callbacks1_tabs"><li class="callbacks1_s1 callbacks_here"><a href="#" class="callbacks1_s1">1</a></li><li class="callbacks1_s2"><a href="#" class="callbacks1_s2">2</a></li></ul>
                </div>
            </div>
        </div>
        <div class="clearfix"> </div>
    </div>
@endsection


@section('content')

    <!-- welcome -->
    <div class="welcome">
        <div class="container">
            <div class="col-md-6 welcome-left">
                <h3 class="wow fadeInLeft animated animated" data-wow-delay=".5s">Welcome</h3>
                <h4 class="wow fadeInLeft animated animated" data-wow-delay=".5s">Go Dubai Tourism " E-Tourism Destination Management Company" expertise in promoting Dubai as a Tourism Destination Since year 2003. </h4>
                <p class="wow fadeInLeft animated animated" data-wow-delay=".5s">Go Dubai Tourism: Consolidator in the travel and tourism sector, has been a key player in the region, offering unmatched services. Regarded as one of the Best Tour Operators in Dubai since 2003. <a href="<?php echo url('about'); ?>">... more</a> </p>

            </div>
            <div class="col-md-6 welcome-right">
                <div class="welcome-right-top wow fadeInUp animated animated" data-wow-delay="-0.5s">
                    <img src="/front-end/images/15.jpeg">
                </div>
                <div class="welcome-right-bottom wow fadeInRight animated animated" data-wow-delay=".5s">
                    <img src="/front-end/images/morning-desertsafari.png">
                </div>
            </div>
            <div class="clearfix"> </div>
        </div>
    </div>
    <!-- //welcome -->
    <!-- welcome-bottom -->
    <div class="welcome-bottom">
        <div class="col-md-6 welcome-bottom-left">
            <div class="welcome-bottom-left-grid">
                <h3 class="wow fadeInUp animated animated" data-wow-delay=".5s">"AT THE TOP BURJ KHALIFA 124 LEVEL" AND DINNER IN HOTEL ARMANI</h3>
                <h4 class="wow fadeInUp animated animated" data-wow-delay=".5s">
                     See all the top attractions in Dubai on this full-day guided tour. Head up to the 124th floor of Burj Khalifa for the best possible views of the city and surrounding area. End the day with dinner at the Armani Hotel, one of the city’s top dining spots.
                </h4>
                <p class="wow fadeInUp animated animated" data-wow-delay=".5s">Enjoy the Beautiful Fountain Show at The Down Town Burj Khalifa. One of Dubai’s most compelling tourist attractions, The Dubai Fountain delights thousands of visitors every day.</p>
                <div class="more-button welcome-button wow fadeInUp animated animated" data-wow-delay=".5s">
                    <a href="<?php echo $data['agentLoginUrl']; ?>" target="_blank" >Click Here    </a>
                </div>
            </div>
        </div>
        <div class="col-md-6 welcome-bottom-right">
            <div class="welcome-bottom-right-top-info wow zoomIn animated animated" data-wow-delay=".5s">
                <div class="welcome-bottom-right-top">

                </div>
            </div>
        </div>
        <div class="clearfix"> </div>
    </div>
    <!-- //welcome-bottom -->
    <!-- services -->
    <div class="services">
        <div class="container">
            <div class="services-info">
                <h3 class="wow fadeInUp animated animated" data-wow-delay=".5s">Featured Trips</h3>
            </div>
            <div class="services-grids">
                <div class="col-md-4 services-grid wow fadeInLeft animated animated" data-wow-delay=".5s">
                    <div class="services-grid-top">
                        <div class="services-grid-img">
                            <img src="/front-end/images/ferrari_world.jpeg" alt="" />
                        </div>
                        <div class="services-grid-info">
                            <h4>Best of Dubai with Abu Dhabi</h4>
                            <p>Please contact us for Best of Dubai Holiday Packages with Abu Dhabi Combo for 3 Nights / 4 Days and up to 6 Nights with all Inclusive.</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 services-grid wow fadeInUp animated animated" data-wow-delay=".5s">
                    <div class="services-grid-top">
                        <div class="services-grid-info">
                            <h4>Dubai Tour with Miracle Garden</h4>
                            <p>Discover a stunning floral display in the middle of the desert with a ticket to Dubai Miracle Garden in Dubailand. Stroll among more than 45 million flowers in the 72,000-square-meter grounds and see flowers covering houses, vintage cars and more.</p>
                        </div>
                        <div class="services-grid-img">
                            <img src="/front-end/images/miracle_garden.jpeg" alt="" />
                        </div>
                    </div>
                </div>
                <div class="col-md-4 services-grid">
                    <div class="services-right wow fadeInRight animated animated" data-wow-delay=".5s">
                        <h4>Hot tours</h4>
                        <p>All Tours on Seat In Coach and Private</p>
                    </div>
                    <div class="services-right services-right-middle wow fadeInRight animated animated" data-wow-delay=".5s">
                        <h4>Transfers</h4>
                        <p>Airport Transfers, Tour Transfers, Meals Transfers, Internal Transfers and Vehicle on Disposal</p>
                    </div>
                    <div class="services-right wow fadeInRight animated animated" data-wow-delay=".5s">
                        <h4>Hotels</h4>
                        <p> Best of 2,3,4,5 Star Hotels in Dubai and all over the UAE. Best Price and Availability Guaranteed.</p>
                    </div>
                </div>
                <div class="clearfix"> </div>
            </div>
        </div>
    </div>
    <!-- //services -->

@endsection
