@extends("front-end.".$data['dmc'].".master")


@section('banner')
    @include("front-end.".$data['dmc'].".non-home-banner")
@endsection

@section('content')
    
    <div class="about-top">
        <!-- container -->
        <div class="container">
            <div class="about-info wow fadeInDown animated animated" data-wow-delay=".5s">
                <h2> Our Services</h2>
            </div>
            <div class="about-top-grids">
                <div class="col-md-8 about-top-grid">
                    
                    <div>
                        We understand how important it is to deliver what you have promised to your customers, delivering your promise is our goal and the key to our success. We still work with you to exceed your customer's expectations helping you to plan and design a new level of service delivery which will give you and your customer's peace of mind. Our experienced knowledgeable team provides a professional and accessible service to all guests, whether it's their first visit or a repeat visit we will help them plan their holiday making best use of their valuable time with their preferred combination of relaxation, site-seeing, shopping and dining.
                    </div>
                    <br>
                    <div>
                        We can organize something special or spectacular for everyone who visits the UAE - be it a trip through the mountains, a feast under the desert skies, shopping at a souk, or driving over dunes."GO DUBAI TOURISM.COM" is ranked the most experienced operator of tours and safaris in the region, and we provide services of an exceptional standard.
                    </div>
                      <br>

                    <div>
                        We organize scheduled departure weekly tours and safaris to cities, beaches, deserts, mountains, museums, hotels, iconic buildings, protected reserves and famed shopping malls. These tours are conducted by car, coach, boat, helicopter, seaplane, and on camelback, accompanied by multilingual guides and trained drivers. All our excursions can also be made available on private or exclusive basis. "GO DUBAI TOURISM.COM" operates seven days a week and has a 24-hour helpline.
                    </div>
                    <br>
                    <div>
                        <ul style="list-style-type:disc">
                            <li>Marhaba airport meet &amp; greet services </li>
                            <li>    Dubai (UAE) Visa Services </li>
                            <li> Airport Transfers </li>
                            <li> Dubai &amp; UAE Hotel Reservations </li>
                            <li> Dubai &amp; UAE Restaurant Reservations </li>
                            <li> Multilingual Guided Tours </li>
                            <li> Car and Coach Rental with &amp; without Chauffeur </li>
                        </ul>            
                        24X7 Customer Care Support
                    </div>
                    <br>
                    <div>
                      <h3 class="text-info"> Daily Tours... </h3>
                       <ul style="list-style-type:disc">
                            <li>Half Day Panoramic City Tour of Dubai</li>
                            <li>Dubai "Shopping Tour"</li>
                            <li>Dubai "Walking Tour"</li>
                            <li>Full Abu Dhabi "Capital City Tour"</li>
                            <li>Dhow Dinner Cruise in Marina / Dubai Creek</li>
                            <li>Desert Safari Dune Bashing with Dinner &amp; Live Entertainment</li>
                            <li>Visit to Burj Khalifa "World's Tallest Tower" 124th Level with Shopping at Dubai Mall “Largest Shopping Mall in the World"</li>
                            <li>Snow Park, SKI and Shopping at Mall of Emirates</li>
                            <li>Enjoy and Watch Dolphin Show at Dubai Dolphinarium</li>
                            <li>Full Day at Water Theme Parks: Aqua Venture and Wild Wadi in Dubai & YAS Water World at Abu Dhabi.</li>
                            <li>Dubai Parks - Hollywood Park, Bollywood Park, Lego Land</li>
                            <li>IMG WORLD OF ADVENTURE</li>
                       </ul>            
                    </div>
                    <br>
                    <div>
                        <h3 class="text-info">Special Interest Tours &amp; Activities...</h3>

                        <ul style="list-style-type:disc">
                            <li>Deep Sea Fishing
                            <li>Hot Air Balloon</li>
                            <li>Yachts Services</li>
                            <li>Dubai Aerial View by Helicopter / Sea Plane</li>
                            <li>Overnight stay in the Desert</li>
                            <li>Adventure Camp</li>
                            <li>Golfing</li>
                            <li>Camel racing</li>
                            <li>Fishing</li>
                            <li>Scuba Diving</li>
                            <li>Ice-skating</li>
                            <li>Motor racing and go-karting</li>
                            <li>Sand skiing</li>
                            <li>Deep sea fishing</li>
                            <li>Luxury Yachts Booking</li>
                            <li>Restaurant Reservations at Hotel Burj Al Arab, Atlantis, Emirates Palace</li>
                        </ul>
                    </div>
                    <br>
                    <div>
                        <strong> M.I.C.E.: </strong> With combine comprehensive destination knowledge, operational experience, painstaking attention to detail, go dubai tourism delivers to every need of the business house. A team of dedicated and experienced MICE team will handle your group from quotation to departure stage. The venues arranged by M.I.C.E. are reasonably priced and high in quality. Whether your clients require small private meeting room, large conference hall of a ballroom with catering and banquet services, M.I.C.E. will make arrangements for it at best competitive rates. Whether it is a family or school reunion, wedding, corporate event, society function or any kind of special event, M.I.C.E. will ensure that it becomes a memorable event for your guests.
                    </div>
                    <br>
                    <div>
                        <strong> Events: </strong> Planning an event ? Leave it to us! Whatever it takes, we can arrange it perfectly. Watch regularly our site for events and happenings in Dubai and all over the UAE.
                    </div>
                    <br>
                    <div>
                        <strong> Entertainment: </strong> Reserve seats for premium Shows, Pubs and Night clubs online with huge discounts which is valid only for booking via our online system.
                    </div>
                </div>
                <div class="col-md-4 about-top-grid wow fadeInRight animated animated" data-wow-delay=".5s">
                    <img src="/front-end/images/services.png" alt="" />
                </div>
                <div class="clearfix"> </div>
            </div>
        </div>
        <!-- //container -->
    </div>

@endsection