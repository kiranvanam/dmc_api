@extends("front-end.".$data['dmc'].".master")


@section('banner')
    @include("front-end.".$data['dmc'].".non-home-banner")
@endsection

@section('content')
    
    <div class="about-top">
        <!-- container -->
        <div class="container">
            <div class="about-info wow fadeInDown animated animated" data-wow-delay=".5s">
                <h2>About Us</h2>
            </div>
            <div class="about-top-grids">
                <div class="col-md-8 about-top-grid">
                    <p>
                       <strong> Go Dubai Tourism </strong> " E-Destination Management Company" expertise in promoting Dubai as a Tourism Destination Since year 2003.                        
                    </p>
                    <p>    
                       <strong> Go Dubai Tourism: </strong> Consolidator in the travel and tourism sector, has been a key player in the region, offering unmatched services. Regarded as one of the Best Tour Operators in Dubai since 2003.
                    </p>
                    <p>
                        We continue to play a key role in supporting the tourism growth initiatives of Dubai. Upholding the highest standards of services and product excellence, our growth has been led by the dedicated efforts of all our staff members, and our commitment to adhering to the highest professional standards and ethics. 
                    </p>
                    <p>
                        The acknowledged performance in terms of customer satisfaction and a devoted focus to provide the finest tourism and travel experience has placed us in the top league of Inbound Tour Operators in Dubai.
                    </p>
                    <p>
                        With our wide and vast network of offices and associates in GCC Countries, Indian sub-continent, Far-East Asia, Africa and CIS countries, Go Dubai Tourism rolls out the entire array of travel services, each tailored to add value to our customers.
                    </p>
                </div>
                <div class="col-md-4 about-top-grid wow fadeInRight animated animated" data-wow-delay=".5s">
                    <img src="/front-end/images/dubai_tourism.png" alt="" />
                </div>
                <div class="clearfix"> </div>
            </div>
        </div>
        <!-- //container -->
    </div>

@endsection