<!DOCTYPE html>
<html>
<head>
    <title> Registration Welcome Email </title>
    <style type="text/css">
        /* Base */

body, body *:not(html):not(style):not(br):not(tr):not(code) {
    font-family: Avenir, Helvetica, sans-serif;
    box-sizing: border-box;
}

body {
    background-color: #F2F4F6;
    color: #74787E;
    height: 100%;
    line-height: 1.4;
    margin: 0;
    width: 100% !important;
    -webkit-text-size-adjust: none;
}

p,
ul,
ol,
blockquote {
    line-height: 1.4;
    text-align: left;
}

a {
    color: #3869D4;
}

a img {
    border: none;
}

/* Typography */

h1 {
    color: #2F3133;
    font-size: 19px;
    font-weight: bold;
    margin-top: 0;
    text-align: left;
}

h2 {
    color: #2F3133;
    font-size: 16px;
    font-weight: bold;
    margin-top: 0;
    text-align: left;
}

h3 {
    color: #2F3133;
    font-size: 14px;
    font-weight: bold;
    margin-top: 0;
    text-align: left;
}

p {
    color: #74787E;
    font-size: 16px;
    line-height: 1.5em;
    margin-top: 0;
    text-align: left;
}

p.sub {
    font-size: 12px;
}

img {
    max-width: 100%;
}

/* Layout */

.wrapper {
    background-color: #f5f8fa;
    margin: 0;
    padding: 0;
    width: 100%;
    -premailer-cellpadding: 0;
    -premailer-cellspacing: 0;
    -premailer-width: 100%;
}

.content {
    margin: 0;
    padding: 0;
    width: 100%;
    -premailer-cellpadding: 0;
    -premailer-cellspacing: 0;
    -premailer-width: 100%;
}

/* Header */

.header {
    padding: 25px 0;
    text-align: center;
}

.header a {
    color: #bbbfc3;
    font-size: 19px;
    font-weight: bold;
    text-decoration: none;
    text-shadow: 0 1px 0 white;
}

/* Body */

.body {
    background-color: #FFFFFF;
    border-bottom: 1px solid #EDEFF2;
    border-top: 1px solid #EDEFF2;
    margin: 0;
    padding: 0;
    width: 100%;
    -premailer-cellpadding: 0;
    -premailer-cellspacing: 0;
    -premailer-width: 100%;
}

.inner-body {
    background-color: #FFFFFF;
    margin: 0 auto;
    padding: 0;
    width: 570px;
    -premailer-cellpadding: 0;
    -premailer-cellspacing: 0;
    -premailer-width: 570px;
}

/* Subcopy */

.subcopy {
    border-top: 1px solid #EDEFF2;
    margin-top: 25px;
    padding-top: 25px;
}

.subcopy p {
    font-size: 12px;
}

/* Footer */

.footer {
    margin: 0 auto;
    padding: 0;
    text-align: center;
    width: 570px;
    -premailer-cellpadding: 0;
    -premailer-cellspacing: 0;
    -premailer-width: 570px;
}

.footer p {
    color: #AEAEAE;
    font-size: 12px;
    text-align: center;
}

/* Tables */

.table table {
    margin: 30px auto;
    width: 100%;
    -premailer-cellpadding: 0;
    -premailer-cellspacing: 0;
    -premailer-width: 100%;
}

.table th {
    border-bottom: 1px solid #EDEFF2;
    padding-bottom: 8px;
}

.table td {
    color: #74787E;
    font-size: 15px;
    line-height: 18px;
    padding: 10px 0;
}

.content-cell {
    padding: 35px;
}

/* Buttons */

.action {
    margin: 30px auto;
    padding: 0;
    text-align: center;
    width: 100%;
    -premailer-cellpadding: 0;
    -premailer-cellspacing: 0;
    -premailer-width: 100%;
}

.button {
    border-radius: 3px;
    box-shadow: 0 2px 3px rgba(0, 0, 0, 0.16);
    color: #FFF;
    display: inline-block;
    text-decoration: none;
    -webkit-text-size-adjust: none;
}

.button-blue {
    background-color: #3097D1;
    border-top: 10px solid #3097D1;
    border-right: 18px solid #3097D1;
    border-bottom: 10px solid #3097D1;
    border-left: 18px solid #3097D1;
}

.button-green {
    background-color: #2ab27b;
    border-top: 10px solid #2ab27b;
    border-right: 18px solid #2ab27b;
    border-bottom: 10px solid #2ab27b;
    border-left: 18px solid #2ab27b;
}

.button-red {
    background-color: #bf5329;
    border-top: 10px solid #bf5329;
    border-right: 18px solid #bf5329;
    border-bottom: 10px solid #bf5329;
    border-left: 18px solid #bf5329;
}

/* Panels */

.panel {
    margin: 0 0 21px;
}

.panel-content {
    background-color: #EDEFF2;
    padding: 16px;
}

.panel-item {
    padding: 0;
}

.panel-item p:last-of-type {
    margin-bottom: 0;
    padding-bottom: 0;
}

/* Promotions */

.promotion {
    background-color: #FFFFFF;
    border: 2px dashed #9BA2AB;
    margin: 0;
    margin-bottom: 25px;
    margin-top: 25px;
    padding: 24px;
    width: 100%;
    -premailer-cellpadding: 0;
    -premailer-cellspacing: 0;
    -premailer-width: 100%;
}

.promotion h1 {
    text-align: center;
}

.promotion p {
    font-size: 15px;
    text-align: center;
}

    </style>
</head>
<body>
    <div class="panel">

        <strong> Dear Mr. {{ $user->name }}, </strong> <br>

        <br>

        Greetings from  Go Dubai Tourism . <br>

        <br>

        We are pleased to send you the following details.

        <br> <br>


       <strong> ONLINE ACCESS </strong>

        <br>

         Go Dubai Tourism , Dubai B2B Online Booking System “confidential login and password” are mentioned below. <br>

        <br>

        The account is set up in <strong> {{ $agency->currency }} </strong> and you now have access to check rates and also the availability of the Hotels, Visas, Airport Transfers, Tours, Attractions and Activities Tickets, Packages Etc.

        <br>
        <pre>
            <strong> Agent   : </strong> {{ $agency->name }}

            <strong> Login    : </strong> {{  $user->email }}

            <strong> Password :</strong>   {{ $password_with_no_encryption }}

        </pre>

        <strong> Website  : </strong>  http://b2b.godubaitourism.com/admin

        <br>
        For optimal user experience we strongly recommend Google Chrome.

        <br>  <br>

        <strong> Booking Procedure </strong> <br>

        Every travel partner is assigned a specific Reservations Executive. Here are your key contacts in our Reservations &amp; Operations Departments.

        <br> <br>


        <strong> Reservations Primary Contact:</strong> Mrs. Srilekha, Reservations Incharge (reservations@godubaitourism.com) <br>

        <strong> Operations Primary Contact: </strong> Mrs. Manasa, Operations Manager (ops@godubaitourism.com) <br>

        <br> <br>

        <strong> Important Notes </strong>

        <ul>
            <li>
                Office hours are daily between 09:30 hrs and 18:30 hrs Dubai local time. However, we can be contacted 24x7 and will respond as and when required.
            </li>
            <li>
                Once a booking is confirmed we will forward the confirmation number together with a Proforma Invoice so as to minimize any calculation errors.
            </li>
            <li>
                Hotel, Transfer and other Service vouchers display our Guest Service and 24x7 Emergency Numbers at the bottom of the page.
            </li>
            <li>
                Payment must be credited into our Bank Account minimum 48 hours prior to the start of the cancellation policy of the booked service. In the event that payment is not received in full, we reserve the right to cancel the booked service without any notice.
            </li>
            <li>
                Kindly send us trade licence or tourism licence copy for documentation purposes.
            </li>
        </ul>


        <br><br>


        I trust the above details meet your requirements. Should you or any members of your team require further information or have any clarifications we are available to assist.

        <br><br> <br>


    <pre>
        <span style="font-size:14px"> Best &amp; Regards; </span>

        <strong> Bhagwan Dass </strong>

        <strong> General Manager </strong>
    </pre>


    <pre>

        <img src="{{ $message->embed('images/royal-eagle/logo.png') }}" style="width:200px; max-width:200px;height:100px">

        301, Saif Al Ashram Building | Deira | United Arab Emirates | P.O. Box No. 181367

        Mobile Number: +97150 3935881

        Email: info@godubaitourism.com

        Web: www.godubaitourism.com

    </pre>

    <strong>DISCLAIMER:</strong>
    <p style="font-size:8">
        This information is confidential and intended solely for the use of the individual to whom it is addressed. If you are not the intended recipient, be advised that you have received this email in error and that any use, dissemination, forwarding, printing, or copying of this email is strictly prohibited. If you have received this email in error please contact the sender. Any views or opinions presented are solely those of the author and do not necessarily represent those of  Go Dubai Tourism Although this email and any attachments are believed to be free of any virus or other defects which might affect any computer or IT system into which they are received, no responsibility is accepted by  Go Dubai Tourism or any of its associated companies for any loss or damage arising in any way from the receipt or use thereof.
    </p>

</div>
</body>
</html>