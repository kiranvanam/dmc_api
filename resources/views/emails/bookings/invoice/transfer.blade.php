
@foreach ($booking->transfers as $transferDetails)
<tr class="details">
    <td class="text-left">
        {{ array_get($transferDetails, 'transfer_service_name') }} <br>

        @if( array_get($transferDetails, 'vendor_code') )
        <span> {{ array_get($transferDetails, 'vendor_code') }} - </span>
        @endif

        {{  array_get($transferDetails, 'direction') }}
             <br>

        Travel Date:{{ array_get($transferDetails, 'travelling_date') }}
    </td>
    <td class="text-left">

        <strong>
            Pick-up:
        </strong>
                {{ array_get($transferDetails, 'pick_up_city_name') }} - {{ array_get($transferDetails, 'pick_up_area_name') }} <br>
        <br>
        <strong>
            Drop-off:
        </strong>
            {{ array_get($transferDetails, 'drop_off_city_name') }} - {{ array_get($transferDetails, 'drop_off_area_name') }} <br>
    </td>
    <td class="text-left">
        @if( array_get($transferDetails,'transfer_type') == 'private')
        <table cellspacing="0" cellpadding="0">
            <tr class="heading">
                <td class="p-0" style="padding:1px">Vehicle</td>
                <td class="p-0" style="padding:1px"> Price</td>
                <td class="p-0" style="padding:1px">Qty</td>
            </tr>
            <tr class="details">
                <td>
                    {{ array_get( $transferDetails, 'vehicle_details.name' ) }} ( {{ array_get( $transferDetails, 'vehicle_details.seater' ) }} -Seater)
                </td>
                <td> {{ \Columbus\Utilities\Booking\BookingUtility::vehicleUnitPrice( $transferDetails) }} {{ $booking->currency }} </td>
                <td>  {{ array_get( $transferDetails, 'vehicle_details.no_of_booked_vehicles') }}   </td>
            </tr>
        </table>
        @endif

        @if( array_get($transferDetails,'transfer_type') == 'sic')
        <table cellspacing="0" cellpadding="0">
            <tr class="heading">
                <td class="p-0" style="padding:1px"> Pax </td>
                <td class="p-0" style="padding:1px"> Price</td>
                <td class="p-0" style="padding:1px"> Qty. </td>
            </tr>

            <tr class="details">
                <td class="p-0">AD</td>
                <td class="p-0">
                    {{ \Columbus\Utilities\Booking\BookingUtility::unitPriceForPaxType( $transferDetails, 'adult_price') }}  {{  $booking->currency }}
                </td>
                <td class="p-0"> {{ $transferDetails->no_of_adults }} </td>
            </tr>
            <tr class="details">
                <td class="p-0"> CH </td>
                <td class="p-0">
                    {{ \Columbus\Utilities\Booking\BookingUtility::unitPriceForPaxType( $transferDetails, 'child_price') }}  {{  $booking->currency }}
                </td>
                <td class="p-0"> {{ $transferDetails->no_of_children }} </td>
            </tr>
            <tr class="details">
                <td class="p-0">  INF  </td>
                <td class="p-0">
                    {{ \Columbus\Utilities\Booking\BookingUtility::unitPriceForPaxType( $transferDetails, 'infant_price') }} {{  $booking->currency }}
                </td>
                <td class="p-0"> {{ $transferDetails->no_of_infants }} </td>
            </tr>
        </table>
        @endif
    </td>
    <td>
        {{ \Columbus\Utilities\Booking\BookingUtility::convertAmount($transferDetails->total_price, $transferDetails->currency) }}
        {{ $booking->currency }}
    </td>
</tr>
@endforeach

