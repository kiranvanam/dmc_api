<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <title>Booking Invoice : {{ $booking->code }} </title>
    @include('emails.css.custom')
</head>

<body style="font-size:12px">
    <div class="invoice-box">
        <div style="text-align:center">
            <h3> Invoice/ Booking Ref.# {{ $booking->code }} </h3>
        </div>
        <table cellpadding="0" cellspacing="0">
            <tr class="top">
                <td colspan="2">
                    <table>
                        <tr>
                            <td class="title">
                                <img src="{{ $message->embed($booking->logoUrlForEmail())}}" style="width:200px; max-width:200px;height:100px">
                            </td>

                            <td>
                                {{ $booking->leadPassengerName() }}-({{ $booking->leadPassengerNationality() }}) <br>
                                {{-- Email: {{ $booking->leadPassengerEmail() }} <br> --}}
                                Phone: {{ $booking->leadPassengerContactNumber() }}
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr class="item">
                <td style="font-size:14px">
                    {{ $booking->dmcAddress()  }} <br>
                                {{  $booking->dmcCodeName() }}
                                    : {{   $booking->dmcCodeNumber() }}

                </td>
            </tr>
            <br>
            <tr class="information">
                <td colspan="2">
                    <table>
                        <tr class="heading">
                            <th style="text-align:left"> Agency Details </th>
                            <th style="text-align:left"> Invoice Details </th>
                        </tr>
                        <tr class="item">
                            <td>
                                {{ $booking->agencyName() }}<br>
                                {{ $booking->agencyAddress() }} <br>
                                Handled By:{{ $booking->agencyUserBookedBy()  }}
                            </td>

                            <td>
                                Invoice #: {{ $booking->invoiceCode() }}<br>
                                GSTN/VAT: {{ $booking->agencyCode() }} <br>
                                Dated: {{ $booking->invoiceDate() }}
                            </td>
                        </tr>
                    </table>
                    <br>
                    Lead passenger :
                    {{  $booking->leadPassengerName() }}
                      (
                        Nationality: {{ $booking->leadPassengerNationality() }}
                      )
                    <br>
                </td>
            </tr>

        </table>

        @include('emails.bookings.booking.booking-item-details')

        <table cellpadding="0" cellspacing="0">
            <tr>
                <td colspan="2">
                    <h4> Invoice Notes </h4>

                    {!! $booking->invoiceNotes()  !!}

                    <br>

                    <h4> Bank details </h4>
                    <table cellpadding="0" cellspacing="0">
                        @foreach($booking->dmcInvoiceBankAccountDetails()  as $bankAccount)
                        <tr class="item">
                            <th> Country </th>
                            <td class="text-left"> {{ $booking->countryNameById( array_get($bankAccount, 'country_id') ) }} </td>
                        </tr>
                        <tr class="item">
                            <th>Name</th>
                            <td class="text-left"> {{ array_get($bankAccount, 'bank_name') }} </td>
                        </tr>
                        <tr class="item">
                            <th> In favour Of </th>
                            <td class="text-left"> {{ array_get($bankAccount, 'in_favour_off') }}  </td>
                        </tr>
                        <tr class="item">
                            <th> Currency </th>
                            <td class="text-left"> {{ array_get($bankAccount, 'currency') }} </td>
                        </tr>
                        @if( array_has( $bankAccount, 'swift_code') )
                        <tr class="item">
                            <th> SWIFT code </th>
                            <td class="text-left">  {{ array_get( $bankAccount, 'swift_code') }}  </td>
                        </tr>
                        @endif
                        @if( array_has( $bankAccount, 'iban_code') )
                        <tr class="item">
                            <th> IBAN Code </th>
                            <td class="text-left"> {{ array_get( $bankAccount, 'iban_code') }} </td>
                        </tr>
                        @endif
                        @if( array_has( $bankAccount, 'ifsc_code') )
                        <tr class="item">
                            <th> IFSC code </th>
                            <td class="text-left"> {{ array_get( $bankAccount, 'ifsc_code') }} </td>
                        </tr>
                        @endif
                        <tr class="item">
                            <th> Address </th>
                            <td class="text-left">
                                Branch: {{ array_get( $bankAccount, 'branch_name') }} <br>
                                {{ array_get( $bankAccount, 'city') }} <br>
                                {{ array_get( $bankAccount, 'address') }}
                            </td>
                        </tr>
                        @endforeach
                    </table>
                </td>
            </tr>
        </table>
    </div>
</body>
</html>
