<table cellpadding="0" cellspacing="0">

    <tr class="item">
        <td style="width:15%;;" class="align-middle" align="right">
            <strong>Service </strong>
        </td>
        <td align="left;" style="padding-left:15px">
            {{ $bookingItem->service_name }} ({!! $bookingItem->grnStarCategory() !!} * )
        </td>
    </tr>
    <tr class="item">
        <td style="width:15%;;" class="align-middle" align="right">
            <strong>Supplier Reference </strong>
        </td>
        <td align="left;" style="padding-left:15px">
                {{ $bookingItem->grnHotelSupplierReference()}}
        </td>
    </tr>
    <tr class="item">
        <td style="width:15%;" align="right" class="align-middle">
            <strong>Address </strong>
        </td>
        <td align="left;" style="padding-left:15px">
                {{ $bookingItem->grnHotelAddress()}}, {{ $bookingItem->city_name }}, {{ $bookingItem->countryName() }} <br>
                Email: {{ $bookingItem->grnHotelEmail() }} <br>
                Contact: {{ $bookingItem->grnHotelContact()  }}
        </td>
    </tr>
    <tr class="item">
        <td style="width:15%;" align="right" class="align-middle">
            <strong> Travel Dates </strong>
        </td>
        <td align="left;" style="padding-left:15px" >
            <strong>Check-in</strong>: {!! $bookingItem->hotelCheckIn() !!} <br>
            <strong>Check-out</strong>: {!! $bookingItem->hotelCheckOut() !!} <br>
        </td>
    </tr>


    {{-- <tr>
        <td style="width:15%;" align="right" class="align-middle">
            <strong> Room Details </strong>
        </td>
        <td align="left;" style="padding-left:15px" >
            @foreach ($bookingItem->grnRooms()  as $room)
                {{ $bookingItem->grnRoomName($room) }}({{ $bookingItem->grnTotalForRoom($room) }}) <br>
            @endforeach
        </td>
    </tr> --}}
</table>

<div>
    {!! $bookingItem->grnHotelDescription()  !!}
</div>

<h5 style=""> Confirmation Details </h5>
<table cellpadding="0" cellspacing="0">
    <tr class="top">
        <th style="width:35%"> Room Details </th>
        <th> Meal </th>
        <th> No.Of Rooms </th>
        <th> Adults</th>
        <th>Children</th>
    </tr>
    @foreach ($bookingItem->grnVoucherBookingItems()  as $booking_item)
        @foreach ($bookingItem->grnRoomsForBookingItem($booking_item)  as $room)
            <tr class="item">
                <td>
                    {{ array_get($room, 'room_type') }}
                    @if ($bookingItem->grnRoomReferenceFor($room))
                    <label style="font-size:10px">
                        ( ref:  {{ $bookingItem->grnRoomReferenceFor($room)  }})
                    </label>
                    @endif
                </td>
                <td>
                    @foreach ($bookingItem->grnRoomInclusitonsForRoom($room) as $inclusion)
                        {{ $inclusion }} <br>
                    @endforeach
                </td>
                <td> {{ array_get($room, "no_of_rooms", 1) }} </td>
                <td>
                    {{ array_get($room,'no_of_adults') }}
                </td>
                <td>
                    {{ array_get($room,'no_of_children') }}
                </td>
            </tr>
            <tr class="top">
                <td colspan="5">
                    @foreach (array_get($booking_item, 'rate_comments')  as $key => $comments)
                        <strong style="text-transform: capitalize;">
                            {{ str_replace( "_", " ", $key) }} :
                        </strong>
                        {{ empty($comments) ? " No ". $key : $comments }}   <br>

                    @endforeach

                </td>
            </tr>
        @endforeach
    @endforeach
</table>

<h5 class="text-info"> Guest Details </h5>
<table cellpadding="0" cellspacing="0" style="font-size:12px">
    <tr class="top">
        <th style="width:60%"> Name </th>
        <th> Type </th>
        <th> Age </th>
    </tr>
    @foreach ($bookingItem->grnVoucherGuestDetails() as $pax)
        <tr class="item">
            <td> {{ $bookingItem->grnUserName($pax) }} </td>
            <td> {{ array_get($pax, 'type', '') }} </td>
            <td> {{ array_get($pax, 'age', '') }} </td>
        </tr>
    @endforeach
</table>
