<h3 class="text-info" style="color:blue;"> Visa Details </h3>

<table cellpadding="0" cellspacing="0">
    <tr class="item">
        <td class="text-right" style="width:15%"> <strong>Name</strong> </td>
        <td> {{ array_get($booking->visa,'name') }} </td>
    </tr>
    <tr class="item">
        <td class="text-right"> Country </td>
        <td> {{ $booking->visa->country_name }} </td>
    </tr>
    <tr class="item">
        <td> <strong>Validity</strong> </td>
        <td> {{ array_get($booking->visa,'visa_details.validity') }} {{ array_get($booking->visa,'visa_details.validity_type')
            }}
        </td>
    </tr>
    <tr class="item">
        <td> <strong>Pax</strong> </td>
        <td> AD - {{ array_get( $booking->visa, 'no_of_adults' ) }}
            <br> CH - {{ array_get( $booking->visa, 'no_of_children' ) }}
            <br> INF - {{ array_get( $booking->visa, 'no_of_infants' ) }}
        </td>
    </tr>
    <tr class="item">
        <td> <strong>Travelling Date</strong> </td>
        <td> {{ $booking->service_start_date }} </td>
    </tr>
</table>

<h2 class="text-info" style="color:blue;"> Pax Details </h2>
<table cellpadding="0" cellspacing="0" style="width:80%">
    <tr class="heading">
        <td> Name </td>
        <td> Type </td>
        <td>Age </td>
        <td> Passport Number</td>
    </tr>
    @if( array_get( $booking->visa, 'pax_details', false) ) @foreach( array_get( $booking->visa, 'pax_details') as $paxDetail
    )
    <tr class="item">
        <td> {{ array_get($paxDetail,'title') . " " . array_get($paxDetail,'name') . " " . array_get($paxDetail,'surname')}}
            </td>
        <td> {{ array_get($paxDetail,'type') }} </td>
        <td> {{ array_get($paxDetail, 'age') }} </td>
        <td> {{ array_get($paxDetail,'passport_number') }} </td>
    </tr>
    @endforeach @endif
</table>

<hr>

<h4 class="text-info" style="color:blue;"> Terms &amp; Conditions </h4>

<div>
    {!! array_get( $booking->visa, 'visa_details.terms_and_conditions', '') !!}
</div>

<h4 class="text-info" style="color:blue;"> Remarks </h4>

<div>
    {!! array_get( $booking->visa, 'visa_details.remarks', '') !!}
</div>

