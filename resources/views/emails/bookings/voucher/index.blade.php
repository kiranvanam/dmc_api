<!doctype html>
<html>

<head>
    <meta charset="utf-8">
    <title>Booking Voucher : {{ $booking->code }} </title>
    @include('emails.css.custom')
</head>
<body>

    <div class="invoice-box">
        <div style="text-align:center">
            <h4> Service Voucher/ Booking Ref.# {{ $booking->code }} </h4>
        </div>
        <table cellpadding="0" cellspacing="0">
            <tr class="top">
                <td colspan="2">
                    <table>
                        <tr>
                            <td class="title">
                                <img src="{{ $message->embed($booking->logoUrlForEmail()) }}" style="width:200px; max-width:200px;height:100px">
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>

            <tr class="information">
                <td colspan="2">

                    <h5 class="text-info" style="color:blue;"> Lead Passenger Details </h5>

                    <strong> Name:</strong>
                    {{ $booking->leadPassengerName() }}
                    <br>

                    <strong> Nationality: </strong> {{ $booking->leadPassengerNationality()}}
                    <br>
                    <strong> Contact </strong> {{ $booking->leadPassengerContactNumber() }}
                    <br>
                    <strong>Email</strong> {{ $booking->leadPassengerEmail() }}


                    <h5 class="text-info" style="color:blue;">  Booking Ref: {{ $booking->code }} </h5>

                    @foreach ($booking->bookingItems as $index => $bookingItem)
                        @if( $booking->isFirstItemForPrivateTour($bookingItem,$index) )
                            <?php $tourTileDetails = $bookingItem->privateTourTitleDetails() ?>
                            <div>
                                <h5 class="text-info">{{ array_get($tourTileDetails, 'name') }} -
                                        {{  array_get($tourTileDetails, 'code') }} </h5>

                                <strong> Tour type: </strong> Private <br>

                                {!! $bookingItem->slotDetails() !!}  <br>

                                {!!  array_get($tourTileDetails, 'description') !!}
                            </div>
                        @endif
                        <div>
                            <span style="color:blue"> Service: </span>
                            <span style="text-transform: uppercase;">
                                {{ $bookingItem->service }}
                                @if( $booking->isFirstItemForPrivateTour($bookingItem,$index) )
                                    (Private)
                                @elseif($bookingItem->isTour())
                                    (SIC)
                                @endif
                            </span>
                        </div>
                        @if ($bookingItem->isTransfer() ||  $bookingItem->isVehicle())
                            @include('emails.bookings.voucher.transfer-details', ['bookingItem' => $bookingItem ])
                        @elseif($bookingItem->isHotel())
                            @include('emails.bookings.voucher.hotels.index', ["bookingItem" => $bookingItem])
                        @else
                            @include('emails.bookings.voucher.booking-item-details', ['bookingItem' => $bookingItem ])
                        @endif
                    @endforeach


                    <h5 class="text-info" style="color:blue;"> Emergency Contact </h5>
                    <strong> Agent Contact Number: </strong> +9714 2865955
                    <br>
                    <strong> Service Operator Number: </strong> +91 - 9910411117 (24X7 India Helpline Number)
                    <br>
                </td>
            </tr>
        </table>

    </div>

</body>

</html>