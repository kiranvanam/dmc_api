
<table cellspacing="0" cellpadding="0">
    <tr class="heading">
        <td colspan="2">
            Visa Details 
        </td>
    </tr>
    <tr class="details">
        <td align="left"> Name </td>
        <td> {{ array_get($booking->visa, 'name') }} </td>
    </tr>
    <tr class="details">
        <td align="left"> Code </td>
        <td> {{ array_get( $booking->visa, 'visa_code') }}/{{ array_get( $booking->visa, 'vendor_code') }} </td>
    </tr>
    <tr class="details">
        <td align="left"> Country </td>
        <td> {{ array_get( $booking->visa, 'country_name') }}({{ array_get( $booking->visa, 'vendor_code') }}) </td>
    </tr>
    <tr class="details">
        <td align="left"> Processing Time </td>
        <td>
            {{ array_get( $booking->visa, 'visa_details.processing_min_days') }} -
                {{ array_get( $booking->visa, 'visa_details.processing_max_days') }}  Days
        </td>
    </tr>
    <tr class="details">
        <td align="left"> Processing Initiation </td>
        <td>
            Between 
                <strong>
                    {{ array_get( $booking->visa, 'visa_details.max_days') }} - 
                    {{ array_get( $booking->visa, 'visa_details.min_days') }} Days 
                </strong>
                Prioir to travelling date
        </td>
    </tr>
    <tr class="item">
        <td> <strong>Pax</strong> </td>
        <td> AD - {{ array_get( $booking->visa, 'no_of_adults' ) }}
            <br> CH - {{ array_get( $booking->visa, 'no_of_children' ) }}
            <br> INF - {{ array_get( $booking->visa, 'no_of_infants' ) }}
        </td>
    </tr>
    <tr class="details">
        <td align="left"> Validity </td>
        <td>
            {{ array_get( $booking->visa, 'visa_details.validity') }} -
                {{ array_get( $booking->visa, 'visa_details.validity_type') }}                 
        </td>
    </tr>
    
    <tr class="details">
        <td align="left"> Description </td>
        <td>
            {!! array_get($booking->visa, 'visa_details.description') !!} 
        </td>
    </tr>
    <tr class="details">
        <td align="left"> Docs Required </td>
        <td>
            {!! array_get($booking->visa,'visa_details.docs_required') !!}                
        </td>                                    
    </tr>
    <tr class="details">
        <td align="left"> Terms &amp; Conditions </td>
        <td>
            {!! array_get($booking->visa,'visa_details.terms_and_conditions') !!}                         
        </td>
    </tr>
    <tr class="details">
        <td align="left"> Remarks </td>
        <td>
            {!! array_get($booking->visa,'visa_details.remarks') !!}                    
        </td>
    </tr>
</table>

<h2 class="text-info" style="color:blue;"> Pax Details </h2>

<table cellpadding="0" cellspacing="0" style="width:80%">
    <tr class="heading">
        <td> Name </td>
        <td> Type </td>
        <td> Passport Number</td>
        <td>Age </td>
    </tr>
    @if( array_get( $booking->visa, 'pax_details', false) ) 
        @foreach( array_get( $booking->visa, 'pax_details') as $paxDetail)
        <tr class="item">
            <td> {{ array_get($paxDetail,'title') . " " . array_get($paxDetail,'name') . " " . array_get($paxDetail,'surname')}}
                </td>
            <td> {{ array_get($paxDetail,'type') }} </td>
            <td> {{ array_get($paxDetail,'passport_number') }} </td>
            <td> {{ array_get($paxDetail, 'age') }} 
                    ( {{ array_get($paxDetail, 'dob') }} )
            </td>
        </tr>            
        @endforeach 
    @endif
</table>