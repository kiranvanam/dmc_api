<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <title>Service Booked with Reference  #: {{ $booking->code }} </title>
    @include('emails.css.custom')
</head>

<body>
    <div class="invoice-box">
        <div style="text-align:center">
            <h3> Service Booking # {{ $booking->code }} </h3>
        </div>
        <table cellspacing="0" cellpadding="0">
            <thead>
                <tr>
                    <th style="width:20%"></th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <th> Booking Code  </th>
                    <th> {{ $booking->code }} </th>
                </tr>
                <tr>
                    <th> Service </th>
                    <th class="text-capitalize"> {{ $booking->service }} </th>
                </tr>
                <tr>
                    <th> Status </th>
                    <th class="text-capitalize"> {{ $booking->status }} </th>
                </tr>
                <tr>
                    <th> Price </th>
                    <th> {{ $booking->total_price  }} {{ $booking->currency }} </th>
                </tr>
                <tr>
                    <th> Payment Paid </th>
                    <th> {{ $booking->total_paid }} </th>
                </tr>
                <tr>
                    <th> Lead Passenger Details </th>
                    <th class="bg-light">
                        {{ $booking->leadPassengerName() }}  <br>
                        Nationality: {{ $booking->leadPassengerNationality() }} <br>
                        Email: {{ $booking->leadPassengerEmail() }} <br>
                        Phone: {{ $booking->leadPassengerContactNumber() }}
                    </th>
                </tr>
                <tr>
                    <th>  Cancellation policies</th>
                    <td>
                        @if( empty($booking->cancellationPolicies()) )
                            <div style="color:red">
                                100% cancellation charges applicable
                            </div>
                        @else
                            <div style="color:red; font-size:14px"> Cancellation Charge Details </div>

                            <table class="table table-sm" style="width:70%;" cellspacing="0" cellpadding="0">
                                <tr style="background-color:white" class="heading">
                                    <th style="width:50%;" class="text-center"> Cancel After/On </th>
                                    <th> Charges Appicable </th>
                                </tr>

                                @foreach($booking->cancellationPolicies() as $policyDetail)
                                    <tr class="item" style="background-color:white">
                                        <td class="text-center">
                                            {{ array_get($policyDetail, 'date' ) }}
                                        </td>
                                        <td class="text-left">
                                            {{ array_get($policyDetail, 'amount') }}
                                            {{ array_get($policyDetail, 'amount_type') == 'percent' ? '%' : array_get($policyDetail, 'amount_type')  }}
                                        </td>
                                    </tr>
                                @endforeach

                            </table>
                        @endif
                    </td>
                </tr>
            </tbody>
        </table>

        @include('emails.bookings.booking.booking-item-details')

        <strong style="color:red"> Note </strong>:
        <pre>
            This is an auto-generated email. Please don't reply to this email.

            If you need any assistance, please email to : ops@royaleagledubai.com

            Plesae mention the booking reference id while emailing to enable us to retrieve the booking.
        </pre>

    </div>
</body>
</html>