<table class="table table-sm font-size-10">
    <thead class="thead-default">
        <tr>
            <th class="p-0"> Room Type</th>
            <th class="p-0">Qty</th>
            <th class="p-0"> Price </th>
        </tr>
    </thead>
    <tbody>
        @foreach ($bookingItem->grnRooms()  as $room)
        <tr>
            <td class="p-0">
                {{ $bookingItem->grnRoomName($room) }}
            </td>
            <td class="p-0">
                {{ $bookingItem->grnTotalForRoom($room) }}
            </td>
        <td class="p-0" v-show="!index" colspan="{{$bookingItem->grnTotalRooms() }}">
                <div class="align-middle">
                    {{ $bookingItem->totalPrice() }}
                    {{ $booking->currency() }}
                </div>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>