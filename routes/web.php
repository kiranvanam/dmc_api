<?php

Route::group(['prefix'=>""], function () {
  Route::get("/", 'StaticPageController@home');
  Route::get("about", 'StaticPageController@about');
  Route::get("services", 'StaticPageController@services');
  Route::get("e-brochures", 'StaticPageController@brochures');
  Route::get("contact", 'StaticPageController@contact');
  Route::get("careers", 'StaticPageController@careers');
});

Route::post('contact-us', 'ContactUsController@create');

Route::get('admin', function() {
    return view('admin-home', ['data'=> [ 'dmc' => "Go Dubai Tourism" ]]);
})->name('admin_home');

Route::group(['prefix'=>"admin/api",'namespace'=>'Api'], function() {
    Route::get('/misc','MiscApiController@index'); // returns all
    Route::get('/timezones', 'MiscApiController@timeZones');
});

Auth::routes();

Route::get('/admin/api/dmc-locations', 'DmcLocationsController@index');

Route::group(['prefix'=>"auth"], function() {
    Route::get('login', function(){
        return view('admin-home', ['data'=> [ 'dmc' => "Go Dubai Tourism" ] ]);
    });
    Route::get('register', function(){
        return view('admin-home', ['data'=> [ 'dmc' => "Go Dubai Tourism" ] ]);
    });
});

/*Route::get("/send-notification", function(){

    \Auth::loginUsingId(1);
    $user = \Auth::user();

    \Mail::send( new AgentRegistered(TravelAgency::first()));


    return $user;
});*/

