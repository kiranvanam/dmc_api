<?php

Route::group(['namespace'=> 'Api'], function() {

  //Route::get('currencies', 'CurrencyController@index');
  Route::get('country-currencies', 'CurrencyController@all');
  Route::post('country-currencies', 'CurrencyController@save');

  Route::get('country-wise-service-taxes', 'CountryWiseServiceTaxController@index');
  Route::post('country-wise-service-taxes', 'CountryWiseServiceTaxController@save');
  Route::put('country-wise-service-taxes/{id}', 'CountryWiseServiceTaxController@update');
  Route::delete('country-wise-service-taxes/{id}', 'CountryWiseServiceTaxController@delete');
  Route::get('/timezones', 'MiscApiController@timeZones');

});

Route::group(['prefix' => "cancellation-policies"], function(){
  Route::get("", 'CancellationPolicyController@index');
  Route::post("", "CancellationPolicyController@create");
  Route::put("{cancellation_policy_id}", 'CancellationPolicyController@update');
  Route::delete("{cancellation_policy_id}", "CancellationPolicyController@delete");
});

Route::group(['middleware'=>['cors']], function(){
  Route::get("resource/gallery", 'GalleryUploaderController@index');
  Route::post("resource/gallery", 'GalleryUploaderController@save');
  Route::delete("resource/gallery/delete", 'GalleryUploaderController@deleteImage');
  Route::group(['namespace' => "Api"], function(){
    Route::post('delete-images', 'MiscApiController@deleteImages');
  });
});
