<?php

namespace Columbus\Services\Products;

use Columbus\Services\Products\Exceptions\InvalidProductApiClientException;
use GuzzleHttp\Client;

class ProductApiClient
{
  protected  $settings = null;
  protected  $searchCriteria = [];
  protected  $client = null;

  function __construct($settings)
  {
    $this->settings = $settings;
  }

  protected function resloveClass($className)
  {
    $apiClientName =$this->settings["api_end_point_name"];
    $class= "\Columbus\Services\Products\\". $apiClientName . "\\". $className;
    
    if(!class_exists($class)) {
      throw new InvalidProductApiClientException($apiClientName);
    }

    return new $class($this->settings);
  }

  protected function resloveRequest() {
    return $this->resloveClass('Request');
  }

  protected function resloveResponse() {
    return $this->resloveClass('Response');
  }

  function  search($url, $searchCriteria) {
    return $this->process($url, $searchCriteria);
  }

  function process($url, $searchCriteria=null) {

    $request = $this->resloveRequest();
    
    $client = new Client(['base_uri' => $request->baseUri(), 'headers' => $request->headers()]);

    $responseApi = $client->request(
                        $request->method(), 
                        $url,
                        [
                          'json' => $request->prepareBody($searchCriteria)
                        ]
                      );
    
    $response = $this->resloveResponse();
    
    return $response->processBody($responseApi);
  }
}