<?php

namespace Columbus\Services\Products\Exceptions;


class InvalidProductApiClientException extends \Exception
{
  function __construct($apiPointName="NotPassed")
  {
    $this->message = " The API client name not resolved for :" . $apiPointName;
    \Log::emergency($this->message);
  }
}