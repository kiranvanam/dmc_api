<?php

namespace  Columbus\Services\Products\GRNConnect;

use Columbus\Services\Products\Contracts\IRequest;


class Request implements IRequest
{
  protected $settings = [];

  public function __construct($settings)
  {
    $this->settings = $settings;
  }

  public function prepareBody($requestData)
  {
    return $requestData;   
  }

  public function baseUri()
  {
    return $this->settings['base_uri'];
  }

  public function url()
  {
    return $this->settings['url'];
  }

  public function headers()
  {
    return $this->settings['headers'];
  }

  public function method()
  {
    return strtoupper($this->settings['method']);
  }
}