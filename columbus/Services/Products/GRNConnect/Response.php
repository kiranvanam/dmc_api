<?php

namespace  Columbus\Services\Products\GRNConnect;

use Columbus\Services\Products\Contracts\IResponse;


class Response implements IResponse
{
  
  // not used $settings as of now, may be useful if we want to set some default variables
  public function __construct($settings)
  {
  
  }

  public function processBody($response)
  { 
      return json_decode($response->getBody());
  }
}