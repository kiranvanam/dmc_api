<?php

namespace Columbus\Services\Products\Accommodation;

use Columbus\Services\Products\Exceptions\InvalidProductApiClientException;

class Accommodation
{
    protected static function resloveClass($service_source_info)
    {
        $service_source_name = $service_source_info['service_source'];// $this->settings[""];
        if($service_source_name == "grn-connect" ) {
            $apiClientName = "GRNConnect";
        }
        
        abort_if(!$apiClientName,  404, "Accommodation client handler not found");

        $class= "\Columbus\Services\Products\Accommodation\\". $apiClientName . "\\Accommodation";
        
        if(!class_exists($class)) {
            throw new InvalidProductApiClientException($apiClientName);
        }

        return new $class();
    }

    public static function bookingDetails($booking_info)
    {
        $accommodation = self::resloveClass($booking_info);
        return $accommodation->bookingDetails($booking_info);
    }

    public static function cancel($booking_info)
    {
        $accommodation = self::resloveClass($booking_info);
        return $accommodation->cancel($booking_info);
    }

    public static function voucher($booking_info)
    {
        $accommodation = self::resloveClass($booking_info);
        return $accommodation->voucher($booking_info);
    }

    public static function search( $booking_info = [ 'service_source' => "grn-connect"] )
    {
        $accommodation = self::resloveClass($booking_info);
        return $accommodation->search();
    }

}