<?php

namespace  Columbus\Services\Products\Contracts;

interface IResponse
{
  public function processBody($body);
}