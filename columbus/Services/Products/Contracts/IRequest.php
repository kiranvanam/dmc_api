<?php

namespace  Columbus\Services\Products\Contracts;


interface IRequest
{
  // base domain url
  public function baseUri();
  // to return url part which has to be appended
  public function url();

  public function headers();

  public function method();
}