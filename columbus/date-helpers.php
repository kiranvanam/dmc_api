<?php

use Carbon\Carbon;

function convertToCarbon($date) 
{
    return ($date instanceof Carbon) ?
                $date: Carbon::parse($date); 
}

if(!function_exists('now')) {
    function now()
    {
        return Carbon::now();
    }
}

/**
 *  +ve - if first date 
 *   0 - if dates are same
 *  -ve - if second is less than first  
 */
function diffInDays($first, $second) 
{
    $first = convertToCarbon($first);
    $second = convertToCarbon($second);

    return $first->diffInDays($second, false);
}

function doesFirstDateComeAfter($first, $second) 
{
    return diffInDays($first, $second)  < 0;
}