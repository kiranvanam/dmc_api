<?php

namespace Columbus\Utilities;

use Carbon\Carbon;


class DateUtil
{

    public static function createDateWith($date, $format="Y-m-d")
    {
        return Carbon::createFromFormat($format, $date);
    }

    public static function createCarbonIfNotCarbon($date, $format="m-d-Y")
    {
        if( !($date instanceof Carbon) ) {
            return Carbon::createFromFormat($format, $date);
        }
        return $date;
    }

    public static function allDatesBetween($start_date, $end_date, $is_last_included=true)
    {
        if( !$end_date){
            $end_date = $start_date;
        }

        $format="Y-m-d";
        $start_date = static::createCarbonIfNotCarbon($start_date, $format);
        $end_date = static::createCarbonIfNotCarbon($end_date, $format);

        if($is_last_included) {
            $end_date->addDay();
        }

        $dayWiseModelObjects = [];
        $dates = [];

        do {
            $dates[] = $start_date->format($format);
            $start_date->addDay();
        } while( $start_date < $end_date);
        return $dates;
    }

    public static function datesForRange($start_date, $end_date=null, $callBack=null)
    {
        if( !$end_date){
            $end_date = $start_date;
        }

        $start_date = static::createCarbonIfNotCarbon($start_date);


        $end_date = static::createCarbonIfNotCarbon($end_date);

        $dayWiseModelObjects = [];
        $data = [];

        do {
            $data = [];

            if($callBack)
               $data = $callBack($start_date);

            if(is_null($data) != true ) {

                $dayWiseModelObjects[] = $data ;
            }

            $start_date->addDay();
        } while( $start_date <= $end_date);

        return $dayWiseModelObjects;
    }

    /* don't consider HH, mm and ss as of now */
    public static function dateForDateAndDateFormat(&$date, &$dateFormat = "Y-m-d")
    {
        $completeDates = explode("T", $date);
        if(count($completeDates) > 1) {
            //$dateFormat = $dateFormat. ' HH:mm:ss'; // if format is enabled theb
            $date = $completeDates[0];
        }

        return Carbon::createFromFormat($dateFormat, $date);
    }

    public static function dateStringForDate($date, $dateFormat="d-m-Y")
    {
        return self::dateForDateAndDateFormat($date)->format($dateFormat);
    }

}