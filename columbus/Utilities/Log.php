<?php

namespace Columbus\Utilities;

trait Log
{

    public function startLog()
    {
        logger("************** $this->log_prefix - START ******************");
    }

    public function endLog()
    {
        logger("************** $this->log_prefix - END ******************");
    }

    public function log($message, $body=[])
    {
        logger($message, $body);
    }
}