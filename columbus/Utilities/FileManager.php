<?php

namespace Columbus\Utilities;

//use App\Models\Photo;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class FileManager
{
    
    function onlyFilePathsIn($dir)
    {
        $files = collect($this->enumerate($dir))->first();
        
        return collect(collect($files)->get('items'))->pluck('path');
    }

    function enumerate($dir)
    {
        $files = static::dirMeta($dir);
        $files['items'] = static::scan($dir);
        return  [$files];
    }

    function createDirectory($name,$path){
        Storage::makeDirectory($path . '/'.$name);
        return static::enumerate($path);
    }


    protected function scan($dir)
    {
        // dd(Storage::files($dir));
        $files = static::addFileMetaInfo(Storage::files($dir));

        $dirs = Storage::directories($dir);

        foreach($dirs as $name){
            $details = static::addDirectoryMetaInfo($name);
            $details['items'] = static::scan($name);
            $files[] = $details;
        }
        return $files[$dir] = $files;
    }

    protected function addFileMetaInfo($files = [])
    {
        $file_details = [];
        foreach($files as $file){
            $file_details[] = $this->fileMeta($file);
        }
        return $file_details;
    }

    protected function addDirectoryMetaInfo($name)
    {
        return static::dirMeta($name);
    }

    protected function fileMeta($name)
    {
        return [
            "name" => basename($name),
            "type" => 'file',
            "path" => $name,
            "size" => Storage::size($name)
        ];
    }

    protected function dirMeta($name)
    {
        return [
            "name" => basename($name),
            "type" => 'folder',
            "path" => $name,
            "items" => []
        ];
    }

    public function savePhotos($path,$photos)
    {

        $filenames = [];
        $count = 0;
        foreach($photos as $file) {
            $rules = array('file' => 'required'); //'required|mimes:png,gif,jpeg,txt,pdf,doc'
            $validator = Validator::make(array('file'=> $file), $rules);
            if($validator->passes()){
                $filename = $file->getClientOriginalName();
                $file->move($path, $filename);
                $filenames[] = $path ."/".$filename;
                $count++;
            }
        }
        return $filenames;
    }

    public function removeFiles($images)
    {
        $result = "File deleted successfully";
        $images = collect($images);

         $images->each( function($path) {
            try {
                Storage::delete($path);
            }catch (\Exception $e) {
                $result = "problem while deleting the photo";
            }
        }); 
        return $result;
    }
}