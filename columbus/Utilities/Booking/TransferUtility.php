<?php

namespace Columbus\Utilities\Booking;

trait TransferUtility
{

    static function vehicleUnitPrice($transferDetail, $toCurrency = null) {


        if(empty($toCurrency))
            $toCurrency = static::$bookingCurrency;
        $unitPricing= array_get( $transferDetail, 'unit_pricing', null);
        if( $unitPricing) {


            $amount = array_get($unitPricing, 'vehicle_price', 1); /* * array_get($unitPricing, 'unitPricing.no_of_booked_vehicles',1) */;

            return static::calculateTotalWithMarkup( $amount,
                                                    array_get($unitPricing, 'currency'),
                                                    array_get($transferDetail, 'markup_details'),
                                                    $toCurrency
                                                );
        }
        return 0;
    }

    public static function unitPriceForPaxType($transferDetail, $prop , $toCurrency = null)
    {
        if(empty($toCurrency))
            $toCurrency = static::$bookingCurrency;

        $unitPricing= array_get( $transferDetail, 'unit_pricing', null);
        if( $unitPricing) {
            $amount = array_get($unitPricing, $prop, 1);

            return static::calculateTotalWithMarkup( $amount,
                                                    array_get($unitPricing, 'currency'),
                                                    array_get($transferDetail, 'markup_details'),
                                                    $toCurrency
                                                );

        }
    }
}