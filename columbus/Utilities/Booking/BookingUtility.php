<?php

namespace Columbus\Utilities\Booking;

class BookingUtility
{
    use TransferUtility;

    protected static $bookingDetails = null;
    protected static $currencyExchangeRates = null;
    protected static $invoiceSettings = null;
    protected static $bookingCurrency = null;

    public static function initialize($bookingDetails)
    {
        static::$bookingDetails = $bookingDetails;
        static::$currencyExchangeRates = array_get($bookingDetails, 'exchange_rates');
        static::$bookingCurrency = array_get($bookingDetails, 'currency'); 
        static::$invoiceSettings = array_get($bookingDetails, 'invoice_details');   
    }
    public static function dmcDetails() 
    {
        return  array_get(static::$invoiceSettings, 'dmc_details', []);
    }
   
    public static function agencyDetails()
    {
        return   array_get(static::$invoiceSettings,'agency_details');
    }

    public static function isMarkup()
    {        
        return  array_get( static::grossTotal(), 'markup', null) == "yes"; 
    }

    public static function isMarkupNotAddedToProductCost()
    {
        return !static::isMarkupAddedToProductCost();
    }

    public static function isMarkupAddedToProductCost() 
    {
        return static::isMarkup() && array_get(static::grossTotal(), 'is_markup_added_to_product_cost', false);
    }

    public static function taxDetails()
    {
        return array_get(static::$invoiceSettings, 'tax_details', []);
        
    }

    public static function grossTotal()
    {
        return array_get(static::$invoiceSettings, 'gross_total');
    }

    public static function getCurrencyExchangeRatesForCurrency($currency) 
    {
        return array_get(static::$currencyExchangeRates, $currency);
    }

     
    
    public static function rateOfExchange($exchangeRate, $no_of_decimals=3) 
    {
        
        $base_rate = array_get($exchangeRate, 'exchange_rate', 0 );
        $final_rate = static::roundUp( $base_rate + ( $base_rate * array_get( $exchangeRate, 'exchange_rate_buffer') / 100));
        
        return  $final_rate;
    }

    public static function calculateTotalWithMarkup($amount, $currency, $markup, $toCurrency) {
        if(empty($markup))
            return 0;
    
        //If to currency is not given, then pass take toCurrency same as amount currency
        if(!$toCurrency)
            $toCurrency = $currency;
            
        $convertedProductPrice = static::convertAmount( $amount, $currency, $toCurrency);
        $markupAmount = 0;

        if( static::isMarkupAddedToProductCost() )
            $markupAmount = static::calCulateMarkupForAmount( $convertedProductPrice, $toCurrency, $markup);
        
        return static::roundUp($convertedProductPrice +  $markupAmount);
    }

    public static function calCulateMarkupForAmount($amount, $currency, $markup) {
        
        if (empty($markup) )
            return 0;
        
        $convertedMarkupAmount = 0;
        $markupAmountType = array_get($markup, 'amount_type', '');
        $markupAmount = array_get($markup, 'amount', 0);
        if ( $markupAmountType == 'percent' || empty($markupAmountType)) {            
            $convertedMarkupAmount =  static::roundUp( $amount * $markupAmount/ 100, 2);
        } else { // convert markup amount to desired currency to add( amount_type = USD/INR/AED/EUR stores currency //)
            $convertedMarkupAmount = static::convertAmount( $markupAmount, $markupAmountType, $currency);
        }

        return $convertedMarkupAmount;
    }

    public static function calculateValueSecondFromFirst( $amountDetails, $targetDetails) 
    {
        return static::calCulateMarkupForAmount(  $amountDetails, $targetDetails);
    }

    /** this converts amount to agneyc currency if toCurrency not provided */
    public static function convertAmount($amount, $fromCurrency, $toCurrency = null)
    {

        if( empty($toCurrency) )
            $toCurrency = static::$bookingCurrency;

        if ($fromCurrency == $toCurrency || !$toCurrency)
            return $amount;

        $convertedAmount = 0;

        $exchangeRates = static::getCurrencyExchangeRatesForCurrency($fromCurrency);

        if (empty($exchangeRates))
            return $convertedAmount;

        $exchangeRate = array_get($exchangeRates, $toCurrency);
                
        return  static::roundUp( $amount * static::rateOfExchange($exchangeRate));

    }

    static function roundUp($value, $places = 2)
    {
        $mult = pow(10, abs($places)); 
        return $places < 0 ? ceil($value / $mult) * $mult : ceil($value * $mult) / $mult;
    }


    public static function netTotal($booking)
    {
        $amount = $booking->booking_price;

        if( static::isMarkupAddedToProductCost() )
            $amount = static::roundUp( $amount + $booking->markup_price);
        
        return $amount;
    }

    public static function markupDetails($booking) {
        // take markups of all services. then add 
        $markup = null;
        $totalMarkupValue = 0;
        if($booking->visa) {

        }

        $transfers = array_get($booking, 'transfers');
        if( empty($transfers) == false ) {
            foreach( $transfers as  $transferDetail) {
                $markuDetail =  array_get($transferDetail, 'markup_details');
                if( empty( $markup) && $markuDetail)
                    $markup = $markuDetail;
                
                /* $totalMarkupValue =  $totalMarkupValue +  
                                    static::calCulateMarkupForAmount( $transferDetail->booking_price, 
                                                                     $transferDetail->currency,
                                                                     $markupDetail ); */
            }
        }
        if( empty($markup))
            return [];


        $markup['value'] = $booking->markup_price;
        return [ $markup ];
    }

    public static function cancellationPolicies($booking = null) 
    {
        if( empty($booking) ) {
            $booking = static::$bookingDetails;
        }

        $cancellationPolicyUtility = new CancellationPolicy;

        return $cancellationPolicyUtility->mergeCancellationPoliciesForBooking($booking);
    }

}