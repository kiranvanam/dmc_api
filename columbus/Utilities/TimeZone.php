<?php
namespace Columbus\Utilities;

class TimeZone
{
    

    /**
     * Detect the timezone id(s) from an offset and dst
     *
     * @param   int     $offset
     * @param   int     $dst
     * @param   bool    $multiple
     * @param   string  $default
     * @return  string|array
     */
    public static function detect_timezone_id($offset, $dst, $multiple = FALSE, $default = 'UTC')
    {
        $detected_timezone_ids = array();

        // Get the timezone list
        $timezones = self::get_timezone_list();

        // Try to find a timezone for which both the offset and dst match
        foreach ($timezones as $timezone_id)
        {
            $timezone_data = self::get_timezone_data($timezone_id);
            if ($timezone_data['offset'] == $offset && $dst == $timezone_data['dst'])
            {
                array_push($detected_timezone_ids, $timezone_id);
                if ( ! $multiple)
                    break;
            }
        }

        if (empty($detected_timezone_ids))
        {
            $detected_timezone_ids = array($default);
        }

        return $multiple ? $detected_timezone_ids : $detected_timezone_ids[0];
    }

    /**
     * Get the current offset and dst for the given timezone id
     *
     * @param   string  $timezone_id
     * @return  int
     */
    public static function get_timezone_data($timezone_id)
    {
        $date = new \DateTime("now");
        $date->setTimezone(timezone_open($timezone_id));

        return [
            'offset' => $date->getOffset() / 3600,
            'dst' => intval(date_format($date, "I"))
        ];
    }


    public static function  get_timezone_list()
    {
        return [
            '(UTC-11:00) Midway Island' => 'Pacific/Midway',
            '(UTC-11:00) Samoa' => 'Pacific/Samoa',
            '(UTC-10:00) Hawaii' => 'Pacific/Honolulu',
            '(UTC-09:00) Alaska' => 'US/Alaska',
            '(UTC-08:00) Pacific Time (US &amp; Canada)' => 'America/Los_Angeles',
            '(UTC-08:00) Tijuana' => 'America/Tijuana',
            '(UTC-07:00) Arizona' => 'US/Arizona',
            '(UTC-07:00) Chihuahua' => 'America/Chihuahua',
            '(UTC-07:00) La Paz' => 'America/Chihuahua',
            '(UTC-07:00) Mazatlan' => 'America/Mazatlan',
            '(UTC-07:00) Mountain Time (US &amp; Canada)' => 'US/Mountain',
            '(UTC-06:00) Central America' => 'America/Managua',
            '(UTC-06:00) Central Time (US &amp; Canada)' => 'US/Central',
            '(UTC-06:00) Guadalajara' => 'America/Mexico_City',
            '(UTC-06:00) Mexico City' => 'America/Mexico_City',
            '(UTC-06:00) Monterrey' => 'America/Monterrey',
            '(UTC-06:00) Saskatchewan' => 'Canada/Saskatchewan',
            '(UTC-05:00) Bogota' => 'America/Bogota',
            '(UTC-05:00) Eastern Time (US &amp; Canada)' => 'US/Eastern',
            '(UTC-05:00) Indiana (East)' => 'US/East-Indiana',
            '(UTC-05:00) Lima' => 'America/Lima',
            '(UTC-05:00) Quito' => 'America/Bogota',
            '(UTC-04:00) Atlantic Time (Canada)' => 'Canada/Atlantic',
            '(UTC-04:30) Caracas' => 'America/Caracas',
            '(UTC-04:00) La Paz' => 'America/La_Paz',
            '(UTC-04:00) Santiago' => 'America/Santiago',
            '(UTC-03:30) Newfoundland' => 'Canada/Newfoundland',
            '(UTC-03:00) Brasilia' => 'America/Sao_Paulo',
            '(UTC-03:00) Buenos Aires' => 'America/Argentina/Buenos_Aires',
            '(UTC-03:00) Georgetown' => 'America/Argentina/Buenos_Aires',
            '(UTC-03:00) Greenland' => 'America/Godthab',
            '(UTC-02:00) Mid-Atlantic' => 'America/Noronha',
            '(UTC-01:00) Azores' => 'Atlantic/Azores',
            '(UTC-01:00) Cape Verde Is.' => 'Atlantic/Cape_Verde',
            '(UTC+00:00) Casablanca' => 'Africa/Casablanca',
            '(UTC+00:00) Edinburgh' => 'Europe/London',
            '(UTC+00:00) Greenwich Mean Time : Dublin' => 'Etc/Greenwich',
            '(UTC+00:00) Lisbon' => 'Europe/Lisbon',
            '(UTC+00:00) London' => 'Europe/London',
            '(UTC+00:00) Monrovia' => 'Africa/Monrovia',
            '(UTC+00:00) UTC' => 'UTC',
            '(UTC+01:00) Amsterdam' => 'Europe/Amsterdam',
            '(UTC+01:00) Belgrade' => 'Europe/Belgrade',
            '(UTC+01:00) Berlin' => 'Europe/Berlin',
            '(UTC+01:00) Bern' => 'Europe/Berlin',
            '(UTC+01:00) Bratislava' => 'Europe/Bratislava',
            '(UTC+01:00) Brussels' => 'Europe/Brussels',
            '(UTC+01:00) Budapest' => 'Europe/Budapest',
            '(UTC+01:00) Copenhagen' => 'Europe/Copenhagen',
            '(UTC+01:00) Ljubljana' => 'Europe/Ljubljana',
            '(UTC+01:00) Madrid' => 'Europe/Madrid',
            '(UTC+01:00) Paris' => 'Europe/Paris',
            '(UTC+01:00) Prague' => 'Europe/Prague',
            '(UTC+01:00) Rome' => 'Europe/Rome',
            '(UTC+01:00) Sarajevo' => 'Europe/Sarajevo',
            '(UTC+01:00) Skopje' => 'Europe/Skopje',
            '(UTC+01:00) Stockholm' => 'Europe/Stockholm',
            '(UTC+01:00) Vienna' => 'Europe/Vienna',
            '(UTC+01:00) Warsaw' => 'Europe/Warsaw',
            '(UTC+01:00) West Central Africa' => 'Africa/Lagos',
            '(UTC+01:00) Zagreb' => 'Europe/Zagreb',
            '(UTC+02:00) Athens' => 'Europe/Athens',
            '(UTC+02:00) Bucharest' => 'Europe/Bucharest',
            '(UTC+02:00) Cairo' => 'Africa/Cairo',
            '(UTC+02:00) Harare' => 'Africa/Harare',
            '(UTC+02:00) Helsinki' => 'Europe/Helsinki',
            '(UTC+02:00) Istanbul' => 'Europe/Istanbul',
            '(UTC+02:00) Jerusalem' => 'Asia/Jerusalem',
            '(UTC+02:00) Kyiv' => 'Europe/Helsinki',
            '(UTC+02:00) Pretoria' => 'Africa/Johannesburg',
            '(UTC+02:00) Riga' => 'Europe/Riga',
            '(UTC+02:00) Sofia' => 'Europe/Sofia',
            '(UTC+02:00) Tallinn' => 'Europe/Tallinn',
            '(UTC+02:00) Vilnius' => 'Europe/Vilnius',
            '(UTC+03:00) Baghdad' => 'Asia/Baghdad',
            '(UTC+03:00) Kuwait' => 'Asia/Kuwait',
            '(UTC+03:00) Minsk' => 'Europe/Minsk',
            '(UTC+03:00) Nairobi' => 'Africa/Nairobi',
            '(UTC+03:00) Riyadh' => 'Asia/Riyadh',
            '(UTC+03:00) Volgograd' => 'Europe/Volgograd',
            '(UTC+03:30) Tehran' => 'Asia/Tehran',
            '(UTC+04:00) Abu Dhabi' => 'Asia/Muscat',
            '(UTC+04:00) Baku' => 'Asia/Baku',
            '(UTC+04:00) Moscow' => 'Europe/Moscow',
            '(UTC+04:00) Muscat' => 'Asia/Muscat',
            '(UTC+04:00) St. Petersburg' => 'Europe/Moscow',
            '(UTC+04:00) Tbilisi' => 'Asia/Tbilisi',
            '(UTC+04:00) Yerevan' => 'Asia/Yerevan',
            '(UTC+04:30) Kabul' => 'Asia/Kabul',
            '(UTC+05:00) Islamabad' => 'Asia/Karachi',
            '(UTC+05:00) Karachi' => 'Asia/Karachi',
            '(UTC+05:00) Tashkent' => 'Asia/Tashkent',
            '(UTC+05:30) Chennai' => 'Asia/Calcutta',
            '(UTC+05:30) Kolkata' => 'Asia/Kolkata',
            '(UTC+05:30) Mumbai' => 'Asia/Calcutta',
            '(UTC+05:30) New Delhi' => 'Asia/Calcutta',
            '(UTC+05:30) Sri Jayawardenepura' => 'Asia/Calcutta',
            '(UTC+05:45) Kathmandu' => 'Asia/Katmandu',
            '(UTC+06:00) Almaty' => 'Asia/Almaty',
            '(UTC+06:00) Astana' => 'Asia/Dhaka',
            '(UTC+06:00) Dhaka' => 'Asia/Dhaka',
            '(UTC+06:00) Ekaterinburg' => 'Asia/Yekaterinburg',
            '(UTC+06:30) Rangoon' => 'Asia/Rangoon',
            '(UTC+07:00) Bangkok' => 'Asia/Bangkok',
            '(UTC+07:00) Hanoi' => 'Asia/Bangkok',
            '(UTC+07:00) Jakarta' => 'Asia/Jakarta',
            '(UTC+07:00) Novosibirsk' => 'Asia/Novosibirsk',
            '(UTC+08:00) Beijing' => 'Asia/Hong_Kong',
            '(UTC+08:00) Chongqing' => 'Asia/Chongqing',
            '(UTC+08:00) Hong Kong' => 'Asia/Hong_Kong',
            '(UTC+08:00) Krasnoyarsk' => 'Asia/Krasnoyarsk',
            '(UTC+08:00) Kuala Lumpur' => 'Asia/Kuala_Lumpur',
            '(UTC+08:00) Perth' => 'Australia/Perth',
            '(UTC+08:00) Singapore' => 'Asia/Singapore',
            '(UTC+08:00) Taipei' => 'Asia/Taipei',
            '(UTC+08:00) Ulaan Bataar' => 'Asia/Ulan_Bator',
            '(UTC+08:00) Urumqi' => 'Asia/Urumqi',
            '(UTC+09:00) Irkutsk' => 'Asia/Irkutsk',
            '(UTC+09:00) Osaka' => 'Asia/Tokyo',
            '(UTC+09:00) Sapporo' => 'Asia/Tokyo',
            '(UTC+09:00) Seoul' => 'Asia/Seoul',
            '(UTC+09:00) Tokyo' => 'Asia/Tokyo',
            '(UTC+09:30) Adelaide' => 'Australia/Adelaide',
            '(UTC+09:30) Darwin' => 'Australia/Darwin',
            '(UTC+10:00) Brisbane' => 'Australia/Brisbane',
            '(UTC+10:00) Canberra' => 'Australia/Canberra',
            '(UTC+10:00) Guam' => 'Pacific/Guam',
            '(UTC+10:00) Hobart' => 'Australia/Hobart',
            '(UTC+10:00) Melbourne' => 'Australia/Melbourne',
            '(UTC+10:00) Port Moresby' => 'Pacific/Port_Moresby',
            '(UTC+10:00) Sydney' => 'Australia/Sydney',
            '(UTC+10:00) Yakutsk' => 'Asia/Yakutsk',
            '(UTC+11:00) Vladivostok' => 'Asia/Vladivostok',
            '(UTC+12:00) Auckland' => 'Pacific/Auckland',
            '(UTC+12:00) Fiji' => 'Pacific/Fiji',
            '(UTC+12:00) International Date Line West' => 'Pacific/Kwajalein',
            '(UTC+12:00) Kamchatka' => 'Asia/Kamchatka',
            '(UTC+12:00) Magadan' => 'Asia/Magadan',
            '(UTC+12:00) Marshall Is.' => 'Pacific/Fiji',
            '(UTC+12:00) New Caledonia' => 'Asia/Magadan',
            '(UTC+12:00) Solomon Is.' => 'Asia/Magadan',
            '(UTC+12:00) Wellington' => 'Pacific/Auckland',
            '(UTC+13:00) Nuku\'alofa' => 'Pacific/Tongatapu'
        ];
    }

    public static function toOffsetWithTimeZone($timezone, $timeZoneType="UTC"){
        $userTimeZone = new \DateTimeZone($timezone);
        $offset = $userTimeZone->getOffset(new \DateTime("now",new \DateTimeZone($timeZoneType))); // Offset in seconds
        $seconds = abs($offset);
        $sign = $offset > 0 ? '+' : '-';
        $hours = floor($seconds / 3600);
        $mins = floor($seconds / 60 % 60);
        $secs = floor($seconds % 60);
        return sprintf("$sign%02d:%02d", $hours, $mins);
    }

    public static function prepareTimesZones()
    {
        return [
            'Pacific/Midway'       => static::toOffsetWithTimeZone('Pacific/Midway'       ). " Midway Island",
            'US/Samoa'             => static::toOffsetWithTimeZone('US/Samoa'             ). " Samoa",
            'US/Hawaii'            => static::toOffsetWithTimeZone('US/Hawaii'            ). " Hawaii",
            'US/Alaska'            => static::toOffsetWithTimeZone('US/Alaska'            ). " Alaska",
            'US/Pacific'           => static::toOffsetWithTimeZone('US/Pacific'           ). " Pacific Time (US &amp; Canada)",
            'America/Tijuana'      => static::toOffsetWithTimeZone('America/Tijuana'      ). " Tijuana",
            'US/Arizona'           => static::toOffsetWithTimeZone('US/Arizona'           ). " Arizona",
            'US/Mountain'          => static::toOffsetWithTimeZone('US/Mountain'          ). " Mountain Time (US &amp; Canada)",
            'America/Chihuahua'    => static::toOffsetWithTimeZone('America/Chihuahua'    ). " Chihuahua",
            'America/Mazatlan'     => static::toOffsetWithTimeZone('America/Mazatlan'     ). " Mazatlan",
            'America/Mexico_City'  => static::toOffsetWithTimeZone('America/Mexico_City'  ). " Mexico City",
            'America/Monterrey'    => static::toOffsetWithTimeZone('America/Monterrey'    ). " Monterrey",
            'Canada/Saskatchewan'  => static::toOffsetWithTimeZone('Canada/Saskatchewan'  ). " Saskatchewan",
            'US/Central'           => static::toOffsetWithTimeZone('US/Central'           ). " Central Time (US &amp; Canada)",
            'US/Eastern'           => static::toOffsetWithTimeZone('US/Eastern'           ). " Eastern Time (US &amp; Canada)",
            'US/East-Indiana'      => static::toOffsetWithTimeZone('US/East-Indiana'      ). " Indiana (East)",
            'America/Bogota'       => static::toOffsetWithTimeZone('America/Bogota'       ). " Bogota",
            'America/Lima'         => static::toOffsetWithTimeZone('America/Lima'         ). " Lima",
            'America/Caracas'      => static::toOffsetWithTimeZone('America/Caracas'      ). " Caracas",
            'Canada/Atlantic'      => static::toOffsetWithTimeZone('Canada/Atlantic'      ). " Atlantic Time (Canada)",
            'America/La_Paz'       => static::toOffsetWithTimeZone('America/La_Paz'       ). " La Paz",
            'America/Santiago'     => static::toOffsetWithTimeZone('America/Santiago'     ). " Santiago",
            'Canada/Newfoundland'  => static::toOffsetWithTimeZone('Canada/Newfoundland'  ). " Newfoundland",
            'America/Buenos_Aires' => static::toOffsetWithTimeZone('America/Buenos_Aires' ). " Buenos Aires",
            'Atlantic/Stanley'     => static::toOffsetWithTimeZone('Atlantic/Stanley'     ). " Stanley",
            'Atlantic/Azores'      => static::toOffsetWithTimeZone('Atlantic/Azores'      ). " Azores",
            'Atlantic/Cape_Verde'  => static::toOffsetWithTimeZone('Atlantic/Cape_Verde'  ). " Cape Verde Is.",
            'Africa/Casablanca'    => static::toOffsetWithTimeZone('Africa/Casablanca'    ). " Casablanca",
            'Europe/Dublin'        => static::toOffsetWithTimeZone('Europe/Dublin'        ). " Dublin",
            'Europe/Lisbon'        => static::toOffsetWithTimeZone('Europe/Lisbon'        ). " Lisbon",
            'Europe/London'        => static::toOffsetWithTimeZone('Europe/London'        ). " London",
            'Africa/Monrovia'      => static::toOffsetWithTimeZone('Africa/Monrovia'      ). " Monrovia",
            'Europe/Amsterdam'     => static::toOffsetWithTimeZone('Europe/Amsterdam'     ). " Amsterdam",
            'Europe/Belgrade'      => static::toOffsetWithTimeZone('Europe/Belgrade'      ). " Belgrade",
            'Europe/Berlin'        => static::toOffsetWithTimeZone('Europe/Berlin'        ). " Berlin",
            'Europe/Bratislava'    => static::toOffsetWithTimeZone('Europe/Bratislava'    ). " Bratislava",
            'Europe/Brussels'      => static::toOffsetWithTimeZone('Europe/Brussels'      ). " Brussels",
            'Europe/Budapest'      => static::toOffsetWithTimeZone('Europe/Budapest'      ). " Budapest",
            'Europe/Copenhagen'    => static::toOffsetWithTimeZone('Europe/Copenhagen'    ). " Copenhagen",
            'Europe/Ljubljana'     => static::toOffsetWithTimeZone('Europe/Ljubljana'     ). " Ljubljana",
            'Europe/Madrid'        => static::toOffsetWithTimeZone('Europe/Madrid'        ). " Madrid",
            'Europe/Paris'         => static::toOffsetWithTimeZone('Europe/Paris'         ). " Paris",
            'Europe/Prague'        => static::toOffsetWithTimeZone('Europe/Prague'        ). " Prague",
            'Europe/Rome'          => static::toOffsetWithTimeZone('Europe/Rome'          ). " Rome",
            'Europe/Sarajevo'      => static::toOffsetWithTimeZone('Europe/Sarajevo'      ). " Sarajevo",
            'Europe/Skopje'        => static::toOffsetWithTimeZone('Europe/Skopje'        ). " Skopje",
            'Europe/Stockholm'     => static::toOffsetWithTimeZone('Europe/Stockholm'     ). " Stockholm",
            'Europe/Vienna'        => static::toOffsetWithTimeZone('Europe/Vienna'        ). " Vienna",
            'Europe/Warsaw'        => static::toOffsetWithTimeZone('Europe/Warsaw'        ). " Warsaw",
            'Europe/Zagreb'        => static::toOffsetWithTimeZone('Europe/Zagreb'        ). " Zagreb",
            'Europe/Athens'        => static::toOffsetWithTimeZone('Europe/Athens'        ). " Athens",
            'Europe/Bucharest'     => static::toOffsetWithTimeZone('Europe/Bucharest'     ). " Bucharest",
            'Africa/Cairo'         => static::toOffsetWithTimeZone('Africa/Cairo'         ). " Cairo",
            'Africa/Harare'        => static::toOffsetWithTimeZone('Africa/Harare'        ). " Harare",
            'Europe/Helsinki'      => static::toOffsetWithTimeZone('Europe/Helsinki'      ). " Helsinki",
            'Europe/Istanbul'      => static::toOffsetWithTimeZone('Europe/Istanbul'      ). " Istanbul",
            'Asia/Jerusalem'       => static::toOffsetWithTimeZone('Asia/Jerusalem'       ). " Jerusalem",
            'Europe/Kiev'          => static::toOffsetWithTimeZone('Europe/Kiev'          ). " Kyiv",
            'Europe/Minsk'         => static::toOffsetWithTimeZone('Europe/Minsk'         ). " Minsk",
            'Europe/Riga'          => static::toOffsetWithTimeZone('Europe/Riga'          ). " Riga",
            'Europe/Sofia'         => static::toOffsetWithTimeZone('Europe/Sofia'         ). " Sofia",
            'Europe/Tallinn'       => static::toOffsetWithTimeZone('Europe/Tallinn'       ). " Tallinn",
            'Europe/Vilnius'       => static::toOffsetWithTimeZone('Europe/Vilnius'       ). " Vilnius",
            'Asia/Baghdad'         => static::toOffsetWithTimeZone('Asia/Baghdad'         ). " Baghdad",
            'Asia/Kuwait'          => static::toOffsetWithTimeZone('Asia/Kuwait'          ). " Kuwait",
            'Africa/Nairobi'       => static::toOffsetWithTimeZone('Africa/Nairobi'       ). " Nairobi",
            'Asia/Riyadh'          => static::toOffsetWithTimeZone('Asia/Riyadh'          ). " Riyadh",
            'Europe/Moscow'        => static::toOffsetWithTimeZone('Europe/Moscow'        ). " Moscow",
            'Asia/Tehran'          => static::toOffsetWithTimeZone('Asia/Tehran'          ). " Tehran",
            'Asia/Baku'            => static::toOffsetWithTimeZone('Asia/Baku'            ). " Baku",
            'Europe/Volgograd'     => static::toOffsetWithTimeZone('Europe/Volgograd'     ). " Volgograd",
            'Asia/Muscat'          => static::toOffsetWithTimeZone('Asia/Muscat'          ). " Muscat",
            'Asia/Tbilisi'         => static::toOffsetWithTimeZone('Asia/Tbilisi'         ). " Tbilisi",
            'Asia/Yerevan'         => static::toOffsetWithTimeZone('Asia/Yerevan'         ). " Yerevan",
            'Asia/Kabul'           => static::toOffsetWithTimeZone('Asia/Kabul'           ). " Kabul",
            'Asia/Karachi'         => static::toOffsetWithTimeZone('Asia/Karachi'         ). " Karachi",
            'Asia/Tashkent'        => static::toOffsetWithTimeZone('Asia/Tashkent'        ). " Tashkent",
            'Asia/Kolkata'         => static::toOffsetWithTimeZone('Asia/Kolkata'         ). " Kolkata",
            'Asia/Kathmandu'       => static::toOffsetWithTimeZone('Asia/Kathmandu'       ). " Kathmandu",
            'Asia/Yekaterinburg'   => static::toOffsetWithTimeZone('Asia/Yekaterinburg'   ). " Ekaterinburg",
            'Asia/Almaty'          => static::toOffsetWithTimeZone('Asia/Almaty'          ). " Almaty",
            'Asia/Dhaka'           => static::toOffsetWithTimeZone('Asia/Dhaka'           ). " Dhaka",
            'Asia/Novosibirsk'     => static::toOffsetWithTimeZone('Asia/Novosibirsk'     ). " Novosibirsk",
            'Asia/Bangkok'         => static::toOffsetWithTimeZone('Asia/Bangkok'         ). " Bangkok",
            'Asia/Jakarta'         => static::toOffsetWithTimeZone('Asia/Jakarta'         ). " Jakarta",
            'Asia/Krasnoyarsk'     => static::toOffsetWithTimeZone('Asia/Krasnoyarsk'     ). " Krasnoyarsk",
            'Asia/Chongqing'       => static::toOffsetWithTimeZone('Asia/Chongqing'       ). " Chongqing",
            'Asia/Hong_Kong'       => static::toOffsetWithTimeZone('Asia/Hong_Kong'       ). " Hong Kong",
            'Asia/Kuala_Lumpur'    => static::toOffsetWithTimeZone('Asia/Kuala_Lumpur'    ). " Kuala Lumpur",
            'Australia/Perth'      => static::toOffsetWithTimeZone('Australia/Perth'      ). " Perth",
            'Asia/Singapore'       => static::toOffsetWithTimeZone('Asia/Singapore'       ). " Singapore",
            'Asia/Taipei'          => static::toOffsetWithTimeZone('Asia/Taipei'          ). " Taipei",
            'Asia/Ulaanbaatar'     => static::toOffsetWithTimeZone('Asia/Ulaanbaatar'     ). " Ulaan Bataar",
            'Asia/Urumqi'          => static::toOffsetWithTimeZone('Asia/Urumqi'          ). " Urumqi",
            'Asia/Irkutsk'         => static::toOffsetWithTimeZone('Asia/Irkutsk'         ). " Irkutsk",
            'Asia/Seoul'           => static::toOffsetWithTimeZone('Asia/Seoul'           ). " Seoul",
            'Asia/Tokyo'           => static::toOffsetWithTimeZone('Asia/Tokyo'           ). " Tokyo",
            'Australia/Adelaide'   => static::toOffsetWithTimeZone('Australia/Adelaide'   ). " Adelaide",
            'Australia/Darwin'     => static::toOffsetWithTimeZone('Australia/Darwin'     ). " Darwin",
            'Asia/Yakutsk'         => static::toOffsetWithTimeZone('Asia/Yakutsk'         ). " Yakutsk",
            'Australia/Brisbane'   => static::toOffsetWithTimeZone('Australia/Brisbane'   ). " Brisbane",
            'Australia/Canberra'   => static::toOffsetWithTimeZone('Australia/Canberra'   ). " Canberra",
            'Pacific/Guam'         => static::toOffsetWithTimeZone('Pacific/Guam'         ). " Guam",
            'Australia/Hobart'     => static::toOffsetWithTimeZone('Australia/Hobart'     ). " Hobart",
            'Australia/Melbourne'  => static::toOffsetWithTimeZone('Australia/Melbourne'  ). " Melbourne",
            'Pacific/Port_Moresby' => static::toOffsetWithTimeZone('Pacific/Port_Moresby' ). " Port Moresby",
            'Australia/Sydney'     => static::toOffsetWithTimeZone('Australia/Sydney'     ). " Sydney",
            'Asia/Vladivostok'     => static::toOffsetWithTimeZone('Asia/Vladivostok'     ). " Vladivostok",
            'Asia/Magadan'         => static::toOffsetWithTimeZone('Asia/Magadan'         ). " Magadan",
            'Pacific/Auckland'     => static::toOffsetWithTimeZone('Pacific/Auckland'     ). " Auckland",
            'Pacific/Fiji'         => static::toOffsetWithTimeZone('Pacific/Fiji'         ). " Fiji",
        ];
    }

    public static function timeZones() {
      $zones_array = array();
      $timestamp = time();
      
      foreach(timezone_identifiers_list() as $key => $zone) {
        date_default_timezone_set($zone);
        $zones_array[$key]['timezone'] = $zone;
        //$zones_array[$key]['offset'] = 'UTC' . date('P', $timestamp);
        $zones_array[$key]['standard'] = "UTC";
        $zones_array[$key]['offset']=  static::toOffsetWithTimeZone($zone);
      }
      
      return $zones_array;
    }

}
