<?php

namespace Columbus\Utilities;

trait BookingStatus
{
    protected $status = ["cancelled", "rejected", "pending", "confirmed", "failed"];

    // "cancelled" > "failed"

    public function isStatusCancelled($status)
    {
        return strtolower($status) == "cancelled";
    }

    public function isStatusFailed($status) 
    {
        return strtolower($status) == "failed";
    }
}