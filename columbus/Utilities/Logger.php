<?php

namespace Columbus\Utilities;

use Illuminate\Support\Facades\Log;

trait Logger
{
    protected $log_prefix = self::class;

    public function startLog( $post_pix = "")
    {
        logger("************** $this->log_prefix - $post_pix  START ******************");
    }

    public function endLog($post_pix = "")
    {
        logger("************** $this->log_prefix - $post_pix  END ********************");
    }

    public function log($message, $body=[])
    {
        logger($message, $body);
    }

    function logDebug()
    {
        Log::debug('User failed to login.', ['id' => $user->id]);
    }

    function logMessage($message)
    {
        Log::info( self::class ."::" . $message . PHP_EOL);
    }

    function logInfo($data, $message="Data")
    {
        Log::info(self::calss. " ::" . $message, $data);
    }

    function logEmergency($message)
    {

    }

    function logAlert($message)
    {

    }

    function logCritical($message)
    {

    }

    function logError($message)
    {

    }
    function logWarning($message)
    {

    }

   function logNotice($message)
   {

   }
}