<?php

use Columbus\Utilities\DateUtil;


function json2Array($body)
{
    return  json_decode($body, JSON_OBJECT_AS_ARRAY);;
}

function formatDate( $date)
{
    return DateUtil::createCarbonIfNotCarbon($date)->toDateString();
}

function convertCurrency($from, $to, $amount){
    $client = new \GuzzleHttp\Client();
    $res = $client->request('GET', 'https://free.currencyconverterapi.com/api/v6/convert?q=' . $from . '_' . $to . '&compact=ultra&apiKey=ed87bb0190a993d72076');

    //$url = file_get_contents('https://free.currencyconverterapi.com/api/v5/convert?q=' . $from . '_' . $to . '&compact=ultra');
    $total = 0;
    try{
        $json = json_decode($res->getBody(), true);
        $rate = implode(" ", $json);
        $total = $rate * $amount;
    } catch(\Exception $exception) {
        $total = 0;
    }
    return $total;
}
if( !function_exists('roundUp') ){
    function roundUp($value, $places = 2)
    {
        return round($value, $places);
        /* $mult = pow(10, abs($places));
        $roundUpValue= $places < 0 ? ceil($value / $mult) * $mult : ceil($value * $mult) / $mult;
        return $roundUpValue; */
    }
}

function random_code($limit = 10)
{
    return substr(base_convert(sha1(uniqid(mt_rand())), 16, 36), 0, $limit);
}

function struuid($entropy)
{
    $s=uniqid("",$entropy);
    $num= hexdec(str_replace(".","",(string)$s));
    $index = 'aeXYZfbklcxnopyzhmGHIquvNOPwr26sdgtQTUVij17EF89LM0ABC345DJKRSW,.+=@#$%&*';
    $base= strlen($index);
    $out = '';
        for($t = floor(log10($num) / log10($base)); $t >= 0; $t--) {
            $a = floor($num / pow($base,$t));
            $out = $out.substr($index,$a,1);
            $num = $num-($a*pow($base,$t));
        }
    return $out;
}
