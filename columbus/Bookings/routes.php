<?php

Route::group(['prefix'=> 'api', 'middleware' => ['auth:api','cors']], function() {

    Route::group(['prefix'=>'group-request'], function() {
        Route::get('', 'GroupRequestController@index');
        Route::post('', 'GroupRequestController@create');
    });
    // route for creating booking by agent
    Route::post('add-booking', 'BookingController@create');

    //otb
    Route::post("/create-otb-booking", "OTBBookingController@create");

    Route::group([ 'prefix'=> 'bookings'], function() {

        Route::get("count-by-status", "BookingStatusCountController@index");

        Route::post('', 'BookingController@search');
        Route::get('{booking_id}', 'BookingController@read');
        //Route::delete('{booking_id}', 'BookingController@cancel');
        Route::put("{booking_id}/finalize", 'BookingController@finalize');
        Route::get("{booking_id}/voucher", 'BookingController@voucher');
        Route::delete("{booking_id}/cancel", 'BookingController@cancel');

        //booking issues
        Route::get("{booking_id}/issues", 'BookingIssueController@index');
        Route::post("{booking_id}/issues", "BookingIssueController@raise");

        Route::get("/{booking_id}/visa-documents", 'BookingVisaDocumentsController@index');
        Route::post("/{booking_id}/visa-documents", 'BookingVisaDocumentsController@uploadDocuments');
        //Route::post("/{booking_id}/visa-documents/{visa_id}", 'BookingVisaDocumentsController@updateDocuments');
        Route::get("/{booking_id}/visa-documents/download", 'BookingVisaDocumentsController@download');
    });

    Route::group([ "prefix" => "/payment-notifications"], function() {
        Route::get("", 'PaymentNotificationController@index');
        Route::post("", "PaymentNotificationController@create");
    });


    Route::post("bookings/{booking_id}/email-invoice", 'InvoiceAndVoucherController@emailInvoice');
    Route::post("bookings/{booking_id}/email-voucher", 'InvoiceAndVoucherController@emailVoucher');
    Route::post("bookings/{booking_id}/email-booking", 'InvoiceAndVoucherController@emailBooking');
});

Route::get("api/bookings/{booking_id}/visa-documents/download", 'BookingVisaDocumentsController@download');

Route::get("mail-invoice", function() {
    //dd(\Columbus\Bookings\Models\Booking::orderBy('id', 'desc')->first());
    \Mail::to('kasanagottusatish@gmail.com')
        ->send(
        new \Columbus\Bookings\Mail\MailingInvoiceRequested(\Columbus\Bookings\Models\Booking::orderBy('id', 'desc')->first())
        );
});

Route::get("bookings/{booking_id}/print-invoice", 'InvoiceAndVoucherController@printInvoice');
Route::get("bookings/{booking_id}/print-voucher", 'InvoiceAndVoucherController@printVoucher');
/* Route::get("bookings/{booking_id}/email-invoice", 'InvoiceAndVoucherController@emailInvoice');
Route::get("bookings/{booking_id}/email-voucher", 'InvoiceAndVoucherController@emailVoucher');
Route::get("bookings/{booking_id}/email-booking", 'InvoiceAndVoucherController@emailBooking'); */

/*
Route::get("voucher-for-booking", function(){
    $bookingItem = \Columbus\Bookings\Models\BookingServiceDetail::find(80);;
    return \Columbus\Bookings\Repositories\HotelRepository::refetchBookingDetails($bookingItem);
});

Route::get("/bookings/{booking_id}/voucher", 'BookingController@voucher');

Route::get("dd-cp", function() {
    $accomodaiton = new \Columbus\ThirdParty\Utilities\GRN\Accommodation();
    return $accomodaiton->cancellationDetails();
}); */