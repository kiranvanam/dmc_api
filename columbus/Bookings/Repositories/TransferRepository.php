<?php

namespace Columbus\Bookings\Repositories;

use Columbus\Utilities\Logger;

class TransferRepository
{
    use Logger;

    public $transfers = null;
        
    public function confirm()
    {
        $this->logMessage("confirm-- Start");
        
        $messages = [];
        $this->transfers->each( function($transferDetail) {
            $this->logMessage( $transferDetail->transfer_type_nanme . "- " . $transferDetail->status);
            if($transferDetail->isConfirmed() == false) {
                $messages = str_replace("_", " ", $transferDetail->direction) . "- $transferDetail->tranfer_type_name  not conifirmed";
            }                        
        });

        $this->logMessage("confirm-- End");

        return $messages;
    }

    public function cancel()
    {
        $this->logMessage("cancel--start");
        $self = $this;
        $this->transfers->each( function($transferDetail) use($self) {
            $transferDetail->cancelled_date = now();
            $transferDetail->cancellation_charges = $self->cancellationCharges($transferDetail);
            $transferDetail->update();
        });

        $this->logMessage("cancel--end");
    }

    public function cancellationCharges( $transferDetail)
    {
        
        $cancellation_charges = empty($transferDetail->cancellation_charges) == false ? 
                                                    $transferDetail->cancellation_charges
                                                    : 0 ; 
        
        return  number_format( $cancellation_charges, 2);
    }

    public function isConfirmed()
    {
        return $this->transfers->each( function($transferDetail) {
                                        return $transferDetail->isConfirmed();
                                    });
    }

    public function isCancelled()
    {
        return $this->transfers->each( function($transferDetail) {
                                        return $transferDetail->isCancelled();
                                    });
    }   
}