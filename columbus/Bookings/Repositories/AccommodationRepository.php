<?php

namespace Columbus\Bookings\Repositories;

use Columbus\Bookings\Models\Accommodation;
use Columbus\Bookings\Repositories\Traits\Accommodation\Fetch;
use Columbus\Bookings\Repositories\Traits\Accommodation\Cancel;
use Columbus\Bookings\Repositories\Traits\Accommodation\Voucher;
use Columbus\Bookings\Repositories\Traits\Accommodation\Confirm;

class AccommodationRepository
{
    use Fetch, Voucher, Confirm, Cancel;

    public function isConfirmed()
    {
        return $this->accommodation->isConfirmed();
    }

    public function isCancelled() {
        return $this->accommodation->isCancelled();
    }

    protected function getErrors($response)
    {
        return array_has($response, 'errors') ? array_get($response, "data", []): [];
    }

    //get cancellaton charges
    public function cancellationCharges()
    {
        if( empty($this->accommodation))
            return 0;

        $cancellation_charges = 0;

        //for GRN 
        if( $this->accommodation->cancellation_details) {
            //if booked later                        
            $cancellation_charges = array_get( $this->accommodation->cancellation_details, "cancellation_charges.amount", 0);
        }
        
        return $cancellation_charges;
    }
}