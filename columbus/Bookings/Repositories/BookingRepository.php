<?php

namespace Columbus\Bookings\Repositories;

use Columbus\Utilities\Logger;
use Columbus\Bookings\Models\Booking;
use Columbus\Bookings\Repositories\Traits\Booking\Fetch;
use Columbus\Bookings\Repositories\Traits\Booking\Cancel;
use Columbus\Bookings\Repositories\Traits\Booking\Confirm;
use Columbus\Bookings\Repositories\Traits\Booking\Voucher;

class BookingRepository
{
    use Fetch, Voucher, Confirm, Cancel, Logger;

    protected $errors = [];
    protected $booking = null;

    function __construct(AccommodationRepository $accommodaiton, 
                        VisaRepository $visaRepo,
                        TransferRepository $transfersRepo)
    {
        $this->accommodationRepo = $accommodaiton;
        $this->visaRepo = $visaRepo;
        $this->transfersRepo= $transfersRepo;
    }

    protected function fetchBookingById($bookingId)
    {
        $this->booking = Booking::fetchById($bookingId);
        $this->accommodationRepo->accommodation = $this->booking->accommodation;
        $this->visaRepo->visa = $this->booking->visa;
        $this->transfersRepo->transfers = $this->booking->transfers; //not required as of now 
        return $this;
    }
    
    function bookingById($bookingId) 
    {   
        logger(PHP_EOL. "BookingRepo::bookingById-- start ". $bookingId . PHP_EOL); 

            $this->fetchBookingById($bookingId)
                ->fetchEachServiceData()
                /**->transfersBooking()
                ->activityBooking();*/
                ->updateBookingDetails();

        logger(PHP_EOL. "BookingRepo::bookingById-- end ". $bookingId . PHP_EOL);
    }

    function finalizeById($bookingId)
    {
        logger(PHP_EOL. "BookingRepo::bookingById-- start ". $bookingId . PHP_EOL); 

            $this->fetchBookingById($bookingId)
                ->confirmAccommodation()
                ->confirmVisa() //  confirmation could be for visa or otb
                ->confirmTransfers()
                /*->activityBooking();*/
                ->updateFinalizeDetails();

        logger(PHP_EOL. "BookingRepo::bookingById-- end ". $bookingId . PHP_EOL);        
    }

    function voucherById($bookingId)
    {
        logger(PHP_EOL. "BookingRepo::voucherById -- start ". $bookingId . PHP_EOL); 

            $this->fetchBookingById($bookingId)
                ->voucherAccommodation()
                ->voucherTransfers()
                /**->activityBooking();
                ->updateBookingVoucherDetails()*/;

        logger(PHP_EOL. "BookingRepo::voucherById -- end ". $bookingId . PHP_EOL);
    }

    function cancelById($bookingId)
    {
        logger( PHP_EOL . "BookingRepo::cancelById -- start " . $bookingId . PHP_EOL); 
        
        $this->fetchBookingById($bookingId)
            ->cancelAccommodation()
            ->cancelVisa()
            ->updateAfterCancellation();

        logger( PHP_EOL . "BookingRepo::cancelById -- end " . $bookingId .  PHP_EOL );
    }



    //utility classes
    protected function updateWith($data)
    {
        return $this->booking->update($data);
    }

    // helper methods
    protected function doesBookingHaveAccommodation()
    {
        return is_null($this->booking->accommodation) == false;
    }

    protected function doesBookingHaveTransfers()
    {
        return is_null($this->booking->transfers) == false;
    }

    protected function doesBookingHaveActivities()
    {
        return is_null($this->booking->activities) == false;
    }

    protected function doesBookingHaveGuide()
    {
        return is_null( $this->booking->guide) == false ;
    }

    protected function doesBookingHaveVisa()
    {
        return is_null($this->booking->visa) == false;
    }


    public function hasErrors()
    {
        return count($this->errors);
    }
    protected function appendErrors($errors)
    {
        if(is_array($errors) == false)
            $errors = [];
        $this->errors = array_merge($this->errors, $errors);
        logger("processing-errors", $errors);
    }
    public function errors() 
    {
        return $this->errors;
    }
    public function result()
    {
        return $this->booking;
    }

    protected function areAllServicesCancelled()
    {
        $areAllServicesCancelled = false;        
        if($this->doesBookingHaveAccommodation())
            $areAllServicesCancelled = $this->accommodationRepo->isCancelled();

        if($this->doesBookingHaveVisa())
            $areAllServicesCancelled = $this->visaRepo->isCancelled();
            
        return $areAllServicesCancelled;
    }

    protected function areAllServicesConfirmed()
    {
        if( $this->doesBookingHaveAccommodation())
            if( empty($this->accommodationRepo->isConfirmed()))
                    return false;

        if($this->doesBookingHaveVisa())
            if(empty( $this->visaRepo->isConfirmed() ))
                  return false;
        return true;
    }

    function bookingCountGroupByStatus($agencyId= null)
    {
        $query = \DB::table('bookings');
        
        if( is_null($agencyId) == false ) {
            $query->where('agency_id', $agencyId);
        }

        return $query->select( \DB::raw('status, count(*) as total'))->groupBy('status')->get();
    }

}