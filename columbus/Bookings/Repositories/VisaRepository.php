<?php

namespace Columbus\Bookings\Repositories;

class VisaRepository
{
    public $visa = null;

    
    public function confirm()
    {
        if(empty($this->visa))
            return [];
        if($this->visa->isConfirmed())
            return [];        
        return [ "Service not yet confirmed"];
    }

    public function cancel()
    {
        logger("VisaRepo::cancel--start");

        $this->visa->cancelled_date = now();
        $this->visa->cancellation_charges = $this->visa->total_price;

        $this->visa->update();
        logger("VisaRepo::cancel-- end");
    }

    public function cancellationCharges()
    {
        $cancellation_charges = $this->visa ? $this->visa->cancellation_charges : 0; 
        
        return  number_format( $cancellation_charges, 2);
    }

    public function isConfirmed()
    {
        return $this->visa->isConfirmed();
    }

    public function isCancelled()
    {
        return $this->visa->isCancelled();
    }
}