<?php

namespace Columbus\Bookings\Repositories\Traits\Accommodation;

use Columbus\Services\Products\Accommodation\Accommodation as AccommodationApiResolver;

trait Cancel
{
        /**
     * why do we need cancelled date ? 
     * 1. 
     * 2.  
     */
    function cancel() 
    {
        
        logger("AccommodationRepo::cancel -- start");
        
        $accommodation = $this->accommodation;
        $response = [];

        // is cancellation is allowed
        if( $accommodation->isCancellationAllowed()  == false ) 
        {
            logger("cancellation not allowed");
            // set cancellation_details if not set
            
            $response = $this->updateCacnellationDetailsIfNotSet();
        } else {          
              
            $response = $this->prepareCancellationPolicies();
        }
        //end log
        logger("AccommodationRepo::cancel -- END" );        
        return $this->getErrors( $response);
    }

    protected function prepareCancellationPolicies()
    {
        $response = [];

        if( $accommodation->isContractedBooking()){
                // prepare cancellation charges
            //$cancellationDetails = $this->calculateCancellationCharges();             
        } 
        else {
            $response = $this->cancelUsingServices();
        }
                    
        logger("Response", $response);
        
        // if processed result has errors, please 
        if( array_has($response, 'errors') )
            return $response;
       
        $this->updateAfterCancellation($response);

        return $response;
    }

    protected function cancelContractedAccommodation() {
        // cancel contracted accommodation
    }

    protected function cancelUsingServices()
    {
        $accomodation = $this->accommodation;
        
        $data = [];            
        $data['service_source'] = $accomodation->service_source;
        
        $cancellationDetails= [];
        
        if($accomodation->isGRNBooking()) {            
            $data['booking_reference'] = $accomodation->bookingReferenceGRN();               
            $cancellationDetails = AccommodationApiResolver::cancel($data);            
        }
        
        return $cancellationDetails;
    }

    protected function updateAfterCancellation($cancellationDetails)
    {        
        $updated = [];
                
        //if cancelled date already set, don't update again
        $updted['status'] = "cancelled";
        if( empty($this->cancelled_date)) 
            $updated['cancelled_date'] = now();
        
        $updated["cancellation_details"] = array_get($cancellationDetails, 'data', $this->accommodation->cancellation_details);
        $updated['cancellation_charges'] = $this->getAccommodationCancellationCharges($updated["cancellation_details"]);
        logger("Data to be updated", $updated);

        $this->accommodation->update($updated);        
    }
   

    protected function updateCacnellationDetailsIfNotSet()
    {
        $accommodation = $this->accommodation;

        if(array_has($accommodation->cancellation_details, "cancellation_charges") == false ) {
            //preate cancellation policies
            $cancellationDetails = [ 'data' => [
                                        "cancellation_charges" => [
                                                "currency" => $accommodation->currency,
                                                "amount" => $accommodation->total_price
                                            ]
                                    ] 
                                ];
            //update
            $this->updateAfterCancellation($cancellationDetails);
        }
        return [];
    }

    protected function getAccommodationCancellationCharges($cancellation_details)
    { 
        //for GRN
        return array_get( $cancellation_details, "cancellation_charges.amount", 0);
    }
            
}