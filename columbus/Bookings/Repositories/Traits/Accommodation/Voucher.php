<?php

namespace Columbus\Bookings\Repositories\Traits\Accommodation;

use Columbus\Services\Products\Accommodation\Accommodation as AccommodationApiResolver;

trait Voucher
{
    function voucher() 
    {
        logger("AccommodationRepo::voucher -- start");

        $accommodation = $this->accommodation;
        
        $response = [];
        
        //$accommodation->voucher_details = null;

        if( is_null($accommodation->voucher_details)) {
                            
            $response = $this->voucherAccommodation();
            
            $this->updateVoucherDetails($response);
        
            logger("Processed Response", $response);

        } else {
            logger("Voucher details are already set");
        }

        //possibly log all these messages to database
        logger("AccommodationRepo::voucher -- start");

        //returning errors
        return  $this->getErrors($response);
    }

    protected function voucherAccommodation()
    {
        if( $this->accommodation->isContractedBooking()) {
            $response = $this->voucherForContractedBooking();
        }                                
        else {
            $response = $this->voucherUsingService();
        }
        return $response;
    }

    protected function voucherForContractedBooking()
    {
        //update with voucher details
        //$this->update(['status' => "confirmed"]);
    }

    protected function voucherUsingService()
    {
        $accommodation = $this->accommodation;

        if( $accommodation->isGRNBooking() ) {
            $data = [];
            $data['service_source'] = $accommodation->service_source; 
            $data['booking_reference'] = $accommodation->bookingReferenceGRN();                
            $response = AccommodationApiResolver::voucher($data);            
            //GrnConnect::fetchBookingDetailsFor($booking_reference );
        }

        return $response;
    }

    protected function updateVoucherDetails($response)
    {
        $accommodation = $this->accommodation;

        if( array_has($response, 'errors') == false ) {
            $updated = [];
            // if no
            $updated['voucher_details'] = array_get($response, 'data', $accommodation->voucher_details);
            
            logger("voucher data to be Updated", $updated);
            
            $accommodation->update( $updated );            
        }
    }
}