<?php

namespace Columbus\Bookings\Repositories\Traits\Accommodation;

use Columbus\Services\Products\Accommodation\Accommodation as AccommodationApiResolver;

trait Confirm
{
    public function confirm()
    {   
        logger("Accommoadtion::confirm -- start");

        $accommodation = $this->accommodation;

        $response= [];
        // if status is already confirmed, then no need to check anything
        //$accommodation->status = "pending";
        if( $this->isConfirmationNotRequired() ) {
            logger("Accommodation confirmation code not executed , status -- " . $accommodation->status );
        }
        else {        
            $response = $this->confirmAccommodation();
            
            $this->updateAfterConfirmation($response);
        
            logger("Processed Response", $response);
        }

        logger("Accommodation::cofirm -- end" );
        return $this->getErrors($response);        
    }

    protected function confirmaccommodation()
    {        
        if( $this->accommodation->isContractedBooking()) {
           $response = $this->confirmContractedAccommodation();                                    
        }                                        
        else {
           $response =  $this->confirmAccommodationUsingServices();
            //GrnConnect::fetchBookingDetailsFor($booking_reference);
        }
        return $response;
    }

    protected function confirmContractedAccommodation()
    {
        // check if still in pending case
        $this->accommodation->update(['status' => "confirmed"]);
    }

    protected function confirmAccommodationUsingServices()
    {
        $response = [];
        
        $data = [];
        $data['service_source'] = $this->accommodation->service_source;
        
        if( $this->accommodation->isGRNBooking() ) {
            $data['booking_reference'] = $this->accommodation->bookingReferenceGRN();
            if( array_get($data, 'booking_reference') != null )
                    $response = AccommodationApiResolver::bookingDetails($data);
        }
        logger("GRN-Response -- ", $response);  
        return $response;
    }

    protected function updateAfterConfirmation($response) 
    {
        if( array_has($response, 'errors') == false ) {
            $accommodation = $this->accommodation;

            $bookingDetails = $response['data'];
            $updated = []; 
            $updated['status'] = array_get($bookingDetails, 'status', $accommodation->status);

            if(array_has($bookingDetails, 'accommodation_details'))
                $updated['accommodation_details'] = array_get($bookingDetails, 'accommodation_details');
            
            logger("Data to updated: ", $updated);

            $accommodation->update($updated);                
        }
    }

    /**
     *  not required if status is one of these
     *  case 1. cancelled
     *  case 2. confirmed
     */
    protected function isConfirmationNotRequired()
    {
        return $this->accommodation->isConfirmed() || $this->accommodation->isCancelled();
    }
}