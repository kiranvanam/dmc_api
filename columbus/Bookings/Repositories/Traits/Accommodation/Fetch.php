<?php

namespace Columbus\Bookings\Repositories\Traits\Accommodation;

use Columbus\Services\Products\Accommodation\Accommodation as AccommodationApiResolver;

trait Fetch
{
    public function prepareBooking()
    {
        logger("AccommodationRepo::prepareBooking -- start");
        
        $accommodation = $this->accommodation;    
        
        $response = [];
        //$accommodation->status = "pending";                
        if( $this->areAccommodationDetailsNotFetched() ) {
            //data required as per the booking service source
            $response = $this->fetchAccommodation();
                        
            $this->updateAfterFetch($response);
        
            logger("Processed Response", $response);
        } 
        else {         
            logger("fetching Accommodation details is not  executed .. Status ". $accommodation->status );
        }
        
        logger("AccommodationRepo::prepareBooking -- end");

        //returning errors
        return  $this->getErrors($response);
    }

    protected function fetchAccommodation()
    {
        if( $this->accommodation->isContractedBooking()) {
           $response = $this->updateContractedBooking();                              
        }                                
        else {
            $response = $this->fetchUsingServices();
        }
        
        return $response;
    }

    protected function updateContractedBooking()
    {
        $this->accommodation->update(['status' => "confirmed"]);  
    }

    protected function fetchUsingServices()
    {
        $accommodation = $this->accommodation;
        if( $accommodation->isGRNBooking() ) {
            $data = [];
            $data['service_source'] = $accommodation->service_source;
            $data['booking_reference'] = $accommodation->bookingReferenceGRN();
            if( array_get($data, 'booking_reference') != null )
                $response = AccommodationApiResolver::bookingDetails($data);
            logger("GRN-Response", $response);
        }
        return $response;
    }

    protected function updateAfterFetch($response)
    {
        $accommodation = $this->accommodation;

        if( count($response ) && array_has($response, 'errors') == false) {
            $accommodationDetails = array_get($response, 'data');
            if( array_has($accommodationDetails, 'accommodation_details') ){
                $updated = [];
                $updated['status'] = array_get($accommodationDetails, "status", $accommodation->status);
                $updated['total_pax'] = array_get($accommodationDetails,'total_pax', $accommodation->total_pax);
                $updated['accommodation_details'] =  array_get($accommodationDetails, "accommodation_details", $accommodation->accommodation_details);
                
                logger( "Data to be updated", $updated);
                
                $accommodation->update($updated);
            }
        }
    }

    protected function areAccommodationDetailsNotFetched()
    {        
        return  $this->accommodation->isPending() ||  
                $this->accommodation->isUnknown() ;
    }
}