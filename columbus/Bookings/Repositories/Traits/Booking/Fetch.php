<?php

namespace Columbus\Bookings\Repositories\Traits\Booking;

trait Fetch
{
    protected function fetchEachServiceData()
    {
        return $this->fetchAccommodation()
                    ->fetchVisa()
                    ->fetchTransfers();
    }

    protected function fetchAccommodation()
    {
        logger("BookingRepo::fetchAccommodation -- start". PHP_EOL);        

        if($this->doesBookingHaveAccommodation()) {
            $errors = $this->accommodationRepo->prepareBooking();          
            
            if( count($errors)) {
                $this->errors = array_merge($this->errors, $errors);
                logger("processing-errors", $errors);
            }
        }
        
        logger("BookingRepo::fetchAccommodation -- end" . PHP_EOL);
        return $this;
    }

    protected function fetchVisa()
    {
        logger("BookingRepo::fetchVisa--start" . PHP_EOL);
        logger("Visa is contracting..no fetching required");
        logger("BookingRepo::fetchVisa -- end" . PHP_EOL);

        return $this;
    }

    protected function fetchTransfers()
    {
        logger("BookingRepo::fetchTransfers--start". PHP_EOL);
        logger("Transfers is contracting..no fetching required");
        logger("BookingRepo::fetchTransfers--end". PHP_EOL);
        return $this;
    }

    // if status not confirmed earlier, update status after all services confirmed
    protected function updateBookingDetails()
    {
        logger("BookingRepo::updating -- start". PHP_EOL);

        if(empty($this->errors)) {
            $updated = [];
            //if one of services status is not confirmed. then make booking status as "pending"
            
            if($this->areAllServicesCancelled())
                $updated['status'] = "cancelled";
            else
                $updated['status'] = $this->bookingStatus(); //$this->getStatus();
            
            
            logger("Updating with Data--", $updated);

            if( count($updated))
                $this->updateWith($updated);
                            
        } else{
            logger("errors after processing bookings of all products", $this->errors()); 
        }

        logger("BookingRepo::updating -- end". PHP_EOL);
    }


    protected function bookingStatus()
    {
        // even if one of services is not confirmed, then status is in pending
        if( $this->areAllServicesConfirmed()) 
            return "confirmed";
                
        return "pending";
    }

}