<?php

namespace Columbus\Bookings\Repositories\Traits\Booking;

trait Voucher
{
    protected function  voucherAccommodation()
    {
        $this->logMessage("voucherAccommodation -- start");
        $errors = null;
        if($this->doesBookingHaveAccommodation()) {
            $errors = $this->accommodationRepo->voucher();          
        }

        if( is_array($errors)) {
            $this->errors = array_merge($this->errors, $errors);
            $this->logInfo( $errors, "processing-errors");
        }

        $this->logMessage("voucherAccommodation -- end");

        return $this;
    }

    protected function voucherTransfers()
    {
        $this->logMessage("voucherTransfers--Start");
        
        $this->logMessage("voucherTransfers--End");
    }

    protected function updateVoucher()
    {
        $this->logMessage("updatingBookingVoucherDetails -- start");

        $this->logMessage("No logic to update");

        $this->logMessage("updatingBookingVoucherDetails -- end");
    }
}