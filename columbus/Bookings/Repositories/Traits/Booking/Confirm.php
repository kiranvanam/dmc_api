<?php

namespace Columbus\Bookings\Repositories\Traits\Booking;

trait Confirm
{
    public function confirmAccommodation()
    {
        logger("BookingRepo::voucherAccommodation -- start");

        $errors = [];

        if($this->doesBookingHaveAccommodation()) {
            $errors = $this->accommodationRepo->confirm();            
            $this->appendErrors( $errors);
        }        

        logger("BookingRepo::voucherAccommodation -- end");

        return $this;                
    }

    public function confirmVisa()
    {
        logger("BookingRepo::confirmVisa-- start". PHP_EOL);
        if($this->doesBookingHaveVisa()) {
            $errors = $this->visaRepo->confirm();
            $this->appendErrors( $errors);
        }
        logger("BookingRepo::confirmVissa-- end" . PHP_EOL);

        return $this;
    }

    public function confirmTransfers()
    {
        logger("BookingRepo::confirmTransfers--start". PHP_EOL);        
        
        if($this->doesBookingHaveTransfers()) {
            $errors = $this->transfersRepo->confirm();
            $this->appendErrors($errors);
        }            

        logger("BookingRepo::confirmTransfers". PHP_EOL);
        return $this;
    }

    public function updateFinalizeDetails()
    {
        // if no errors and all services are comfirmed, then finalize the booking
        $updated = [];
        if($this->areAllServicesConfirmed()) {
            $updated['status'] = "finalized";
            $updated['finalized_date'] = now();
            logger("Booking: Data to be updated");
            $this->booking->update($updated);
        }
        logger("Data to be updated", $updated);
    
    }
}