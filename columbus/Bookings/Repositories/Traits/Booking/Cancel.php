<?php

namespace Columbus\Bookings\Repositories\Traits\Booking;

trait Cancel
{
    protected function cancelAccommodation()
    {
        logger("BookingRepo::cancelAccommodation -- start");
        


        if($this->doesBookingHaveAccommodation()) {
            $errors = $this->accommodationRepo->cancel();          
            
            $this->appendErrors($errors);
        }
        
        logger("BookingRepo::cancelAccommodation -- end");
        return $this;
    }

    protected function cancelVisa()
    {
        logger("BookingRepo::cancelVisa-- start". PHP_EOL);
        
        if($this->doesBookingHaveVisa()) {
            $errors = $this->visaRepo->cancel();
            $this->appendErrors($errors); 
        }

        logger("BookingRepo::cancelVisa-- end". PHP_EOL);

        return $this;
    }

    protected function updateAfterCancellation()
    {

        if($this->hasErrors()) 
            return ;
        
        // update the details
        if( is_null($this->booking->cancelled_date) || $this->booking->cancellation_charges <= 0 ) {
            $updated = [];
            $updated['status'] = 'cancelled';
            $updated['cancelled_date'] = now();

            // calculate cancellation amount to update cancellation_charges column & refund amount
            $updated['cancellation_charges'] = $this->calculateServiceCancellationCharges();
            $updated['refund'] = $this->booking->total_price - $updated['cancellation_charges'];

            logger("Updating with Data", $updated);

            $this->booking->update($updated);

        }
    }

    protected function calculateServiceCancellationCharges()
    {
        //caluclate each service cancellation charges & add them for this booking
        $cancellationAmount = 0;
        //Accommodation
        $cancellationAmount = $this->accommodationRepo->cancellationCharges();
        $cancellationAmount += $this->visaRepo->cancellationCharges();
        
        //add other services

        return $cancellationAmount;
    }
}