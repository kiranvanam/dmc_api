<?php

namespace Columbus\Bookings\Repositories;

use Columbus\ThirdParty\Utilities\GRN\Accommodation as GrnHotel;


class HotelRepository
{

    /* update the bookingItemDetails aray with processed data */
    public static function addBooking( &$bookingItemDetails, $lead_passenger_details)
    {
        $service_source = array_get($bookingItemDetails, 'service_source');
        if(static::isGrn($service_source)) {
            $lead_passenger_details['client_nationality'] = $lead_passenger_details['nationality'];
            unset($lead_passenger_details['nationality']);
            $bookingDetails = array_get($bookingItemDetails, 'other_details.booking_details', []);
            $bookingDetails['holder'] = $lead_passenger_details;
            $accommodation = new GrnHotel();
            $result = $accommodation->book($bookingDetails);
            //update the status in final bookingItemDetails
            if(array_has($result, 'status')) {
                $bookingItemDetails['status'] = $result['status'];
            }else {
                $bookingItemDetails['status']= "failed";
            }
            $bookingItemDetails['other_details'] = $result;
        }

        //return array_get($bookingItemDetails,'other_details');
    }

    public static function refetchBookingDetails($bookingItem)
    {
        if($bookingItem->isSourceGRN()) {
            $accommodation = new GrnHotel();
            if( array_has($bookingItem->other_details, 'search_id') )
                return $accommodation->fetchBookingWithSearchId(array_get($bookingItem->other_details, 'search_id'));
            elseif(array_has($bookingItem->other_details, 'booking_reference'))
                return $accommodation->fetchBookingWithReference(array_get($bookingItem->other_details, 'booking_reference'));
        }
        return [];
    }

    public static function fetchVoucherDetails($bookingItem)
    {
        if($bookingItem->isSourceGRN()) {
            $accommodation = new GrnHotel();
            if($bookingItem->isHotelBookingRequestConfirmed() &&
                    $bookingItem->hasGRNHotelVoucherNotGenerated()){
                return $accommodation->generateVoucher($bookingItem->other_details);
            }
        }
        return [];
    }

    public static function cancelBooking($bookingItem)
    {
        logger("*** " . self::class ."::cancelBooking - START *****");
        if( $bookingItem->isHotelCancelled() ) {
            return array_get($bookingItem->other_details, 'cancellation_details');
        }
        if($bookingItem->isSourceGRN()) {
            $accommodation = new GrnHotel();
            return $accommodation->cancelBooking($bookingItem->other_details);
        }
        logger("******" . self::class ."::cancelBooking - END *****");
    }

    public static function isGrn($service) {
        return $service == "grn";
    }

}