<?php

namespace Columbus\Bookings\Models;

use Columbus\Utilities\Logger;
use Illuminate\Database\Eloquent\Model;
use Columbus\Destination\Models\Country;
use Columbus\Contracts\Visas\Models\Visa;
use Columbus\Bookings\Utilities\GRNPricing;
use Columbus\Bookings\Utilities\UnitPricing;
use Columbus\Bookings\Utilities\BookingStatus;
use Columbus\Bookings\Utilities\ServiceDetail;
use Columbus\Contracts\Tours\Models\TourTitle;
use Columbus\Bookings\Utilities\PaxUnitPricing;
use Columbus\Contracts\Meals\Models\MealVendor;
use Columbus\Contracts\Vehicles\Models\Vehicle;
use Columbus\Contracts\Guides\Models\GuideTitle;
use Columbus\Bookings\Utilities\PointOrSlotDetail;
use Columbus\Contracts\Tickets\Models\TicketTitle;
use Columbus\Bookings\Repositories\HotelRepository;
use Columbus\Bookings\Utilities\BookingItem\Cancel;
use Columbus\Bookings\Utilities\SingleUnitTypePricing;
use Columbus\Contracts\Transfers\Models\TransferService;
use Columbus\Contracts\Tours\Models\TourTitleVendorDetail;
use Columbus\Bookings\Utilities\BookingItem\TrasnsferDetail;

class BookingServiceDetail extends Model
{
    use BookingStatus, UnitPricing, PaxUnitPricing, SingleUnitTypePricing,
        ServiceDetail,TrasnsferDetail, PointOrSlotDetail, GRNPricing,
        Cancel, Logger;

    protected $fillable = [ 'booking_id','service_source', 'status',
                    'service','service_name', 'service_id','service_code',
                    'vendor_id', 'vendor_code',
                    'booking_price','markup_price', 'total_price', 'currency', 'cancellation_charges',
                    'city_id','city_name','country_id','country_code',
                    'travel_date',
                    'unit_pricing', 'markup_details',
                    'cancellation_policies', 'other_details',
                ];

    protected $dates = ['travel_date'];

    protected $casts = [
            'unit_pricing'=> 'array',
            'markup_details' => 'array',
            'cancellation_policies' => 'array',
            'other_details' => 'array',
         ];

    function countryName()
    {
        return Country::nameByCCA3($this->country_code);
    }

    function serviceSource()
    {
       return $this->service_source;
    }

    function isSourceGRN()
    {
        return $this->serviceSource() == "grn";
    }

    function isSourceContracting()
    {
        return $this->serviceSource() == "contracting";
    }

    function isHotel()
    {
        return $this->service == "hotel";
    }

    function isTour()
    {
        return $this->service == "tour";
    }

    function isTicket()
    {
        return $this->service == "ticket"
                    ||  $this->service == 'ticket';
    }

    function isVehicle()
    {
        return $this->service == "vehicle";
    }

    function isTransfer()
    {
        return $this->service == "transfer";
    }

    function isPrivateTransfer()
    {
        return $this->isTransferService() && array_has($this->unit_pricing,  'vehicle_name');
    }

    /* for transfer service there are not tour_title_details */
    public function isTransferService()
    {
        return array_has($this->other_details, 'transfer_service_details');
    }

    function isMeal()
    {
        return $this->service == 'meal';
    }

    function isGuide()
    {
        return $this->service == "guide";
    }

    function isVisa()
    {
        return $this->service == "visa";
    }

    static function createBookingItemForBooking($booking, $bookingItem)
    {
        $bookingItem['booking_id']= $booking->id;
        // if hotel and service source  is third party api
        if(array_get($bookingItem,'service') == 'hotel')
        {
            HotelRepository::addBooking($bookingItem, $booking->lead_passenger_details);
            //logger("Other-details", $bookingItem['other_details']);
        }
        /* in case of  third party service, service_id is 0*/
        if( !isset($bookingItem['service_id'])){
            $bookingItem['service_id'] = 0;
        }
        return self::create($bookingItem);
    }

    /* private tour related */
    function privateTourServiceID() {
        return array_has( $this->other_details, 'tour_details.id');
        // don't do this why because for sic tour also, tour_title_details will be laoded
        /* || array_has( $this->other_details, 'tour_title_details.id') */
    }
    function isPrivateTourService() {
        return $this->privateTourServiceID();
                    /* || array_has( $this->other_details, 'tour_details.code')
                    || array_has( $this->other_details, 'tour_title_details.code'); */
    }

    function prepareServiceDetails()
    {
        $this->startLog("prepareServiceDetails");
        $this->log(" Details for Service: ". $this->service ."( $this->service_source )" );

        $other_details = $this->other_details;
        if(empty($other_details))
            $other_details = [];

        if($this->isHotel()) {
            $this->prepareHotelDetails($other_details);
        }
        if( $this->isTour()) {
            $other_details['service_details'] = $this->prepareTourDetails();
        }elseif ($this->isTicket()) {
            $other_details['service_details'] = $this->prepareTicketDetails();
        }elseif ($this->isVehicle() || $this->isPrivateTransfer() ) {
            $this->prepareTransferDetails($other_details );
        }elseif($this->isMeal()) {
            $other_details['service_details'] = $this->prepareMealDetails();
        }elseif($this->isGuide()) {
            $other_details['service_details'] = $this->prepareGuideDetails();
        }elseif($this->isVisa()) {
            $other_details['service_details'] = $this->prepareVisaDetails();
        }

        if( $this->isPrivateTourService() ) {
            $other_details['tour_title_details'] = $this->loadTourTitleDetails();
        } if( $this->isTransferService() ) {
            $other_details['service_details'] = $this->loadTransferServiceDetails();
        }
        $this->other_details = $other_details;

        $this->endLog("prepareServiceDetails");
    }

    /* this is called for only SIC tour */
    function prepareTourDetails()
    {
        $tout_title_id = $this->service_id;
        $vendor_id = $this->vendor_id;
        return  TourTitle::where('id', $tout_title_id)
                        ->with(['vendor'=> function($query) use($vendor_id) {
                             $query->where('vendor_id', $vendor_id);
                        }])
                        ->first();
    }

    function prepareTicketDetails()
    {
        $ticket_title_id = $this->service_id;
        $vendor_id = $this->vendor_id;
        return TicketTitle::where('id', $ticket_title_id)
                            ->with(['vendor'=> function($query) use($vendor_id) {
                                $query->where('vendor_id', $vendor_id);
                            }])
                            ->first();
    }

    function prepareTransferDetails(&$other_details)
    {
        //here for transfer service prepare transfer service details tooo.
        if( $this->isPrivateTourService()) {
            $vehicle_id = $this->service_id;
        }else {
            $vehicle_id = array_get($this->unit_pricing, 'vehicle_id');
        }
        $other_details['vehicle_details'] = Vehicle::where('id', $vehicle_id)->with('brand')->first();
    }

    function prepareMealDetails()
    {
        $restaurant_id = array_get( $this->other_details, 'restaurant.id');
        return MealVendor::where('id', $restaurant_id)
                        ->with(['mealPlan'=> function($query){
                                $query->where('id', $this->service_id);
                            }]
                        )
                        ->first();
    }

    function prepareGuideDetails()
    {
        return GuideTitle::where('id', $this->service_id)
                        ->with(['vendor' => function($query) {
                            $query->where('vendor_id', $this->vendor_id);
                        }])
                        ->first();
    }

    function prepareVisaDetails()
    {
        return Visa::where('id', $this->service_id)->with('details')->first();
    }

    public function loadTourTitleDetails()
    {
        $tour_title_id = $this->privateTourServiceID();
        return TourTitle::find($tour_title_id);
        // don't do this wasy as there was an error
        //$this->other_details['tour_details'] = TourTitle::find($tour_title_id);
    }

    public function loadTransferServiceDetails()
    {
        return TransferService::where('id', array_get($this->other_details, 'transfer_service_details.id'))->first();
    }

    public function prepareHotelDetails(&$other_details)
    {
        if( $this->isSourceGRN() ) {
            if($this->hasGRNHotelVoucherNotGenerated()) {
                $this->log("fetching GRN Voucher details");
                $result = HotelRepository::fetchVoucherDetails($this);
                if( !empty($result) ) {
                    $other_details['voucher_details'] = $result;
                    $this->update(['other_details'=> $other_details]);
                }
            }
        }
    }

    public function travelDate()
    {
        return empty($this->travel_date) ? 'Indalid Date' :  $this->travel_date->format('d-m-Y') ;
    }

    public function slotDetails()
    {
        $start_time = array_get($this->unit_pricing, 'start_time');
        $end_time = array_get($this->unit_pricing, 'end_time');

        $slot = "";
        if( !empty($start_time))
            $slot = "<strong>Start time</strong>:$start_time <br/>";

        if(!empty($end_time))
            $slot = $slot . " <strong>End time</strong>:$end_time <br/>";

        return $slot;
    }
}