<?php

namespace Columbus\Bookings\Models;

use Illuminate\Support\Facades\Log;
use Columbus\Bookings\Utilities\Grn;
use Illuminate\Database\Eloquent\Model;
use Columbus\Bookings\Utilities\GrnConnect;
use Columbus\Bookings\Utilities\BookingStatus;
use Columbus\Services\Products\Accommodation\Accommodation as AccommodationApiResolver;

class Accommodation extends Model
{
    use Grn, BookingStatus;

    public $timestamps = false;

    protected $table = "accommodation_bookings";

    protected $fillable = [ 'booking_id', 'service_source','currency', 'total_price', 'status', 'total_rooms','country_code',
                            'destination_name', 'check_in', 'check_out',
                            'payment_deadline_date',
                            'cancelled_date', 'cancellation_charges', 'cancellation_details',
                            'name', 'total_pax', 'star_category', 'accommodation_details', 'voucher_details',
                        ];

    protected $dates = [ 'cancelled_date', 'payment_deadline_date', 'check_in', 'check_out'] ;
    protected $casts = [
        "accommodation_details" => "array",
        "voucher_details" => "array",
        "cancellation_details" => "array",
    ];

    public static function addHotelBooking( $hotelDetails, $booking)
    {

        $hotelDetails['booking_id'] = $booking['id'];

        return self::create($hotelDetails);
    }

    //if secondDate is not passed, then today's date is considered
    function isCancellationAllowed( $secondDate = null )
    {
        if(!$secondDate)
            $secondDate = now();
        //dd(doesFirstDateComeAfter($this->check_in, $secondDate), "check-ins");
        return doesFirstDateComeAfter($this->check_in, $secondDate);
    }


    function isAlreadyCancelled()
    {
        // handling case : if cancelled_date is set and status not updated, then update status
        if($this->isNotCancelled()) {
            if($this->cancelled_date) {
                $this->update(['status'=> "cancelled"]);
            }
        }
        return $this->isCancelled();
    }

    // booking services
    public function isContractedBooking()
    {
        return $this->service_source == "contracting";
    }

    function isAPIBooking()
    {
        $apiServiceSourceNames= ['grn-connect'];

        return  array_has($apiServiceSourceNames, $this->service_source);
    }

}