<?php

namespace Columbus\Bookings\Models;

use Illuminate\Database\Eloquent\Model;
use Columbus\Bookings\Utilities\BookingStatus;

class Transfer extends Model
{
    use BookingStatus;

    public $table = "transfer_bookings";
    
    public $timestamps = false;

    public $protected = ['travelling_date', 'payment_deadline_date', 'cancelled_date'];

    protected $fillable = ['booking_id','service_source',
                'transfer_service_name', 'transfer_service_id', 'is_transfer_type_with_luggage',
                'transfer_type','direction',
                'pick_up_city_id','pick_up_city_name','pick_up_area_id','pick_up_area_name','pick_up_time','pick_up_point_details',        
                'drop_off_city_id','drop_off_city_name', 'drop_off_area_id','drop_off_area_name','drop_off_point_details',
                'country_code', 'vendor_id',
                'no_of_adults','no_of_children',
                'booking_price', 'markup_price', 'total_price','currency', 'unit_pricing',
                'status', 'markup_details',
                'travelling_date', 'payment_deadline_date', 
                'cancelled_date', 'cancellation_policies',
                 'vehicle_details', 'service_details',  'voucher_details', 
            ];

    protected $casts = [
            'vocuer_details' => "array",
            "cancellation_policies" => "array",
            "unit_pricing" => "array",
            "vehicle_details" => "array",
            "service_details" => "array",
            "markup_details" => "array"
    ];

    public static function addBooking($transferDetails, $booking)
    {
        $transfers = collect([]);
        // for point-2-point transfers [ "on_ward" => booking_details, "return" => "booking_details" => booking_details ]
        if( array_has( $transferDetails, 'on_ward') || array_has($transferDetails, 'return')) {

            if(array_has($transferDetails, 'on_ward')) {
                $onWardTransferDetails = $transferDetails['on_ward'];
                $onWardTransferDetails['booking_id'] = $booking->id;
                $transfers->push(self::create($onWardTransferDetails));
            }
    
            if( array_has( $transferDetails, 'return')) {
                $returnTransferDetails = $transferDetails['return'];
                $returnTransferDetails['booking_id'] = $booking->id;
                $transfers->push( self::create($returnTransferDetails));
            }
        } else { // for all title based transfer bookings [ "title  => booking_details ]
            $pricingDetails= array_get($transferDetails, 'title');            
            $pricingDetails['booking_id']= $booking->id;
            $transfers->push( self::create($pricingDetails));
        }
        return $transfers;
    }
}