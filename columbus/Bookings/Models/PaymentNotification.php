<?php

namespace Columbus\Bookings\Models;

use Illuminate\Database\Eloquent\Model;

class PaymentNotification extends Model
{
    protected $fillable = ['agency_id', 'created_by_user', 'amount', 'currency', 
                           'payment_date', 
                           'payment_towards', 'booking_code',
                           'payment_type', 'payment_mode', 'bank_or_person_name',
                           'transaction_reference' 
                    ];
}