<?php

namespace Columbus\Bookings\Models;

use Carbon\Carbon;
use Columbus\Utilities\Logger;
use Columbus\Bookings\Utilities\DMC;
use Columbus\Bookings\Utilities\Tax;
use Columbus\Bookings\Utilities\Agency;
use Columbus\Bookings\Utilities\MarkUp;
use Illuminate\Database\Eloquent\Model;
use Columbus\Agents\Models\TravelAgency;
use Columbus\Bookings\Utilities\Invoice;
use Columbus\SystemSettings\Models\Code;
use Columbus\Bookings\Utilities\DMCConfig;
use Columbus\Bookings\Utilities\TotalPricing;
use Illuminate\Database\Eloquent\SoftDeletes;
use Columbus\Bookings\Utilities\BookingStatus;
use Columbus\Bookings\Utilities\LeadPassenger;
use Columbus\Bookings\Models\BookingServiceDetail;
use Columbus\Bookings\Utilities\CurrencyConverter;
use Columbus\Bookings\Utilities\Booking\Cancel;
use Columbus\Bookings\Utilities\BookingPrivateTourDetail;

class Booking extends Model
{
    use SoftDeletes;
    use BookingStatus;
    use Invoice, Tax, MarkUp, CurrencyConverter,
        LeadPassenger, TotalPricing,
        DMC, Agency, BookingPrivateTourDetail, DMCConfig, Cancel;

    use Logger;

    protected $dates = ['deleted_at', 'voucher_date', 'invoice_date', 'payment_deadline_date', 'service_start_date', 'service_end_date', 'finalized_date', 'cancelled_date'];

    //const CREATED_AT = 'booking_date';

    protected $fillable = ['code', 'service', 'currency'
                           ,'booking_price', 'markup_price', 'total_price'
                           , 'total_paid', 'payment_type', 'status', 'is_payment_paid'
                           , 'finalized_date', 'voucher_date'
                           , 'invoice_date', 'invoice_code', 'invoice_details'
                           , 'payment_deadline_date', 'service_start_date', 'service_end_date'
                           , 'cancelled_date', 'cancellation_charges', 'refund_amount'
                           , 'booked_by_user_name','booked_by_user_id', 'agency_id', 'agency_name',  'distributor_id'
                           , 'lead_passenger_details', 'exchange_rates'
                        ];


    protected $casts = [
        'lead_passenger_details' => 'array',
        'invoice_details' => 'array',
        "exchange_rates"=> "array"
    ];

    public function bookingItems()
    {
        return $this->hasMany(BookingServiceDetail::class, 'booking_id');
    }


    public function voucherDetails()
    {
        $this->startLog("voucherDetails");

        /* $booking = static::where('id', $booking_id)->with("bookingItems")->first(); */
        if( !empty($this->bookingItems) ) {
            $this->bookingItems->each(function($bookingItem) {
                $bookingItem->prepareServiceDetails();
            });
        }
        $this->endLog("voucherDetailsByd");
        return $this;
    }

    public static function detailsById($booking_id)
    {
        return static::where('id', $booking_id)->with("bookingItems")->first();
    }
    /**
     * 1. create booking
     * 2. create respective booking details
     */
    public static function addBooking($bookingDetails)
    {
        $finalStatus = "confirmed";
        $service = array_get($bookingDetails, 'service');
        $booking = self::createBookingWith($bookingDetails);
        //create subservice details
        foreach ($bookingDetails['booking_items'] as $bookingItem) {
            $bookingItem = BookingServiceDetail::createBookingItemForBooking($booking, $bookingItem);
            if( !empty($bookingItem) && $bookingItem->isNotConfirmed()) {
                $finalStatus = $bookingItem->status;
            }
        }
        $booking->update(['status'=>$finalStatus]);
        return static::detailsById($booking->id);
    }

    protected static function agencyReferenceDetails(&$data)
    {
        $userLoggedIn = \Auth::user();
        $agencyDetails = TravelAgency::agencyById($userLoggedIn->agency_id); //distributorIdForAgency($userLoggedIn->agency_id);

        $data['booked_by_user_name'] = $userLoggedIn->name;
        $data['booked_by_user_id'] = $userLoggedIn->id;
        $data['agency_id'] = $userLoggedIn->agency_id;
        $data['agency_name'] = $agencyDetails->name;
        $data['distributor_id'] = $agencyDetails->distributor_id;
    }

    protected static function createBookingWith(&$bookingDetails)
    {
        //return $bookingDetails;
        $booking_fields = ['agency_reference',  'service'
                            ,'total_price', 'booking_price', 'markup_price' , 'currency'
                            , 'payment_type'
                            ,'total_paid', 'status'
                            ,'lead_passenger_details', 'booking_notes'
                            ,'payment_deadline_date', 'service_start_date', 'service_end_date'
                            , 'exchange_rates'
                        ];

        $data = collect($bookingDetails)->except(['booking_items', 'booking_date'])->all();

        //prepare agency details required for this booking
        self::agencyReferenceDetails($data);

        $data['code'] = Code::getNextCodeForModule("bookings");

        $booking = Booking::create($data);
        $booking->generateInvoiceCodeIfConfirmed();
        return $booking;
    }


    public function generateInvoiceCodeIfConfirmed()
    {
        if($this->isConfirmed()) {
            $this->update([
                    'invoice_date' => \Carbon\Carbon::now(),
                    'invoice_code' => Code::getNextCodeForModule("invoices")
                    ]
                );
        }
    }

}
