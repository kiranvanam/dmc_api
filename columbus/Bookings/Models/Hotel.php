<?php

namespace Columbus\Bookings\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Columbus\Bookings\Utilities\GrnConnect;

class Hotel extends Model
{
    
    public $timestamps = false;
    
    protected $table = "hotel_bookings";

    protected $fillable = [ 'booking_id', 'service_source','currency', 'total_price', 'status', 'total_rooms','country_code', 
                            'destination_name', 'check_in', 'check_out', 'cancelled_date', 'payment_deadline_date', 'hotel_name', 'total_pax', 'star_category', 'accommodation_details'
                        ]; 
    
    protected $dates = [ 'cancelled_date', 'payment_deadline_date', 'check_in', 'check_out'] ;                      
    protected $casts = [
        'accommodation_details' => 'array'
    ];

    public static function addHotelBooking( $hotelDetails, $booking) 
    {
        
        $hotelDetails['booking_id'] = $booking['id'];    

        return self::create($hotelDetails);
    }

    /**
     * why do we need cancelled date ? 
     * 1. 
     * 2.  
     */
    function cancel() 
    {
        /**
         *  if p
         */
        if($this->payment_deadline_date < Carbon::now() ) {            
            $this->save([ 'cancelled_date' => Carbon::now()]);
        } else {
            if($this->service_source == "grn-connect") {
                GrnConnect::cancelBooking($this);
            }
        }

    }
    
    
}