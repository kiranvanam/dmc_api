<?php

namespace Columbus\Bookings\Models;

use Illuminate\Database\Eloquent\Model;

class Issue extends Model
{
    protected $table = "booking_issues";

    public $timestamps = false;

    protected $fillable = ['booking_id', 'category', 'reported_by_user', 'subject', 'message', 'reported_date'];    
}
