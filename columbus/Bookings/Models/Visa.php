<?php

namespace Columbus\Bookings\Models;

use Illuminate\Database\Eloquent\Model;
use Columbus\Bookings\Utilities\BookingStatus;

class Visa extends Model
{
    use BookingStatus;
    
    public $timestamps = false;
    
    protected $table = "visa_bookings";


    protected $fillable = ['booking_id', 'service_source', "name", 'visa_id', 'visa_code', 
                            "vendor_id", 'vendor_code', 
                            "no_of_adults", "no_of_children", 'no_of_infants', 
                            "currency", "total_price", "unit_pricing", "pax_details", 
                            "status", "country_code", "country_name",
                            "payment_deadline_date", 
                            "cancelled_date", "cancellation_charges", "cancellation_details",
                            "visa_details", 'markup', "voucher_detais",
                            "document_paths"
                        ];
    protected $dates = [ 'cancelled_date', 'payment_deadline_date', 'check_in', 'check_out'] ;                      
    protected $casts = [
        "visa_details" => "array",
        "markup" => "array",
        "voucher_details" => "array",
        "cancellation_details" => "array",
        "document_paths" => "array",
        "unit_pricing" => "array",
        "pax_details" => "array"
    ];



    public static function addBooking( $visaDetails, $booking) 
    {
        logger("Visa::addBooking - Start - Visa Booking details", $visaDetails);                        
        $visaDetails['status'] = static::finalStatusFor( array_get($visaDetails, 'status'));
        $visaDetails['booking_id'] = $booking['id'];
        $visaDetails['cancellation_charges'] = 0;
        $visa = self::create($visaDetails);

        logger("Visa::addBooking - End - Visa booking created id - " . $visa ? $visa->id : "Not found");

        return $visa;
    }
}