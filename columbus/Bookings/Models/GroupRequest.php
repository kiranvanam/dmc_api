<?php

namespace Columbus\Bookings\Models;

use Illuminate\Database\Eloquent\Model;

class GroupRequest extends Model
{
    protected $fillable = [ "country_id", "country_name", "nationality", "pax_details",
                        "travel_date", "no_of_nights", "budget_per_person", "currency",
                        "preferred_hotel_details", "tour_guide", "conference_facilities",
                        "other_services", "additional_requirements",
                        'agency_id', 'agency_name'
                    ];

    protected $casts = [
                "pax_details" => "array",
                "preferred_hotel_details" => "array",
                "other_services" => 'array',
            ];
}