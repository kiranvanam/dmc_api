<?php

namespace Columbus\Bookings\Utilities;

trait DMCConfig
{

    protected function config()
    {
        return config('dmc');
    }

    protected function dmcFolder()
    {
        return array_get( $this->config(), "gallery", "logo");
    }
    /*
     config file - dmc - contains the dmc name and locaiton where other files(logo..) are stored
     for emails - /images/image-path - dont work ,it must be wihtour /.
     */
    public function logoUrl()
    {
        $dmcFolder = $this->dmcFolder();
        return   "/images/$dmcFolder/logo.png";
    }

    public function logoUrlForEmail()
    {
        $dmcFolder = $this->dmcFolder();
        return   "images/$dmcFolder/logo.png";
    }
}