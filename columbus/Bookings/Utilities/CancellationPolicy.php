<?php

namespace Columbus\Bookings\Utilities;

use Carbon\Carbon;
use Columbus\Utilities\DateUtil;

class CancellationPolicy
{
    protected $travel_date = "";
    protected $booking_date = "";
    protected $dateFormat = "";


    function prepareCancellationPolicies($rules, $dates)
    {
        if( !isset($dates['booking_date']) ) {
            return [];
        }

        $this->booking_date = array_get( $dates, 'booking_date');
        $this->travel_date = array_get($dates, 'travel_date');

        if( empty($this->travel_date)) {
            return [];
        }
        $dateFormat= 'Y-m-d';
        $booking_date = $this->booking_date;
        $travel_date = $this->travel_date;

        $isAlreadyUnderCancellation = false;
        $rulesWithDates= [];
        collect($rules)
            ->first( function($ruleDetail)
                        use(&$rulesWithDates, $dateFormat, $travel_date, $booking_date, $isAlreadyUnderCancellation)  {
                if( $isAlreadyUnderCancellation )
                    return false;

                if ( isset( $ruleDetail['is_rule_for_cancellation_after_booking']) ) {
                    // check diff in dates between booking-date - last(but one) rule cancel-before
                    $ruleDetail['date'] = $booking_date;
                    $rulesWithDates = $ruleDetail;
                } else {
                    $date = $travel_date->copy()
                                    ->subDay($ruleDetail['cancel_before']);

                    if( $date->greaterThanOrEqualTo($booking_date) ) {
                        $ruleDetail['date'] = $date->toDateString();
                        $rulesWithDates[] = $ruleDetail;
                    } else if($date->lessThan($booking_date)) {
                        $ruleDetail["date"] = $booking_date->toDateString();
                        $rulesWithDates[] = $ruleDetail;
                        $isAlreadyUnderCancellation = true;
                    }
                }
                return false;
            });
        /*
            rulesWithDates = descening order [ travel-date to booking-date rules ]
            rulesWithDates = acending order [ booking-date to travel-date]
         */
        return $this->formatDuplicateDatedRules( $rulesWithDates);
    }

    /*
       rules = acending order is preferred way
       it removes duplicates rules -> when 2 immediate rules
                1. have same date
                2. same percent of cancellation charges
     */

    public function formatDuplicateDatedRules($rules) {
        if( empty($rules) )
            return [];

        $formattedRules  = [];
        $dateFormat = 'Y-m-d';

        $formattedRules[] =$rules[0];

        collect($rules)
            ->each(function($rule) use(&$formattedRules, $dateFormat, $rules) {
                $formattedRule = $formattedRules[ count($formattedRules) - 1 ];

                if( isset($formattedRule['date']) ) {

                    if ( DateUtil::dateForDateAndDateFormat($rule['date'],$dateFormat)
                            ->equalTo(DateUtil::dateForDateAndDateFormat($formattedRule['date'],$dateFormat))) {
                        if( $rule['amount'] > $formattedRule['amount'] ) {
                            $formattedRule['amount'] = $rule['amount'];
                        }
                    } else if ($rule['amount'] == $formattedRule['amount']) { // but amounts are same but dats are different, then consider the previous one
                        $formattedRule['amount'] = $rule['amount'];
                    } else {
                        $formattedRules[] = $rule;
                    }
                }
            });
        return $formattedRules;
    }

    public function mergeCancellationPoliciesForBooking($booking)
    {
        $cancellationPolicies = [];

        foreach ( array_get($booking, 'bookingItems') as $bookingItem) {
            $this->travel_date = array_get($bookingItem, 'travel_date');
            $cancellationPolicies[] =  array_get($bookingItem, 'cancellation_policies');
        }
        $cancellationPolicies = $this->mergeCancellationPolicies($cancellationPolicies);
        $cancellationPolicies = $this->formatDateForPolicies($cancellationPolicies);
        return $cancellationPolicies ;
    }

    public function formatDateForPolicies($rules, $outPutFormat='d-m-Y', $inputFormat="Y-m-d")
    {
        if( empty($rules))
            return [];
        $finalrules = [];
        foreach ($rules as $rule) {
            $rule['date'] = DateUtil::dateForDateAndDateFormat($rule['date'],$inputFormat)->format($outPutFormat);
            $finalRules[] = $rule;
        }

        return $finalRules;
    }

    public function mergeCancellationPolicies($rules) {
        //$rules = arrayOfRuleArrays; //JSON.parse(JSON.stringify(arrayOfRuleArrays))
        $finalRules = $rules[0];
        if(empty($finalRules))
            $finalRules = [];

        collect( $rules)
            ->each( function($policRules) use(&$finalRules) {
                    collect($policRules)
                        ->each( function($ruleDetails) use(&$finalRules) {
                                $this->insertRuleIntoRules( $finalRules, $ruleDetails);
                            });
                }
            );
        // needs to formatted
        return $this->formatDuplicateDatedRules($finalRules);

    }
    public function insertRuleIntoRules(&$rules, $newRule) {

        $insertPosition = -1;  //count($rules);
        $isItemToBeRaplaced = 0;
        $dateFormat ="Y-m-d";
        if( empty($rules) )
            $rules = [];
        foreach ($rules as $index => $currentRule) {
            $newDate = DateUtil::dateForDateAndDateFormat($newRule['date'],$dateFormat);
            //var_dump($currentRule, $newRule, $rules);
            $currentDate = DateUtil::dateForDateAndDateFormat($currentRule['date'],$dateFormat);
            if( $newDate->greaterThan($currentDate) ) {
                if( count($rules) -1 == $index ) {
                    if($newRule['amount'] > $currentRule['amount']) {
                        $insertPosition = count($rules);
                    }
                }
            }else if( $newDate->lessThan($currentDate)) {
                // either replace  or insert or ignore
                if( $index != 0 ) {
                    $previousRule = $rules[$index-1];
                    $previosDate =  DateUtil::dateForDateAndDateFormat($previousRule['date'],$dateFormat);
                    if( $newDate->between($previosDate, $currentDate))
                    {
                        if( $newRule['amount'] > $previousRule['amount']) {
                            if ($newRule['amount'] < $currentRule['amount']) {
                                $insertPosition= $index;
                            } else if ($newRule['amount'] > $currentRule['amount']) {
                                $isItemToBeRaplaced = 1;
                                $insertPosition = index;
                            }else {
                                $insertPosition = -1;
                            }
                        }
                    }
                }else {
                    if ($newRule['amount'] > $currentRule['amount']) {
                        $isItemToBeRaplaced = 1;
                    }
                    $insertPosition = $index;
                }
            } else if( $newDate->equalTo( $currentDate)) {
                if( $newRule['amount'] > $currentRule['amount']) {
                    $insertPosition = $index;
                    $isItemToBeRaplaced = 1;
                } else if ($newRule['amount'] == $currentRule['amount'] ) {
                    $insertPosition = -1;
                }
                break;
            }
        }

        //if new rule insert at the end
        if( $insertPosition > -1 ){
            array_splice( $rules, $insertPosition, $isItemToBeRaplaced, [ $insertPosition=> $newRule]);

             /* $rules =   array_slice($rules, 0, $insertPosition, true) +
                         [$insertPosition => $newRule ] +
                         array_slice($rules, $insertPosition + 1, NULL, true);
            */
        }
        return $rules;
    }

    public function cancellationChargesOnCancellingToday($rules, $totalAmount, $dateFormat="Y-m-d" )
    {
        if( empty($rules))
            return $totalAmount;
        $cancelledDate = Carbon::now();
        $rules = collect($rules);
        $matchedRule = $rules->first( function($rule) use($cancelledDate, $dateFormat) {
                                $date = array_get($rule, 'date');
                                if(!empty($date) ){
                                    $ruleDate = DateUtil::createDateWith($date, $dateFormat);
                                    return $cancelledDate->greaterThanOrEqualTo($ruleDate);
                                }
                            });

        if(!empty($matchedRule)) {
            // 300 * 20/100
            logger("Matched Cancelled Polocy: ", $matchedRule);
            $amount = array_get($matchedRule,'amount', 1);

            if(array_get($matchedRule,'amount_type') == 'percent') {
                $percent = roundUp($amount/ 100 , 2);
                $amount = roundUp($percent * $totalAmount);
            }
            logger("Cancellation charges : $amount");
            return $amount;
        }
        return 0;
    }
}