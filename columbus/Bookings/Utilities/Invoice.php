<?php

namespace Columbus\Bookings\Utilities;

trait Invoice
{

    public function invoiceSettings()
    {
        return $this->invoice_details;
    }

    public  function grossTotalSettings()
    {
        return array_get($this->invoiceSettings(), 'gross_total');
    }

    public function bookingDate($format = 'd-m-Y') 
    {
        return $this->created_at->format($format);
    }

    public function invoiceDate()
    {
        if( empty($this->invoice_date))
            return "";

        return $this->invoice_date->format( 'd-m-Y' );
    }

    public function invoiceCode()
    {
        return $this->invoice_code;
    }

    public function cancellationPolicies()
    {

        $cancellationPolicyUtility = new CancellationPolicy;

        return $cancellationPolicyUtility->mergeCancellationPoliciesForBooking($this);
    }

    public function invoiceNotes()
    {
        return array_get( $this->invoiceSettings(), 'invoice_notes');
    }

}