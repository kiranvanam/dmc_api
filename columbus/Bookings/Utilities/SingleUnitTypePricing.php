<?php

namespace Columbus\Bookings\Utilities;

trait SingleUnitTypePricing
{
    function totalUnitsPrice() {
        return roundUp( $this->unitPrice() * $this->noOfUnits());
    }

    function  unitPrice() {
        return $this->convertAmount( $this->totalUnitBasePrice());
    }

    function totalUnitBasePrice() {
        return $this->addMarkupAndTaxesToBasePrice( $this->unitBasePrice());
    }

    function unitBaseMarkupPrice() {
        return $this->markupValueForAmount($this->unitBasePrice());
    }

    function noOfUnits()
    {
        return   array_has($this->unitPricing(), 'no_of_units') 
                    ? array_get($this->unitPricing(), 'no_of_units')
                    : 1;
    }

    function unitBasePrice() 
    {
        return array_get($this->unitPricing(), 'price', 0);
    }
}