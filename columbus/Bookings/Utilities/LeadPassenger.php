<?php

namespace Columbus\Bookings\Utilities;

trait LeadPassenger
{

    public function leadPassenger()
    {
        return $this->lead_passenger_details;
    }

    public function leadPassengerName() {
        return  array_get($this->leadPassenger(), 'title'). " . "
                . array_get($this->leadPassenger(),'name') . "  "
                . array_get($this->leadPassenger(), 'surname');
    }

    public function leadPassengerContactNumber() {
        return  array_get($this->leadPassenger(), 'phone_number');
    }

    public function leadPassengerEmail() {
        return  array_get($this->leadPassenger(), 'email');
    }

    public function leadPassengerNationality()
    {
        return  array_get($this->leadPassenger(), 'nationality');
    }
}