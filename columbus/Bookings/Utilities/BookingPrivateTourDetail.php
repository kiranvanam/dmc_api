<?php

namespace Columbus\Bookings\Utilities;

trait BookingPrivateTourDetail
{


    public function privateTourTitleDetails()
    {
        return $this->firstBookingItemTourDetails();
    }

    public function firstBookingItemTourDetails()
    {
        $bookingItem = $this->bookingItems->first();
        if( empty($bookingItem))
            return NULL;

        return $bookingItem->isPrivateTourService();
    }

    public function isPrivateTour()
    {
        return !empty($this->firstBookingItemTourDetails());
    }

    public function isBookingItemPrivateTour($bookingItem)
    {

        if( $bookingItem instanceof \Columbus\Bookings\Models\BookingServiceDetail)
            return $bookingItem->isPrivateTourService();
        return false;

    }

    public function isBookingItemPrivateTourAtIndex($index)
    {
        $bookingItem = $this->bookingItems
                            ->filter( function($bookingItem, $itemIndex) use($index) {
                                        if($index == $itemIndex)
                                            return true;
                                        return false;
                                });

        return $this->isBookingItemPrivateTour($bookingItem->first());
    }


    public function isFirstItemForPrivateTour($bookingItem,$currentIndex)
    {
        // previousIndex = currentIndex-1 nextIndex= currentIndex + 1
        if( $currentIndex ==0 ) {
            return $this->isBookingItemPrivateTour($bookingItem);
        }
        if( $currentIndex > 0) {
            $isCurrentOnePrivateTour = $this->isBookingItemPrivateTour($bookingItem);
            $isPreviousOnePrivateTour = $this->isBookingItemPrivateTourAtIndex($currentIndex-1);
            return $isCurrentOnePrivateTour && !$isPreviousOnePrivateTour;

        }
        return false;
    }
}