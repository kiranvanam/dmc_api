<?php

namespace Columbus\Bookings\Utilities;

trait PointOrSlotDetail
{
    function pointDetails()
    {
        return array_get($this->other_details, 'point_details', []);
    }

    public function pickUpTime()
    {
        return array_get($this->pointDetails(), 'pick_up_time', '');
    }

    public function pickUpPointDetails()
    {
        return array_get($this->pointDetails() ,'pick_up_point_details', '');
    }

    public function dropOffPointDetails()
    {
        return array_get($this->pointDetails() ,'drop_off_point_details', '');
    }
}