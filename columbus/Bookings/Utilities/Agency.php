<?php

namespace Columbus\Bookings\Utilities;

trait Agency
{
    public function agencyDetails()
    {
        return  array_get($this->invoiceSettings(), 'agency_details', []);
    }

    public function agencyName()
    {
        return array_get( $this->agencyDetails(), 'name');
    }

    public function agencyAddress()
    {
        return array_get( $this->agencyDetails(), 'address');
    }

    public function agencyCode()
    {
        return array_get( $this->agencyDetails(), 'registration_code');
    }

    public function agencyUserBookedBy()
    {
        return $this->booked_by_user_name;
    }
}