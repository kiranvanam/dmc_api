<?php

namespace Columbus\Bookings\Utilities;

class GrnConnect
{
    public function paraseHotelDetails(&$details) {

        $booking_details = $details['booking_details'];
    

        $hotelDetails = [];

        $hotelDetails['service_source'] = $details['service_source'];
        $hotelDetails['check_in'] = $booking_details['checkin'];
        $hotelDetails['check_out'] = $booking_details['checkout'];
        $hotelDetails['total_price'] = $booking_details['price']['total'];
        $hotelDetails['pax_details'] = $this->getPaxDetails($booking_details);
        //$hotelDetails['booking_details'] = $booking_details;
        $hotelDetails['hotel_details'] = $this->getHotelDetails($booking_details);
        $hotelDetails['price_details']= "";
        $hotelDetails['other_details'] = $this->getOtherDetails($booking_details);

        return $hotelDetails;
    }

    protected function getHotelDetails(&$booking_details)
    {
        $hotel = $booking_details['hotel'];
        $fields = ['name', 'hotel_code', 'description', 'city', 'category'];

        return collect($hotel)->only($fields)->all();
    }

    protected function getOtherDetails(&$booking_details) {

        $fields = ['booking_id', 'booking_reference', 'booking_date', 'booking_comment', 'currency'];
        
        return collect($booking_details)->only($fields)->all();
    }

    protected function getPaxDetails(&$booking_details) {
        $paxDetails = $booking_details['hotel']['paxes'];
        $total = count($paxDetails);

        $adults = 0;
        $children = 0;

        for ($i=0; $i < $total ; $i++) { 
            if($paxDetails[$i]['type'] == 'AD')
                ++$adults; 
            else
                ++$children;
        }

        if($adults || $children)
            $total .= "(";
        
        $total .= $adults ? "$adults(A)-" : "";
        $total .= $children ? "$children(C)" : "";
        
        if($adults || $children)
            $total .= ")";

        return [
              'total_pax' =>  $total,
              'pax_details'=> $paxDetails
        ];
    }
}