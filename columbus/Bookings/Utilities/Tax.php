<?php

namespace Columbus\Bookings\Utilities;

trait Tax
{

    public function tax() 
    {
        return array_get($this->grossTotalSettings(), 'tax');
    }
    
    
    public function isTax()
    {
        return $this->isTaxOnMarkup() || $this->isTaxOnNetTotal();
    }

    public function isTaxOnMarkup()
    {
        return $this->tax() == 'on-markup';
    }
    
    public function isTaxOnNetTotal()
    {
        return $this->tax() == 'on-net';
    }

    public function taxes()
    {
        return array_get($this->invoiceSettings(), 'tax_details');
    }

    public function areTaxesIntoProductCost()
    {
        return array_get($this->grossTotalSettings(), 'is_tax_added_to_product_cost');
    }

    public function areTaxesDisplayedSeperatly()
    {
        if ($this->isTax() && $this->areTaxesIntoProductCost())
            return 0;

        return $this->isTax();
    }

    public function taxDetails()
    {
        if( ! $this->isTax() )
            return NULL;

        $taxes = $this->taxes();
        $taxableAmount = $this->taxableAmount();            
        foreach($taxes as $tax) 
        {
            $tax['value'] = roundUp( $taxableAmount * ( $tax['amount']/ 100));                                  
        }
        return $taxes;
    }
}
