<?php

namespace Columbus\Bookings\Utilities;

use Columbus\Utilities\DateUtil;

trait GRNPricing
{
    function grnRooms()
    {
        return array_get($this->unitPricing(), 'rooms' , []);
    }

    function hotelStatus()
    {
        //if($this->isSourceGRN())
        return array_has($this->other_details, 'booking_status')
                    ? array_has($this->other_details, 'booking_status')
                    : array_get($this->other_details, "status", "failed");
    }

    function isHotelCancelled()
    {
        return array_has($this->other_details, 'cancellation_details.cancellation_charges');
    }

    function isHotelNotCancelled()
    {
        return $this->isHotelCancelled() == false;
    }

    function isHotelBookingRequestPending()
    {
        return $this->hotelStatus() == "pending";
    }

    function isHotelBookingRequestConfirmed()
    {
        return $this->hotelStatus() == "confirmed";
    }

    function hotelCheckIn()
    {
        $checkin = array_get($this->other_details, 'checkin');

        return empty($checkin) ? "Invalid date" : DateUtil::dateStringForDate($checkin);
    }

    function hotelCheckOut()
    {
        $checkout = array_get($this->other_details, 'checkout');
        return empty($checkout) ? "Invalid date" : DateUtil::dateStringForDate($checkout);
    }

    function grnStarCategory()
    {
        return array_get($this->other_details, "hotel.category", 0);
    }

    function hasGRNHotelVoucherNotGenerated()
    {
        return !array_has($this->other_details, 'voucher_details')
                || array_has($this->other_details, 'voucher_details.errors');

    }

    function hasGRNHotelVoucherDetailsNotFetched()
    {
        return !array_get($this->other_details, 'voucher_details');
    }

    function grnHotelVoucherDataFor($path, $default = "")
    {
        return array_get($this->other_details, "voucher_details.$path", $default);
    }

    function grnHotelAddress()
    {
        return $this->grnHotelVoucherDataFor('hotel.address');
    }

    function grnHotelEmail()
    {
        return $this->grnHotelVoucherDataFor('hotel.email');
    }

    function grnHotelContact()
    {
        return $this->grnHotelVoucherDataFor('hotel.phone_number');
    }

    function grnHotelSupplierReference()
    {
        return $this->grnHotelVoucherDataFor('supplier_reference');
    }
    function grnVoucherBookingItems()
    {
        return $this->grnHotelVoucherDataFor("hotel.booking_items", $default = []);
    }
    function grnRoomsForBookingItem($bookingItem)
    {
        return array_get($bookingItem, "rooms", []);
    }

    function grnRoomReferenceFor($room)
    {
        return array_get($room, "room_reference");
    }

    function grnRoomInclusitonsForRoom($room)
    {
        return array_get($room, 'inclusions', ["Room only"]);
    }

    function grnVoucherGuestDetails()
    {
        return $this->grnHotelVoucherDataFor('hotel.paxes', []);
    }

    function grnUserName($pax =[])
    {
        return array_get($pax, 'title', '' ) . " "
                    .  array_get($pax, 'name', '') . " "
                    . array_get($pax, 'surname', '');
    }

    function grnHolderName()
    {
        return $this->grnHotelVoucherDataFor('holder.title') . ""
                . $this->grnHotelVoucherDataFor('holder.name') . ""
                . $this->grnHotelVoucherDataFor('holder.surname');
    }

    function grnHolderEmail()
    {
        return $this->grnHotelVoucherDataFor('holder.email');
    }

    function grnHolderContact()
    {
        return $this->grnHotelVoucherDataFor('holder.phone_number');
    }

    function grnHotelDescription()
    {
        return  $this->grnHotelVoucherDataFor('hotel.description');
    }
    /*
    function grnTotalPrice() {
        return roundUp( $this->grnUnitPrice());
    }

     function  grnUnitPrice() {
        return $this->convertAmount( $this->grnTotalUnitBasePrice());
    }

    function grnTotalUnitBasePrice() {
        return $this->addMarkupAndTaxesToBasePrice( $this->unitBasePrice());
    }

    function unitBaseMarkupPrice() {
        return $this->markupValueForAmount($this->grnUnitBasePrice());
    }

    function grnUnitBasePrice()
    {
        return array_get($this->unitPricing(), 'price', 0);
    }
    */

    function grnRoomName($room)
    {
        return  array_get($room,'room_type') . "-". array_get($room,'boarding');
    }

    function grnTotalForRoom($room)
    {
        return array_has($room, 'no_of_rooms') ? array_get($room, 'no_of_rooms') : array_has($room, 'no_of_units');
    }

    function grnTotalRooms() {
        return count($this->grnRooms());
    }
}