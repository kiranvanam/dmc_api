<?php

namespace Columbus\Bookings\Utilities\Booking;

use Columbus\Bookings\Utilities\CancellationPolicy;

trait Cancel
{
    function cancelBookingServices()
    {
        $this->startLog();
        $this->log("Boking of Service : " . $this->service);
        $this->bookingItems->each(function($bookingItem){
            $bookingItem->cancel();
        });
        $this->endLog();
        return $this;
    }

    public function updateCancellationCharges()
    {
        $total_cancellation_charges = 0;
        $final_status = "cancelled";

        $this->bookingItems->each(function($bookingItem) use(&$final_status, &$total_cancellation_charges) {
            if($bookingItem->isNotCancelled()) {
                $final_status = "cancellation-failed";
            }
        });

        $total_cancellation_charges = $this->totalCancellationCharges();

        $this->status = $final_status;
        $this->cancellation_charges = $total_cancellation_charges;
        $this->refund_amount =  roundUp($this->total_price - $total_cancellation_charges);
        $this->log("Total Booking cancelltion charges: ". $total_cancellation_charges);
        $this->save();
        return $this;
    }

    public function totalCancellationCharges()
    {
        $cancellationPolicyUtility = new CancellationPolicy;

        $policies = $cancellationPolicyUtility->mergeCancellationPoliciesForBooking($this);
        // now date is in the following format, so provide the format of date string
        $dateFormat = 'd-m-Y';
        return $cancellationPolicyUtility->cancellationChargesOnCancellingToday($policies, $this->total_price, $dateFormat);
    }
}