<?php

namespace Columbus\Bookings\Utilities;

trait MarkUp
{
    
    public function markUp()
    {
        return array_get($this->grossTotalSettings(), 'markup', 'no');
    }

    public function isMarkup() {
        return  $this->markUp() == 'yes';
    }

    public function isMarkupUpIntoProductCost() {
        return array_get($this->grossTotalSettings(), 'is_markup_added_to_product_cost', 0);
    }

    
    public function isMarkupDisplayedSeperatly() {

        if( $this->isMarkup() && $this->isMarkupUpIntoProductCost())
            return 0;
            
        return $this->isMarkup();
    }

    /*
       it calculates all product markups & returns one markup value with total markup_prices 
     */
    public function markupDetails() {
        if( $this->isMarkup() == 0 )
            return NULL;
        
        $markup = NULL;

        foreach($this->bookingItems() as $bookingItem) 
        {
            if( empty($markup)) {
                $markup = array_get($bookingItem, 'markup_details');
                $markup['value'] = 0;
            }

            $markup['value'] += $this->convertAmount($bookingItem['markup_price'], $bookingItem['currency']);
        }
        if($markup)
            $markup['value'] = roundUp($markup['value']);

        return $markup;
    }

    public function netMarkUp() {
        $markupDetails = $this->markupDetails();
        return array_get( $markupDetails, 'value', 0);
    }
}