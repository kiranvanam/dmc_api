<?php

namespace Columbus\Bookings\Utilities;

trait BookingStatus
{

    public function isConfirmed()
    {
        return  strtolower($this->status) == "confirmed";
    }

    public function isNotConfirmed()
    {
        return $this->isConfirmed() == false ;
    }

    public function isPending()
    {
        return strtolower($this->status) == "pending";
    }

    public function isUnknown()
    {
        return strtolower($this->status) == "unknown";
    }

    public function isCancelled()
    {
        return strtolower($this->status) == "cancelled";
    }

    public function isNotCancelled()
    {
        return $this->isCancelled() != true;
    }

    public static function finalStatusFor($status)
    {
        if(empty($status))
            return "pending";

        return  ($status == "on-request") ? "pending" : "confirmed";
    }
}