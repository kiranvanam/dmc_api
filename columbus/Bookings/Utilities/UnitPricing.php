<?php

namespace Columbus\Bookings\Utilities;

trait UnitPricing
{
    public function unitPricing()
    {
        return $this->unit_pricing;
    }
    /*
     $booking is required for few helpers
      1. isTax and isTaxAddedToProductCost
      2. isMarkup & isMarkupAddedToProductCost
     */
    public function setBooking(&$booking)
    {
        $this->booking = $booking;
    }

    public function baseCurrency()
    {
        return array_get($this->unitPricing(), 'currency');
    }


    /*
        total booking item price
     */
    public function totalPrice()
    {
        if( $this->isPricingPerPax())
            return $this->totalPaxPrice();
        else if($this->isPricingSingleUnitType()) {
            return $this->totalUnitsPrice();
        }
    }

    public function isPricingPerPax()
    {
        return  !is_null(array_get($this->unitPricing(), 'adult_price'));
    }

    public function isPricingSingleUnitType() {
        return !$this->isPricingPerPax();
    }

    public function isPricingHotelGRN()
    {
        return $this->isSourceGRN();
    }

        /*
        to calculate total price base price with markup & product cost to product cost as per invoice settings
        base_price
        markup_price ( if is markup part of unit pricing)
        total_tax_price ( if tax is to be included in product price )
     */
    public function addMarkupAndTaxesToBasePrice($basePrice)
    {
        if(!$basePrice)
            return 0;
        $totalMarkup = 0;
        $totalTax = 0;

        if ($this->booking->isMarkupUpIntoProductCost()) {
            $totalMarkup = $this->markupValueForAmount($basePrice);
        }

        if ($this->booking->areTaxesIntoProductCost()) {
            $totalTax = 0;
            if ($this->booking->isTaxOnMarkup()) {
                $totalTax = $this->taxValueForAmount($totalMarkup);
            }
            if ($this->booking->isTaxOnNetTotal()) {
                $totalTax = $this->taxValueForAmount($basePrice);
            }
        }

        $total = roundUp( $basePrice + $totalMarkup + $totalTax);
        return $total;
    }

    public function markupValueForAmount($amount)
    {
        return $this->booking->calculateValueForAmountFromAmountTypeDetails($this->markup_details,
                                                                    $amount,
                                                                    $this->baseCurrency()
                                                                );
    }

    public function taxValueForAmount($amount)
    {
        $totalTax = 0;
        foreach($this->booking->taxes() as  $taxDeail) {
            $totalTax += $this->booking->calculateValueForAmountFromAmountTypeDetails($taxDeail,
                                                                            $amount,
                                                                            $this->baseCurrency()
                                                                        );
        }
        return $totalTax;
    }

}