<?php

namespace Columbus\Bookings\Utilities;

trait TotalPricing
{
    
    public function netTotal()
    {
        $total = 0;

        foreach ($this->bookingItems as $bookingItem) {
            $bookingItem->setBooking($this);
            $total += $bookingItem->totalPrice();
        }
        return  roundUp($total, 0);
    }

    public function totalMarkup()
    {
        return $this->totalServiceCharge();
    }


    public function grossTotal() {

        $total= $this->netTotal();

        if( $this->isMarkupDisplayedSeperatly() )
            $total += $this->totalMarkup();
        
        if( $this->areTaxesDisplayedSeperatly() )
            $total += $this->totalTaxAmount();
        

        return roundUp($total);
    }

    public function totalTaxAmount()
    {
        $taxes = $this->taxes();
        $total = 0;            
        foreach($taxes as $tax) 
        {
            $total += roundUp( array_get($tax, 'value',0));                                  
        }
        return $total;
    }

    public function taxableAmount() 
    {
        if ($this->isTaxOnNetTotal())
            return $this->netTotal();

        if ($this->isTaxOnMarkup()  )
            return $this->totalMarkup();
        //dd($this->isTaxOnNetTotal(), $this->isTaxOnMarkup());
        return 0;
    }

    public function totalServiceCharge()
    {
        $total = 0;

        foreach ($this->bookingItems as $bookingItem) {
            $bookingItem->setBooking($this);
            $total += $bookingItem->totalMarkup();
        }
        return roundUp($total,0);
    }

    
}