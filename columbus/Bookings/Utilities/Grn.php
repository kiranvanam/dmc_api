<?php

namespace Columbus\Bookings\Utilities;

trait Grn
{
    public function bookingReferenceGRN()
    {
        //dd($this->accommodation_details, array_has( $this->accommodation_details, 'booking_reference') );
        if( array_has( $this->accommodation_details, 'booking_reference'))
             return array_get( $this->accommodation_details, 'booking_reference' );
        
        return array_get(array_get($this->accommodation_details, 'other_details'), 'booking_reference');
    }


    public function isGRNBooking(Type $var = null)
    {
        return $this->service_source == "grn-connect";
    }
}