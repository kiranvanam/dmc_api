<?php

namespace Columbus\Bookings\Utilities;

trait DMC
{
    // helpers
    public function dmcDetails() 
    {
        return  array_get($this->invoiceSettings() , 'dmc_details', []);
    }

    public function dmcAddress()
    {
        return  array_get($this->dmcDetails(), 'address');
    }

    public function dmcCodeName()
    {
        return array_get($this->dmcDetails(), 'registration_code_name');
    }

    public function dmcCodeNumber()
    {
        return  array_get($this->dmcDetails(), 'registration_code_number');
    }

    public function dmcInvoiceBankAccountDetails()
    {
        return array_get( $this->dmcDetails(), 'bank_accounts', []);
    }

    public function countryNameById($country_id)
    {
        $country = \Columbus\Destination\Models\Country::where('id', $country_id)->first();

        if( empty($country) )
            return $country_id;

        return "$country->name ( $country->cca3) " ;
    }
}