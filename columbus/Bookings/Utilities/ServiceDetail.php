<?php

namespace Columbus\Bookings\Utilities;

trait ServiceDetail
{

    public function productCode()
    {
        $code = $this->service_code;
        if( !empty($this->vendor_code) )
            $code = $code  ." / " . $this->vendor_code;

        return $code;
    }

    public function otherDetails()
    {
        return $this->other_details;
    }

    public function serviceDetails()
    {
        if( empty(array_get($this->other_details,'service_details'))) {
            $this->prepareServiceDetails();
        }
        $serviceDetails = array_get($this->other_details,'service_details');
        // for transfer-services
        if( $this->isTransferService()) {
            $serviceDetails = array_get($serviceDetails,  $this->isPrivateTransfer() ? "private_details" : 'sic_details');
        }
        return $serviceDetails;
    }

    /* for private tours */
    public function privateTourTitleDetails() {

        if( ! $this->isTourTitleDetailsLoadedForPrivateTour())
            $this->serviceDetails(); // $this->prepareServiceDetails()

        return array_get( $this->otherDetails(), 'tour_title_details');
    }

    public function isTourTitleDetailsLoadedForPrivateTour()
    {
        return array_get( $this->otherDetails(), 'tour_title_details');
    }

    /* this is for guide or tour title or ticket title  */
    public function titleDetails()
    {
        return $this->serviceDetails();
    }

    public function titleDescription()
    {
        return array_get( $this->titleDetails(), 'description');
    }


    public function vendorDetails()
    {
        // this is actully the vendor product detaills not stored into the DB
        //but to preseve this value in serializing models of Email seding Invoice/Booking detials/Voucher
        if( empty($this->titleDetails())) {
            $this->prepareServiceDetails();
        }
        /* for trasnfers not vendor, it's direct */
        $subProperity = "vendor";

        if( $this->isTransfer())
            return $this->serviceDetails();

        if( $this->isVisa())
             $subProperity = "details";
        if( $this->isMeal()) {
            $subProperity = "mealPlan";
        }
        /* except trasnfer and  restaurant, all other have same format like
            other_details:{
                service_details:{
                    //title details
                    id: ''
                    name:'',
                    description:'',
                    //vendor service details
                    vendor:{

                    }
                }
            }
        */
        return array_get($this->titleDetails(), $subProperity);
    }

    public function description()
    {
        if( array_has($this->vendorDetails(), "itinerary"))
            return array_get($this->vendorDetails(), 'itinerary');

        return array_get($this->vendorDetails(), 'description');
    }

    public function inclusions()
    {
        return array_get($this->vendorDetails(), 'inclusions');
    }

    public function exclusions()
    {
        return array_get($this->vendorDetails(), 'exclusions');
    }

    public function termsAndConditions()
    {
        return array_get($this->vendorDetails(), 'terms_and_conditions');
    }

    public function remarks()
    {
        return array_get($this->vendorDetails(), 'remarks');
    }

    public function docsRequired()
    {
        return array_get( $this->vendorDetails(), 'docs_required', 'No');
    }

}