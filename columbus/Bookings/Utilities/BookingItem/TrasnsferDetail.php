<?php

namespace Columbus\Bookings\Utilities\BookingItem;

trait TrasnsferDetail
{
    public function vehicleDetails()
    {
        if( empty( array_get( $this->otherDetails(), 'vehicle_details') )) {
            $this->prepareServiceDetails();
        }
        return array_get( $this->otherDetails(), 'vehicle_details');
    }

    public function vehicleName()
    {
        return array_get($this->vehicleDetails(), 'name');
    }

    public function vehicleDescription()
    {
        return array_get($this->vehicleDetails(), 'description');
    }

    public function passengersOnly()
    {
        return array_get( $this->vehicleDetails(), 'passengers_only' );
    }

    public function passengersWithLuggage()
    {
        return array_get( $this->vehicleDetails(), 'passengers_with_luggage');
    }

    public function noOfSmallBags()
    {
        return array_get( $this->vehicleDetails(), 'small_bags');
    }

    public function smallBagWeight()
    {
        return array_get( $this->vehicleDetails(), 'small_bag_weight');
    }

    public function noOfLargeBags()
    {
        return array_get( $this->vehicleDetails(), 'large_bags');
    }

    public function largeBagWeight()
    {
        return array_get( $this->vehicleDetails(), 'large_bag_weight');
    }
    /* thes are defined in PointOrSlotDetail trait */
    /* public function pointDetails()
    {
        return array_get( $this->otherDetails(), 'point_details');
    }

    public function pickUpTime()
    {
        return array_get( $this->pointDetails(), 'pick_up_time');
    }

    public function pickUpPointDetails()
    {
        return array_get( $this->pointDetails(), 'pick_up_point_details');
    }

    public function dropOffPointDetails()
    {
        return array_get( $this->pointDetails(), 'drop_off_point_detials');
    } */
}