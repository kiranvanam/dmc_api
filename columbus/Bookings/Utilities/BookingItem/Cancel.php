<?php

namespace Columbus\Bookings\Utilities\BookingItem;

use Columbus\Bookings\Repositories\HotelRepository;
use Columbus\Bookings\Utilities\CancellationPolicy;

trait Cancel
{
    public function cancel()
    {
        $this->startLog("cancel");
        $this->log(" service : $this->service  ( $this->sub_service )");
        if($this->isHotel()) {
           $this->cancelHotel();
        }else{
            $this->cancelContractProduct();
        }
        $this->log("Cancellation status: ". $this->status);
        $this->releaseProductToInventory();

        $this->endLog("cancel");

        return $this;
    }

    /**
     * this updates 3 coloumns
     * "status"
     * cancellation_charges,
     *  other_details['cancellation_details']
     */
    function cancelHotel()
    {
        $this->startLog("cancelHotel");
        $other_details = $this->other_details;
        $cancellationDetails = HotelRepository::cancelBooking($this);
        $cancellation_charges = array_get($cancellationDetails, 'cancellation_charges.amount', 0);
        $status = array_get($cancellationDetails, 'status', 'failed');
        if($status == "confirmed"){
            $status = "cancelled";
        }elseif($status == "already-cancelled"){
            $cancellation_charges = $this->calculateCancellationCharges();
            $status = "cancelled";
        }
        else{
            $status = "cancellation-" . $status;
        }

        //update bookingitem
        $status = $status;
        $cancellation_charges = $cancellation_charges;
        $other_details['cancellation_details'] = $cancellationDetails;
        $this->update([
                    "status" => $status,
                    "cancellation_charges" => $cancellation_charges,
                    "other_details" => $other_details
                ]);
        $this->endLog("cancelHotel");
    }

    function cancelContractProduct()
    {
        $this->status = "cancelled";
        $this->cancellation_charges = $this->calculateCancellationCharges();
        $this->save();
    }

    function releaseProductToInventory()
    {
        //update inventory count for the respective product
        return $this;
    }

    function calculateCancellationCharges()
    {
        if($this->isVisa()) {
            return $this->total_price;
        }
        $policy = new CancellationPolicy;
        return $policy->cancellationChargesOnCancellingToday($this->cancellation_policies, $this->total_price);
    }

    /**
     * called from Booking to calculate every service cancellation charges
     * to store totla cancellation charges in Booking
     */
    function cancellationChargesInCurrency($booking)
    {
        return $booking->convertAmount($this->cancellation_charges, $this->currency, $booking->currency);
    }
}