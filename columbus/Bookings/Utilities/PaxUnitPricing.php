<?php

namespace Columbus\Bookings\Utilities;

trait PaxUnitPricing
{

    public function noOfAdults()
    {
        return array_get($this->unitPricing(), 'no_of_adults', 1);
    }

    public function noOfChildren()
    {
        return array_get($this->unitPricing(), 'no_of_children', 0);
    }

    public function noOfInfants()
    {
        return array_get($this->unitPricing(), 'no_of_infants', 0);
    }

    /*
        total booking item price 
     */
    public function totalPaxPrice()
    {
        return roundUp(
                $this->adultsTotaPrice() 
                + $this->childrenTotaPrice() 
                + $this->infantsTotaPrice()
                ); 
    }

    /*  total price for each pax  */
    public function adultsTotaPrice() 
    {
        return $this->adultUnitPrice() * $this->noOfAdults() ;
    }

    public function childrenTotaPrice() 
    {
        return $this->childUnitPrice() * $this->noOfChildren() ;
    }

    public function infantsTotaPrice() 
    {
        return $this->infantUnitPrice() * $this->noOfInfants() ;
    }

    /*
     calculate the final displayable price conversion by default to booking currency
      
     booking currency is different from product base currency 
     */
    public function adultUnitPrice( $currency = null )
    {
        return $this->booking->convertAmount($this->adultBaseUnitPrice(), $this->baseCurrency());
    }

    public function childUnitPrice( $currency = null )
    {
        return $this->booking->convertAmount($this->childBaseUnitPrice(), $this->baseCurrency());
    }

    public function infantUnitPrice( $currency = null)
    {
        return $this->booking->convertAmount($this->infantBaseUnitPrice(), $this->baseCurrency());
    }

    /*
      calculating total unit base prices  = 
        total_price = base price  
                    + markup ( if markup to be included in product cost)  
                    + taxes (if tax to be added to product cost) 
     */
    public function adultBaseUnitPrice()
    {
        $adultPrice =  array_get( $this->unitPricing(), 'adult_price') ;
        return $this->addMarkupAndTaxesToBasePrice( $adultPrice );
    }

    public function childBaseUnitPrice()
    {
        $adultPrice =  array_get( $this->unitPricing(), 'child_price', 0) ;
        return $this->addMarkupAndTaxesToBasePrice( $adultPrice );
    }

    public function infantBaseUnitPrice()
    {
        $adultPrice =  array_get( $this->unitPricing(), 'infant_price', 0) ;
        return $this->addMarkupAndTaxesToBasePrice( $adultPrice );
    }

    /* 
    markup total calculations
     */
    /*
        total booking item price 
     */
    public function totalMarkup()
    {
        return roundUp(
                $this->adultsTotalMarkup() 
                + $this->childrenTotalMarkup() 
                + $this->infantsTotalMarkup()
                ); 
    }

    /*  total price for each pax  */
    public function adultsTotalMarkup() 
    {
        return $this->adultUnitMarkupPrice() * $this->noOfAdults() ;
    }

    public function childrenTotalMarkup() 
    {
        return $this->childUnitMarkupPrice() * $this->noOfChildren() ;
    }

    public function infantsTotalMarkup() 
    {
        return $this->infantUnitMarkupPrice() * $this->noOfInfants() ;
    }

    public function adultUnitMarkupPrice( $currency = null )
    {
        return $this->booking->convertAmount($this->adultBaseUnitMarkupPrice(), $this->baseCurrency());
    }

    public function childUnitMarkupPrice( $currency = null )
    {
        return $this->booking->convertAmount($this->childBaseUnitMarkupPrice(), $this->baseCurrency());
    }

    public function infantUnitMarkupPrice( $currency = null)
    {
        return $this->booking->convertAmount($this->infantBaseUnitMarkupPrice(), $this->baseCurrency());
    }

    public function adultBaseUnitMarkupPrice()
    {
        $adultPrice =  array_get( $this->unitPricing(), 'adult_price') ;
        return $this->markupValueForAmount( $adultPrice );
    }

    public function childBaseUnitMarkupPrice()
    {
        $adultPrice =  array_get( $this->unitPricing(), 'child_price', 0) ;
        return $this->markupValueForAmount( $adultPrice );
    }

    public function infantBaseUnitMarkupPrice()
    {
        $adultPrice =  array_get( $this->unitPricing(), 'infant_price', 0) ;
        return $this->markupValueForAmount( $adultPrice );
    }

    public function convertAmount($amount)
    {
        return $this->booking->convertAmount($amount, $this->baseCurrency());
    }
    
}