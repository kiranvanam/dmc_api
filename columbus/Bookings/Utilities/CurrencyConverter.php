<?php

namespace Columbus\Bookings\Utilities;

trait CurrencyConverter
{
    public function currency()
    {
        return $this->currency;
    }

    public function exchangeRates()
    {
        return $this->exchange_rates;
    }

    public function exchangeRatesFor($currency)
    {
        return array_get($this->exchangeRates(), $currency);
    }

    /*
        $amountTypeDetails = [ 'amount'=> '',
                                'amount_type => ''
                            ]
     */

    public function calculateValueForAmountFromAmountTypeDetails($amountTypeDetails, $totalAmount, $totalAmountCurrency) {
        // if amount type details are empty
        if (empty($amountTypeDetails)
                || !array_get($amountTypeDetails, 'amount', 0)
                || !array_get($amountTypeDetails, 'amount_type', '')
                || !$totalAmount )
            return 0;

        $convertedMarkupAmount = 0;
        $amountType = array_get($amountTypeDetails, 'amount_type', '');
        $amount = array_get($amountTypeDetails, 'amount', 0);
        if ( $amountType == 'percent' || empty($amountType)) {
            $convertedMarkupAmount =  roundUp( $totalAmount * $amount/ 100, 2);
        } else { // convert markup amount to desired currency to add( amount_type = USD/INR/AED/EUR stores currency //)
            $convertedMarkupAmount = $this->convertAmount( $amount, $amountType, $totalAmountCurrency);
        }

        return $convertedMarkupAmount;
    }


    public function convertAmount($amount, $amountCurrency, $toCurrency = null)
    {

        if( empty($toCurrency) )
            $toCurrency = $this->currency();

        if ($amountCurrency == $toCurrency || !$toCurrency)
            return $amount;

        $convertedAmount = 0;

        $exchangeRates = $this->exchangeRatesFor($amountCurrency);

        if (empty($exchangeRates))
            return $convertedAmount;

        $exchangeRate = array_get($exchangeRates, $toCurrency);
        return  roundUp($amount * $this->rateOfExchange($exchangeRate), 2);
    }

    public  function rateOfExchange($exchangeRate, $no_of_decimals=3)
    {

        $base_rate = array_get($exchangeRate, 'exchange_rate', 0 );
        $final_rate = roundUp( $base_rate + ( $base_rate * array_get( $exchangeRate, 'exchange_rate_buffer') / 100), $no_of_decimals);
        return  $final_rate;
    }
}
