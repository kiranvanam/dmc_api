<?php

namespace Columbus\Bookings\Providers;

use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class BookingsServiceProvider extends ServiceProvider
{
    protected $namespace = "Columbus\Bookings\Http\Controllers";

    public function boot()
    {
        $this->app->router->group(['namespace' => $this->namespace], function() {
            if (! $this->app->routesAreCached()) {
                require __DIR__.'/../routes.php';
            }
        });
        
        //$this->loadViewsFrom(__DIR__.'/../resources/views', 'acl');

        $this->loadMigrationsFrom(__DIR__.'/../database/migrations');

        // Publish seeds
        //$this->publishes([__DIR__ . '/database/seeds/' => base_path('database/seeds')], 'seeds');
    }
    
}