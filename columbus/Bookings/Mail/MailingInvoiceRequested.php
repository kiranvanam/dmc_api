<?php

namespace Columbus\Bookings\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Columbus\Bookings\Models\Booking;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class MailingInvoiceRequested extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    public $booking;

    public function __construct(Booking $booking, $emails = [])
    {
        $this->booking = $booking;
    }

    public function build()
    {
        return $this->from(config('mail.username'))
                    //->bcc()
                    ->subject('Invoice/Booking Ref.#' . $this->booking->code)
                    ->view('emails.bookings.invoice.index');
    }

}