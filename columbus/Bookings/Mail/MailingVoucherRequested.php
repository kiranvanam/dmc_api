<?php

namespace Columbus\Bookings\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Columbus\Bookings\Models\Booking;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class MailingVoucherRequested extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;
    
    public $booking;

    public function __construct(Booking $booking)
    {
        $this->booking = $booking;
    }

    public function build()
    {
        return $this->from(config('mail.username'))                    
                    ->subject('Voucher/Booking Ref.#' . $this->booking->code)
                    ->view('emails.bookings.voucher.index');
    }
}