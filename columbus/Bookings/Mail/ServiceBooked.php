<?php

namespace Columbus\Bookings\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Columbus\Bookings\Models\Booking;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ServiceBooked extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    public $booking = null;

    public function __construct(Booking $booking)
    {
        $this->booking = $booking;
    }

    public function build()
    {
        return $this->from(config('mail.username'))
                    ->subject('You have generated a Booking Ref.#' . $this->booking->code)
                    ->view('emails.bookings.booking.index');
    }

}