<?php

namespace Columbus\Bookings\Http\Controllers;

use Columbus\Bookings\Models\Booking;
use App\Http\Controllers\ApiController;
use Columbus\Agents\Models\TravelAgency;
use Columbus\Bookings\Repositories\BookingRepository;

class BookingController extends ApiController
{

    protected $bookingRepo = null;

    function __construct(BookingRepository $bookingRepo) {
        $this->bookingRepo = $bookingRepo;
    }

    public function search()
    {

        $bookings = Booking::query();
        $user = \Auth::user();
        $agency_id =  request('agency_id') ? request('agency_id') : $user->agency_id;
        //for ageny only agent bookings
        if( $agency_id ) {
            $bookings->where('agency_id', $agency_id);
        } // for distributor all his agent bookings
        elseif( $user->distributor_id ) {
            $bookings->where('distributor_id', $user->distributor_id);
        }

        //$bookings->orderBy('booking_code', 'desc');

        return $this->ok($bookings->orderBy('created_at', 'desc')->get(), "Booking list");
    }

    public function read( $booking_id )
    {
        return $this->ok( Booking::detailsById($booking_id) , "Booking details" );
    }

    public function create()
    {
        logger("***". self::class. "::create START ***");

        $bookingData = request()->all();
        logger("Booking data: ". json_encode($bookingData));

        $booking = Booking::addBooking($bookingData);

        if( empty($booking))
            return $this->invalidRequest($bookingData , "Booking can't be created with incomplete info");

        $agency  = TravelAgency::updateBalancesForBooking($booking);

        \Mail::to($agency->admin_login_email)
            ->send(new \Columbus\Bookings\Mail\ServiceBooked($booking));

        logger("***". self::class. "::create END ***");

        return $this->ok($booking , "Booking Details");
    }


    public function finalize($booking_id)
    {
        return $this->ok("Not implemnted" , "Booking finalized" );
    }

    public function voucher($booking_id)
    {
        logger("***". self::class ."::voucher - START ***");
        $booking = Booking::findOrFail($booking_id)
                          ->voucherDetails();
        logger("***". self::class ."::voucher - END ***");
        return $this->ok( $booking, "Booking Voucher details");
    }


    public function cancel($booking_id)
    {
        logger("********". self::class . "::cancel -- START ****");
        $booking = Booking::findOrFail($booking_id)
                        ->cancelBookingServices()
                        ->updateCancellationCharges();
        $message = " booking with reference: $booking->code is " . ($booking->isCancelled() ? " Cancelled" : " Failed");
        logger( $message);
        logger("********". self::class . "::cancel -- END **** :");
        return $this->ok( $booking , $message);
    }

    function checkAvailability()
    {
        $booking = \Columbus\Bookings\Models\Booking::orderBy('id', 'desc')->first();
        $this->bookingRepo->bookingById($booking->id);
        $booking = $this->bookingRepo->result();
        \Columbus\Utilities\Booking\BookingUtility::initialize( $booking);

        /* \Mail::to("chagamkamalakar@gmail.com")
            ->send(new \Columbus\Bookings\Mail\ServiceBooked($booking));
            \Mail::to("chagamkamalakar@gmail.com")
            ->send(new \Columbus\Bookings\Mail\MailingInvoiceRequested($booking));

            \Mail::to("chagamkamalakar@gmail.com")
            ->send(new \Columbus\Bookings\Mail\MailingVoucherRequested($booking));
            */

    }
}
