<?php

namespace Columbus\Bookings\Http\Controllers;

use Columbus\Utilities\Zip;
use Columbus\Utilities\FileManager;
use Columbus\Bookings\Models\Booking;
use App\Http\Controllers\ApiController;
use Illuminate\Support\Facades\Storage;

class BookingVisaDocumentsController extends ApiController
{

    public function index($booking_id, FileManager $fileManager)
    {
        $booking = Booking::findorFail($booking_id);
        $visaDirectory = "uploads/bookings/$booking->code/visa";
        return $this->ok( $fileManager->onlyFilePathsIn($visaDirectory) );
    }


    function download($booking_id)
    {

        $booking = Booking::findorFail($booking_id);

        $visaDirectory = public_path("uploads/bookings/$booking->code/visa");

        if(!file_exists($visaDirectory)){
            return "No document is uploaded for this visa. Please upload Required documents first";
        }

        $booking_code = $booking->code ? $booking->code : "download";

        $zipFileName =   "$booking_code.zip";

        $zipFileFullPath = public_path("uploads/bookings/$booking_code/$booking_code.zip");
        //first delete if any file exisit
        Storage::disk('tmp')->delete( $zipFileFullPath);

        Zip::zipDir($visaDirectory , $zipFileFullPath);

        // Create Download Response
        if(file_exists($zipFileFullPath)){
            // Set Header
            $headers = array(
                'Content-Type' => 'application/octet-stream',
            );

            return response()->download($zipFileFullPath,$zipFileName,$headers);
        }

        return $this->internalError("Problem happened, please try again");
    }
}