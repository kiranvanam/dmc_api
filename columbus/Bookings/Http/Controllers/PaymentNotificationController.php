<?php

namespace Columbus\Bookings\Http\Controllers;

use App\Http\Controllers\ApiController;
use Columbus\Bookings\Models\PaymentNotification;

class PaymentNotificationController extends ApiController
{
    function index()
    {
        $notifications = PaymentNotification::where('agency_id', request('agency_id'))->get();

        return $this->ok($notifications, "Payment Notfications");
    }

    function create()
    {
        $user = \Auth::user();
        $data = request()->all();
        $data['agency_id'] = $user->agency_id;
        $data['created_by_user'] = $user->name; 
        
        return $this->ok(PaymentNotification::create($data), "Payment Notification Created");
    }
}