<?php

namespace Columbus\Bookings\Http\Controllers;

use Columbus\Bookings\Models\Issue;
use \App\Http\Controllers\ApiController;

class BookingIssueController extends ApiController
{
    public function index($booking_id)
    {
        return $this->ok(Issue::where('booking_id', $booking_id)->get(), "Issues raised for booking");
    }    

    public function raise($booking_id)
    {
        $data = request()->all();
        
        $userLoggedIn = \Auth::user();
        $data['reported_by_user'] = $userLoggedIn->name;;
        
        return $this->ok(Issue::create($data), "Issue raised for this booking");
    }
}