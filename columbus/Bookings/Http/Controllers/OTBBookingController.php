<?php

namespace Columbus\Bookings\Http\Controllers;

use Columbus\Bookings\Models\Booking;
use App\Http\Controllers\ApiController;
use Columbus\Agents\Models\TravelAgency;

class OTBBookingController extends ApiController
{
    
    function create() 
    {
        $data = request()->all(); 

        //preapre booking detials
        logger( "OTB-Booking-- start ". PHP_EOL );
        logger("booking request data" , $data);
        $booking  = array_only($data, 
                                [ 'payment_type', 'service',
                                'lead_passenger_details',
                                'total_price', 'currency',                                
                                ]
                            );
        $booking['status'] = "confirmed";
        $booking['booking_date'] = $booking['payment_deadline_date']                                   
                                 = now();
        $booking['service_start_date'] = $booking['service_end_date']
                                       = $data['travel_date'];
        
        //prepare otb(visa) visa service details
        $OTBDetails['service_source'] = "contracting";        
        $OTBDetails['total_price'] = array_get($data, "total_price");
        $OTBDetails['name'] = array_get($data, "airlines_name");
        $OTBDetails['currency']= array_get($data, "pax_pricing.currency");
        $OTBDetails['paymend_dedline_date'] = array_get($data, "travel_date");
        $OTBDetails['status'] = "confirmed";
        $OTBDetails['country_code'] = array_get($data, "country_code");
        $OTBDetails['no_of_adults'] = array_get($data,'pax.no_of_adults');
        $OTBDetails['no_of_children'] = array_get($data,'pax.no_of_children');
        $OTBDetails['visa_details'] =[
                                      "pax_pricing" => array_get($data, 'pax_pricing'),
                                      "paxes" => array_get($data, 'paxes'),
                                      "airlines_code" => array_get( $data, "airlines_code"),
                                      "pnr_code" => array_get($data, "pnr_code"),
                                      "city_name" => array_get($data, 'city_name'),
                                      "remarks" => array_get($data, "remarks")
                                    ];

        
        $booking['booking_items'] = $OTBDetails; 

        $booking = Booking::addBooking($booking);
        
        if( $errors = array_get($booking, 'errors', null)) 
            return $this->invalidRequest( $errors, "You can't create booking with incomplete info");

        TravelAgency::updateBalancesForBooking($booking);
        
        logger("OTB-Booking - end". PHP_EOL);

        return $this->ok($booking , "Booking Details");

    }
}