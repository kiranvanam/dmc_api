<?php

namespace Columbus\Bookings\Http\Controllers;

use App\Http\Controllers\Controller;
use Columbus\Bookings\Models\Booking;
use Columbus\Bookings\Mail\MailingInvoiceRequested;
use Columbus\Bookings\Mail\MailingVoucherRequested;

class InvoiceAndVoucherController extends Controller
{

    public function emailBooking($booking_id)
    {
        $booking = Booking::detailsById($booking_id);
        if( empty($booking) )
            return;
        $email_ids = request('email_ids');
        if( !empty($email_ids))
            \Mail::to($email_ids[0])
                ->send(new \Columbus\Bookings\Mail\ServiceBooked($booking));
    }


    function emailInvoice($booking_id)
    {
        $booking = Booking::detailsById($booking_id);

        if( empty($booking) )
            return;
        $email_ids = request('email_ids');
        if( !empty($email_ids))
            \Mail::to($email_ids[0])
                ->send(new MailingInvoiceRequested($booking));
    }

    function emailVoucher($booking_id)
    {
        $booking = Booking::detailsById($booking_id);
        if( empty($booking) )
            return;
        $email_ids = request('email_ids');
        if( !empty($email_ids))
            \Mail::to($email_ids[0])
                ->send(new MailingVoucherRequested($booking));
    }

    public function printInvoice($booking_id)
    {
        $booking = Booking::where('id', $booking_id)->first();
        return view('print.bookings.invoice.index')->with(['booking'=>$booking, 'title'=>'Invoice']);
    }

    function printVoucher($booking_id)
    {
        $booking = Booking::detailsById($booking_id);
        return view('print.bookings.voucher.index')->with(['booking'=>$booking, 'title'=>"Voucher"]);
    }
}