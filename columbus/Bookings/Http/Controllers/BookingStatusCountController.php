<?php

namespace Columbus\Bookings\Http\Controllers;

use App\Http\Controllers\ApiController;
use Columbus\Bookings\Repositories\BookingRepository;

class BookingStatusCountController extends ApiController
{
    
    function index(BookingRepository $bookingRepository)
    {
        return $this->ok($bookingRepository->bookingCountGroupByStatus(request('agencyId')));
    }
}