<?php

namespace Columbus\Bookings\Http\Controllers;

use App\Http\Controllers\ApiController;
use Columbus\Bookings\Models\GroupRequest;

class GroupRequestController extends ApiController
{
    function index()
    {
        $agency_id = request('agency_id');
        $requests = null;
        $groupRequestQuery = GroupRequest::query();

        if(!empty($agency_id))
            $groupRequestQuery->where('agency_id',$agency_id);

        return $this->ok( $groupRequestQuery->get() , "Group Requests");
    }

    function create()
    {
        $data = request()->all();
        $groupRequest = GroupRequest::create($data);
        return $this->ok($groupRequest, "Group request submitted");
    }
}