<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddBookingPriceAndMarkupPriceColumnsToTransferBookingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('transfer_bookings', function (Blueprint $table) {
            $table->decimal('booking_price', 8,2)->default(0)->after('no_of_children');
            $table->decimal('markup_price', 8,2)->default(0)->after('booking_price');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('transfer_bookings', function (Blueprint $table) {
            $table->dropColumn('booking_price');
            $table->dropColumn('markup_price');
        });
    }
}
