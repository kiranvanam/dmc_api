<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFinalizedDateColumnToBookingsTable extends Migration
{

    public function up()
    {
        Schema::table('bookings', function (Blueprint $table) {
            $table->timestamp('finalized_date')->after('booking_date')->nullable();
        });
    }

    public function down()
    {
        Schema::table('bookings', function (Blueprint $table) {
            $table->dropColumn('finalized_date');
        });
    }
}
