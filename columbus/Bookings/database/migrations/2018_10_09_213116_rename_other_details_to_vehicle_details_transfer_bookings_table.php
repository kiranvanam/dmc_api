<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenameOtherDetailsToVehicleDetailsTransferBookingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('transfer_bookings', function (Blueprint $table) {
            if (Schema::hasColumn('transfer_bookings', 'other_details'))
                $table->renameColumn('other_details', 'vehicle_details');
            
            $table->string('pick_up_time')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('transfer_bookings', function (Blueprint $table) {
            //
        });
    }
}
