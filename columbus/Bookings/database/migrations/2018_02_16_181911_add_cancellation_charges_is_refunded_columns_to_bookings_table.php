<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCancellationChargesIsRefundedColumnsToBookingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('bookings', function (Blueprint $table) {
             $table->decimal('cancellation_charges', 9,2)->default(0)->after('cancelled_date');
             $table->decimal('refund_amount', 9,2)->default(0)->after('cancellation_charges');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('bookings', function (Blueprint $table) {
            $table->dropColumn('cancellation_charges');
            $table->dropColumn('refund_amount');
        });
    }
}
