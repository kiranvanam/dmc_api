<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIsTransferWithLuggageAndMakeColumnsNullableTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('transfer_bookings', function (Blueprint $table) {
            $table->boolean('is_transfer_type_with_luggage')->default(false)->after('transfer_type_name');
            $table->string('status',25)->nullable()->change();
            
            $table->integer('drop_off_city_id')->nullable()->change();;
            $table->string('drop_off_city_name')->nullable()->change();;
            $table->integer('drop_off_area_id')->nullable()->change();;
            $table->string('drop_off_area_name')->nullable()->change();;
            $table->string('drop_off_point_details', 500)->nullable()->change();;
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('transfer_bookings', function (Blueprint $table) {
            $table->dropColumn('is_transfer_type_with_luggage');
        });
    }
}
