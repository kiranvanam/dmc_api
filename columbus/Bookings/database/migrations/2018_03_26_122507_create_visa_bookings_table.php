<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVisaBookingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('visa_bookings', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('booking_id');            
            $table->string('service_source', 50);
            $table->string('name');
            $table->integer('vendor_id')->nullable();
            $table->smallInteger('no_of_adults');
            $table->smallInteger('no_of_children')->default(0);
            
            $table->string('currency', 10)->index();
            $table->decimal('total_price', 8,2);

            $table->string('status', 20); //initiated,confirmed,pending,failed,rejected,onrequest
                        
            $table->string('country_code',5)->index();            
            
            $table->timestamp('payment_deadline_date')->nullable()->index();
            $table->timestamp('cancelled_date')->nullable()->index();
            
            $table->text('visa_details');
            $table->text('voucher_details')->nullable();
            $table->text('cancellation_details')->nullable();

            //index
            $table->index('booking_id');
            $table->index('service_source');
            $table->index('vendor_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('visa_bookings');
    }
}
