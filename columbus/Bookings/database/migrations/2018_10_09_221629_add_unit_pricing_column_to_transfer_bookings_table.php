<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUnitPricingColumnToTransferBookingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('transfer_bookings', function (Blueprint $table) {
            $table->string('unit_pricing', 1000)->default()->after('total_price');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('transfer_bookings', function (Blueprint $table) {
            $table->dropColumn('unit_pricing');
        });
    }
}
