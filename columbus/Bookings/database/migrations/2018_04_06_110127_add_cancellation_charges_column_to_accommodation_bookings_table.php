<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCancellationChargesColumnToAccommodationBookingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('accommodation_bookings', function (Blueprint $table) {
            $table->decimal('cancellation_charges', 9,2)->after('cancelled_date');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('accommodation_bookings', function (Blueprint $table) {
            $table->dropColumn('cancellation_charges');
        });
    }
}
