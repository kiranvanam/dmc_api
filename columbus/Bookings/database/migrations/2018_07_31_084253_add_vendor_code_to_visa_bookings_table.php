<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddVendorCodeToVisaBookingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('visa_bookings', function (Blueprint $table) {
            $table->integer('visa_id')->nullable()->after('name');
            $table->string('visa_code')->nullable()->after('visa_id');
            $table->string('vendor_code', 50)->nullable()->after('vendor_id');
            $table->string('unit_pricing', 1000)->nullable()->after('total_price');
            $table->text('pax_details')->nullable()->after('unit_pricing');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('visa_bookings', function (Blueprint $table) {
            $table->dropColumn('visa_id');
            $table->dropColumn('visa_code');
            $table->dropColumn('vendor_code');
            $table->dropColumn('unit_pricing');
            $table->dropColumn('pax_details');
        });
    }
}
