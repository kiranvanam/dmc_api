<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGroupRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        /*
        country_id: '', nationality:'',
        pax:{ group_size: '10-15', no_of_adults: 0, no_of_children:0, no_of_infants:'' },
        travel_date: '', no_of_nights:'', budget_per_person:'', currency:'',
        preferred_hotel_details:{ category:[], location:'', hotels:[], meal_plans:[]},
        tour_guide: '',
        conference_facilities: '',
        other_services:[],
        additional_requirements: ''
         */
        Schema::create('group_requests', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('country_id');
            $table->string('country_name');
            $table->string('nationality');
            $table->date('travel_date');
            $table->string('no_of_nights', 50);
            $table->string('pax_details');
            $table->string('budget_per_person');
            $table->string('currency');
            $table->string('preferred_hotel_details', 1000);
            $table->string('tour_guide')->nullable();
            $table->string('conference_facilities')->nullable();
            $table->string('other_services')->nullable();
            $table->string('additional_requirements', 1000)->nullable();
            $table->integer('agency_id')->index();
            $table->string('agency_name');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('group_requests');
    }
}
