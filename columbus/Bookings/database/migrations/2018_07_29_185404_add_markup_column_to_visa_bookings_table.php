<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMarkupColumnToVisaBookingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('visa_bookings', function (Blueprint $table) {
            $table->string('markup', 500)->nullable()->after("visa_details");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('visa_bookings', function (Blueprint $table) {
            $table->dropColumn('markup');
        });
    }
}
