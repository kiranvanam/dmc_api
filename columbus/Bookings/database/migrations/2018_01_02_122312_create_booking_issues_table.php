<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBookingIssuesTable extends Migration
{
    public function up()
    {
        Schema::create('booking_issues', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('booking_id');
            $table->string('category');
            $table->string('subject');
            $table->string('message', 1000);
            $table->string('reported_by_user',100);
            $table->timestamp('reported_date')->default(\DB::raw('CURRENT_TIMESTAMP'));
        });
    }

    public function down()
    {
        Schema::dropIfExists('booking_issues');
    }
}
