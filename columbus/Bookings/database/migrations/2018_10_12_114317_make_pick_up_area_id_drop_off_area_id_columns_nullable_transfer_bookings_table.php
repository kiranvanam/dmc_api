<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MakePickUpAreaIdDropOffAreaIdColumnsNullableTransferBookingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('transfer_bookings', function (Blueprint $table) {
            $table->integer('pick_up_area_id')->nullable()->change();
            $table->text('pick_up_point_details', 500)->nullable()->change();
            $table->integer('drop_off_area_id')->nullable()->change();
            $table->text('drop_off_point_details', 500)->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('transfer_bookings', function (Blueprint $table) {
            //
        });
    }
}
