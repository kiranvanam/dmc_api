<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBookingServiceDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('booking_service_details', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('booking_id')->index();
            $table->string('service_source', 25)->index();
            $table->string('service', 100);
            $table->string('service_name');
            $table->integer('service_id');
            $table->string('service_code',25);
            $table->integer('vendor_id')->default(0);
            $table->string('vendor_code', 25)->default("");            
            $table->decimal('booking_price', 8,2);
            $table->decimal('markup_price', 8,2);
            $table->decimal('total_price', 8,2);
            $table->string('currency',10);
            $table->string('status', 25);
            //$table->string('slot',20)->nullable();
            $table->integer('city_id')->index();
            $table->string('city_name');
            $table->integer('country_id')->index();
            $table->string('country_code',6)->index();
            $table->date('travelling_date');
            $table->string('unit_pricing',1000);
            $table->string('markup_details',500)->nullable();
            $table->text('cancellation_policies')->nullable();
            $table->text('other_details')->nullable(); // for transfers pickup & drop-off details
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('booking_service_details');
    }
}
