<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenameTransferTypeIdAndTransferTypesNameColumnsForTransferBookingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('transfer_bookings', function (Blueprint $table) {
            if (Schema::hasColumn('transfer_bookings', 'transfer_type_id'))
                $table->renameColumn('transfer_type_id', 'transfer_service_id');
            if (Schema::hasColumn('transfer_bookings', 'transfer_type_name'))
                $table->renameColumn('transfer_type_name', 'transfer_service_name');

            if (Schema::hasColumn('transfer_bookings', 'category'))
                $table->renameColumn('category', 'transfer_type');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('transfer_bookings', function (Blueprint $table) {
            //
        });
    }
}
