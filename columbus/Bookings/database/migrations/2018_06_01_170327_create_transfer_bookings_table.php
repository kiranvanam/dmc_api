<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransferBookingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transfer_bookings', function (Blueprint $table) {
            $table->increments('id');
            
            $table->integer('booking_id');            
            $table->string('service_source', 50);
            
            $table->string('transfer_type_id');
            $table->string('transfer_type_name');
            
            $table->date('travelling_date');            

            $table->string('category', 25);
            $table->string('direction', 25)->default('on_ward');
            
            $table->integer('pick_up_city_id');
            $table->string('pick_up_city_name');
            $table->integer('pick_up_area_id');
            $table->string('pick_up_area_name');
            $table->string('pick_up_time', 10);
            $table->string('pick_up_point_details', 500);
            
            $table->integer('drop_off_city_id');
            $table->string('drop_off_city_name');
            $table->integer('drop_off_area_id');
            $table->string('drop_off_area_name');
            $table->string('drop_off_point_details', 500);
            
            $table->string('country_code',5)->index();                        
            $table->integer('vendor_id')->nullable();
            
            $table->smallInteger('no_of_adults');
            $table->smallInteger('no_of_children')->default(0);
            
            $table->decimal('total_price', 8,2);
            $table->string('currency', 10)->index();
            
            $table->string('status', 20); //initiated,confirmed,pending,failed,rejected,onrequest                        
            
            $table->timestamp('payment_deadline_date')->nullable()->index();
            $table->timestamp('cancelled_date')->nullable()->index();
            
            // vehicle, pick_up, drop_off:{ city[id,name], area[id,name]}
            $table->text('other_details'); 
            $table->text('voucher_details')->nullable();
            $table->text('cancellation_details')->nullable();

            //index
            $table->index('booking_id');
            $table->index('service_source');
            $table->index('vendor_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transfer_bookings');
    }
}
