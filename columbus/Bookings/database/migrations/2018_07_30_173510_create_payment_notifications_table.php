<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentNotificationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payment_notifications', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('agency_id')->index();
            $table->string('created_by_user', 50);
            $table->decimal('amount',9,2);
            $table->string("currency", 10); 
            $table->timestamp('payment_date');
            $table->string("payment_towards", 50);
            $table->string("booking_code", 50)->nullable();
            $table->string('payment_type', 100);
            $table->string("payment_mode", 100)->nullable(); 
            $table->string("bank_or_person_name");  // source name is either bank name or person name 
            $table->string("transaction_reference", 100);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payment_notifications');
    }
}
