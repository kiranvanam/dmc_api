<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddServiceDetailsToTransferBookingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('transfer_bookings', function (Blueprint $table) {            
            if(!Schema::hasColumn('transfer_bookings', 'service_details') )
                $table->text('service_details')->nullable()->after('vehicle_details');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('transfer_bookings', function (Blueprint $table) {
            if(Schema::hasColumn('transfer_bookings', 'service_details') )
                $table->dropColumn('service_details');
        });
    }
}
