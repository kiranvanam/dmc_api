<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddVendorCodeColumnToTransferBookingsTable extends Migration
{
    /**
     * Run the migrations.
     * 
     *
     * @return void
     */
    public function up()
    {
        Schema::table('transfer_bookings', function (Blueprint $table) {
            $table->integer('product_id')->default(0)->after('transfer_service_name');
            $table->string('vendor_code')->nullable()->after('product_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('transfer_bookings', function (Blueprint $table) {
            $table->dropColumn('product_id');
            $table->dropColumn('vendor_code');
        });
    }
}
