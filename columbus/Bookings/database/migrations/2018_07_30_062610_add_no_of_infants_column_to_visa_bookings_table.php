<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNoOfInfantsColumnToVisaBookingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('visa_bookings', function (Blueprint $table) {
            $table->integer('no_of_infants')->after('no_of_adults');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('visa_bookings', function (Blueprint $table) {
            $table->dropColumn('no_of_infants');
        });
    }
}
