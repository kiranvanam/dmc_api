<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MakeVendorIdAndVendorCodeColumnsNullableInBookingServiceDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('booking_service_details', function (Blueprint $table) {
            $table->string('service_code',25)->nullable()->change();
            $table->integer('vendor_id')->nullable()->change();
            $table->string('vendor_code', 25)->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('booking_service_details', function (Blueprint $table) {
            //
        });
    }
}
