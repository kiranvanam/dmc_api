<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCancellationChargesColumnToVisaBookingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('visa_bookings', function (Blueprint $table) {
            $table->decimal('cancellation_charges', 9,2)->after('cancelled_date');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('visa_bookings', function (Blueprint $table) {
            $table->dropColumn('cancellation_charges');
        });
    }
}
