<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddVoucherDetailsColumnToAccommoationBookingsTable extends Migration
{

    public function up()
    {
        Schema::table('accommodation_bookings', function (Blueprint $table) {
            $table->text('voucher_details', 1000)->nullable();
        });
    }

    public function down()
    {
        Schema::table('accommodation_bookings', function (Blueprint $table) {
            $table->dropColumn('voucher_details');
        });
    }
}
