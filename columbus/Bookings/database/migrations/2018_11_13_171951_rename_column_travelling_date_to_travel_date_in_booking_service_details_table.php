<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenameColumnTravellingDateToTravelDateInBookingServiceDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('booking_service_details', function (Blueprint $table) {
            if(Schema::hasColumn('booking_service_details', 'travelling_date')) {
                $table->renameColumn('travelling_date', 'travel_date');
            } 
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('booking_service_details', function (Blueprint $table) {
            //
        });
    }
}
