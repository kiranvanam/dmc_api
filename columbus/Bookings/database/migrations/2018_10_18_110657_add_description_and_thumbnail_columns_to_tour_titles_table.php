<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDescriptionAndThumbnailColumnsToTourTitlesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tour_titles', function (Blueprint $table) {
            $table->string('description', 1500)->nullable()->after('type');
            $table->string('thumbnail')->nullable()->after('description');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tour_titles', function (Blueprint $table) {
            $table->dropColumn('description');
            $table->dropColumn('thumbnail');
        });
    }
}
