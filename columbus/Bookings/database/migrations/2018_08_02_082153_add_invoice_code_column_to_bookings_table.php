<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddInvoiceCodeColumnToBookingsTable extends Migration
{
    
    public function up()
    {
        Schema::table('bookings', function (Blueprint $table) {
            $table->string('invoice_code', 50)->default('')->after('invoice_date');
        });
    }

    public function down()
    {
        Schema::table('bookings', function (Blueprint $table) {
            $table->dropColumn('invoice_code');
        });
    }
}
