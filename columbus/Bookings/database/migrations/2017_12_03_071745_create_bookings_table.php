<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBookingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bookings', function (Blueprint $table) {
            $table->increments('id');
            $table->string('code', 25);
            $table->string('service',50);
            $table->string('currency', 10); //while converting price from one to another, no need to get agency currency          
                        
            //payment & status related
            $table->decimal('total_price', 9,2);
            $table->decimal('total_paid', 9,2)->default(0);
            $table->string('payment_type',50)->nullable();
            $table->string('status', 20)->index();
            $table->boolean('is_payment_paid')->default(false);

            //lead passanger realted
            $table->string('lead_passenger_details', 250);            
            
            //to filter bookings list
            $table->string('booked_by_user_name',100);
            $table->integer('booked_by_user_id');
            $table->integer('agency_id');
            $table->string('agency_name', 100);
            $table->integer('distributor_id')->nullable(); // to list the bookings of a distributor, no need to search based on agency_ids             
            
            //timestamps
            $table->timestamp('booking_date')->default(\DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('payment_deadline_date')->nullable();
            $table->timestamp('invoice_date')->nullable();
            $table->timestamp('voucher_date')->nullable();
            $table->timestamp('cancelled_date')->nullable();
            $table->timestamp('service_start_date')->nullable();
            $table->timestamp('service_end_date')->nullable();
            
            //other details           
            $table->string('exchange_rates',500)->nullable();
            

            $table->timestamp('updated_at')->nullable();            
            $table->softDeletes();

            $table->index('code');
            $table->index('service');
            $table->index('status', 'booking_status_index');
            $table->index('booked_by_user_id');
            $table->index('agency_id');
            $table->index('distributor_id');
            $table->index('booking_date');
            $table->index('service_start_date');
            $table->index('service_end_date');
            $table->index(['service_start_date', 'service_end_date']);
            $table->index('payment_deadline_date');            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bookings');
    }
}
