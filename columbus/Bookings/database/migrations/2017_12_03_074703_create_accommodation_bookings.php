<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAccommodationBookings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('accommodation_bookings', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('booking_id')->index();

            $table->string('name');
            $table->decimal('star_category', 2,1);
            $table->string('total_pax', 30);
            
            $table->string('service_source', 50)->index();
            
            $table->string('currency', 10)->index();
            $table->decimal('total_price', 8,2);

            $table->string('status', 20); //initiated,confirmed,pending,failed,rejected,onrequest
            
            $table->smallInteger('total_rooms');
            
            $table->string('country_code',5)->index();            
            $table->string('destination_name', 50);
            
            $table->timestamp('check_in')->nullable()->index();
            $table->timestamp('check_out')->nullable()->index();
            $table->timestamp('payment_deadline_date')->nullable()->index();
            $table->timestamp('cancelled_date')->nullable()->index();
            
            $table->text('accommodation_details');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('accommodation_bookings');
    }
}
