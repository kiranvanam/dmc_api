<?php

namespace Columbus\Destination\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Area extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $fillable = ['name', 'slug', 'code', 'city_id', 'lat', 'lng', 'status'];

    public function city() {
        return $this->belongsTo(City::class);
    }
}
