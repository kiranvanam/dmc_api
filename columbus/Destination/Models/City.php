<?php

namespace Columbus\Destination\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class City extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $fillable = ['name', 'slug', 'code', 'region_id', 'country_id', 'lat', 'lng', 'status'];

    public function areas() {
        return $this->hasMany(Area::class)->withTrashed();
    }

    public function region() {
        return $this->belongsTo(Region::class);
    }

    public function country() {
        return $this->region->country;
    }

    public function deleteCity() {
        $this->areas()->delete();
        $this->delete();
    }

    public function deletePermanently()
    {
        $this->areas()->forceDelete();
        $this->forceDelete();
    }

    public function restoreCity()
    {
        $this->areas()->restore();
        $this->restore();
    }
}
