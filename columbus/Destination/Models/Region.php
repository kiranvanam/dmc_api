<?php

namespace Columbus\Destination\Models;

use Illuminate\Database\Eloquent\Model;

class Region extends Model
{
    public $timestamps = false;
    protected $fillable = ['name', 'slug', 'code', 'country_id', 'lat', 'lng', 'status'];

    public function cities() {
        return $this->hasMany(City::class);
    }

    public function country() {
        return $this->belongsTo(Country::class);
    }

    public function deleteCities() {
        foreach($this->cities as $city) {
            $city->deleteAreas();
            $city->delete();
        }
    }
}
