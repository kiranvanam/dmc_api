<?php

namespace Columbus\Destination\Models;

use Illuminate\Database\Eloquent\Model;

class Market extends Model
{
    public $timestamps = false;
    protected $fillable = ['name', 'slug', 'code'];

    public function countries() {
        return $this->hasMany(Country::class);
    }
}
