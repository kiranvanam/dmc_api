<?php

namespace Columbus\Destination\Models;

use App\Currency;
use Columbus\Contracts\Visas\Models\Visa;
use Columbus\Settings\Models\BankAccount;
use Columbus\Settings\Models\Tax;
use Illuminate\Database\Eloquent\Concerns\belongsToMany;
use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    public $timestamps = false;

    protected $fillable = ['name', 'slug', 'nationality', 'cca2', 'cca3', 'currency', 'calling_code', 'region', 'subregion', 'lat', 'lng', 'is_vendor', 'currency_id', 'is_agent'];

    public function regions() {
        return $this->hasMany(Region::class);
    }

    public function cities_r() {
        return $this->hasManyThrough(City::class, Region::class);
    }

    public function cities() {
        return $this->hasMany(City::class)->withTrashed();
    }

    public function markets() {
        return $this->belongsTo(Market::class);
    }

    public function deleteRegions() {
        foreach ($this->regions as $region) {
            $region->deleteCities();
            $region->delete();
        }
    }

    public function bankAccounts() {
        return $this->hasMany(BankAccount::class);
    }

    public function taxes() {
        return $this->hasMany(Tax::class);
    }

    public function visas() {
        return $this->belongsToMany(Visa::class, 'country_visa', 'country_id', 'visa_id')->withPivot('eligibility_type');
    }

    public function currencies()
    {
        return $this->belongsToMany(Currency::class);
    }

    public static function resetMarketByCountryId($market_id)
    {
        self::where('market_id', $market_id)
                ->update(['market_id' => 0]);
    }

    public static function nameById($id)
    {
        $country = Country::find($id);

        return empty($country) ? "Not found" : $country->name;
    }

    public static function nameByCCA3($cca3)
    {
        $country = Country::where('cca3', $cca3)->first();

        return empty($country) ? $cca3 : $country->name;
    }
}
