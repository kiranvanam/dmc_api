<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCountriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('countries', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('slug');
            $table->string('nationality');
            $table->char('cca2', 2);
            $table->char('cca3', 3);
            $table->json('currency');
            $table->json('calling_code');
            $table->string('region');
            $table->string('subregion');
            $table->float('lat');
            $table->float('lng');
            $table->boolean('is_vendor')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('countries');
    }
}
