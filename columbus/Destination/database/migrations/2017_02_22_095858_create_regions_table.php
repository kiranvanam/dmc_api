<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRegionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('regions', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name',100);
            $table->string('slug',100);
            $table->string('code',100);
            $table->integer('country_id')->unsigned();
            $table->decimal('lng',10,6)->default(0);
            $table->decimal('lat',10,6)->default(0);
            $table->smallInteger('status')->default(Columbus\Constants\Status::ACTIVE);

            $table->index(['name','country_id','slug']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('regions');
    }
}
