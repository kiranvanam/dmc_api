<?php

namespace Columbus\Destination\Http\Controllers;

use Illuminate\Http\Request;
use Columbus\Destination\Models\Area;
use Columbus\Destination\Models\City;
use App\Http\Controllers\ApiController;
use Columbus\Destination\Models\Country;

class CountryController extends ApiController
{
    public function fetchAllLocations(Request $request) {
        return $this->ok(Country::with('cities.areas')->orderBy('name')->get());
    }

    public function all(Request $request) {
        return $this->ok(Country::orderBy('name')->get());
    }

    public function dmcCountries()
    {
        return $this->ok(Country::where('is_vendor', 1)->with('cities.areas')->get(), "DMC Countries list");
    }

    public function addToVendorCountries($country_id)
    {
        $country = Country::find($country_id);
        $country->update(['is_vendor' => 1]);
        $cities = City::withTrashed()
                        ->where('country_id', $country_id)
                        ->get();

        $cities->every(function($city) {
            $areas = Area::withTrashed()
                        ->where('city_id', $city->id)
                        ->restore();
            $city->restore();
        });
        $country = Country::where('id', $country_id)->with('cities.areas')->first();
        return $this->ok($country, "Country added to vendor list");
    }

    public function deleteFromVendorCountries($country_id)
    {

        $country = Country::with('cities')
                        ->where('id', $country_id)
                        ->first();
        $country->cities->every( function($city) {
            $areas = Area::withTrashed()
                        ->where('city_id', $city->id)
                        ->delete();

            $city->delete();
        });

        $country->update(['is_vendor' => 0]);


        return  $this->ok($country, "Country and related regions, cities & areas are deleted ");
    }

}
