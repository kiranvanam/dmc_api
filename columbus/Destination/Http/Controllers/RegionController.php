<?php

namespace Columbus\Destination\Http\Controllers;

use App\Http\Controllers\ApiController;
use Columbus\Destination\Http\Requests\RegionCreateRequest;
use Columbus\Destination\Models\Region;
use Illuminate\Http\Request;

class RegionController extends ApiController
{
    public function all(Request $request) {
        return $this->ok(Region::all());
    }

    public function save(RegionCreateRequest $request)
    {
        return $this->ok(Region::create($request->all()), "Region created successfully");
    }

    public function update($id, RegionCreateRequest $request)
    {
        $region = Region::find($id);
        $region->update($request->all());
        return $this->ok($region , "Region details updated successfully");
    }

    public function delete($id)
    {
        $region = Region::find($id);
        //Delete All The Cities
        $region->deleteCities();
        //Finally, Delete The Region
        $region->delete();
        return $this->ok($region, "Region deleted");
    }
}
