<?php

namespace Columbus\Destination\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;
use Columbus\Destination\Models\Market;
use Columbus\Destination\Models\Country;
use Columbus\Destination\Http\Requests\MarketCreateRequest;

class MarketController extends ApiController
{
    public function all(Request $request) {
        return $this->ok(Market::all());
    }

    public function save(MarketCreateRequest $request) {
        return $this->ok(Market::create($request->all()), "Market Created Successfully");
    }

    public function update($market_id, Request $request)
    {
        $data = $request->all();
        $market = Market::find($market_id);

        $market->update($data);

        return $this->ok($market, "Market Update Successful");
    }

    public function delete($market_id) {
        $market = Market::find($market_id);
        $market->delete();

        // reset mrket_id flag to 0 as this market is no more
        Country::resetMarketByCountryId($market_id);

        return $this->ok($market);
    }
}
