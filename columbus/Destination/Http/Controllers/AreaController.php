<?php

namespace Columbus\Destination\Http\Controllers;

use App\Http\Controllers\ApiController;
use Columbus\Destination\Http\Requests\AreaCreateRequest;
use Columbus\Destination\Models\City;
use Columbus\Destination\Models\Area;
use Illuminate\Http\Request;

class AreaController extends ApiController
{
    public function byCity()
    {
        $query = Area::query();

        if(request('city_id'))
            $query->where('city_id', request('city_id'));

        return $this->ok( $query->get(), "Areas for the selected ciyt");
    }

    public function save(AreaCreateRequest $request)
    {
        return $this->ok(Area::create($request->all()));
    }

    public function update($area_id, Request $request)
    {
        $data = $request->all();
        $area = Area::find($area_id);
        $area->update($data);

        return $this->ok($area, "Area details Updated  successfully");
    }

    public function fetchByCity($id) {
        return $this->ok(
            Area::where('city_id', $id)->get(),
            "Fetching Areas By City Is Successful"
        );
    }

    public function delete($area_id)
    {
        $area = Area::findOrFail($area_id);
        $area->delete();
        return $this->ok($area, "Area deleted successfully");
    }

    public function forceDelete($area)
    {
        $area = Area::findOrFail($area);
        $area->forceDelete();
        return $this->ok( $area, "Area deleted permanently");
    }

    public function restore($area_id)
    {
        $area = Area::onlyTrashed()->where('id',$area_id)->first();

        if($area)
            $area->restore();

        return $this->ok($area, "Area restored");
    }
}
