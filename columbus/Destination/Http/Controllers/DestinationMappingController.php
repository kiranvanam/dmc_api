<?php

namespace Columbus\Destination\Http\Controllers;

use App\Http\Controllers\ApiController;
use Columbus\Destination\Models\Country;

class DestinationMappingController extends ApiController
{

    public function mapCities()
    {
        $countries = Country::with('regions')->get();

        $countries->each( function($country) {
                    $country->regions->each(function($region) use($country) {
                        $region->cities->each( function($city) use($country) {
                            $city->update(['country_id'=> $country->id]);
                        });
                        //echo "$city->name- $city->country_id - $country->id <br>";
                    } );
                });
        return $this->ok("City is mapped to Country", "Country-to-City mapping is processed");
    }
}