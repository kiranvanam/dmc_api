<?php

namespace Columbus\Destination\Http\Controllers;

use App\Http\Controllers\ApiController;
use Columbus\Destination\Models\Country;

class MarketCountriesController extends ApiController
{
    
    public function update($market_id) {
        
        Country::resetMarketByCountryId($market_id);

        Country::whereIn('id', request()->get('country_ids'))->update(['market_id' => $market_id]);

        return $this->ok(Country::get(), "Updated Market Countries");
    }
}