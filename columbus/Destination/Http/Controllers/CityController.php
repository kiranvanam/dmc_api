<?php

namespace Columbus\Destination\Http\Controllers;

use App\Http\Controllers\ApiController;
use Columbus\Destination\Http\Requests\CityCreateRequest;
use Columbus\Destination\Models\City;
use Illuminate\Http\Request;

class CityController extends ApiController
{
    public function all(Request $request)
    {
        return $this->ok(City::all());
    }

    public function fetchByRegion($id) {
        return $this->ok(
            City::where('region_id', $id)->get(),
            "Fetching Cities By Region Is Successful"
        );
    }

    public function save(CityCreateRequest $request)
    {
        return $this->ok(City::create($request->all()), "City created successfully");
    }

    public function update($city_id, CityCreateRequest $request)
    {
        $city = City::findOrFail($city_id);
        $city->update($request->all());
        return $this->ok($city , "City details updated successfully");
    }

    public function delete($city_id)
    {
        $city = City::findOrFail($city_id);

        $city->deleteCity();

        return $this->ok($city, "City & areas are trashed successfully");
    }

    public function forceDelete($city_id)
    {
        $city = City::findOrFail($city_id);
        $city->deletePermanently();
        return $this->ok( $city, "City & areas deleted permanently");
    }

    public function restore($city_id)
    {
        $city = City::onlyTrashed()->where('id',$city_id)->first();

        if($city){
            $city->restoreCity();
        }
        return $this->ok($city, "City restored");
    }
}
