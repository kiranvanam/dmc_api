<?php

namespace Columbus\Destination\Http\Requests;

use Columbus\Constants\LocationLevel;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class AreaCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'city_id' => 'required'
        ];
    }

    protected function prepareForValidation()
    {
        $this->merge([
                'slug' => str_slug($this->get('name')),
            ]
        );
    }

    /**
     * customizing error messages for (field,rule)
     */
    public function messages()
    {
        return [
            'name.required' => "Area name must be provided",
            "city_id.required" => "Please Select A City Before Adding The Area"
        ];
    }

}
