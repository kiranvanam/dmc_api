<?php

Route::group(['prefix'=>'/api', /*'middleware'=>'auth'*/], function(){

    // return all data(countries-> destinations)
    Route::get('locations', 'CountryController@fetchAllLocations');

    Route::put('market-countries/{id}', 'CountryController@updateMarket');

    Route::group(['prefix'=>'countries'], function() {
        Route::get('/', 'CountryController@all');
        Route::put('/add-to-vendors/{id}','CountryController@addToVendorCountries');
        Route::put('/delete-from-vendors/{id}', 'CountryController@deleteFromVendorCountries');
    });

    Route::group(['prefix' => 'regions'], function () {
        Route::get('/', 'RegionController@all');
        Route::get('/{country_id}', 'RegionController@fetchByCountry');
        Route::post('/', 'RegionController@save');
        Route::put('/{id}', 'RegionController@update');
        Route::delete('/{id}', 'RegionController@delete');
    });

    Route::group(['prefix' => 'cities'], function() {
        Route::get('/', 'CityController@all');
        Route::get('/{region_id}', 'CityController@fetchByRegion');
        Route::get('/country/{region_id}', 'CityController@fetchByCountry');
        Route::post('/', 'CityController@save');
        Route::put('/{id}', 'CityController@update');
        Route::delete('/{id}', 'CityController@delete');
        Route::delete("{id}/force", 'CityController@forceDelete');
        Route::put('/{id}/restore', 'CityController@restore');
    });

    Route::group(['prefix' => 'areas'], function(){
        Route::get("/", 'AreaController@byCity');
        Route::get('/{city_id}', 'AreaController@fetchByCity');
        Route::post('/', 'AreaController@save');
        Route::put('/{id}', 'AreaController@update');
        Route::delete('/{id}', 'AreaController@delete');
        Route::delete("{id}/force", 'AreaController@forceDelete');
        Route::put('/{id}/restore', 'AreaController@restore');
    });

    Route::group(['prefix' => 'markets'], function(){
        Route::get('/', 'MarketController@all');
        Route::post('/', 'MarketController@save');
        Route::put('/{id}', 'MarketController@update');
        Route::delete('/{id}', 'MarketController@delete');

        Route::put("{market_id}/countries", 'MarketCountriesController@update');
    });
});

Route::get("/map-country-to-cities", 'DestinationMappingController@mapCities');
//Route::group(['prefix' => 'destinations'], function(){
//  Route::post('/destinations','DestinationController@save');
//  Route::put('/destinations{id}', 'DestinationController@update');
//});
