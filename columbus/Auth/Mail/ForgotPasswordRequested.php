<?php

namespace Columbus\Auth\Mail;

use App\User;
use Illuminate\Mail\Mailable;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ForgotPasswordRequested extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    public $user;
    public $password_with_no_encryption;

    public function __construct(User $user, $password)
    {
        \Log::info('User details to login.', $user->toArray());
        $this->user = $user;
        $this->password_with_no_encryption = $password;

    }

    public function build()
    {
        return $this->from(config('mail.username'))
                    ->subject(config('app.name') . " forgot password request")
                    ->view('emails.forgot-password');
    }
}