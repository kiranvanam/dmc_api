<?php

namespace Columbus\Auth;

trait VendorPermissionHelper
{
    // Helper methods
    // as it's difficult to refer  complete permission chain every time,
    // we make use some of these methods

    public function hasVendor($permission)
    {
        $permission = "vendors.contracts." . $permission;
        return $this->hasPermission($permission);
    }

    public function hasVendorHotels($permission)
    {
        $permission = "hotels." . $permission;
        return $this->hasVendor($permission);
    }

    public function hasVendorTours($permission)
    {
        $permission = "tours." . $permission;

        return $this->hasVendor($permission);
    }

    public function hasVendorVisas($permission)
    {
        $permission = "visas." . $permission;

        return $this->hasVendor($permission);
    }
}
