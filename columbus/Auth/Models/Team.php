<?php

namespace Columbus\Auth\Models;

use Illuminate\Database\Eloquent\Model;

class Team extends Model
{
    public $timestamps = false;

    protected  $fillable = ['name', 'slug', 'owner_id', 'description'];
}
