<?php

namespace Columbus\Auth\Models;

use Columbus\Auth\Models\Permission;
use Illuminate\Database\Eloquent\Model;

class Resource extends Model
{
    public $timestamps = false;

    protected $fillable = ['name', 'slug', 'parent_resource_id'];

    public function permissions()
    {
      return $this->hasMany(Permission::class);
    }

    public function resources()
    {
      return  $this->hasMany(Resource::class,'parent_resource_id')->with('resources')->with('permissions');
    }

    public function FunctionName($value='')
    {
      # code...
    }
    public function withPermissions()
    {
      return $this->hasMany(Permission::calss)->with('permissions');
    }
}
