<?php

namespace Columbus\Auth\Models;

use Illuminate\Database\Eloquent\Model;

class Permission extends Model
{
  public $table = "resource_has_permissions";
  public $timestamps = false;
  protected $fillable = ['name', 'slug', 'resource_id'];
}
