<?php

namespace Columbus\Auth\Models;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    public $timestamps = false;

    protected $fillable = ['name', 'slug', 'module', 'description', 'owner_id'];

    public static function createRole($data)
    {
        if($role = Role::where('name', $data['name'])->where('module', $data['module'])->orWhere('slug',$data['slug'])->first()){
            $role->update($data);
        }
        else {
            $role = Role::create($data);
        }
        return  $role;
    }

    public function permissions()
    {
        return $this->belongsToMany(Permission::class, 'role_has_permissions');
    }
}
