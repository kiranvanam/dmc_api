<?php


Route::post('/auth/api-login', 'ApiLoginController@login');
Route::post('/auth/refresh', 'ApiLoginController@refreshToken');

Route::group(['prefix'=>'api', 'middleware' => ['auth:api','cors'] ] , function () {
    // resource-permissions
    Route::get('/permissions', 'ResourceController@index');
    //roles
    Route::get('/roles', 'RoleController@index');
    Route::post('/roles', 'RoleController@save');
    Route::put('/roles', 'RoleController@update');
    Route::put('/roles/{role_id}', 'RoleController@update');

    //user
    Route::get("/logged-in-user", 'UserController@userDetails');
    Route::get('/permissions', 'ResourceController@userPermissions');
    Route::get("/users", 'UserController@index');
    Route::post("/users", 'UserController@create');
    Route::put("/users/{user_id}", 'UserController@update');
    Route::put("/users/{user_id}/update-password", 'UpdatePasswordController@update');

});

Route::post("/api/users/forgot-password", 'ForgotPasswordController@sendEmail');

//Route::get("/password-change", 'PassWordChangeController@change');