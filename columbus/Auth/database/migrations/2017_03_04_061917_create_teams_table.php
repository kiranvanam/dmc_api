<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTeamsTable extends Migration
{
    
    public function up()
    {
        Schema::create('teams', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name',100);
            $table->string('description', 500);
            $table->integer('owner_id');
        });

        Schema::create('team_has_users', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('team_id');
            $table->integer('user_id');
        });

    }

    public function down()
    {
        Schema::dropIfExists('team_has_users');
        Schema::dropIfExists('teams');
    }
}
