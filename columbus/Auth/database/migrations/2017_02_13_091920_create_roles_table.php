<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRolesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('roles', function (Blueprint $table) {
            $table->increments('id');
            $table->string("name",100);
            $table->string("slug",100);
            $table->string("module", 50);
            $table->string("description",500);
        });

        Schema::create('role_has_permissions', function (Blueprint $table){
            $table->increments('id');
            $table->integer('role_id');
            $table->integer('permission_id');
        });

        Schema::create('user_has_roles', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->integer('role_id');
        });
    }

    public function down()
    {
        Schema::dropIfExists('roles');
        Schema::dropIfExists('role_has_permissions');
        Schema::dropIfExists('user_has_roles');
    }
}
