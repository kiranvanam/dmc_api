<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDesignationAgencyIdOwnerIdStatusColumnsToUsersTable extends Migration
{    
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            
            $table->string('designation',150)->nullable()->after('name');
            $table->string('mobile',15)->nullable()->after('designation');
            $table->string('telephone',20)->nullable()->after('mobile');
            
            $table->string('status',30)->default('in-active')->after('is_confirmed');
            $table->string('reason_for_suspending', 500)->nullable()->after('status');
            
            $table->integer('agency_id')->default(0)->after('reason_for_suspending');
            $table->integer('owner_id')->default(0)->after('agency_id');
        });
    }

    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('designation');
            $table->dropColumn('mobile');
            $table->dropColumn('telephone');
            $table->dropColumn('status');
            $table->dropColumn('reason_for_suspending');
            $table->dropColumn('agency_id');
            $table->dropColumn('owner_id');
        });
    }
}
