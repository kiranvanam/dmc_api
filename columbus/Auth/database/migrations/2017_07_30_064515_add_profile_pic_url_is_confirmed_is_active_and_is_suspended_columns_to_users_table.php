<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddProfilePicUrlIsConfirmedIsActiveAndIsSuspendedColumnsToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('profile_pic_url')->nullable()->after('remember_token');
            $table->boolean('is_confirmed')->default(false)->after('profile_pic_url');
            $table->boolean('is_active')->default(false)->after('is_confirmed');
            $table->boolean('is_suspended')->default(false)->after('is_active');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('profile_pic_url');
            $table->dropColumn('is_confirmed');
            $table->dropColumn('is_active');
            $table->dropColumn('is_suspended');
        });
    }
}
