<?php

namespace Columbus\Auth;


trait PermissionChecker {

    protected $resources = [];

    public function initialize($permissions)
    {
      /*
        as of now we are not removing resources which don't have permissions or child resources
        but has to be removed to avoid displaying  menu in case menu doesn't have any sub-menu
      */
      $this->resources = $this->preProcess($permissions);

      $this->resources = array_values($this->cleanResourceWhichDoesntHavePermissionsOrSubResources($this->resources));

    }

    // formats Resources into parent-child relations 
    protected function preProcess($resources) {
      
      //step 1) groupBy based on parent_resource_id
       $groupByParentResource = $resources->groupBy('parent_resource_id')->toArray();

      // step 2) all base resources have parent_resource_id=0, then construct it's children recursively 
      $baseResources = array_map( function($resource) use($groupByParentResource) {
                          return  $this->attachChildrenToParent($resource, $groupByParentResource) ; 
                        }, 
                        $groupByParentResource["0"]
                      );
      return $baseResources;
    }

    function getResources() {
      if($this->resources){
        return $this->resources;
      }
      return [];
    }

    // recursively insert children into corresponding parent
    protected function attachChildrenToParent($parentResource, $groupByParentResource) {
      //var_dump($parentResource, $groupByParentResource);

      $resources = [];
      if(isset($groupByParentResource[$parentResource['id']]))
        $resources = $groupByParentResource[$parentResource['id']];

      $parentResource['resources'] = $resources; 

      $parentResource['resources'] = array_map(function($resource) use($groupByParentResource) {
                                        return $this->attachChildrenToParent($resource, $groupByParentResource);
                                      },
                                      $parentResource['resources']
                                    );
      return $parentResource;
    } 

    protected function cleanResourceWhichDoesntHavePermissionsOrSubResources($resources) {

       // consider a resource if it has at least 1 permission or sub-resource 
        return array_filter($resources, function($resource) {
            return  count($resource['permissions']) || 
                    count($this->cleanResourceWhichDoesntHavePermissionsOrSubResources($resource['resources']));
            });
    }

    protected function permissionSearch($permission) {

        $permissions = explode(".", $permission);
        // if either permissions or resources is empty, return false
        if(empty($permissions) ||  empty($this->getResources()))
          return false;
        return $this->searchFor($this->getResources(), $permissions);
    }
  
    // recursive search
    protected function searchFor($resources, &$permissions)
    {
      
        // if permissions are left, then continue with search
        $resource = [];
        foreach ($resources as $resource_tmp) {
            if($resource_tmp['slug'] == $permissions[0]) {
                array_shift($permissions);
                $resource = $resource_tmp;
                break;
            }
        }

        // if search request is only for resource 
        if($resource && empty($permissions)) {
            return true;
        }

        //if resource not found, then no  need to search in it's sub-resources & permissions 
        if(!$resource) {
          return false;
        }
        
        //search for in permissions of this resource
        foreach($resource['permissions'] as $resourcePermission ) {
            if($resourcePermission['slug'] ==  $permissions[0] ){
                array_shift($permissions);
                break;
            }
        }

        //if no more permissions, then requested permission is allowed for this user
        if(empty($permissions)) {
          return true;
        }

        return $this->searchFor($resource['resources'], $permissions);
    }

}
