<?php

namespace Columbus\Auth\Transformers;

use App\Transformers\AbstractTransformer;


class UserTransformer extends AbstractTransformer
{
  public function transformModel(Model $user)
  {
    $output = [
        'name'      => $item->name,
        'type'      => [
                'key'   => $item->type,
                'name'  => $item->typeName
        ],
    ];

    return $output;
  }

}