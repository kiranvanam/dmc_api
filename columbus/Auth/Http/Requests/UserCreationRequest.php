<?php

namespace Columbus\Auth\Http\Requests;

use App\User;
use Illuminate\Validation\Rule;
use Columbus\Auth\Mail\UserAdded;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Columbus\SystemSettings\Models\Code;
use Illuminate\Foundation\Http\FormRequest;

class UserCreationRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $validationRules = [
            "name" => "required",
            "designation" => "required",
            "telephone"=> "required",
            "mobile" => "required",
            //'email' => "required|unique:users,email",
            //'profile_pic_url'=> "required",
            'status' => "required|string" ,
            'reason_for_suspending' => "",
            'agency_id'=> "required",
            'roles'=>'required',
            'timezone' => 'required|string'
            //'owner_id' => "required"
        ];

        if($this->id) {
            if($this->is_password_changed )
                $validationRules['password'] = "required|string";

            $user= User::find($this->id);

            $validationRules['email'] = ['required', Rule::unique('users')->ignore($user->id)];
        }

        return $validationRules;
    }

    public function prepareForValidation()
    {
        $user = Auth::user();
        $this->merge(['owner_id' => $user ? $user->id : "" ]);
    }

    public function save2()
    {
        //$data = $this->only(['name', 'designation', 'password', 'email']);
        $user = User::where('email', 'chagamkamalakar@gmail.com')->first();
        $user->password_with_no_encryption = $this->paasword;

        \Mail::send(new UserAdded($user, $this->password));
        return $user;
    }

    public function save()
    {
        $data = $this->all();
        $password = $data['password'];

        $this->generateCode($data);
        $this->removeNullsInRoles($data);

        $data['password'] = bcrypt($data['password']);
        $user = User::create($data);
        $user->password_with_no_encryption = $password;
        $user->roles()->sync($data['roles']);

        \Mail::to($user->email)
                ->send(new UserAdded($user, $password));
        return $user->load('roles');
    }

    public function update($user_id)
    {
        $user = User::findOrFail($user_id);
        $data = $this->all();

        //if password needs to be changed, then hash it, else remove from data as it's null
        if(isset($data['is_password_changed']) && $data['is_password_changed'] )
            $data['password'] = bcrypt($data['password']);
        else
            unset($data['password']);

        $this->generateCode($data);
        $this->removeNullsInRoles($data);

        $user->update($data);

        $user->roles()->sync($data['roles']);

        return $user->load('roles');
    }


    protected function generateCode(&$data)
    {
        if( !array_has($data, 'code'))
            $data['code'] = Code::getNextCodeForModule("users");
        else
            unset($data['code']);
    }

    protected function removeNullsInRoles(&$data)
    {
        $data['roles'] = array_filter($data['roles'], function($var){return !is_null($var);} );
    }


}