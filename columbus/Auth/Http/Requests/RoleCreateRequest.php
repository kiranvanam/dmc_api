<?php

namespace Columbus\Auth\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RoleCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "name" => "required",
            "description" => "nullable|string",
            "module"=> "required"
        ];
    }

    /*public function prepareForValidation()
    {
        $this->merge(['slug' => str_slug($this->get('name'))]);
    }*/
}
