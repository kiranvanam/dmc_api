<?php

namespace Columbus\Auth\Http\Requests;

use App\User;
use Columbus\Auth\Mail\ForgotPasswordRequested;
use Illuminate\Foundation\Http\FormRequest;

class ForgotPasswordRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'email' => "required|email|exists:users,email"
        ];
    }

    public function messages()
    {
        return [
            'email.required' => "Registered email must be provided",
            'email.email' => "Provide a valid email",
            'email.exists' => "Provide a registered email.",
        ];
    }

    public function sendEmail()
    {
        $user = User::where('email', $this->email)->firstOrFail();
        $crypted = bin2hex($user->email);
        $start = mt_rand(0, strlen($crypted)- 10);
        $password_with_no_encryption = substr($crypted, $start , 10);
        $user->update(['password' => bcrypt($password_with_no_encryption)]);
        \Mail::to($user->email)
                ->send(new ForgotPasswordRequested($user, $password_with_no_encryption));
    }
}