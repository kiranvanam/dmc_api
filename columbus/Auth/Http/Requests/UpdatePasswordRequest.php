<?php

namespace Columbus\Auth\Http\Requests;

use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Http\FormRequest;

class UpdatePasswordRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            "new_password" => "required|string|min:10|confirmed",
            'new_password_confirmation' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'new_password.required' => "Password field must be filled.",
            'new_password.min' => "Password filed length must be min of 10 characters length.",
            'new_password.confirmed' => "Password and Confirm password must match.",
            'new_password_confirmation.required' => "Confirmation password field must be filled.",
        ];
    }

    public function update()
    {
        $user = Auth::user();
        $data['password'] = bcrypt($this->new_password);
        $user->update($data);
        return $user;
    }




}