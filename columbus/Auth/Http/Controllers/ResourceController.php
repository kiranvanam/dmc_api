<?php

namespace Columbus\Auth\Http\Controllers;

use App\Http\Controllers\ApiController;
use App\User;
use Columbus\Auth\Models\Resource;
use Illuminate\Support\Facades\Auth;


class ResourceController extends ApiController
{

    public function index()
    {
        return $this->ok(
            Resource::where('parent_resource_id',0)
                ->with('resources')
                ->with('permissions')
                ->get(),
                "Resource-Permissions details"
                );
    }

    public function userPermissions()
    {
        return  $this->ok(Auth::user()->permissions());
    }
}