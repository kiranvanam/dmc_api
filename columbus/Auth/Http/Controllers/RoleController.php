<?php

namespace Columbus\Auth\Http\Controllers;

use App\Http\Controllers\ApiController;
use Columbus\Auth\Http\Requests\RoleCreateRequest;
use Columbus\Auth\Models\Role;
use Illuminate\Http\Request;

class RoleController extends ApiController
{

    public  function index()
    {
      $roleIds = Role::select('id')->get()->pluck('id');

      $permissionIds = \DB::table('role_has_permissions')->whereIn('role_id', $roleIds)->get()->pluck('permission_id');
      return  $this->ok( Role::with('permissions')->get() ,  "Permissions for the roles logged-in user(you) created");
    }


    public function save(RoleCreateRequest $request)
    {
        // add owner id while creating, but don't touch this while updating
        $data = $request->only(['name', 'module', 'description', 'owner_id']);
        $data['owner_id'] = \Auth::user()->id;
        $role = $this->createOrUpdateRoleWithPermissions(
                      $data,
                      $request->get('permissions')
                    );

        // no need to return permission_ids for this role, these these record creation doesn't have any impact
        return $this->ok($role, "Role created successfully");
    }

    public function update(RoleCreateRequest $request)
    {
        $role = $this->createOrUpdateRoleWithPermissions(
                      $request->only(['name', 'module', 'description']),
                      $request->get('permissions')
                    );

        // no need to return permission_ids for this role, these these record creation doesn't have any impact
        return $this->ok($role, "Role Updated successfully");
    }

    protected function createOrUpdateRoleWithPermissions($roleData, $permission_ids)
    {
        $roleData['slug'] = str_slug($roleData['name']);

        $role = Role::where('name', $roleData['name'])->where('module', $roleData['module'])->first();

        if(!$role){
            $role = Role::create($roleData);
        }else {
            $role->update($roleData);
        }

        if($permission_ids) {
         $role->permissions()->sync($permission_ids);
        }

        return  $role;
    }
}