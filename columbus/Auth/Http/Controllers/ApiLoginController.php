<?php

namespace Columbus\Auth\Http\Controllers;

use App\Http\Controllers\ApiController;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Laravel\Passport\Client;

class ApiLoginController extends ApiController
{
  private  $oauthClient = null;
  public function __construct()
  {
    $this->middleware('guest', ['except' => 'logout']);
    $this->oauthClient = Client::find(2);
  }


  public function login(Request $request)
  {
    
    $validator = \Validator::make($request->all(), [
        'email' => 'required|email|exists:users,email',
        'password' => 'required',
    ]);

    if ($validator->fails()) {
      return $this->validationErrors($validator->errors());
    }

    $email = $request->input('email');
    $password = $request->input('password');
    $request->request->add([
      'username' => $email,
      'password' => $password,
      'grant_type' => 'password',
      'client_id' => $this->oauthClient->id,
      'client_secret' => $this->oauthClient->secret,
      'scope' => '*'
    ]);

    $tokenRequest = Request::create(
      env('APP_URL').'/oauth/token',
      'post'
    );
    return $this->ok(json_decode(Route::dispatch($tokenRequest)->getContent()));
  }

  public function refreshToken(Request $request)
  {
      $request->request->add([
          'grant_type' => 'refresh_token',
          'refresh_token' => $request->refresh_token,
          'client_id' => $this->oauthClient->id,
          'client_secret' => $this->oauthClient->secret,
      ]);

      $proxy = Request::create(
          '/oauth/token',
          'POST'
      );

      return Route::dispatch($proxy);
  }
}
