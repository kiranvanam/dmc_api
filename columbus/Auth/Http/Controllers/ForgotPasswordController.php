<?php

namespace Columbus\Auth\Http\Controllers;

use App\Http\Controllers\ApiController;
use Columbus\Auth\Http\Requests\ForgotPasswordRequest;


class ForgotPasswordController extends ApiController
{

    public function sendEmail(ForgotPasswordRequest $request)
    {
        return $this->ok($request->sendEmail(), "Email has been sent please check the details.");
    }
}
