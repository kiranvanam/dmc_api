<?php

namespace Columbus\Auth\Http\Controllers;

use App\User;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\ApiController;
use Columbus\Auth\Http\Requests\UserCreationRequest;

class UserController extends ApiController
{

    public  function index()
    {
        if( request()->query('agency_id') ) {
            return $this->ok($this->agencyUsers(request()->query('agency_id')), "Agency users");
        } else if( request()->query('distributor_id') ) {
            return $this->ok($this->distributorUsers(request()->query('distributor_id')), "Distributor users");
        }

        return  $this->ok(User::where('agency_id', '==', 0)->where('distributor_id','==', 0)->with('roles')->get(), "User listing" );
    }

    public function userDetails()
    {
        return $this->ok(Auth::user() , "User details");
    }

    public function create(UserCreationRequest $request)
    {
        return $this->ok( $request->save(), "User added");
    }

    public function update($agency_id, UserCreationRequest $request)
    {
        return $this->ok($request->update($agency_id), "User details updated");
    }

    public function agencyUsers($agency_id)
    {
        return User::where('agency_id', $agency_id)->with('roles')->get();
    }

    public function distributorUsers($distributor_id)
    {
        return User::where('distributor_id', $distributor_id)->with('roles')->get();
    }


}