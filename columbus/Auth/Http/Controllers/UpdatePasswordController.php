<?php

namespace Columbus\Auth\Http\Controllers;

use App\User;
use App\Http\Controllers\ApiController;
use Columbus\Auth\Http\Requests\UpdatePasswordRequest;

class UpdatePasswordController extends ApiController
{
    function change()
    {
        $user = User::where('email', 'info@godubaitourism.com')->first();
        $user->update([ "password" => bcrypt("GDT@123")]);
        return $user;
    }

    function update($user_id, UpdatePasswordRequest $request)
    {
        return $this->ok($request->update(), "Password changed");
    }
}