<?php

Route::group(['prefix'=> '/api', 'middleware'=>['auth:api','cors']], function(){
   Route::post('countries', 'CountryController@save');
   
   Route::get("/agencies/{agency_id?}", 'TravelAgencyController@index');
   //Route::get("/agencies", 'TravelAgencyController@index');
   
   Route::post("agencies/{agency_id}/approve", 'TravelAgencyApprovalController@approve');
   Route::put("/agencies/{agency_id}", "TravelAgencyController@update");
   Route::put("/agencies/{agency_id}/timezone", "TravelAgencyController@updateTimezone");
   Route::put("/agencies/{agency_id}/confirm", "TravelAgencyController@confirm");
   Route::put("/agencies/{agency_id}/markups", "TravelAgencyController@updateMarkups");
   Route::put("/agencies/{agency_id}/contact-details", "TravelAgencyController@updateContactDetails");
   Route::put("/agencies/{agency_id}/invoice-currencies", "TravelAgencyController@updateInvoiceCurrencies");
   //Route::put("/agencies/{agency_id}/wallet", "TravelAgencyController@updateWallet");

   Route::group([ "prefix" => "/agencies/{agency_id}/wallet"], function() {
        Route::get('', 'AgencyWalletHistoryController@index');
        Route::post("", 'AgencyWalletHistoryController@create');
   });

    //Distributors

    Route::get("/distributors/{distributor_id?}", 'DistributorController@index');
    Route::post("/distributors", 'DistributorController@create');
    Route::put("/distributors/{distributor_id}/contact-details", 'DistributorController@updateContactDetails');


});

Route::post('/agencies', 'TravelAgencyRegistrationController@register');
