<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDistributorDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('distributor_details', function (Blueprint $table) {
            $table->increments('id');
            $table->string('code', 25);
            $table->string('name');
            $table->string('slug')->nullable()->unique();
            $table->string('iata_or_tids_number')->nullable();
            $table->string('contact_person_name');
            $table->string('email');
            $table->string('designation');            
            $table->string('telephone',20);
            $table->string('mobile',20);
            $table->string('fax',20)->nullable();
            $table->string('currency',6);
            $table->integer('country_id')->index();
            $table->string('city');
            $table->string('address');
            $table->string('postal_code',20);
            $table->string('timezone',50);
            $table->string('website')->nullable();
            $table->string('logo_url')->nullable();
            $table->boolean('is_active')->default(false);
            $table->string('status',25)->default('pending');
            $table->string('reason_for_account_not_active', 500)->nullable();
            $table->string('contact_details', 1000)->nullable(); 
            $table->integer('created_by')->default(0);
            $table->integer('total_agents')->default(0);
            $table->string('other_details', 500)->nullable();
            
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('distributor_details');
    }
}
