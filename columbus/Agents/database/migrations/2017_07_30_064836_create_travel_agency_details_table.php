<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTravelAgencyDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('travel_agency_details', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('slug')->nullable()->unique();
            $table->string('agency_email');
            $table->string('admin_login_email');
            $table->string('contact_person_name');
            $table->string('designation');
            $table->string('iata_or_tids_number')->nullable();
            $table->string('currency',10);
            $table->integer('country_id')->index();
            $table->string('city');
            $table->string('address');
            $table->string('postal_code',20);
            $table->string('timezone',50);
            $table->string('telephone',20);
            $table->string('mobile',20);
            $table->string('fax',20)->nullable();
            $table->string('website')->nullable();
            $table->string('logo_url')->nullable();
            $table->boolean('is_active')->default(false);
            $table->boolean('is_suspended')->default(false);

            $table->integer('created_by')->default(0);
            //$table->integer('agency_owner_id')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('travel_agency_details');
    }
}
