<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDistributerIdCreditLimitOtherDetailsColumnToTravelDetailsAgencyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('travel_agency_details', function (Blueprint $table) {
            $table->integer('distributor_id')->default(0)->after('created_by');
            $table->decimal('credit_limit', 10,2)->nullable()->after('markups'); // 1000 0000
            $table->string('other_details', 500)->nullable()->after('invoice_settings');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('travel_agency_details', function (Blueprint $table) {
            $table->dropColumn('distributor_id');
            $table->dropColumn('credit_limit');
            $table->dropColumn('other_details');
        });
    }
}
