<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCompanyRegistrationCodeColumnToTravelAgencyDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('travel_agency_details', function (Blueprint $table) {
            $table->string('company_registration_code', 100)->nullable()->after('website');            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('travel_agency_details', function (Blueprint $table) {
            $table->dropColumn('company_registration_code');
        });
    }
}
