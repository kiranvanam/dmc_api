<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAgencyWalletHistoryTable extends Migration
{    
    public function up()
    {
        Schema::create('travel_agency_wallet_history', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('agency_id')->index();
            $table->decimal('available_balance', 10,2);
            $table->decimal('added_credit', 10,2);
            $table->string('currency');
            $table->string('recharged_by_user', 100);
            $table->timestamps();
        });
    }


    public function down()
    {
        Schema::dropIfExists('travel_agency_wallet_history');
    }
}
