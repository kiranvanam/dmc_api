<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MakeColumnDefaultValuesAsNullableToTravelAgencyDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('travel_agency_details', function (Blueprint $table) {
            $table->integer('distributor_id')->nullable()->change();
            $table->decimal('credit_limit', 10,2)->nullable()->change(); // 1000 0000
            $table->decimal('credit_limit_used', 10,2)->nullable()->change();
            $table->decimal('account_balance', 10, 2)->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('travel_agency_details', function (Blueprint $table) {
            //
        });
    }
}
