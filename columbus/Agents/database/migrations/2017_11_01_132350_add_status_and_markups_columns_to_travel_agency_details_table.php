<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddStatusAndMarkupsColumnsToTravelAgencyDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('travel_agency_details', function (Blueprint $table) {
            $table->string('status',15)->default('pending')->after('created_by');
            $table->string('reason_for_not_approving', 1000)->nullable()->after('status');
            $table->string('markups', 1000)->nullable()->after('reason_for_not_approving');
            $table->string('contact_details', 1000)->nullable()->after('markups');
            $table->string('invoice_settings', 1000)->nullable()->after('contact_details');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('travel_agency_details', function (Blueprint $table) {
            $table->dropColumn('status');
            $table->dropcolumn('reason_for_not_approving');
            $table->dropColumn('markups');
            $table->dropColumn('contact_details');
            $table->dropColumn('invoice_settings');
        });
    }
}
