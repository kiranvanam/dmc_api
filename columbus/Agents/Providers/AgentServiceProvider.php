<?php

namespace Columbus\Agents\Providers;

use Illuminate\Support\Facades\Route;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class AgentServiceProvider extends ServiceProvider
{
    protected $namespace = "Columbus\Agents\Http\Controllers";

    public function boot()
    {
        $this->app->router->group(['namespace' => $this->namespace], function() {
            if (! $this->app->routesAreCached()) {
                require __DIR__.'/../routes.php';
            }
        });
                
        $this->loadMigrationsFrom(__DIR__.'/../database/migrations');
    }
}
