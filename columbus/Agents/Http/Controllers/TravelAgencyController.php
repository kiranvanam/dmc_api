<?php

namespace Columbus\Agents\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;
use Columbus\Agents\Models\TravelAgency;

class TravelAgencyController extends ApiController
{
    
    public function index($agency_id=null)
    {
        if($agency_id)
            return $this->ok( TravelAgency::findOrFail($agency_id), "Agency details");
        else if( request()->query('distributor_id') ) {
            return $this->ok(TravelAgency::where('distributor_id', request()->query('distributor_id'))->orderBy('created_at', 'desc')->get() , "Distributor Agencies"); 
        }
        //all agencies
        return $this->ok(TravelAgency::orderBy('created_at', 'desc')->get(), "Agencies" );
    }

    public function update($agency_id)
    {
        $travelAgency = $this->getModel($agency_id);
        $travelAgency->update(request(['currency', 'timezone', 'status', 'reason_for_not_approving', 'other_details', 'distributor_id']));
        return $this->ok($travelAgency, "Agency details updated");
    }

    public function updateWallet($agency_id)
    {
        $travelAgency = $this->getModel($agency_id);
        $travelAgency->update( request(['credit_limit', 'credit_limit_used', 'account_balance']));

        return $this->ok([
                'id' => $travelAgency->id,
                'credit_limit' => $travelAgency->credit_limit,
                'account_balance' => $travelAgency->account_balance,
                'credit_limit_used'=> $travelAgency->credit_limit_used
                ],                 
                "Wallet is updated");
    }

    public function updateMarkups($agency_id)
    {
        $travelAgency = $this->getModel($agency_id);
        $travelAgency->update(['markups'=> request(['service_markups', 'label'])]);
        return $this->ok( $travelAgency , "Markups updated");
    }

    public function updateContactDetails($agency_id)
    {
        $travelAgency = $this->getModel($agency_id);
        $travelAgency->update( request(['contact_details']));
        return $this->ok( $travelAgency , "Contace details updated");
    }

    public function updateInvoiceCurrencies($agency_id)
    {
        $travelAgency = $this->getModel($agency_id);

        $travelAgency->update( ['invoice_settings'=> request(['currencies', 'invoice_taxes'])] );

        return $this->ok($travelAgency, "Agency Invoice currencies updated");
    }

    public function confirm($agency_id)
    {
        $travelAgency = $this->getModel($agency_id);

        $travelAgency->update([ 'status'=> 'confirmed']);
    }

    protected function getModel($agency_id)
    {
        return TravelAgency::where('id', $agency_id)->first();
    }
}