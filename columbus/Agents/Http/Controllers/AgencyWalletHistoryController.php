<?php

namespace Columbus\Agents\Http\Controllers;

use Illuminate\Support\Facades\DB;
use App\Http\Controllers\ApiController;
use Columbus\Agents\Http\Controllers\WalletHistory;

class AgencyWalletHistoryController extends ApiController
{
    public function index($agency_id)
    {

        $history = WalletHistory::where('agency_id', $agency_id)
                                                        ->orderBy('created_at','desc')
                                                        ->get();

        return $this->ok( $history, "Wallet Hisotry for agency" );
    }

    public function create($agency_id)
    {
        $agency = \Columbus\Agents\Models\TravelAgency::where('id', $agency_id)->first();

        $data= request()->only(['agency_id', 'available_balance', 'added_credit', 'currency']);
        $data['available_balance'] = $agency->account_balance ? $agency->account_balance : 0;
        $data['recharged_by_user'] = \Auth::user()->name;

        $txnRecord = WalletHistory::create($data);

        //update agency account_balance with
        $totalBalance = $txnRecord['available_balance'] + $txnRecord['added_credit'];
        $agency->update(['account_balance' => $totalBalance]);

        return $this->ok( $txnRecord , "Credit is added to the agency");
    }
}