<?php

namespace Columbus\Agents\Http\Controllers;

use App\Http\Controllers\ApiController;
use Columbus\Agents\Http\Requests\TravelAgencyCreationFormRequest;
use Columbus\Agents\Mail\AgentRegistered;
use Columbus\Utilities\FileManager;

class TravelAgencyRegistrationController extends ApiController
{

    public function register(TravelAgencyCreationFormRequest $request, FileManager $fileManager)
    {
        $agency = $request->save();

        // save logo
        if ($request->hasFile('logo_url')) {
            $path = "uploads/images/countries/$agency->country_id/agents/$agency->id" ;
            //update logo url
            $images = $fileManager->savePhotos($path , array_wrap($request->file('logo_url')));
            $agency->logo_url = $images[0];
            $agency->save();
        }


        // send email
        \Mail::to($agency->admin_login_email)
            ->cc("info@godubaitourism.com")
            ->send(new AgentRegistered($agency));

        return $this->ok($agency);
    }
}