<?php

namespace Columbus\Agents\Http\Controllers;

use App\User;
use Columbus\Auth\Models\Role;
use Columbus\Auth\Mail\UserAdded;
use App\Http\Controllers\ApiController;
use Columbus\Agents\Models\TravelAgency;
use Columbus\SystemSettings\Models\Code;

class TravelAgencyApprovalController extends ApiController
{
    public function approve($agency_id)
    {
        $agency = TravelAgency::find($agency_id);
        $randomString = request('password');
        $role = Role::where('slug', 'agency')->first();
        $data = [
            'name' => $agency->contact_person_name,
            "email" => $agency->admin_login_email,
            "designation"=> $agency->designation,
            "mobile" => $agency->mobile ? $agency->mobile : $agency->telephone,
            "status" => "active",
            "agency_id" => $agency->id,
            "timezone" => $agency->timezone,
            "password" => bcrypt($randomString),
            "roles"=> [ $role->id ],
            "code"=> ""
        ];

        $user = User::create($data);
        $user->code = Code::getNextCodeForModule("users");
        $user->save();
        $user->roles()->sync($data['roles']);

        $agency->status ="approved";
        $agency->save();

        $cc = [];
        \Mail::to($user->email)
            ->cc($cc)
            ->send(new \Columbus\Agents\Mail\AgencyApproved($agency, $user, $randomString));

        return $this->ok( $agency, "Agency Approved and login details sent to the provided email");
    }
}