<?php

namespace Columbus\Agents\Http\Controllers;

use App\Http\Controllers\ApiController;
use Columbus\Destination\Models\Country;

class CountryController extends ApiController
{
  public function save()
  {
    Country::where('is_agent', 1)
                ->update(['is_agent' => 0]);

    
    return $this->ok(
          Country::whereIn('id', request()->get('country_ids'))->update(['is_agent' => 1]),
          "Returning The Number Of Rows Affected"
        );
  }
}