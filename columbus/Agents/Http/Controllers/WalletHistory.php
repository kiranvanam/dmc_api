<?php

namespace Columbus\Agents\Http\Controllers;

use Illuminate\Database\Eloquent\Model;

class WalletHistory extends Model
{
    public $table = "travel_agency_wallet_history";
    
    protected $fillable = ['agency_id', 'available_balance', 'added_credit', 'currency', 'recharged_by_user'];

}