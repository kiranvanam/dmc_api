<?php

namespace Columbus\Agents\Http\Controllers;

use App\Http\Controllers\ApiController;
use Columbus\Agents\Models\Distributor;
use Columbus\Agents\Http\Requests\DistributorCreationRequest;

class DistributorController extends ApiController
{
    public function index($distributor_id = null)
    {
        
        return $this->ok(Distributor::all(), "Distributors");
    }

    public function create(DistributorCreationRequest $request)
    {        
        return $this->ok($request->save(), "Distributor added");
    }

    public function updateContactDetails($distributor_id)
    {
        $distributor = Distributor::findOrFail($distributor_id);

        $distributor->update( request(['contact_details']));

        return $this->ok($distributor, "Contact details updated");
    }
}
