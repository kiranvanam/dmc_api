<?php

namespace Columbus\Agents\Http\Requests;

use Columbus\Agents\Models\TravelAgency;
use Columbus\SystemSettings\Models\Code;
use Illuminate\Foundation\Http\FormRequest;

class TravelAgencyCreationFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "name" => "required",
            //"agency_email" => "required|email",
            "admin_login_email"=> "required|email|unique:travel_agency_details,admin_login_email",
            "contact_person_name" => "required|string",
            "designation" => "required|string",
            //"iata_or_tids_number"
            "currency" => "required|string",
            "country_id"=> "required|integer",
            'city' => "required|string",
            'address' => "required|string",
            'postal_code' => "required|string",
            'timezone'=> "required|string",
            'telephone'=> "required" ,
            'mobile'=> "required",
            //'fax' => "required",
            //'logo_url' => "required",
        ];
    }
    function messages() {
        return [
            'admin_login_email.unique' => "This email already is in use. If you are the authenticated user of this email and forgot your password, please contact admin@royaleagledubai.com"
        ];
    }

    public function prepareForValidation()
    {
        $this->merge(['slug' => str_slug($this->get('name'))]);
    }

    public function save()
    {
        $data = $this->all();
        $data['code'] = Code::getNextCodeForModule("agents");
        return TravelAgency::create($data);
    }
}
