<?php

namespace Columbus\Agents\Http\Requests;

use Illuminate\Support\Facades\Auth;
use Columbus\Agents\Models\Distributor;
use Columbus\SystemSettings\Models\Code;
use Illuminate\Foundation\Http\FormRequest;

class DistributorCreationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "name" => "required",
            "contact_person_name" => "required|string",
            "email" => "required|email",
            "designation" => "required|string",
            'telephone'=> "required" ,
            'mobile'=> "required",
            //"iata_or_tids_number"
            "currency" => "required|string",
            "country_id"=> "required|integer",
            'city' => "required|string", 
            'address' => "required|string", 
            'postal_code' => "required|string", 
            'timezone'=> "required|string",             
            //'fax' => "required",
            //'logo_url' => "required",
        ];
    }

    public function prepareForValidation()
    {
        $this->merge(['slug' => str_slug($this->get('name'))]);

    }

    public function save()
    {
        $data = $this->all();
        $data['code'] = Code::getNextCodeForModule("distributors");
        $data['created_by'] = Auth::id();
        return Distributor::create($data);
    }
}
