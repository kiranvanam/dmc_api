<?php

namespace Columbus\Agents\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TravelAgency extends Model
{   
    use SoftDeletes;
    protected $dates = ['deleted_at'];

    protected $table = "travel_agency_details";
    
    protected  $fillable = ['code', 'name', 'slug','agency_email', 'admin_login_email' ,'contact_person_name','designation','iata_or_tids_number',
                            'currency', 'country_id', 'city', 'address', 'postal_code', 'timezone', 'telephone', 'mobile','fax','website', 'company_registration_code', 
                            'logo_url', 'is_active','is_suspended', 'created_by', 'distributor_id', 'status', 'reason_for_not_approving', 'markups', 'contact_details',
                            'invoice_settings', 'credit_limit', 'credit_limit_used'  ,'account_balance', 
                           ];
    
    protected $casts = [
        'markups' => 'array',
        'contact_details' => 'array',
        'invoice_settings' => 'array'
    ];


    public static function agencyById($agencyId)
    {
        return self::findOrFail($agencyId);
    }

    public static function distributorIdForAgency($agencyId)
    {
        $agency  = self::agencyById($agencyId);

        if($agency) {
            return $agency->distributor_id;
        }

        return null;
    }

    public static function updateBalancesForBooking($booking)
    {
        $agency = self::where('id', $booking->agency_id)->first();
        if($agency) {
            $data = [];
            if($booking['payment_type'] == 'credit-limit'){
                $agency->credit_limit_used =  $agency->credit_limit_used + $booking->total_price; 
            } elseif($booking['payment_type'] == 'account-balance') {
                $agency->account_balance =  $agency->account_balance - $booking->total_price;  
            }
            $agency->save();
        }
        
        return $agency;
    }
}