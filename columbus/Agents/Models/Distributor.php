<?php

namespace Columbus\Agents\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Distributor extends Model
{   
    use SoftDeletes;
    protected $dates = ['deleted_at'];

    protected $table = "distributor_details";
    
    protected  $fillable = ['code', 'name', 'slug', 'email', 'contact_person_name','designation','iata_or_tids_number',
                            'currency', 'country_id', 'city', 'address', 'postal_code', 'timezone', 'telephone', 'mobile','fax','website', 
                            'logo_url', 'is_active','is_suspended', 'created_by', 'distributer_id', 'status', 'reason_for_account_not_active', 'contact_details',
                            'total_agents', 'other_details'
                           ];
    
    protected $casts = [
        'contact_details' => 'array',
        'other_details' => 'array'
    ];
}