<?php

namespace Columbus\Agents\Mail;


use Columbus\Agents\Models\TravelAgency;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class AgencyApproved extends Mailable
{
    use Queueable, SerializesModels;

    public $agency;
    public $user;
    public $password_with_no_encryption;

    public function __construct($agency, $user, $password_with_no_encryption)
    {
        $this->agency = $agency;
        $this->user = $user;
        $this->password_with_no_encryption = $password_with_no_encryption;
    }

    
    public function build()
    {
        return $this->from(config('mail.username'))
                    ->subject(config('app.name'). " Log-in Credentials for ".  $this->agency->name  ." !!!")                    
                    ->view('emails.agency-activated');
    }
}