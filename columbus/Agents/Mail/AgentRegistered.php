<?php

namespace Columbus\Agents\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Columbus\Agents\Models\TravelAgency;
use Columbus\Destination\Models\Country;
use Illuminate\Contracts\Queue\ShouldQueue;

class AgentRegistered extends Mailable
{
    use Queueable, SerializesModels;

    public $agency;
    public $country_name = [];

    public function __construct(TravelAgency $agency)
    {
        $this->agency = $agency;
        $this->country_name = Country::nameById($agency->country_id);
    }

    
    public function build()
    {
        return $this->from(config('mail.username'))
                    ->bcc(config('mail.bcc'))
                    ->subject( config('app.name'). ' - Agency registration Acknowledgement')
                    ->view('emails.registrations.agency');
    }
}
