<?php

namespace Columbus\ThirdParty\Utilities\GRN;

trait Refetch
{
    public function refetchRatesForHotel($data)
    {
        $this->startLog();

        $this->log("Refetch Hotel Rates :", $data);
        $client = $this->httpClient();
        $url = $this->refetchRatesForHotelUrl($data['search_id'], $data['hotel_code']);

        $response = $client->get($url);

        $result = $this->parseBody($response);

        $this->endLog();

        return $result;
    }

    public function fetchHotelGallery($hotel_code)
    {
        $this->startLog();

        $this->log("Fetching hotel gallery : $hotel_code");

        $client = $this->httpClient();
        $url = $this->hotelGalleryUrl($hotel_code);

        $response = $client->get($url);

        $result = $this->parseBody($response);

        $this->endLog();

        return $result;
    }

    public function fetchCancellationPolicies($data)
    {
        $this->startLog();

        $this->log("Fetching Cancellation Policies :", $data);

        $client = $this->httpClient();
        $url = $this->hotelCancellationPoliciesUrl(array_get($data, 'search_id'));

        $response = $client->post($url, ["json"=> $data]);

        $result = $this->parseBody($response);

        $this->endLog();

        return $result;
    }

    public function rateRecheck($data)
    {
        $this->startLog();

        $this->log("Rate Recheck :", $data);

        $requestData = array_only($data, ['group_code', 'rate_key']);
        $requestData['hotel_inf'] = false;

        $client = $this->httpClient();
        $url = $this->hotelRateRecheckUrl(array_get($data, 'search_id'));

        $response = $client->post($url, ["json"=> $requestData]);

        $result = $this->parseBody($response);

        $this->endLog();

        return $result;
    }
}