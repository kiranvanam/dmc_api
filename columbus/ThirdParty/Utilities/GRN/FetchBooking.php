<?php

namespace Columbus\ThirdParty\Utilities\GRN;

use Carbon\Carbon;
use Illuminate\Support\Facades\Log;

trait FetchBooking
{

    public function bookingDetails($bookingInfo)
    {
        Log::info("GRN::FETCHING-START");
        Log::info("Booking info", $bookingInfo);
        $data = [];
        if( empty($bookingInfo) == false ) {

            $url = "hotels/bookings/". array_get($bookingInfo, "booking_reference", "");
            try {

                $response = $this->httpClient()
                                    ->request(
                                        "GET",
                                        $url
                                    );

                $this->responseBody = json2Array($response->getBody());
                //$this->responseBody; //

                Log::info('GRN-Response', $this->responseBody );

                if( $this->hasResponseErrors())
                {
                    $data["errors"] = true;
                    $data["data"] = $this->errors();
                }else {
                    $data["data"] =  $this->prepareAccommodationDetails();
                }
            }
            catch(\Exception $exception)
            {
                logger("Exception while processing cancellation");
                $errorMsg = 'Exception on line '.$exception->getLine().' in '.$exception->getFile()
                            .': <b>'.$exception->getMessage();
                logger( $errorMsg);
                $data['errors'] = true;
                $data['data'] = [ "Problem happened from supplier end, please contact it technical team"];
            }

            Log::info("GRN-Processed Response", $data);
        }
        Log::info("GRN::FETCHING-END");

        return $data;
    }

    protected function prepareAccommodationDetails() {

        return [
            "service_source" => "grn-connect",
            "name" => $this->hotelDetailsForField('hotel.name'),
            "check_in" => $this->formatDate($this->hotelDetailsForField('checkin')),
            "check_out" => $this->formatDate($this->hotelDetailsForField('checkout')),
            "payment_deadline_date" => $this->cancelltionStartDate(),
            "total_price" => $this->hotelDetailsForField("price.total"),
            //currency: this.agency.agencyCurrency(),
            //destination_name: this.searchingDestinationName(),
            //total_rooms: this.hotelSearchCriteria.no_of_rooms,
            "country_code"=> $this->countryCode(),

            "star_category" => $this->hotelDetailsForField("hotel.category"),
            "status" => $this->getStatus(),
            "total_pax" => $this->paxDetails(),

            "accommodation_details" => [
                "description" => $this->hotelDetailsForField("hotel.description"),
                "address" => $this->hotelDetailsForField("hotel.address"),
                "description" => $this->hotelDetailsForField("hotel.description"),
                /* "booking_reference"=> $this->hotelDetailsForField("booking_reference"),
                "booking_id" => $this->hotelDetailsForField("booking_id"), */
                "booking_comments" => $this->hotelDetailsForField("booking_comment"),
                "booking_items" => $this->preprareRoomDetails(),
                "paxes" => $this->hotelDetailsForField("hotel.paxes"),
                /* "url" => $this->hotelDetailsForField("hotel.url"),
                "contact_number" => $this->hotelDetailsForField("hotel.phone_number"),
                "image" => $this->hotelDetailsForField("hotel.image"),
                "fax" => $this->hotelDetailsForField("hotel.fax"),
                "email" => $this->hotelDetailsForField("hotel.email"), */
                "additional_info" => $this->hotelDetailsForField("additional_info"),
                "payables_at_hotel" => $this->payablesAtHotel(),
                "other_details" => [
                    "booking_reference" => $this->hotelDetailsForField("booking_reference"),
                    "booking_id" => $this->hotelDetailsForField("booking_id"),
                ],
            ],
            "lead_passenger_details" => $this->leadPassaengerDetails()
        ];
    }

    protected function getStatus()
    {
        $status = $status = $this->hotelDetailsForField("grn_booking_status");
        if( $status == "C" ) {
            return "cancelled";
        }
        $status = $this->hotelDetailsForField("status");
        if( $status )
            return $status;
        return $this->hotelDetailsForField("booking_status", "unknown");
    }

    protected function countryCode()
    {
        $country_code = $this->hotelDetailsForField('hotel.country_code');

        $countryTransforms = [ "AE" => "UAE"];

        foreach ($countryTransforms as $value => $replaceWith) {
            if( $country_code == $value) {
                $country_code = $replaceWith;
                break;
            }
        }

        return  $country_code;
    }

    // prepare service_start_date & payment_deadline_date(based on cancellation policies)
    protected function cancelltionStartDate() {
        $firstDate = null; // when cancelaltion starts

        collect( $this->hotelDetailsForField("hotel.booking_items"))
            ->each( function($bookingItem) use(&$firstDate) {
                $cancellation_policy = array_get($bookingItem, 'cancellation_policy');
                if($cancellation_policy) {
                    collect($cancellation_policy["details"])
                        ->each( function($policyDetail) use(&$firstDate) {
                            if ($firstDate <  $this->formatDate($policyDetail["from"]))
                                    $firstDate = $this->formatDate($policyDetail["from"]);
                        });
                }
            });

        return $firstDate ? $this->formatDate($firstDate) : '';
    }

    protected function formatDate($date)
    {
        return Carbon::parse($date)->toDateTimeString();
    }


    protected function leadPassaengerDetails() {
        return [
            "name" => $this->hotelDetailsForField("holder.title") . " "
                      . $this->hotelDetailsForField("holder.name") . " "
                      . $this->hotelDetailsForField("holder.surname"),
            "mobile"=> $this->hotelDetailsForField("holder.phone_number"),
            "email" => $this->hotelDetailsForField("holder.email"), // consider agency email for this field. it's a B2B so need to provide any email
            "nationality" =>  $this->hotelDetailsForField("nationality")
        ];
    }

    protected function hotelDetailsForField($fieldName, $defaultValue="")
    {
        return array_get( $this->responseBody, $fieldName, $defaultValue);
    }

    protected function payablesAtHotel()
    {
        $payables =[];
        collect( $this->hotelDetailsForField('price.breakdown'))
                ->each( function ($breakDownPricings, $breakDownName) use(&$payables) {
                     collect($breakDownPricings)
                        ->each(function($breakDownDetail) use(&$payables) {
                                if( array_get( $breakDownDetail, 'included' ) != true ) {
                                    $payables[] = $breakDownDetail;
                                }
                        });
                });
        return $payables;
    }

    protected function preprareRoomDetails() {
        $rooms = [];
        $self = $this;
        //dd($this->hotelDetailsForField("hotel.booking_items"));
        collect($this->hotelDetailsForField("hotel.booking_items"))
            ->each(function($item) use(&$rooms, $self) {
                $room = [];
                $room["price"] = $item["price"];
                $room["cancellation_policy"] = array_get($item, "cancellation_policy", []);
                $room["includes_boarding"] = array_get($item, "includes_boarding");
                $room["boarding_details"] =  array_get($item, "boarding_details");

                $room["room_code"] = array_get($item, "room_code", "");
                $room["rate_key"] = array_get($item,"rate_key", "");
                //$room["rate_comments"] = array_get($item, "rate_comments");
                $room['rooms'] = [];

                collect($item["rooms"])
                    ->each( function($roomDetails) use(&$room, $self) {
                        $rd = [];
                        $rd["room_type"] = array_get($roomDetails, "room_type");
                        $rd["no_of_rooms"] = array_get($roomDetails,"no_of_rooms");
                        $rd["description"] = array_get($roomDetails, "description", "");
                        $rd["no_of_infants"] = array_get($roomDetails, "no_of_infants", 0);
                        $rd["no_of_children"] = array_get($roomDetails, "no_of_children");
                        $rd["no_of_adults"] = array_get($roomDetails, "no_of_adults");
                        $room['rooms'][] = $rd;
                    });
                $rooms[] = $room;

            });
        return $rooms;
    }

    protected function paxDetails()
    {
        $adults = 0;
        $children = 0;

        collect($this->hotelDetailsForField("hotel.paxes"))
            ->each( function($pax) use(&$adults, &$children) {
                $this->isAdult( $pax) ? ++$adults : ++$children;
            });
        return $children ?
                    ( ( $adults + $children ) . "(" . $adults . "-A, " . $children . "-C)" )
                    : ( $adults . "(" . $adults . "-A)");
    }

    function isAdult($paxDetail) {
        return array_get($paxDetail, "type", "AD") == "AD";
    }
}