<?php

namespace Columbus\ThirdParty\Utilities\GRN;

use Illuminate\Support\Facades\Log;

trait FetchVoucher
{
    public function generateVoucher($booking_info)
    {
        $this->startLog();

        $booking_reference = array_get($booking_info, 'booking_reference');

        $this->log(" Voucher Generate Request : booking reference - $booking_reference");

        $client = $this->httpClient();
        $url = $this->hotelFetchVoucherUrl($booking_reference);

        $response = $client->post($url);

        $result = $this->parseBody($response, true);

        $this->endLog();

        return $result;
    }

    public function fetchvoucher($booking_info)
    {
        $this->startLog();

        $booking_reference = array_get($booking_info, 'booking_reference');

        $this->log(" Voucher request: booking reference - $booking_reference");

        $client = $this->httpClient();
        $url = $this->hotelFetchVoucherUrl($booking_reference);

        $response = $client->get($url);

        $result = $this->parseBody($response);

        $this->endLog();

        return $result;
    }

}