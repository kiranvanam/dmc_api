<?php

namespace Columbus\ThirdParty\Utilities\GRN;

use Exception;

trait Book
{
    function fetchBookingWithSearchId($search_id)
    {
        $this->startLog();
        $this->log("Booking serarch with search-id: $search_id");
        $httpClient = $this->httpClient();
        $url = $this->hotelBookingUrl();

        $response = $httpClient->get($url);
        $result = $this->parseBody($response);
        $booking = null;
        if($this->isRequestSuccess($result)) {
           $booking = array_filter(array_get($result, 'bookings', []),
                                    function($booking) use ($search_id) {
                                        return $search_id == array_get($booking, 'search_id');
                                    }
                                );
        }

        $this->endLog();
        return $booking;
    }

    function fetchBookingWithReference($booking_reference)
    {
        $this->startLog();
        $this->log("Booking fetch with reference: $booking_reference");
        $httpClient = $this->httpClient();
        $url = $this->hotelBookingUrl($booking_reference);

        $response = $httpClient->get($url);
        $booking = $this->parseBody($response);

        $this->endLog();
        return $booking;
    }
    public function book($bookingDetails)
    {
        $this->startLog();
        $this->log("Booking Request", $bookingDetails);


        $client = $this->httpClient();
        $url = $this->hotelBookingUrl();
        $result = [];

        try {
            $response = $client->post($url, ["json"=> $bookingDetails]);
            $result = $this->parseBody($response);
        }catch(Exception $e) {
            $bookingDetails['errors'] = [ "code" => $e->getResponse()->getStatusCode(),
                                         "body" => $e->getResponse()->getBody()
                                        ];
            $this->log("Request Exception: ", $bookingDetails['errors']);
            $result = $bookingDetails;
        }

        $this->endLog();

        return $result;
    }
}