<?php

namespace Columbus\ThirdParty\Utilities\GRN;

use Carbon\Carbon;
use GuzzleHttp\Pool;
use GuzzleHttp\Client;
use GuzzleHttp\Promise;
use Columbus\ThirdParty\Accommodation\MasterData\GRN\Models\GrnDestination;

trait Search
{

    public function otherSearchParameters()
    {
        return [
                "currency" => "AED",
                "rates" => "concise",// "concise",//"comprehensive",
                "cuttoff_time" => 45000,
                /* "more_results" => true, */
                "hotel_category" => [],
            ];
    }

    /**
     *  reset variables so these will be used in this process
     * hotelsForSearchId=> [ 'search-id' => [ hotel1, hotel2 ],
     *             'search-id'=> [hotel1 hotel2]
     *          ]
     *
     * refetchSearchForSearchIds => [ 'search-id' => false,
     *             'search-id'=> true
     *          ]
     * false - no-refetch all hoels are retrived
     * true - refetch required
     *  step1: check if search-id/more_results - has more_reuslts if so prepare htpp client
     *  step 2: process again the result set with promises = []
     */
    public function search($searchCriteria)
    {
        $this->startLog();

        $this->searchCriteria = array_merge($searchCriteria, $this->otherSearchParameters());

        $this->hotelsForSearchId = [];
        $this->promises = [];
        $this->refetchSearchForSearchIds = [];

        //by default it logs every response whether it's failed or success.
        // in search requet handling we log at the final step
        $this->disableLogForSuccessResponse();

        $result = $this->formatSearchCriteria()
                        ->prepareAndSendSearchRequest() // step 1
                        ->processResponses() // step 2
                        ->resendSearchRequestsIfResponseHasMoreResultsFlagSet() // step 3 ( loop of step-2 )
                        ->sortHotels(); // step 4

        $this->endLog();
        return $result;
        //return json2Array($response->getBody());
    }

    /**
     * it removes unwated params in search request
     *  1. remove no_of_children and children_ages if no_of_children=0
     *  2. remove grn_code
     */
    public function formatSearchCriteria()
    {
        // delete no_of_children and children_ages from rooms if no_of_children=0
        $this->log("Search Request Before Format: ", $this->searchCriteria);
        $rooms = array_get($this->searchCriteria, 'rooms');
        $rooms = array_map( function($room) {
                                if( array_get($room, 'no_of_children', 0) == 0)
                                    unset($room['children_ages']);
                                unset($room['no_of_children']);
                                return $room;
                            }, $rooms);
        unset( $this->searchCriteria['hotel_category']);

        // remove grn_code(destination code from search data)
        $this->grn_destination_code = $this->searchCriteria['grn_code'];
        unset( $this->searchCriteria['grn_code']);

        $this->searchCriteria['rooms'] = $rooms;

        $this->log("Search Request Before Format: ", $this->searchCriteria);
        return $this;
    }



    function prepareAndSendSearchRequest()
    {
        $this->promises = $this->prepareAsynchronusHttpRequests();
        return $this;
    }

    function prepareAsynchronusHttpRequests()
    {
        $searchCriteria = $this->searchCriteria;
        $client = $this->httpClient();
        $url = $this->hotelsUrl();
        $hotelCodes = $this->hotelCodesForDestination($this->destinationCode());
        $promises = [];
        //$hotelSets= [];
        array_walk($hotelCodes, function($hotelSet4SearchId) use(&$promises,  $searchCriteria, $client, $url) {
            $searchCriteria['hotel_codes'] = $hotelSet4SearchId;
            logger("Searhc-criteria", $searchCriteria);
            $promises[] = $client->postAsync($url, [ "json" => $searchCriteria]);
        });
        return $promises;
    }

    function processResponses()
    {
        // Wait for the requests to complete, even if some of them fail
        $responses = Promise\settle($this->promises)->wait();

        foreach ($responses as $index => $result) {
            if($result['state'] === 'fulfilled')
            {
                $response = $result['value'];
                if($response->getStatusCode() == 200)
                {
                    $body = $this->parseBody($response);

                    $search_id = array_get($body, "search_id", '');

                    $resultHotelSet = array_get( $body, "hotels", []);
                    $no_of_hotels = array_get($body, "no_of_hotels", 0);
                    $areMoreResults = array_get($body, "more_results", false);

                    logger( "Total Hotels: $no_of_hotels");

                    if( $areMoreResults )
                        $this->log("Refetch required for - search($index) : $search_id");

                    $this->refetchSearchForSearchIds[$search_id] = $areMoreResults;
                    $this->hotelsForSearchId[$search_id] = $resultHotelSet;
                }
            } else if($result['state'] === 'rejected'){
                logger("GRN- Requested Rejected Response ");
                logger("Failed for search  : $index");
                logger("Reason ", [array_get($result, 'reason', "Reason is unknown")]);
                logger("***********END***********");
            } else  {
                logger("GRN- ERR: unknown exception");
                logger(" Failed for search : $index ");
                logger("***********END***********");
            }

        }
        // clear promises and refill in next step if refetch required for responses with more_results set true
        $this->promises = [];
        return $this;
    }

    function resendSearchRequestsIfResponseHasMoreResultsFlagSet()
    {
        $this->pendingRefetchPromises = [];
        $no_of_refetch_attempts = 1;

        $client = $this->httpClient();

        while($this->doesSearchRequireRefetch() && $no_of_refetch_attempts++ <= 3  ) {
            //create  promises for async HTTP requests to refetch
            array_walk($this->refetchSearchForSearchIds, function($isRefetch, $search_id) use($client) {
                if($isRefetch) {
                    $url = $this->hotelFefetchSearhResultlUrl($search_id);
                    $this->promises[] = $client->getAsync($url);
                }
            });
            $this->processResponses();
        }

        return $this;
    }

    public function doesSearchRequireRefetch()
    {
        array_walk($this->refetchSearchForSearchIds,
                    function($isRefetch, $search_id){
                        if($isRefetch)
                            $this->refetchSearchForSearchIds[$search_id] = $this->doesSearchIdHasMoreResults($search_id);
                    }
                );
        return  array_first($this->refetchSearchForSearchIds,
                            function($isRefetch, $search_id) {
                                return $isRefetch;
                            });
    }

    function doesSearchIdHasMoreResults($search_id)
    {
        $client = $this->httpClient();
        $this->log("Checking whetehr more_results are avialble With GRN for Search : $search_id  ");
        $url = $this->hotelFefetchMoreSearhResultChecklUrl($search_id);
        $response = $client->get($url);
        $result  = $this->parseBody($response);
        $areMoreResults = array_get($result, 'more_results', false);
        $totla_hotels = array_get($result, 'no_of_hotels', 'not returned');
        $this->log( " are More result available for search($search_id) - $areMoreResults : " . (  $areMoreResults ? "YES - $totla_hotels" : "NO - $totla_hotels" ), $result );
        return $areMoreResults;
    }

    function sortHotels()
    {
        $hotels = $this->mergeFinalResults();
        // final sorting can be optimized with other alogrithems
        usort($hotels,
                function($first, $second) use(&$no_of_hotels) {

                    $firstPrice = array_get( $first, 'min_rate.price') ?? array_get( $first, 'rates.0.price');
                    $secondPrice = array_get( $second, 'min_rate.price') ?? array_get( $second, 'rates.0.price');

                    return $firstPrice == $secondPrice ? 0 : $firstPrice < $secondPrice ? -1 : 1;
                });

        $total_hotels = count($hotels);
        $this->log(" Final Result has : $total_hotels Hotels");

        return [
            "no_of_hotels" => $total_hotels,
            "hotels" => $hotels
        ];
    }

    function mergeFinalResults()
    {
        $this->log("Final Hotels");
        $hotels = [];
        array_walk($this->hotelsForSearchId,
                function($hotelSet4SearchId, $search_id) use(&$hotels) {
                    $this->log(" search-id : $search_id , hotels: ", $hotelSet4SearchId);
                    $hotelSet4SearchId = $this->appendSearchIdToEveryHotel($hotelSet4SearchId, $search_id);
                    $hotels = array_merge($hotels, $hotelSet4SearchId);
                });
        return $hotels;
    }

    function appendSearchIdToEveryHotel($hotelSet4SearchId, $search_id)
    {
        return array_map(
                        function($hotel) use($search_id) {
                            $hotel['search_id'] = $search_id;
                            return $hotel;
                        },
                        $hotelSet4SearchId
                    );
    }

    public function destinationCode()
    {
        return $this->grn_destination_code; //array_get($this->searchCriteria, 'grn_code');
    }


    public function hotelCodesForDestination($destination_code)
    {
        $hotel_codes = GrnDestination::hotelCodesForDestination($destination_code);

        return array_chunk($hotel_codes, 250);
    }

    private function isRatesComprehensive()
    {
        return array_get($this->searchCriteria, 'rates', 'concise') == "comprehensive" ;
    }
}