<?php

namespace Columbus\ThirdParty\Utilities\GRN;

use Illuminate\Support\Facades\Log;

trait CancelBooking
{
    public function cancelBooking($booking_info)
    {
        $this->startLog();
        $booking_reference = array_get($booking_info, 'booking_reference');
        //$cancellation_comments = [];
        $this->log("Booking Cancellation booking-reference: $booking_reference");

        $cancellationDetails = [];

        if(empty($booking_reference) ){
            $cancellationDetails['error'] = "booking reference is missing";
        } else{
            $httpClient = $this->httpClient();
            $url = $this->hotelBookingUrl($booking_reference);
            $response = $httpClient->delete($url/* , ['json'=> $cancellation_comments] */);
            $cancellationDetails = $this->parseBody($response);

            /* waht if there was an error while cancelling in previous attempt
                and some error happed
            */
            if($this->isHotelAlreadyCancelled($cancellationDetails)){
                $cancellationDetails =[];
                $cancellationDetails["status"] = "already-cancelled";
            }
        }

        $this->endLog();

        //return $cancellationDetails;
        return $cancellationDetails;
       // abort(501, "Required Booikng reference code not fouund. pelase contact technical team" );
    }

    protected function isHotelAlreadyCancelled($result) {
        //dd( array_get($this->responseBody, "errors"), $this->errorCode());
        $errors = array_get($result, "errors", []);
        $alredyCancelError= array_filter($errors,
                            function($error) {
                                return array_get($error, 'code') == 2003;
                            });
        return empty($alredyCancelError) == false;
    }

    function cancellationDetails()
    {
        return json_decode('{
            "status": "confirmed",
            "cancellation_reference": "3pdfyjwuhixt2yyntfswtix36u",
            "cancellation_charges": {
                "currency": "INR",
                "amount": 2657
            },
            "cancel_date": "2019-04-24T11:46:05",
            "booking_reference": "rncm75527csaiygwvtxzbyuxry",
            "booking_price": {
                "currency": "INR",
                "amount": 2657
            },
            "booking_id": "GRN-201904-040039"
        }', true);
    }
}