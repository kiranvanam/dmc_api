<?php

namespace Columbus\ThirdParty\Utilities\GRN;

use GuzzleHttp\Client;
use Columbus\Utilities\Logger;

class Accommodation
{
    use Auth;
    use Search;
    use CancelBooking;
    use FetchBooking;
    use FetchVoucher;
    use Refetch;
    Use Url;
    Use Book;
    use Logger;

    protected $searchCriteria;
    protected $is_log_for_success_response_enabled = true;

    function __construct()
    {
        $this->initializeApiAuthenticationSettings();
    }

    protected function hasResponseErrors()
    {
        return array_has( $this->responseBody, 'errors');
    }


    protected function errors()
    {
        return array_get( $this->responseBody, 'errors.0.messages');
    }

    protected function errorCode()
    {
        return array_get( $this->responseBody, 'errors.0.code');
    }

    public function httpClient()
    {
        return  new Client(['base_uri' => $this->url()  , 'headers' =>  $this->settings['headers']]);
    }

    protected function url()
    {
        return $this->settings['base_uri'];
    }

    protected function method()
    {
        return $this->settings['method'];
    }

    protected function isInvalidBookingReference()
    {
        return $this->errorCode() == 6000;
    }

    function disableLogForSuccessResponse()
    {
        $this->is_log_for_success_response_enabled = false;
    }

    function isLogForSuccessResponseEnabled()
    {
        return $this->is_log_for_success_response_enabled;
    }

    function parseBody($response)
    {
        $result = [];
        if( $response->getStatusCode() == 200) {
            $result = \json2Array($response->getBody());

            if($this->isRequestSuccess($result)) {
                if($this->isLogForSuccessResponseEnabled()){
                    $this->log(" Request Successful: ", $result);
                }
            }else {
                $this->log(" Request failed with Errors : ", $result);
            }
        }
        return $result;
    }

    function logErrorsIfAny($result, $message="")
    {
        if( $this->isRequestSuccess($result)) {
            return;
        }

        $this->log("$message Response Errors :", $result);
    }

    function isRequestSuccess($result)
    {
        $errors = array_get($result, 'errors');
        return empty($errors);
    }

}