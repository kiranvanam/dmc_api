<?php

namespace Columbus\ThirdParty\Utilities\GRN;

trait Url
{

    public function hotelsUrl()
    {
        $url = "hotels/availability";
        $this->log("URL : $url");
        return $url;
    }

    public function hotelFefetchSearhResultlUrl($search_id)
    {
        $url = "hotels/availability/$search_id/?rates=concise"; //?hotel_info=false
        $this->log("URL: $url");
        return $url;
    }

    public function hotelFefetchMoreSearhResultChecklUrl($search_id)
    {
        $url = "hotels/availability/$search_id/more_results"; //?hotel_info=false
        $this->log("URL: $url");
        return $url;
    }

    public function refetchRatesForHotelUrl($search_id, $hotel_code)
    {
        $url = "hotels/availability/$search_id?hcode=$hotel_code&bundled=true";

        $this->log("URL: $url");
        return $url;
    }

    public function hotelGalleryUrl($hotel_code)
    {
        $url = "hotels/$hotel_code/images";

        $this->log("URL: $url");
        return $url;
    }

    public function hotelCancellationPoliciesUrl($search_id)
    {
        $url = "hotels/availability/$search_id/rates/cancellation_policies/";

        $this->log("URL: $url");
        return $url;
    }

    public function hotelRateRecheckUrl($search_id)
    {
        $url = "hotels/availability/$search_id/rates?action=recheck";

        $this->log("URL: $url");
        return $url;
    }

    public function hotelBookingUrl($booking_reference = null)
    {
        $url = empty($booking_reference) ? "hotels/bookings" : "hotels/bookings/$booking_reference";
        $this->log("URL : $url");
        return $url;
    }

    public function hotelFetchVoucherUrl($booking_reference)
    {
        $url = "hotels/bookings/$booking_reference/voucher";
        $this->log("URL : $url");
        return $url;
    }

}