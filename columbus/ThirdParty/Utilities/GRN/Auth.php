<?php

namespace Columbus\ThirdParty\Utilities\GRN;

use Illuminate\Support\Facades\App;

trait Auth
{
    public function initializeApiAuthenticationSettings()
    {
        $api_key = ""; //"production-api-key";
        $base_uri = "";
        if (App::environment(['local', 'staging'])) {
            $this->log("GRN Testing env");
            $api_key = "35d4ad56cf1dad172a0d5e35ac575bb2"; //it@gdt testing api-key
            //$api_key = "027fc54b57b947ebc85702d6dbf2c6b3"; //itoverseas@gdt
            $base_uri = 'https://api-sandbox.grnconnect.com/api/v3/';
        }elseif( App::environment(['production', 'prod']) ) {
            $this->log("GRN Production/Live env");
            $api_key = "1bf4af783f7e76e753b4dfc06a8ddedb";
            $base_uri = "https://v3-n3-api.grnconnect.com/api/v3/";
        }
        // read from database or hardcode for each client
        $this->settings = [
            "headers" => [
                    "Content-Type" => "application/json",
                    "Accept" => "application/json",
                    "api-key" => $api_key
                ],
            'base_uri' => $base_uri,
        ];
    }
}