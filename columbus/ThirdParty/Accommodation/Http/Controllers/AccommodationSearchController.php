<?php

namespace Columbus\ThirdParty\Accommodation\Http\Controllers;

use Carbon\Carbon;
use App\Http\Controllers\ApiController;
use Columbus\ThirdParty\Utilities\GRN\Accommodation;

class AccommodationSearchController extends ApiController
{

  public function search()
  {

    $data = request()->all();

    $searchCriteria = $data;

    $d1 = Carbon::now();

    $searchCriteria = [
              "hotel_codes" => [],
              "checkin" => "2019-04-14",
              "checkout" => "2019-04-16",
              "client_nationality" => "IN",
              "currency" => "INR",
              "rates" => "concise",
              "cuttoff_time" => 15000,
              "more_results" => true,
              "hotel_category" => [],
              "rooms" => [
                    [
                      "adults" => "2"
                    ],
                    [
                      "adults" => "2"
                    ]
              ]
        ];

    $accommodation =  new Accommodation();
    $searchResult = $accommodation->search($searchCriteria);

    $d2 = Carbon::now();
    $total_processing_time = $d2->diffInSeconds($d1);
    $results['no_of_hotels'] = count($searchResult);
    $results['time_taken'] = $total_processing_time;
    logger("GRN- response total time taken - $total_processing_time seconds");
    $results['hotels'] = $searchResult;

    return $this->ok($results, "Hotel Results");
  }

  public function store()
  {
/*     $searchCriteria = [
               "hotel_codes" => [],
              "checkin" => "2019-04-14",
              "checkout" => "2019-04-16",
              "client_nationality" => "IN",
              "currency" => "INR",
              "rates" => "comprehensive",
              "cuttoff_time" => 15000,
              "more_results" => true,
              "hotel_category" => [],
              "rooms" => [
                    [
                      "adults" => "2"
                    ],
                    [
                      "adults" => "3"
                    ]
              ]
        ];

    $accommodation =  new Accommodation();
    $searchResult = $accommodation->search($searchCriteria);

    \Storage::disk('public')->put("grn-search-hotels-comprehensive.json", json_encode($searchResult));
 */
    $result = \Storage::disk('public')->get("/grn-search-hotels-comprehensive.json");
    return  json_decode($result, true);
  }

  public function cancel()
  {
    $accommodation = AccommodationBooking::first();

    $bookingReference = array_get($accommodation->accommodation_details['other_details'], 'booking_reference');

    $apiClient = new Accommodation();

    return $apiClient->cancel($bookingReference);
  }
}