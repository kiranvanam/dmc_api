<?php

namespace Columbus\ThirdParty\Accommodation\MasterData\GRN;

use Illuminate\Console\Command;
use Columbus\ThirdParty\Accommodation\MasterData\GRN\Models\GrnDestination;

class MasterSeeder extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'thirdparty:grn-master';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'It feeds GRN Hotel Data';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // read hotels
         $hotelSeeder = new HotelSeeder();
        list("created"=> $created, "updated" => $updated) = $hotelSeeder->seed();
        $this->info(" Hotels Created: $created , Updated: $updated");
        // read destination
        $destinationSeeder = new DestinationSeeder();
        list("created"=> $created, "updated" => $updated) = $destinationSeeder->seed();
        $this->info(" Hotels Created: $created , Updated: $updated");

        // read hotel images for hotel_code

    }
}
