<?php

namespace Columbus\ThirdParty\Accommodation\MasterData\GRN;

use Columbus\Utilities\CSVFileReader;
use Columbus\ThirdParty\Accommodation\MasterData\GRN\Models\GrnHotel;

class HotelSeeder
{

    public function seed()
    {
        $headers = [
                "Hotel Code" => "hotel_code",
                "Hotel Name" => "hotel_name",
                "Description" => "description",
                "City Code" => "city_code" ,
                "Destination Code" => "destination_code",
                "Country Code" => "country_code",
                "Star Category" => "star_category",
                "Address"=> "address",
                "Postal Code" => "postal_code",
                "Latitude" => "latitude",
                "Longitude" => "longitude",
                "Accommodation Type" => "accommodation_type",
            ];

        $fileName = dirname(__FILE__) . "/seeders/hotel_master.tsv";
        $loader = new CSVFileReader($fileName);
        $loader->setDelimiter("\t");
        //$loader->setHeaders($headers);
        $hotels = [];
        $newRecords = 0;
        $updatedRecords = 0;
        foreach ($loader->getItems() as $index => $record) {

            $newRecord = [];
            array_walk( $record , function($value, $key) use($headers, &$newRecord) {
                if(array_key_exists($key, $headers))
                    $newRecord[$headers[$key]] = $value;
            });
            $hotel = GrnHotel::where('hotel_code', $newRecord['hotel_code'])->first();

             if( empty($hotel)) {
                GrnHotel::create($newRecord);
                ++$newRecords;
            }else {
                $hotel->update($newRecord);
                ++$updatedRecords;
            }
        }
        return  [ "created" => $newRecords,  "updated" => $updatedRecords ];
    }
}
