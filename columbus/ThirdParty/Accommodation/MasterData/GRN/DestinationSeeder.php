<?php

namespace Columbus\ThirdParty\Accommodation\MasterData\GRN;

use Columbus\Utilities\CSVFileReader;
use Columbus\ThirdParty\Accommodation\MasterData\GRN\Models\GrnHotel;
use Columbus\ThirdParty\Accommodation\MasterData\GRN\Models\GrnDestination;

class DestinationSeeder
{

    function seed()
    {
        $headers = [
                "Destination Name" => "destination_name" ,
                "Destination Code" => "destination_code",
                "Country Code" => "country_code",
            ];

        $fileName = dirname(__FILE__) . "/seeders/dest_master.tsv";
        $loader = new CSVFileReader($fileName);
        $loader->setDelimiter("\t");

        $hotels = [];
        $newRecords = 0;
        $updatedRecords = 0;


        foreach ($loader->getItems() as $index => $record) {
            if( $this->isCountryDestinationRequired( array_get($record, 'Country Code'))) {
                $newRecord = [];
                array_walk( $record , function($value, $key) use($headers, &$newRecord) {
                    if(array_key_exists($key, $headers))
                        $newRecord[$headers[$key]] = $value;
                });
                $destination_code = $newRecord['destination_code'];
                $destination = GrnDestination::where('destination_code', $destination_code)->first();

                $newRecord["hotel_codes"] = GrnHotel::hotelCodesByDestination($destination_code);

                if( empty($destination)) {
                    GrnDestination::create($newRecord);
                    ++$newRecords;
                }else {
                    $destination->update($newRecord);
                    ++$updatedRecords;
                }
            }
        }
        return  [ "created" => $newRecords,  "updated" => $updatedRecords ];
    }

    private function isCountryDestinationRequired($country_code)
    {
        //var_dump($country_code, array_search( $country_code,  ["AE"]));
        return array_search( $country_code,  ["AE"]) > -1;
    }
}