<?php

namespace Columbus\ThirdParty\Accommodation\MasterData\GRN\Models;

use Illuminate\Database\Eloquent\Model;

class GrnDestination extends Model
{
    public $table = "grn_destinations";
    protected $fillable =  [ "country_code", "destination_name", "destination_code", "hotel_codes"];

    protected $casts = [
            "hotel_codes" => "array"
        ];

    static function hotelCodesForDestination($destination_code)
    {
        $destination = GrnDestination::where('destination_code', $destination_code)->first();
        if(empty($destination))
            return [];

        return array_get($destination->toArray(), 'hotel_codes', []);
    }

    static function allMatchedDestinationsForName($destination_name)
    {
        return  static::where('destination_name', 'LIKE', $destination_name)
                        ->select('destination_name as grn_name', 'destination_code as grn_code')
                        ->get()
                        ->toArray();
    }
}