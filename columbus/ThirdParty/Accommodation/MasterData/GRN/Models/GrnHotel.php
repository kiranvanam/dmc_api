<?php

namespace Columbus\ThirdParty\Accommodation\MasterData\GRN\Models;


use Illuminate\Database\Eloquent\Model;

class GrnHotel extends Model
{
    public $table = "grn_hotels";
    protected $fillable =  [ "hotel_code","hotel_name", "description",
                "city_code","destination_code", "country_code",
                "star_category",  "adddress","postal_code",
                "latitude", "longitude", "accommodation_type"];

    static function hotelCodesByDestination($destination_code)
    {
        return GrnHotel::where('destination_code', $destination_code)
                        //->where('city_code', 'C!000968')
                        ->get()
                        ->pluck('hotel_code')
                        ->toArray();
    }
}
