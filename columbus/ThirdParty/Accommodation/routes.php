<?php

use App\Http\Controllers\ApiController;

Route::prefix('api/search/')
      //->middleware('auth:api')

      ->group(function(){
            //Route::post('hotels/', 'AccommodationSearchController@search');
            Route::get('grn-hotels/', 'AccommodationSearchController@search');
            Route::get('grn-hotels-store', 'AccommodationSearchController@store');
            Route::get('hotels/cancel', 'AccommodationSearchController@cancel');
            //Route::get('hotels/grn', 'HotelSearchController@search');
      });
Route::get("store-grn-response", function(){


});
