<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGrnHotelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        /*
        $headers = [ "hotel_code","hotel_name", "description",
            "city_code","destination_code", "country_code",
            "star_category",  "adddress","postal_code",
            "latitude", "longitude", "accommodation_type",
            ];
         */
        Schema::create('grn_hotels', function (Blueprint $table) {
            $table->increments('id');
            $table->string('hotel_code', 50)->index();
            $table->string('hotel_name');
            $table->text('description');
            $table->string('city_code', 50)->index();
            $table->string('destination_code', 50)->index();
            $table->string('country_code', 10)->index();
            $table->string('star_category', 4)->nullable();
            $table->string('address')->nullable();
            $table->string('postal_code')->nullable();
            $table->string('latitude')->nullable();
            $table->string('longitude')->nullable();
            $table->string('accommodation_type')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //Schema::dropIfExists('grn_hotels');
    }
}
