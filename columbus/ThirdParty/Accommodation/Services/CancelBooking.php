<?php

namespace Columbus\ThirdParty\Hotel\Services;

trait CancelBooking
{
    public function cancel($booking_reference)
    {
        return ["booking_reference" => $booking_reference];
    }
}