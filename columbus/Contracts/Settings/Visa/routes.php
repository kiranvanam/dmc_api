<?php

Route::group(['prefix'=>'/api/settings/visa', /*'middleware'=>'auth'*/], function(){
    Route::group(['prefix' => 'child-ages'], function () {
        Route::get('/', 'VisaChildAgeController@all');
        Route::post('/', 'VisaChildAgeController@save');
        Route::put('/{id}', 'VisaChildAgeController@update');
        Route::delete('/{id}', 'VisaChildAgeController@delete');
    });
});
