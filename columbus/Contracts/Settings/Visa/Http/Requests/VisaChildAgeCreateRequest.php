<?php

namespace Columbus\Contracts\Settings\Visa\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class VisaChildAgeCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'country_id' => 'required',
            'min_age' => 'required',
            'max_age' => 'required',
        ];
    }

    protected function prepareForValidation()
    {
        $this->merge([]);
    }

    /**
     * customizing error messages for (field,rule)
     */
    public function messages()
    {
        return [
            'country_id.required' => 'Please Select A Country',
            'min_age.required' => 'Please Enter A Valid Min Age',
            'max_age.required' => 'Please Enter A Valid Max Age',
        ];
    }

}
