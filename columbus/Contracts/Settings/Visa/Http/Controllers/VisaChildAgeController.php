<?php

namespace Columbus\Contracts\Settings\Visa\Http\Controllers;

use App\Http\Controllers\ApiController;
use Columbus\Contracts\Settings\Visa\Http\Requests\VisaChildAgeCreateRequest;
use Columbus\Destination\Models\Country;
use Columbus\Contracts\Settings\Visa\Models\VisaChildAge;
use Illuminate\Http\Request;

class VisaChildAgeController extends ApiController
{
    public function all(Request $request) {
        return $this->ok(VisaChildAge::all());
    }

    public function save(VisaChildAgeCreateRequest $request)
    {
        return $this->ok(VisaChildAge::create($request->all()), "Child Age Created Successfully");
    }

    public function update($id, VisaChildAgeCreateRequest $request)
    {
        $vca = VisaChildAge::find($id);
        $vca->update($request->all());
        return $this->ok($vca , "Child Age Details Updated Successfully");
    }

    public function delete($id)
    {
        $vca = VisaChildAge::find($id);

        // Delete The Tax
        $vca->delete();

        return $this->ok($vca, "Child Age Deleted Successfully");
    }
}
