<?php

namespace Columbus\Contracts\Settings\Visa\Models;

use Illuminate\Database\Eloquent\Model;

class VisaChildAge extends Model
{
    public $timestamps = false;
    protected $fillable = ['country_id', 'min_age', 'max_age'];
}
