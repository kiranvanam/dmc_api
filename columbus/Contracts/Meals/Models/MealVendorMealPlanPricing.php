<?php

namespace Columbus\Contracts\Meals\Models;

use Illuminate\Database\Eloquent\Model;


class MealVendorMealPlanPricing extends Model
{   
    protected $table = "meal_vendor_meal_plan_pricing";
    public  $timestamps = false;

    protected $fillable = ['meal_vendor_meal_plan_detail_id', 'date', 'time_slots_with_pricing_details', 'currency', 'is_available', 
                            'is_on_request'];

    protected $dates = ['date'];

    protected $casts = [
        "time_slots_with_pricing_details" => "array"
    ];
}
