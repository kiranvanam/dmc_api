<?php

namespace Columbus\Contracts\Meals\Models;

use Illuminate\Database\Eloquent\Model;


class MealCategory extends Model
{   
    public  $timestamps = false;

    protected $fillable = ['name', 'country_id'];

}
