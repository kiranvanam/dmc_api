<?php

namespace Columbus\Contracts\Meals\Models;

use Illuminate\Database\Eloquent\Model;


class MealPreference extends Model
{   
    public  $timestamps = false;

    protected $fillable = ['name'];

}
