<?php

namespace Columbus\Contracts\Meals\Models;

use Illuminate\Database\Eloquent\Model;

class MealVendorDetailCancellationPolicy extends Model
{   
    protected $table = "meal_vendor_detail_cancellation_policies";
    
    protected $fillable = ['meal_vendor_detail_id',
                            'booking_start_date', 'booking_end_date',
                            'travelling_start_date', 'travelling_end_date',
                            'is_non_refundable',
                            'policy_rules'
                        ];

    protected $casts = [
        'policy_rules' => 'array'
    ];
}