<?php

namespace Columbus\Contracts\Meals\Models;

use Illuminate\Database\Eloquent\Model;
use Columbus\Contracts\Meals\Models\MealVendorMealPlanPricing;


class MealVendorPlan extends Model
{
    protected $table = "meal_vendor_meal_plan_details";
    public  $timestamps = false;

    protected $fillable = ['meal_vendor_detail_id', 'name', 'code',
            'description', 'menu_details', 'inclusions', 'exclusions','terms_and_conditions',
            'remarks', 'filters', 'gallery'
        ];

    protected $casts = [
        'gallery'=>'array',
        'filters' => 'array'
    ];

    /*  */
    public function pricing()
    {
        return $this->hasOne(MealVendorMealPlanPricing::class, 'meal_vendor_meal_plan_detail_id');
    }
}
