<?php

namespace Columbus\Contracts\Meals\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class MealType extends Model
{   
    public $timestamps = false;
    
    protected $fillable = ['name', 'is_menu_item_wise', 'country_id'];

}
