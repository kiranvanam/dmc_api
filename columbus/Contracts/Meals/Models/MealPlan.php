<?php

namespace Columbus\Contracts\Meals\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class MealPlan extends Model
{   
    public $timestamps = false;

    protected $fillable = ['name', 'country_id'];

}
