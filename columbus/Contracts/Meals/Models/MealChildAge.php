<?php

namespace Columbus\Contracts\Meals\Models;

use Illuminate\Database\Eloquent\Model;


class MealChildAge extends Model
{   
    public  $timestamps = false;

    protected $fillable = [ 'country_id', 'min_age', 'max_age', 'description'];

}
