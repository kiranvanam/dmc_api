<?php

namespace Columbus\Contracts\Meals\Models;

use Illuminate\Database\Eloquent\Model;


class MealVendorPlanMenuItem extends Model
{   
    protected $table = "meal_vendor_plan_menu_item_details";
    
    public  $timestamps = false;

    protected $fillable = ['meal_vendor_plan_id', 'name', 'code', 'quantity', 'price', 'description', 
                 'inclusions', 'exclusions','remarks', 'gallery'];

    protected $casts = [
        'gallery'=>'array'
    ];
}
