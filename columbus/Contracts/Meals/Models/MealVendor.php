<?php

namespace Columbus\Contracts\Meals\Models;

use App\CancellationPolicy;
use Illuminate\Database\Eloquent\Model;


class MealVendor extends Model
{   
    protected $table = "meal_vendor_details";

    public  $timestamps = false;

    protected $fillable = ['city_id','vendor_id', 'vendor_code', 
                        'category_id', 
                        'name', 
                        'meal_plan_ids', 'description', 'thumbnail'];

    protected $casts = [
        'meal_plan_ids' => 'array'
    ];

    public function mealPlans()
    {
        return $this->hasMany(MealVendorPlan::class, 'meal_vendor_detail_id');
    }

    public function mealPlan()
    {
        return $this->hasOne(MealVendorPlan::class, 'meal_vendor_detail_id');
    }

    function cancellationPolicies()
    {
        return $this->hasOne(CancellationPolicy::class, 'service_id')->where('service', 'meal-vendor');
    }
}
