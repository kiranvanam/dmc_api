<?php

use Columbus\Contracts\Meals\Models\MealVendorPlanPricing;
use Columbus\Utilities\DateUtil;
use Illuminate\Http\Request;


Route::group(['prefix'=> '/api', 'middleware'=> ['auth:api','cors']], function(){

     Route::get("meal-settings", 'MealPlanController@mealSettings');

     Route::group(['prefix'=>'meal-types'], function() {
        Route::get('', 'MealTypeController@index');
        Route::post('', 'MealTypeController@create');
        Route::put("/{id}", 'MealTypeController@update');
        Route::delete('/{id}', 'MealTypeController@delete');
     });

     Route::group(['prefix'=>'meal-plans'], function() {
        Route::get('', 'MealPlanController@index');
        Route::post('', 'MealPlanController@create');
        Route::put("/{id}", 'MealPlanController@update');
        Route::delete('/{id}', 'MealPlanController@delete');
     });

     Route::group(['prefix'=>'meal-child-ages'], function() {
        Route::get('', 'ChildAgeController@index');
        Route::post('', 'ChildAgeController@create');
        Route::put("/{id}", 'ChildAgeController@update');
        Route::delete('/{id}', 'ChildAgeController@delete');
     });

     Route::group(['prefix'=>'cuisines'], function() {
        Route::get('', 'CuisineController@index');
        Route::post('', 'CuisineController@create');
        Route::put("/{id}", 'CuisineController@update');
        Route::delete('/{id}', 'CuisineController@delete');
     });

     Route::group(['prefix'=>'meal-categories'], function() {
        Route::get('', 'MealCategoryController@index');
        Route::post('', 'MealCategoryController@create');
        Route::put("/{id}", 'MealCategoryController@update');
        Route::delete('/{id}', 'MealCategoryController@delete');
     });

     Route::group(['prefix'=>'meal-preferences'], function() {
        Route::get('', 'MealPreferenceController@index');
        Route::post('', 'MealPreferenceController@create');
        Route::put("/{id}", 'MealPreferenceController@update');
        Route::delete('/{id}', 'MealPreferenceController@delete');
     });

    // this route is used by private-tours to map restaurants to private tour
    Route::get("/meal-vendors",'MealVendorDetailController@byCity');

    Route::group(['prefix'=> 'vendors/{vendor_id}/meals'], function (){
        Route::get('', 'MealVendorDetailController@index');
        Route::post('', 'MealVendorDetailController@create');
        Route::put('{id}', 'MealVendorDetailController@update');
            Route::put('{id}/thumbnail', 'MealVendorDetailController@updateThumbnail');

        Route::group(['prefix' => "{meal_vendor_detail_id}/cancellation-policies"], function(){
            Route::get("", 'MealVendorCancellationPolicyController@index');
            Route::post("", "MealVendorCancellationPolicyController@create");
            Route::put("{cancellation_policy_id}", 'MealVendorCancellationPolicyController@update');
            Route::delete("{cancellation_policy_id}", "MealVendorCancellationPolicyController@delete");
        });

        Route::group(['prefix'=> '{meal_vendor_detail_id}/plan-details'], function() {
            Route::post('/', 'MealVendorPlanDetailController@create');
            Route::put('{meal_vendor_meal_plan_id}', 'MealVendorPlanDetailController@update');
            Route::put('{meal_vendor_meal_plan_id}/filters', 'MealVendorPlanDetailController@updateFilters');
            Route::post('/{meal_vendor_meal_plan_id}/gallery', 'MealVendorPlanDetailController@updateGallery');

            Route::post("{meal_vendor_meal_plan_id}/pricing", 'MealVendorMealPlanPricingController@pricingForRange');
            Route::put("{meal_vendor_meal_plan_id}/pricing", 'MealVendorMealPlanPricingController@update');
        });

    });

    Route::group(['prefix'=> 'menu-items'], function(){
        Route::get('', 'MenuItemController@index');
        Route::post('', 'MenuItemController@create');
        Route::put('{menu_item_id}', 'MenuItemController@update');
        Route::post('{menu_item_id}/gallery', 'MenuItemController@uploadGallery');
    });
});
