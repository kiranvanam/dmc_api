<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMealVendorPlanPricingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('meal_vendor_meal_plan_pricing', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('meal_vendor_meal_plan_detail_id');
            $table->date('date');
            $table->text('time_slots_with_pricing_details')->null();
            $table->boolean('is_available')->default(1);
            $table->boolean('is_on_request')->default(false);

            $table->index('meal_vendor_meal_plan_detail_id', 'meal_vendor_meal_plan_detail_id_index');
            $table->index('date');
            $table->index(['meal_vendor_meal_plan_detail_id', 'date'], 'meal_vendor_meal_plan_detail_id_and_date_index');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('meal_vendor_meal_plan_pricing');
    }
}
