<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCurrencyColumnToMealVendorMealPlanPricingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('meal_vendor_meal_plan_pricing', function (Blueprint $table) {
            $table->string('currency', 6)->default('USD')->after('time_slots_with_pricing_details');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('meal_vendor_meal_plan_pricing', function (Blueprint $table) {
            $table->dropColumn('currency');
        });
    }
}
