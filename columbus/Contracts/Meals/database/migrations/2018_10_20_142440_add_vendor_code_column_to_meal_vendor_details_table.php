<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddVendorCodeColumnToMealVendorDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('meal_vendor_details', function (Blueprint $table) {
            $table->string('vendor_code', 25)->default("")->after('vendor_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('meal_vendor_details', function (Blueprint $table) {
            $table->dropColumn('vendor_code');
        });
    }
}
