<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFiltersColumnToMealVendorMealPlanDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('meal_vendor_meal_plan_details', function (Blueprint $table) {
            $table->string('filters', 100)->nullable()->after('remarks');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('meal_vendor_meal_plan_details', function (Blueprint $table) {
            $table->dropColumn('filters');
        });
    }
}
