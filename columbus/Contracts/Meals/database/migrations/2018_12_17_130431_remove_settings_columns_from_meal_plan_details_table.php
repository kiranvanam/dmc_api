<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveSettingsColumnsFromMealPlanDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('meal_vendor_meal_plan_details', function (Blueprint $table) {

            if(Schema::hasColumn('meal_vendor_meal_plan_details', 'meal_plan_id'))
                $table->dropColumn('meal_plan_id');

            if(Schema::hasColumn('meal_vendor_meal_plan_details', 'cuisine_id'))
                $table->dropColumn('cuisine_id');

            if(Schema::hasColumn('meal_vendor_meal_plan_details', 'meal_preference_id'))
                $table->dropColumn('meal_preference_id');

            if(Schema::hasColumn('meal_vendor_meal_plan_details', 'meal_type_id'))
                $table->dropColumn('meal_type_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('meal_vendor_meal_plan_details', function (Blueprint $table) {
            //
        });
    }
}
