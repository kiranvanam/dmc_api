<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIsMenuItemWiseColumnToMealTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('meal_types', function (Blueprint $table) {
            $table->smallInteger('is_menu_item_wise')->default(0)->after('name');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('meal_types', function (Blueprint $table) {
            $table->dropColumn('is_menu_item_wise');
        });
    }
}
