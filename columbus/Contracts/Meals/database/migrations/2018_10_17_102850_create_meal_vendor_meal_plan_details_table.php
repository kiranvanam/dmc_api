<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMealVendorMealPlanDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('meal_vendor_meal_plan_details', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('meal_vendor_detail_id');
            $table->string('code',100);
            $table->integer('meal_plan_id');
            $table->integer('cuisine_id');
            $table->integer('meal_preference_id');
            $table->integer('meal_type_id');
            $table->string('description')->nullable();
            $table->string('menu_details')->nullable();
            $table->string('inclusions')->nullable();
            $table->string('exclusions')->nullable();
            $table->string('terms_and_conditions')->nullable();
            $table->string('cancellation_policies')->nullable();
            $table->string('remarks')->nullable();
            $table->string('gallery')->nullable();
            $table->timestamps();

            $table->index('meal_vendor_detail_id');
            $table->index('meal_plan_id');
            $table->index('cuisine_id');
            $table->index('meal_preference_id');
            $table->index('meal_type_id');            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('meal_vendor_meal_plan_details');
    }
}
