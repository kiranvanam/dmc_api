<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeColumnDataTypesForMealVendorMealPlanDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('meal_vendor_meal_plan_details', function (Blueprint $table) {
            $table->text('description')->change();
            $table->text('menu_details')->change();
            $table->text('inclusions')->change();
            $table->text('exclusions')->change();
            $table->text('terms_and_conditions')->change();
            $table->text('cancellation_policies')->change();
            $table->text('remarks')->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('meal_vendor_meal_plan_details', function (Blueprint $table) {
            //
        });
    }
}
