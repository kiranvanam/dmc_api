<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenameColuumnGalleryToThumbnailInMealVendorDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('meal_vendor_details', function (Blueprint $table) {
            
            if(Schema::hasColumn('meal_vendor_details', 'gallery') ) {
                $table->renameColumn('gallery', 'thumbnail');
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('meal_vendor_details', function (Blueprint $table) {
            //
        });
    }
}
