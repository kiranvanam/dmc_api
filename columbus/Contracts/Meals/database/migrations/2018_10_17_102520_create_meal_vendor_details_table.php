<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMealVendorDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('meal_vendor_details', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('city_id')->index();
            $table->integer('vendor_id');
            $table->integer('category_id');
            $table->string('meal_plan_ids',100)->default("");            
            $table->text('description')->nullable();
            $table->text('gallery')->nullable();
            $table->timestamps();
            $table->index('vendor_id');            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('meal_vendor_details');
    }
}
