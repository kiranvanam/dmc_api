<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMealVendorDetailCacellationPoliciesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('meal_vendor_detail_cancellation_policies', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('meal_vendor_detail_id')->index('meal_vendor_detail_id_cance_policy_index');
            $table->date('booking_start_date')->nullable();
            $table->date('booking_end_date')->nullable();
            $table->date('travelling_start_date')->nullable();
            $table->date('travelling_end_date')->nullable();            
            $table->boolean('is_non_refundable')->default(false);
            $table->text('policy_rules')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('meal_vendor_detail_cancellation_policies');
    }
}
