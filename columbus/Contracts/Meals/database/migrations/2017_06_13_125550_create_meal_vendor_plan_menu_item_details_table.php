<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMealVendorPlanMenuItemDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('meal_vendor_plan_menu_item_details', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('meal_vendor_plan_id');
            $table->string('name',200);
            $table->string('code', 50);
            $table->string('quantity',200);
            $table->decimal('price', 7, 2);        
            $table->string('description')->nullable();
            $table->string('inclusions')->nullable();
            $table->string('exclusions')->nullable();
            $table->string('remarks')->nullable();
            $table->string('gallery')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('meal_vendor_plan_menu_item_details');
    }
}
