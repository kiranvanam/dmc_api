<?php
namespace Columbus\Contracts\Meals\Http\Controllers;

use App\Http\Controllers\ApiController;
use Columbus\Contracts\Meals\Http\Requests\MealPlanCreateFormRequest;
use Columbus\Contracts\Meals\Models\Cuisine;
use Columbus\Contracts\Meals\Models\MealCategory;
use Columbus\Contracts\Meals\Models\MealChildAge;
use Columbus\Contracts\Meals\Models\MealPlan;
use Columbus\Contracts\Meals\Models\MealPreference;
use Columbus\Contracts\Meals\Models\MealType;


class MealPlanController extends ApiController
{

    public function mealSettings()
    {
        return $this->ok([
                "meal_plans" => MealPlan::all(),
                "meal_child_ages"=> MealChildAge::all(),
                "meal_types" => MealType::all(),
                "cuisines" => Cuisine::all(),
                "meal_categories" => MealCategory::all(),
                "meal_preferences" => MealPreference::all()
            ],
            "Meal Settings");
    }

    public function index()
    {
        return $this->ok(MealPlan::all(), "Meal plans");
    }

    public function create(MealPlanCreateFormRequest $request)
    {
        return $this->ok($request->save(), "Meal Plan created");
    }

    public function update($meal_plan_id, MealPlanCreateFormRequest $request)
    {
        return $this->ok($request->update($meal_plan_id), "Meal Plan updated");
    }

    public function delete($meal_plan_id)
    {
        $plan = MealPlan::find($meal_plan_id);

        if($plan) {
            $plan->delete();
        }

        return $this->ok( $plan , "Meal plan deleted");
    }
}