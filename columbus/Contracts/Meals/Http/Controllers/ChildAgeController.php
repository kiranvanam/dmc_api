<?php
namespace Columbus\Contracts\Meals\Http\Controllers;

use App\Http\Controllers\ApiController;
use Columbus\Contracts\Meals\Http\Requests\MealChildAgeCreateFormRequest;
use Columbus\Contracts\Meals\Models\MealChildAge;


class ChildAgeController extends ApiController
{
    
    public function index()
    {
        return $this->ok(MealChildAge::all(), "Meal Child Ages");
    }

    public function create(MealChildAgeCreateFormRequest $request)
    {
        return $this->ok($request->save(), "Meal Child Age created"); 
    }

    public function update($id, MealChildAgeCreateFormRequest $request)
    {
        return $this->ok($request->update($id), "Meal Child Age updated"); 
    }    
    
    public function delete($id)
    {
        $child_age = MealChildAge::find($id);
        
        if($child_age) {
            $child_age->delete();
        }

        return $this->ok( $child_age , "Meal child age deleted");
    }
}