<?php
namespace Columbus\Contracts\Meals\Http\Controllers;

use App\Http\Controllers\ApiController;
use Columbus\Contracts\Meals\Http\Requests\MealTypeCreateFormRequest;
use Columbus\Contracts\Meals\Models\MealType;


class MealTypeController extends ApiController
{

    public function index()
    {
        return $this->ok(MealType::all(), "Meal types");
    }

    public function create(MealTypeCreateFormRequest $request)
    {
        return $this->ok($request->save(), "Meal type created"); 
    }

    public function update($meal_type_id, MealTypeCreateFormRequest $request)
    {
        return $this->ok($request->update($meal_type_id), "Meal type updated"); 
    }    
    
    public function delete($meal_type_id)
    {
        $type = MealType::find($meal_type_id);
        
        if($type) {
            $type->delete();
        }

        return $this->ok( $type , "Meal type deleted");
    }
}