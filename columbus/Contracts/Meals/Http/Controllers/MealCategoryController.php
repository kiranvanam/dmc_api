<?php
namespace Columbus\Contracts\Meals\Http\Controllers;

use App\Http\Controllers\ApiController;
use Columbus\Contracts\Meals\Http\Requests\MealCategoryCreateFormRequest;
use Columbus\Contracts\Meals\Models\MealCategory;


class MealCategoryController extends ApiController
{
    
    public function index()
    {
        return $this->ok(MealCategory::all(), "Meal categories");
    }

    public function create(MealCategoryCreateFormRequest $request)
    {
        return $this->ok($request->save(), "Category created"); 
    }

    public function update($category_id, MealCategoryCreateFormRequest $request)
    {
        return $this->ok($request->update($category_id), "Category updated"); 
    }    
    
    public function delete($category_id)
    {
        $category = MealCategory::find($category_id);
        
        if($category) {
            $category->delete();
        }

        return $this->ok( $category , "Category deleted");
    }
}