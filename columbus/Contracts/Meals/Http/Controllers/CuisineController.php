<?php
namespace Columbus\Contracts\Meals\Http\Controllers;

use App\Http\Controllers\ApiController;
use Columbus\Contracts\Meals\Http\Requests\CuisineCreateFormRequest;
use Columbus\Contracts\Meals\Models\Cuisine;


class CuisineController extends ApiController
{
    
    public function index()
    {
        return $this->ok(Cuisine::all(), "Cuisines");
    }

    public function create(CuisineCreateFormRequest $request)
    {
        return $this->ok($request->save(), "Cuisine created"); 
    }

    public function update($cuisine_id, CuisineCreateFormRequest $request)
    {
        return $this->ok($request->update($cuisine_id), "Cuisine updated"); 
    }    
    
    public function delete($cuisine_id)
    {
        $cuisine = Cuisine::find($cuisine_id);
        
        if($cuisine) {
            $cuisine->delete();
        }

        return $this->ok( $cuisine , "Cuisine deleted");
    }
}