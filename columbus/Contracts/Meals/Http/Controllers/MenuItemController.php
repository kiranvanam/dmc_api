<?php
namespace Columbus\Contracts\Meals\Http\Controllers;

use App\Http\Controllers\ApiController;
use Columbus\Contracts\Meals\Models\MealVendorPlanMenuItem;
use Columbus\Utilities\FileManager;
use Illuminate\Http\Request;


class MenuItemController extends ApiController
{

    public function index()
    {
        $meal_vendor_plan_id = request()->get('meal_vendor_plan_id');
        return $this->ok(MealVendorPlanMenuItem::where('meal_vendor_plan_id', $meal_vendor_plan_id)->get(), "Meal types");
    }

    public function create(Request $request)
    {
        return $this->ok( MealVendorPlanMenuItem::create($request->all()), "Menu item created");
    }
    
    public function update($menu_item_id, Request $request)
    {
        $item  = MealVendorPlanMenuItem::find($menu_item_id);

        if($item) {
            $item->update($request->all());
        }
        return $this->ok($item, "Menu item updated");
    }

    public function uploadGallery($menu_item_id, Request $request, FileManager $fileManager)
    {
        $menuItem = MealVendorPlanMenuItem::find($menu_item_id);
        
        $gallery = [];
        if($menuItem) {
            if($menuItem && $menuItem->gallery)  {
                $gallery = $menuItem->gallery;
            }
            
            if ($request->hasFile('images')) {
                $country_id = $request->get('country_id');
                $vendor_id = $request->get('vendor_id');
                
                $path = "uploads/images/countries/$country_id/meals-vendors/$vendor_id/menu-items/$menuItem->id";

                $gallery = array_merge($gallery, $fileManager->savePhotos($path , $request->file('images')));

            }
            
            $menuItem->update(['gallery'=> $gallery]);
            
        }
        return $this->ok($menuItem, "Meal gallery updated");
    }
}