<?php
namespace Columbus\Contracts\Meals\Http\Controllers;

use App\Http\Controllers\ApiController;
use Columbus\Contracts\Meals\Models\MealVendor;
use Illuminate\Http\Request;

class MealVendorDetailController extends ApiController
{
    
    public function index($vendor_id)
    {
        $mealVendor = MealVendor::where('vendor_id',$vendor_id)->with('mealPlans')->first();
        return $this->ok( $mealVendor, "Meal Vendor  details");
    }

    public function byCity()
    {
        return $this->ok( MealVendor::where('city_id', request('city_id'))->get(), "Meal Vendors For the given City");
    }

    public function create(Request $request)
    {
        $data = $request->all();
        return $this->ok(MealVendor::create($data), "Meal Vendor Details are created");
    }

    public function update(Request $request)
    {
        $data = $request->all();
        $vendor_id = $request->vendor_id;
        $mealVendor = MealVendor::find($vendor_id);

        if($mealVendor) {
            $mealVendor->update($data);
        }


        return $this->ok($mealVendor, "Meal Vendor Details are updated");
    }    

    public function updateThumbnail($vendor_id, $meal_vendor_id)
    {
        $data = request()->only(['thumbnail']);
        $mealVendor = MealVendor::find($meal_vendor_id);

        if($mealVendor) {
            $mealVendor->update($data);
        }


        return $this->ok($mealVendor, "Meal Vendor Details are updated");
    }
}