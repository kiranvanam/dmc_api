<?php

namespace Columbus\Contracts\Meals\Http\Controllers;

use Illuminate\Http\Request;
use Columbus\Utilities\FileManager;
use App\Http\Controllers\ApiController;
use Columbus\SystemSettings\Models\Code;
use Columbus\Contracts\Meals\Models\MealVendorPlan;


class MealVendorPlanDetailController extends ApiController
{

    public function create(Request $request)
    {
        $data = $request->all();
        $data['code'] = Code::getNextCodeForMeal();
        $mealVendorPlan = MealVendorPlan::create($data);

        return $this->ok($mealVendorPlan, "Meal Details created");
    }

    public function update( Request $request)
    {
        $meal_vendor_plan_id = $request->id;
        $data = $request->all();
        $mealVendorPlan = MealVendorPlan::find($meal_vendor_plan_id);
        if($mealVendorPlan) {
            if(empty($mealVendorPlan->code)) {
                $data['code'] = Code::getNextCodeForMeal();
            }
            $mealVendorPlan->update($data);
        }
        return $this->ok($mealVendorPlan, "Meal details updated");
    }

    public function updateFilters()
    {
        $meal_vendor_plan_id = request('id');
        $filters = request('filters');
        $mealVendorPlan = MealVendorPlan::findOrFail($meal_vendor_plan_id);

        $mealVendorPlan->update(['filters' => $filters]);

        return $this->ok($mealVendorPlan, "Meal Plan Filters Updated");
    }

}
