<?php

namespace Columbus\Contracts\Meals\Http\Controllers;

use App\Http\Controllers\ApiController;
use Columbus\Contracts\Meals\Models\MealVendorDetailCancellationPolicy as CancellationPolicy;

class MealVendorCancellationPolicyController extends ApiController
{
    function index($vendor_id, $meal_vendor_detail_id)
    {
        
        $policies = CancellationPolicy::where('meal_vendor_detail_id', $meal_vendor_detail_id)->get();        
        return $this->ok($policies);
    }

    function create($vendor_id, $meal_vendor_detail_id)
    {
        $data = request()->all();
        $policy = CancellationPolicy::create($data);
        return $this->ok($policy, "Cancellation Policy created");
    }

    function update($vendor_id, $meal_vendor_detail_id, $cancellation_policy_id)
    {
        $data = request()->all();
        $policy = CancellationPolicy::findOrFail($cancellation_policy_id);
        $policy->update($data);
        return $this->ok($policy, "Policy Details are updated");
    }

    function delete($vendor_id, $meal_vendor_detail_id, $cancellation_policy_id)
    {
        $policy = CancellationPolicy::findOrFail($cancellation_policy_id);
        $policy->delete();
        return $this->ok($policy, "Cancellation Policy has been Deleted");
    }
}