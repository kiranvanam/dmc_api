<?php
namespace Columbus\Contracts\Meals\Http\Controllers;

use App\Http\Controllers\ApiController;
use Columbus\Contracts\Meals\Http\Requests\MealPreferenceCreateFormRequest;
use Columbus\Contracts\Meals\Models\MealPreference;


class MealPreferenceController extends ApiController
{
    
    public function index()
    {
        return $this->ok(MealPreference::all(), "Meal preferences");
    }

    public function create(MealPreferenceCreateFormRequest $request)
    {
        return $this->ok($request->save(), "Meal Preference created"); 
    }

    public function update($meal_preference_id, MealPreferenceCreateFormRequest $request)
    {
        return $this->ok($request->update($meal_preference_id), "Meal Preference updated"); 
    }    
    
    public function delete($meal_preference_id)
    {
        $preference = MealPreference::find($meal_preference_id);
        
        if($preference) {
            $preference->delete();
        }

        return $this->ok( $preference , "MealPreference deleted");
    }
}