<?php

namespace Columbus\Contracts\Meals\Http\Requests;

use Columbus\Contracts\Meals\Models\Cuisine;
use Illuminate\Foundation\Http\FormRequest;

class CuisineCreateFormRequest extends FormRequest
{
    
    public function authorize()
    {
        return true;
    }

    
    public function rules()
    {
        return [
            'name' => 'required'
        ];
    }

    protected function prepareForValidation()
    {
    }

    
    public function messages()
    {
        return [
            'name.required' => "Provide the name field",
        ];
    }

    public function save()
    {
        return Cuisine::create($this->all());
    }

    public function update($cuisine_id)
    {
        $cuisine = Cuisine::find($cuisine_id);
        if($cuisine) {
            $cuisine->update($this->all());
        }
        return $cuisine;
    }


}
