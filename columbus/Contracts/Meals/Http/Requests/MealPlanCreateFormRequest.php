<?php

namespace Columbus\Contracts\Meals\Http\Requests;

use Columbus\Contracts\Meals\Models\MealPlan;
use Illuminate\Foundation\Http\FormRequest;

class MealPlanCreateFormRequest extends FormRequest
{
    
    public function authorize()
    {
        return true;
    }

    
    public function rules()
    {
        return [
            'country_id' => 'required',
            'name' => 'required'
        ];
    }

    protected function prepareForValidation()
    {
    }

    
    public function messages()
    {
        return [
            'country_id.required' => "Country is not selected",
            'name.required' => "Provide the name field",
        ];
    }

    public function save()
    {
        return MealPlan::create($this->all());
    }

    public function update($meal_plan_id)
    {
        $mealPlan = MealPlan::find($meal_plan_id);
        if($mealPlan) {
            $mealPlan->update($this->all());
        }
        return $mealPlan;
    }


}
