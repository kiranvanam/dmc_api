<?php

namespace Columbus\Contracts\Meals\Http\Requests;

use Columbus\Contracts\Meals\Models\MealChildAge;
use Illuminate\Foundation\Http\FormRequest;

class MealChildAgeCreateFormRequest extends FormRequest
{
    
    public function authorize()
    {
        return true;
    }

    
    public function rules()
    {
        return [
            'country_id' => 'required',
            'min_age' => 'required',
            'max_age' => "required"

        ];
    }

    protected function prepareForValidation()
    {
    }

    
    public function messages()
    {
        return [
            'country_id.required' => "Country is not selected",
            'min_age.required' => "Select Min Age field",
            'max_age.required' => "Select Max age field"
        ];
    }

    public function save()
    {
        return MealChildAge::create($this->all());
    }

    public function update($meal_plan_id)
    {
        $childAge = MealChildAge::find($meal_plan_id);
        if($childAge) {
            $childAge->update($this->all());
        }
        return $childAge;
    }


}
