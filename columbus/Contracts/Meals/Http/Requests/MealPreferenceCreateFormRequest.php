<?php

namespace Columbus\Contracts\Meals\Http\Requests;

use Columbus\Contracts\Meals\Models\MealPreference;
use Illuminate\Foundation\Http\FormRequest;

class MealPreferenceCreateFormRequest extends FormRequest
{
    
    public function authorize()
    {
        return true;
    }

    
    public function rules()
    {
        return [
            'name' => 'required'
        ];
    }

    protected function prepareForValidation()
    {
    }

    
    public function messages()
    {
        return [
            'name.required' => "Provide the name field",
        ];
    }

    public function save()
    {
        return MealPreference::create($this->all());
    }

    public function update($meal_preference_id)
    {
        $meal_preference = MealPreference::find($meal_preference_id);
        if($meal_preference) {
            $meal_preference->update($this->all());
        }
        return $meal_preference;
    }


}
