<?php

namespace Columbus\Contracts\Meals\Http\Requests;

use Columbus\Contracts\Meals\Models\MealType;
use Illuminate\Foundation\Http\FormRequest;

class MealTypeCreateFormRequest extends FormRequest
{
    
    public function authorize()
    {
        return true;
    }

    
    public function rules()
    {
        return [
            'country_id' => 'required',
            'name' => 'required',

        ];
    }

    protected function prepareForValidation()
    {
    }

    
    public function messages()
    {
        return [
            'country_id.required' => "Country is not selected",
            'name.required' => "Name must be provided",
            ];
    }

    public function save()
    {
        return MealType::create($this->all());
    }

    public function update($meal_type_id)
    {
        $type = MealType::find($meal_type_id);
        if($type) {
            $type->update($this->all());
        }
        return $type;
    }


}
