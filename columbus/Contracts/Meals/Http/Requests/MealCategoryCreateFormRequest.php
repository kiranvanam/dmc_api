<?php

namespace Columbus\Contracts\Meals\Http\Requests;

use Columbus\Contracts\Meals\Models\MealCategory;
use Illuminate\Foundation\Http\FormRequest;

class MealCategoryCreateFormRequest extends FormRequest
{
    
    public function authorize()
    {
        return true;
    }

    
    public function rules()
    {
        return [
            'country_id' => "required",
            'name' => 'required'
        ];
    }

    protected function prepareForValidation()
    {
    }

    
    public function messages()
    {
        return [
            "country_id.required" => "Please Select country ",
            'name.required' => "Provide the name field",
        ];
    }

    public function save()
    {
        return MealCategory::create($this->all());
    }

    public function update($category_id)
    {
        $category = MealCategory::find($category_id);
        if($category) {
            $category->update($this->all());
        }
        return $category;
    }


}
