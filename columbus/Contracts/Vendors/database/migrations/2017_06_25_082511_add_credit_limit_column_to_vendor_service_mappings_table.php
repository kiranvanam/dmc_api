<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCreditLimitColumnToVendorServiceMappingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return 
     */
    public function up()
    {
        Schema::table('vendor_service_mappings', function (Blueprint $table) {
            $table->decimal('credit_limit')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('vendor_service_mappings', function (Blueprint $table) {
            $table->dropColumn('credit_limit');
        });
    }
}
