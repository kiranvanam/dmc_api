<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsTimeStampsAndCountryIdToVendorServicesTable extends Migration
{
    
    public function up()
    {
        Schema::table('vendor_services', function (Blueprint $table) {
            $table->integer('country_id')->after('id');
            $table->timestamps();
            $table->softDeletes();	
        });
    }
    
    public function down()
    {
        Schema::table('vendor_services', function (Blueprint $table) {
            $table->dropColumn('country_id');
            $table->dropColumn('created_at');
            $table->dropColumn('updated_at');
            $table->dropColumn('deleted_at');
        });
    }
}
