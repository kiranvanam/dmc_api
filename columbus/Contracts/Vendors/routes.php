<?php

Route::group(['prefix' => 'api', 'middleware' => ['auth:api','cors']], function() {

    Route::group(['prefix' => 'vendors'], function() {
        Route::get('/', 'VendorController@index');
        Route::post('/', 'VendorController@save');
        Route::put('{id}', 'VendorController@update');
        Route::put('{id}/details', 'VendorController@updateDetails');
        
        Route::put('{id}/bank-details', 'VendorBankAccountDetailsController@update');    
        Route::post('{id}/license-details', 'VendorLicenseDetailsController@update');

        Route::delete('{id}', 'VendorController@delete');

        Route::get('{vendor_id}/contracts-api-data-mappings', 'VendorContractsApiDataMappingController@index');
        Route::post('{vendor_id}/contracts-api-data-mappings', 'VendorContractsApiDataMappingController@save');
        Route::post('{vendor_id}/contracts-api-data-mappings/update-mappings', 
            'VendorContractsApiDataMappingController@updateMappings');

        Route::put('{id}/transfer-types', 'VendorTransferTypesMappingController@update');

        Route::put('{id}/services', 'VendorController@updateServices');

        Route::post("{id}/vendor-services-credit-limits",
                    'VendorServiceMappingController@updateCreditLimit');
    });

    Route::group(['prefix'=> 'services'], function (){
        
        Route::get('', 'VendorServiceController@index');
        Route::post('', 'VendorServiceController@create');
        Route::put('/{id}', 'VendorServiceController@update');
        Route::delete('/{id}', 'VendorServiceController@delete');

        Route::get('all', 'ServiceController@index');
        Route::post("/toggle", 'ServiceController@toggle');
        Route::post("{id}/sub-services", 'ServiceController@createSubservice');
        Route::put("{service_id}/sub-services/{sub_service_id}", 'ServiceController@updateSubservice');
        Route::put("{service_id}/sub-services/{sub_service_id}/toggle", 'ServiceController@toggleSubservice');
        Route::delete("{service_id}/sub-services/{sub_service_id}", 'ServiceController@deleteSubservice');
    });
    
}); 
