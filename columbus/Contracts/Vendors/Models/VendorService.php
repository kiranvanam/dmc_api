<?php

namespace Columbus\Contracts\Vendors\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class VendorService extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];

    protected $fillable = ['country_id', 'name', 'slug', 'parent_id', 'group_by_label', 'values'];

    protected $casts = [
        'values' => 'array'
    ];

    public function subServices()
    {
        return $this->hasMany(VendorService::class, 'parent_id')->withTrashed();
    }

    public function subServicesAll()
    {
        return $this->hasMany(VendorService::class, 'parent_id')->withTrashed();
    }

    //
    public static function fetchAll($country_id = null)
    {
        $services = self::withTrashed()->whereNull('parent_id');
        
        if($country_id) {
            logger("VendorService::fetchAll", [$country_id]);
            $services->where('country_id', $country_id);
        }

        $services = $services->with('subServices')->get();

        $services = $services->map( function($service) {
                            //dd($service->subServices);
                            if($service->subServices()->count()) {
                                $service->sub_services = $service->subServices->groupBy('group_by_label');
                                
                            }

                            return $service; 
                    });

        return $services;
    }

    public static function transferServiceId()
    {
        return self::where('name', 'Transfers')->first()->id;
    }

    public static function createSubTransferService($data)
    {
        $data['slug'] = str_slug($data['name']);
        $data['group_by_label'] = "Type";
        $data['parent_id'] = static::transferServiceId();
        return self::create($data);
    }

    public static function updateSubTransferService($data)
    {
        $data['slug'] = str_slug($data['name']);
        //$data['group_by_label'] = "Type";
        
        $service = static::withTrashed()
                        ->where( 'id', $data['vendor_service_id'])
                        ->first();
        if($service) {
            $service->update($data);
        }
        return $service;
    }

    public static function deleteSubTransferService($vendor_service_id)
    {
        $service = self::find($vendor_service_id);
        
        if($service) {
            $service->delete();
        }
        return $service;   
    }

    public function deleteSubServices()
    {
        self::where('parent_id', $this->id)->delete();
    }
    public function restoreSubServices()
    {
        self::where('parent_id', $this->id)
            ->each( function($service) {
                         $service->restore();
                    }
            );
    }

    public function isTransfer()
    {
        return $this->slug == "transfers";
    }
}
