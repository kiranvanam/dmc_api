<?php

namespace Columbus\Contracts\Vendors\Models;

use Illuminate\Database\Eloquent\Model;

class VendorServiceMapping extends Model
{
    public $table = "vendor_service_mappings";
    public $timestamps = false;
    protected  $fillable = ['vendor_id', 'vendor_service_id', 'values', 'credit_limit'];

    protected $casts = [
        'values' => 'array'
    ];

}