<?php

namespace Columbus\Contracts\Vendors\Models;


use Illuminate\Database\Eloquent\Model;

class VendorContractsApiDataMapping extends Model
{
    public $timestamps = false;

    protected $fillable = ['vendor_id', 'country_id', 'mappings'];

    protected $casts = ['mappings'=> 'array'];
}
