<?php

namespace Columbus\Contracts\Vendors\Models;

use Columbus\Contracts\Vendors\Models\VendorServiceMapping;
use Illuminate\Database\Eloquent\Model;

class Vendor extends Model
{
    public $timestamps = false;
    protected  $fillable = ['name', 'code', 'slug', 'country_id', 'region_id', 'city_id', 'description', 'address', 'website', 
                            'transfer_type_ids', 'contact_details', 
                            'bank_account_details', 'license_details', 'module', 'api_credentials', 
                            'api_vendor_countries', 'credit', 'is_active'
                        ];

     protected $casts = [
        'contact_details' => 'array',
        'api_credentials'=> 'array',
        'bank_account_details' => 'array',
        'license_details' => 'array',
        'transfer_type_ids' => 'array'
    ];


    public function vendorServices()
    {
        return $this->hasMany(VendorServiceMapping::class);
    }
}
