<?php

namespace Columbus\Contracts\Vendors\Http\Controllers;

use App\Http\Controllers\ApiController;
use Columbus\Contracts\Vendors\Models\VendorContractsApiDataMapping;
use Illuminate\Http\Request;

class VendorContractsApiDataMappingController extends ApiController
{


    public function index($vendorId)
    {
        return $this->ok(VendorContractsApiDataMapping::all());
    }

    public function save($vendor_id, Request $request)
    {
        $apiMappings = $request->get('mappings'); 
        $vendorApiMappings = [];
        
        array_walk($apiMappings, function($mappings) use($vendor_id, &$vendorApiMappings) {
            
            $vendorApiMapping = VendorContractsApiDataMapping::firstOrCreate([ 
                                                            'vendor_id' => $vendor_id,
                                                            'country_id' => $mappings['country_id']
                                                        ]);
            $vendorApiMapping->update(['mappings' => $mappings['mappings']]);
            $vendorApiMappings[] = $vendorApiMapping;
        });

        return $this->ok(VendorContractsApiDataMapping::all());
    }

    public function updateMappings($vendorId, Request $request)
    {   
        $vendorCountryIds = $request->only(['vendor_id', 'country_id']);

        $vendorApiMapping = VendorContractsApiDataMapping::firstOrCreate($vendorCountryIds);
        $vendorApiMapping->update(['mappings' => $request->get('mappings')]);
        
        return $this->ok($vendorApiMapping);
    }
}