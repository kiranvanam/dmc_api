<?php
namespace Columbus\Contracts\Vendors\Http\Controllers;

use App\Http\Controllers\ApiController;
use Columbus\Contracts\Vendors\Models\Vendor;
use Columbus\Utilities\FileManager;
use Illuminate\Http\Request;

class VendorLicenseDetailsController extends ApiController
{

    private $fileManager = null;

    public function __construct(FileManager $fileManager)
    {
        $this->fileManager = $fileManager;
    }

    public function update($vendor_id, Request $request)
    {
        $vendor = Vendor::find($vendor_id);
        
        $licenseDetails =  $request->only(['number',"expiry_date"]);

        if ($request->hasFile('images')) {
            $path = "uploads/countries/$vendor->country_id/cities/$vendor->city_id/vendors/$vendor->id/licenses";

            $licenseDetails['images'] = $this->fileManager->savePhotos($path , $request->file('images'));

        }
        
        $vendor->update( ['license_details'=> $licenseDetails]);

        return $this->ok($vendor, "Bank account details updated");
    }
}