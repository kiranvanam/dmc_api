<?php

namespace Columbus\Contracts\Vendors\Http\Controllers;

use App\Http\Controllers\ApiController;
use Columbus\Contracts\Vendors\Models\Vendor;
use Illuminate\Http\Request;

class VendorTransferTypesMappingController extends ApiController
{
    public function update($vendor_id, Request $request)
    {

        $vendor = Vendor::find($vendor_id);

        if($vendor) {
            $vendor->update($request->only(['transfer_type_ids']));
        }

        return $this->ok($vendor, "Vendor Transfer Types mappings are updated");
    }
}