<?php

namespace Columbus\Contracts\Vendors\Http\Controllers;

use App\Http\Controllers\ApiController;
use Columbus\Contracts\Vendors\Http\Requests\VendorContactDetailsForm;
use Columbus\Contracts\Vendors\Http\Requests\VendorCreationForm;
use Columbus\Contracts\Vendors\Models\Vendor;
use Columbus\Contracts\Vendors\Models\VendorServiceMapping;
use Illuminate\Http\Request;

class VendorController extends ApiController
{
    public function index()
    {
        return $this->ok(Vendor::with('vendorServices')->get(), "Vendors List");
    }

    public function save(VendorCreationForm $form)
    {
        return $this->ok($form->save(), "Vendor created");
    }

    public function update(VendorCreationForm $form)
    {
        return  $this->ok($form->update(), "Vendor updated");
    }

    public function delete($id)
    {
        return  $this->ok(Vendor::find($id)->delete(), "Vendor Deleted");
    }

    public function updateDetails(VendorContactDetailsForm $form)
    {
        return  $this->ok($form->update(), "Vendor Contact details Updated");
    }

    function updateServices($vendor_id, Request $request) {
        $vendor = Vendor::find($vendor_id);
        
        if($vendor) {
            $vendor_services = $request->get('vendor_services');

            foreach ($vendor_services as $vendor_service) {
                if($vendor_service['vendor_service_id'] ) {                        
                    $vendor_service['vendor_id'] = $vendor_id;
                    $mapping =  VendorServiceMapping::where('vendor_id', $vendor_id)
                                ->where('vendor_service_id', $vendor_service['vendor_service_id'])->first();

                    if($mapping) {
                        $mapping->update($vendor_service);
                    }else{
                        VendorServiceMapping::create($vendor_service);
                    }
                }
            }            
            //$vendor->update();
            //$vendor->vendorServices = $vendor->vendorServices;
        }

        return $this->ok($vendor->load('vendorServices'), "Vendor service mappings updated");
    }

}
