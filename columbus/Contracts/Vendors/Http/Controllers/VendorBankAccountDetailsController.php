<?php
namespace Columbus\Contracts\Vendors\Http\Controllers;

use App\Http\Controllers\ApiController;
use Columbus\Contracts\Vendors\Models\Vendor;
use Illuminate\Http\Request;

class VendorBankAccountDetailsController extends ApiController
{
    public function update($vendor_id, Request $request)
    {
        $vendor = Vendor::find($vendor_id);

        if( !$vendor) {
            return $this->notFound("Vendor Bank account not found", ['description'=> "The requested vendor details can't be updated as vendor not found in the system"]);
        }

        $data= $request->only(['bank_account_details']);  
        $vendor->update($data);

        return $this->ok($vendor, "Bank account details updated");
    }
}