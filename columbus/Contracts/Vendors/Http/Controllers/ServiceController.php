<?php

namespace Columbus\Contracts\Vendors\Http\Controllers;

use App\Http\Controllers\ApiController;
use Columbus\Contracts\Vendors\Models\VendorService;

class ServiceController extends ApiController
{

    protected function fetchServices()
    {
        $country_id = request('country_id');

        $services = $services = VendorService::withTrashed()
                                            ->whereNull('parent_id');

        if( !empty($country_id) ) {
            $services->where('country_id', $country_id);
        }


        return $services->with('subServicesAll')->get();
    }

    public function index() 
    {        
        return $this->ok( $this->fetchServices() , "Services listing");
    }

    public function toggle()
    {
        $service_names = collect(request('service_names'));
        $country_id = request('country_id');


        $service_names->each( function($service_name) use($country_id) {

            $service = VendorService::withTrashed()
                                ->where('name', $service_name)
                                ->where('country_id', $country_id)
                                ->whereNull('parent_id')
                                ->first();
    
            if( empty($service) ) {
                $data['name'] = $service_name;
                $data['country_id'] = $country_id;
                $data['slug'] = str_slug($service_name);
    
                VendorService::create($data);
            }

            if( !empty($service) && $service->trashed()) {
                $service->restore();
                $service->restoreSubServices();
            }
        });

        //delete all services where those are unchecked
        $services = VendorService::whereNotIn('name', request('service_names'))
                                ->where('country_id', $country_id)
                                ->whereNull('parent_id')
                                ->get();

        $services->each( function($service) { 
            $service->delete();
            $service->deleteSubServices();
        });

        return $this->ok( $this->fetchServices(), "Available Services Updated" );
    }

    public function createSubService($service_id)
    {    
        $data = request()->only(['parent_id', 'name', 'values', 'country_id']);
        $data['slug'] = str_slug($data['name']); 

        $subService = VendorService::withTrashed()
                                ->where('name', $data['name'])
                                ->where('country_id', $data['country_id'] )
                                ->where('parent_id', $service_id)
                                ->first();
        if( empty($subService))
        {
            VendorService::create($data);
            
            $service = VendorService::find($service_id);
        
            if( !empty($service) &&  $service->isTransfer()) {
                \Columbus\Contracts\Transfers\Models\TransferService
                                ::createWith(['name' =>  $data['name'], 
                                            'country_id' => $data['country_id'],
                                            'vendor_service_id' => $subService->id
                                            ]);    
            }

            return $this->ok($this->fetchServices(), "Sub Service created");
        }

        return $this->ok(null, "Subservice already created");
    }

    public function toggleSubService($service_id, $sub_service_id)
    {
        $subService = VendorService::withTrashed()->where('id', $sub_service_id)->first();

        
        
        if( !empty($subService) )  {
            if ($subService->trashed()) {        
                $subService->restore();                
            } else {
                $subService->delete();
            }

            //if service is transfer, then update transfer_services as well
            $service = VendorService::find($service_id);
        
            if( !empty($service) &&  $service->isTransfer()) {
                \Columbus\Contracts\Transfers\Models\TransferService::toggleByVendorServiceId($subService->id);    
            }

            return $this->ok( $this->fetchServices(), "Service toggled");
        }

        return $this->ok(null, "Service not found");
    }

    public function updateSubService($service_id, $sub_service_id)
    {
        $data = request()->only(['parent_id', 'name', 'values', 'country_id']);
        $data['slug'] = str_slug($data['name']);

        $subService = VendorService::withTrashed()
                                    ->find($sub_service_id);

        if( !empty($subService)) {
            $subService->update($data);
            
            //if service is transfer, then update transfer_services as well
            $service = VendorService::find($service_id);
        
            if( !empty($service) &&  $service->isTransfer()) {
                $serviceData = request()->only(['name',  'country_id']);
                \Columbus\Contracts\Transfers\Models\TransferService::updateByVendorServiceId($subService->id, $serviceData);
            }
            
            return $this->ok( $this->fetchServices(), "Subservice details updated");
        }

        return $this->ok(null, "Subservice not found to update");
    }

    public function deleteSubService($service_id, $sub_service_id) {
        
        $service = VendorService::withTrashed()->find($sub_service_id);

        if( !empty($service)) {
            $service->forceDelete();
            return $this->ok($this->fetchServices(), "Service is deleted");
        }

        return $this->ok(null,  "Service not found to delete");
    }
 
}