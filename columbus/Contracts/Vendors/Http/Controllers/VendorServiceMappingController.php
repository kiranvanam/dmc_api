<?php

namespace Columbus\Contracts\Vendors\Http\Controllers;

use App\Http\Controllers\ApiController;
use Columbus\Contracts\Vendors\Models\VendorServiceMapping;
use Illuminate\Http\Request;

class VendorServiceMappingController extends ApiController
{

    public function updateCreditLimit($vendor_id, Request $request)
    {   
        $vendorServiceMapping = VendorServiceMapping::where('vendor_id',$vendor_id)
                                    ->where('vendor_service_id', 
                                            $request->get('vendor_service_id'))
                                    ->first();

        if($vendorServiceMapping) {
            $vendorServiceMapping->credit_limit = $request->get('credit_limit');
            $vendorServiceMapping->save(); 
        }

        return $this->ok($vendorServiceMapping, "Vendor price updated");
    }

    

}
