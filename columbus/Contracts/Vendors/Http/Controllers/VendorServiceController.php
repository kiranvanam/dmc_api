<?php

namespace Columbus\Contracts\Vendors\Http\Controllers;

use App\Http\Controllers\ApiController;
use Columbus\Contracts\Transfers\Models\TransferType;
use Columbus\Contracts\Vendors\Models\VendorService;
use Illuminate\Http\Request;


class VendorServiceController extends ApiController
{
    
    function index()
    {
        return $this->ok(VendorService::fetchAll(request('country_id')) , "Services list");  
    }


    public function create(Request $request)
    {
        $data = $this->prepareData($request);
        $vendorService = $this->createService($data);


        return $this->ok($vendorService, "Vendor Service added");
    }

    public function update($vendorService_id, Request $request)
    {
        $data = $this->prepareData($request);
        
        $vendorService = VendorService::withTrashed()->where('id',$vendorService_id)->first();

        if($vendorService) {
            
            $vendorService->update($data);

            $this->updateTransferType($vendorService);
        }

        return $this->ok($vendorService, "Vendor Service updated");
    }

    public function delete($vendorService_id)
    {
        $vendorService = VendorService::find($vendorService_id);

        if($vendorService) {
            $vendorService->delete();
            $this->deleteTransferType($vendorService);
        }
        return $this->ok($vendorService, "Vendor Service deleted");
    }

    protected function prepareData($request)
    {
        $data = $request->all();
        $data['slug'] = str_slug($data['name']);        
        $data['values'] =  $data['values'] ? $data['values'] : [];
        return $data;
    }

    protected function createService($data)
    {
        
        $parentServiceForLasterInserted = "";
        
        if( isset($data['parent_id']) && $data['parent_id'] ) {
            $service = VendorService::find($data['parent_id']);
            $parentServiceForLasterInserted = $service->name;
        }
        $subService = VendorService::create($data);
        
        // make an even here so in Transfers module it gets created
        if($parentServiceForLasterInserted == "Transfers") {
            $fields = [ 'vendor_service_id' => $subService->id,
                        'name' => $subService->name,
                        'description' => ""
                    ]; 
            TransferType::create($fields);                           
        }
        return $subService;
    }

    protected function updateTransferType($subService)
    {
        
        $parentServiceForLasterInserted = "";
        
        if( isset($subService['parent_id']) && $subService['parent_id'] ) {
            $service = VendorService::find($subService['parent_id']);
            $parentServiceForLasterInserted = $service->name;
        }

        
        // Update in main transfer types also
        if($parentServiceForLasterInserted == "Transfers") {
            $fields = [ 'vendor_service_id' => $subService->id,
                        'name' => $subService->name,
                        'description' => ""
                    ]; 
            $transferType = TransferType::where('vendor_service_id', $subService->id)->first();

            if($transferType) {
                $transferType->update($fields);
            }                           
        }
        return $subService;
    }

    protected function deleteTransferType($subService)
    {
        
        $parentServiceForLasterInserted = "";
        
        if( isset($subService['parent_id']) && $subService['parent_id'] ) {
            $service = VendorService::find($subService['parent_id']);
            $parentServiceForLasterInserted = $service->name;
        }

        
        // Update in main transfer types also
        if($parentServiceForLasterInserted == "Transfers") {
            
            $transferType = TransferType::where('vendor_service_id', $subService->id)->first();

            if($transferType) {
                $transferType->delete();
            }                           
        }
    }
}