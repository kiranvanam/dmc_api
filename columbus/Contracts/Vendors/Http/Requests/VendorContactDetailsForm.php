<?php

namespace Columbus\Contracts\Vendors\Http\Requests;

use Columbus\Contracts\Vendors\Models\Vendor;
use Illuminate\Foundation\Http\FormRequest;


class VendorContactDetailsForm extends FormRequest
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [];/* [
            'id' => 'required|numeric',
            'contact_details' => "array",
            'api_credentials' => "array"
        ];*/
    }

    public function update()
    {
        $vendor = Vendor::find($this->get('id'));
        $vendor->update($this->except(['id']));
        return $vendor;
    }

    public function updateApiDetails()
    {
        $vendor = Vendor::find($this->get('id'));
        $vendor->update($this->only(['api_credentials']));
        return $vendor;
    }
}
