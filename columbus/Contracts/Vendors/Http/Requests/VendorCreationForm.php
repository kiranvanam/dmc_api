<?php

namespace Columbus\Contracts\Vendors\Http\Requests;

use Columbus\SystemSettings\Models\Code;
use Illuminate\Foundation\Http\FormRequest;
use Columbus\Contracts\Vendors\Models\Vendor;

class VendorCreationForm extends FormRequest
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'country_id' => 'required|numeric',
            'name' => 'required',
            'description' => 'required',
            'address' => 'required',
        ];
    }

    public function save()
    {
        $data = $this->all();
        $data['slug'] = $data['name'];
        $data['code']= Code::getNextCodeForModule("vendors");
        return Vendor::create($data);
    }

    public function update()
    {
        $vendor = Vendor::find($this->get('id'));
        $vendor->update($this->except(['_method', 'id']));
        return $vendor;
    }
}
