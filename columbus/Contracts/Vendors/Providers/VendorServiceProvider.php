<?php

namespace Columbus\Contracts\Vendors\Providers;

use Illuminate\Support\Facades\Route;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class VendorServiceProvider extends ServiceProvider
{
    protected $namespace = "Columbus\Contracts\Vendors\Http\Controllers";

    public function boot()
    {
        $this->app->router->group(['namespace' => $this->namespace], function() {
            if (! $this->app->routesAreCached()) {
                require __DIR__.'/../routes.php';
            }
        });
        
        //$this->loadViewsFrom(__DIR__.'/../resources/views', 'acl');

        $this->loadMigrationsFrom(__DIR__.'/../database/migrations');

        // Publish seeds
        //$this->publishes([__DIR__ . '/database/seeds/' => base_path('database/seeds')], 'seeds');
    }
}
