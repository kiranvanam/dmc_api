<?php

use Columbus\Contracts\Transfers\Models\TransferService;

Route::group(['prefix'=>'/contracts/api/transfers', 'middleware'=>['auth:api','cors']], function() {
    Route::group(['prefix' => 'point'], function () {
        Route::get('/', 'TransferPointController@all');
        Route::post('/', 'TransferPointController@save');
        Route::put('/{id}', 'TransferPointController@update');
        Route::delete('/{id}', 'TransferPointController@delete');

        Route::group(['prefix' => 'types'], function () {
            Route::get('/', 'TransferPointTypeController@all');
            Route::post('/', 'TransferPointTypeController@save');
            Route::put('/{id}', 'TransferPointTypeController@update');
            Route::delete('/{id}', 'TransferPointTypeController@delete');
        });
    });

    Route::group(['prefix' => 'vehicle-disposal'], function () {
        Route::get('/', 'VehicleDisposalController@all');
        Route::post('/', 'VehicleDisposalController@save');
        Route::put('/{id}', 'VehicleDisposalController@update');
        Route::delete('/{id}', 'VehicleDisposalController@delete');

        Route::group(['prefix' => 'pricing'], function () {
            Route::post('/', 'VehicleDisposalPricingController@save');
            Route::put('/{id}', 'VehicleDisposalPricingController@update');
            Route::delete('/{id}', 'VehicleDisposalPricingController@delete');
        });
        Route::get('/{vd_id}/vendor/{vendor_id}/vehicle/{vehicle_id}/month/{month}/year/{year}', 'VehicleDisposalPricingController@all');
        //Route::get('/{vd_id}/vendor/{vendor_id}/vehicle/{vehicle_id}/month/{month}/year/{year}', 'VehicleDisposalPricingController@all');
    });

    Route::group(['prefix' => 'area-transfer'], function () {
        Route::get('/', 'AreaTransferController@all');
        Route::post('/', 'AreaTransferController@save');
        Route::group(['prefix' => 'sic'], function () {
            Route::get('/area-transfer/{area_transfer_id}/month/{month}/year/{year}', 'AreaTransferSicPricingController@all');
            Route::post('/', 'AreaTransferSicPricingController@save');
        });
        //Route::get('/{vd_id}/vendor/{vendor_id}/vehicle/{vehicle_id}/month/{month}/year/{year}', 'VehicleDisposalPricingController@all');
    });
});

Route::group(['prefix'=>'api/transfer-services', 'middleware'=>['auth:api','cors']], function() {
    Route::get('/', 'TransferServiceController@index');
    Route::post('/', 'TransferServiceController@save');
    Route::put('/{id}', 'TransferServiceController@update');
    Route::delete('/{id}', 'TransferServiceController@delete');

    Route::group(['prefix' => "{transfer_service_id}/cancellation-policies"], function(){
        Route::get("", 'TranserServiceCancellationPolicyController@index');
        Route::post("", "TranserServiceCancellationPolicyController@create");
        Route::put("{cancellation_policy_id}", 'TranserServiceCancellationPolicyController@update');
        Route::delete("{cancellation_policy_id}", "TranserServiceCancellationPolicyController@delete");
    });
});
//Tour filters
Route::group(['prefix' => "api/transfer-filters"], function() {
    Route::get("", "TransferFilterController@index");
    Route::post("", "TransferFilterController@create");
    Route::put("{filter_id}", "TransferFilterController@update");
    Route::delete("{filter_id}", "TransferFilterController@delete");
    Route::put("{filter_id}/restore", "TransferFilterController@restore");
    // not used PUT as above put("{filter_id}) is over-riding the below route, so keep as POST
    Route::post("update-filter-group-heading", 'TransferFilterController@updateFilterGroupLabel');
});

Route::group(['prefix'=>'api/transfers', /* 'middleware'=>['auth:api','cors'] */], function() {

    Route::group(['prefix' => "point-2-point-transfer-routes/" ], function() {

        Route::get("", "Point2PointTransferRouteController@index");
        Route::post("", 'Point2PointTransferRouteController@create');
        Route::put("/{id}", 'Point2PointTransferRouteController@update');

        Route::get("{id}/sic-pricing", 'Point2PointTransferRouteSicPricingController@pricingBetweenDates');
        Route::post("{id}/sic-pricing", "Point2PointTransferRouteSicPricingController@update");

        Route::get("{id}/private-pricing", 'Point2PointTransferRoutePrivateVehiclePricingController@pricingBetweenDates');
        Route::post("{id}/private-pricing", "Point2PointTransferRoutePrivateVehiclePricingController@update");
    });

    Route::group(['prefix' => "titles"], function(){
        Route::get("", 'TransferTitleController@index');
        Route::post("", 'TransferTitleController@create');
        Route::put("/{trasfer_title_id}", "TransferTitleController@update");

        Route::get("{tranfer_title_id}/private-pricing", 'TransferTitlePrivateVehiclePricingController@pricingBetweenDates');
        Route::post("{tranfer_title_id}/private-pricing", 'TransferTitlePrivateVehiclePricingController@update');

        Route::get("{tranfer_title_id}/sic-pricing", 'TransferTitleSICPricingController@pricingBetweenDates');
        Route::post("{tranfer_title_id}/sic-pricing", 'TransferTitleSICPricingController@update');

    });
});


Route::group(['prefix' => '/api/contracts/vehicles'], function() {

    Route::group(['prefix' => '{vehicle_id}'], function() {
        Route::post('/attach-to-vendor/{vendor_id}', 'VendorVehicleMappingsController@attach');
        Route::post('/detach-from-vendor/{vendor_id}', 'VendorVehicleMappingsController@detach');
    });

    Route::post('for-vendor/{vendor_id}', 'VendorVehicleMappingsController@forVendor');
});

//Serch Related

Route::group(['prefix'=>'api/search/transfers', 'middleware'=>['auth:api','cors']], function() {
    Route::post("", 'SearchController@search');
    Route::post("/{transfer_service_id}/vehicle-availability", 'SearchController@vehicleAvailability');
});
