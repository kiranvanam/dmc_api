<?php

namespace Columbus\Contracts\Transfers\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class AreaTransferCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'from_country_id' => 'required',
            'to_country_id' => 'required',
            'from_city_id' => 'required',
            'to_city_id' => 'required',
            'from_area_id' => 'required',
            'to_area_id' => 'required',
            'vendor_id' => 'required'
        ];
    }

    protected function prepareForValidation()
    {
    }

    /**
     * customizing error messages for (field,rule)
     */
    public function messages()
    {
        return [
            'from_country_id.required' => "Please Select From Country",
            'to_country_id.required' => "Please Select To Country",
            'from_city_id.required' => "Please select from city",
            'to_city_id.required' => "Please select city details",
            'from_area_id.required' => "Please select from area",
            'to_area_id.required' => "Please select to area",
            'vendor_id.required' => "Please select a vendor",
        ];
    }

}
