<?php

namespace Columbus\Contracts\Transfers\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class VehicleDisposalPricingCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'date' => 'required',
            'vehicle_disposal_id' => 'required',
            'vendor_id' => 'required',
            'quantity' => 'required',
            'vehicle_id' => 'required',
            'price' => 'required',
        ];
    }

    protected function prepareForValidation()
    {
    }

    /**
     * customizing error messages for (field,rule)
     */
    public function messages()
    {
        return [
            'date.required' => "Please select a valid date",
            'vehicle_disposal_id.required' => "Invalid Vehicle Disposal Selection",
            'vendor_id.required' => "Please select a vendor before submitting the request",
            'quantity.required' => "Please provide the quantity details",
            'vehicle_id.required' => "Please select a vehicle before submitting the request",
            'price' => "Please mention the price"
        ];
    }

}
