<?php

namespace Columbus\Contracts\Transfers\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class VehicleDisposalCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'country_id' => 'required',
            'city_id' => 'required',
            'code' => 'required',
            'hours' => 'required',
        ];
    }

    protected function prepareForValidation()
    {
        $this->merge([
                'slug' => str_slug($this->get('name')),
            ]
        );
    }

    /**
     * customizing error messages for (field,rule)
     */
    public function messages()
    {
        return [
            'name.required' => "Name Must Be Provided",
            'country_id.required' => "Please select a country before submitting the request",
            'city_id.required' => "Please select a city before submitting the request",
            'hours' => "Please mention the hours",
            'code' => "Please provide a valid code"
        ];
    }

}
