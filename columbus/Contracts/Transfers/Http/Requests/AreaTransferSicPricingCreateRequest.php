<?php

namespace Columbus\Contracts\Transfers\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class AreaTransferSicPricingCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'date' => 'required',
            'area_transfer_id' => 'required',
            'price' => 'required',
            'quantity' => 'required',
        ];
    }

    protected function prepareForValidation()
    {
    }

    /**
     * customizing error messages for (field,rule)
     */
    public function messages()
    {
        return [
            'date.required' => "Please select a valid date",
            'area_transfer_id.required' => "Invalid Area Transfer Selection",
            'quantity.required' => "Please provide the quantity details",
            'price' => "Please mention the price"
        ];
    }

}
