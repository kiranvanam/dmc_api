<?php

namespace Columbus\Contracts\Transfers\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class TransferPointCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'country_id' => 'required',
            'region_id' => 'required',
            'city_id' => 'required',
            'area_id' => 'required',
            'code' => 'required',
            'transfer_point_type_id' => 'required'
        ];
    }

    protected function prepareForValidation()
    {
        $this->merge([
                'slug' => str_slug($this->get('name')),
            ]
        );
    }

    /**
     * customizing error messages for (field,rule)
     */
    public function messages()
    {
        return [
            'name.required' => "Transfer Point Name Must Be Provided",
            'code.required' => "Transfer Point Code Must Be Provided",
            'country_id.required' => "Please Select Country",
            'region_id.required' => "Please Select Region",
            'city_id.required' => "Please Select City",
            'area_id.required' => "Please select Area",
            'tp_type_id.required' => "Transfer Point Type Must Be Provided",
        ];
    }

}
