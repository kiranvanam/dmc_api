<?php

namespace Columbus\Contracts\Transfers\Http\Requests;

use Columbus\Contracts\Transfers\Models\TransferService;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class TransferServiceCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
        ];
    }
/*
    protected function prepareForValidation()
    {
        $this->merge([
                'slug' => str_slug($this->get('name')),
            ]
        );
    }*/

    public function messages()
    {
        return [
            'name.required' => "Name must be provided"
        ];
    }


    public function save($vendor_service_id)
    {
        $data = $this->all();
        $data['vendor_service_id'] = $vendor_service_id;
        return TransferService::create($data);
    }

    public function update($id)
    {
        $type = TransferService::withTrashed()
                                ->where( 'id', $id)
                                ->first();

        if($type) {
            $type->update($this->all());
        }
        return $type;
    }

    public function delete($id)
    {
        $type = TransferService::withTrashed()
                                ->where( 'id', $id)
                                ->first();

        if($type) {
            return $type->delete();
        }
        return false;
    }

}
