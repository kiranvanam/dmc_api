<?php
namespace Columbus\Contracts\Transfers\Http\Controllers;

use Auth;
use App\Http\Controllers\ApiController;
use Columbus\Contracts\Transfers\Http\Requests\TransferPointCreateRequest;
use Columbus\Contracts\Transfers\Models\TransferPoint;
use Columbus\Contracts\Transfers\Models\TransferPointType;
use Illuminate\Http\Request;

class TransferPointController extends ApiController
{
    public function all() {
        Auth::user()->hasPermission('vendors');
        return $this->ok(
            [ 'list' => TransferPoint::all(), 'types' => TransferPointType::all()],
            "Fetching Transfer Point Types is Successful"
        );
    }

    public function save(TransferPointCreateRequest $request)
    {
        return $this->ok(TransferPoint::create($request->all()), "Transfer Point details added successfully");
    }

    public function update($id, TransferPointCreateRequest $request)
    {
        $data = $request->all();
        $transferPoint = TransferPoint::find($id);
        $transferPoint->update($data);

        return $this->ok($transferPoint, "Transfer Point details Updated successfully");
    }

    public function delete($id)
    {
        $transferPoint = TransferPoint::find($id);
        $transferPoint->delete();
        return $this->ok($transferPoint, "Transfer Point deleted successfully");
    }
}
