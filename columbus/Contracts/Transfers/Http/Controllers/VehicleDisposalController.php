<?php
namespace Columbus\Contracts\Transfers\Http\Controllers;

use Auth;
use App\Http\Controllers\ApiController;
use Columbus\Contracts\Transfers\Http\Requests\VehicleDisposalCreateRequest;
use Columbus\Contracts\Transfers\Models\VehicleDisposal;
use Illuminate\Http\Request;

class VehicleDisposalController extends ApiController
{
    public function all() {
        Auth::user()->hasPermission('vendors');
        return $this->ok(
            VehicleDisposal::all(),
            "Fetching Vehicle Disposal Types is Successful"
        );
    }

    public function save(VehicleDisposalCreateRequest $request)
    {
        return $this->ok(VehicleDisposal::create($request->all()), "Vehicle Disposal details added successfully");
    }

    public function update($id, VehicleDisposalCreateRequest $request)
    {
        $data = $request->all();
        $vehicleDisposal = VehicleDisposal::find($id);
        $vehicleDisposal->update($data);

        return $this->ok($vehicleDisposal, "Vehicle Disposal details Updated successfully");
    }

    public function delete($id)
    {
        $vehicleDisposal = VehicleDisposal::find($id);
        $vehicleDisposal->delete();
        return $this->ok($vehicleDisposal, "Vehicle Disposal deleted successfully");
    }
}
