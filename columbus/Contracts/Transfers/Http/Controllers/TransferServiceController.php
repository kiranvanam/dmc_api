<?php
namespace Columbus\Contracts\Transfers\Http\Controllers;


use App\Http\Controllers\ApiController;
use Columbus\Contracts\Transfers\Http\Requests\TransferServiceCreateRequest;
use Columbus\Contracts\Transfers\Models\TransferService;
use Columbus\Contracts\Vendors\Models\VendorService;
use Illuminate\Http\Request;

class TransferServiceController extends ApiController
{
    public function index()
    {
        $services= TransferService::when( request('services') == "all" , 
                                            function($query) {
                                                $query->withTrashed();
                                           }
                                    )
                                    ->get();
        
        return $this->ok( $services);
    }

    public function save(TransferServiceCreateRequest $request)
    {
        $vendorSubTransferService = VendorService::createSubTransferService($request->only(['name','description']));
        return $this->ok($request->save($vendorSubTransferService->id), "Transfer service created");
    }

    public function update($id, TransferServiceCreateRequest $request) 
    {
        $data = $request->only(['country_id', 'vendor_service_id', 'name', 'category', 
                                'description', 'is_sic', 'is_private',
                                'sic_details', 'private_details']);
        $service = $request->update($id);

        if($service) {
            $data['vendor_service_id'] = $service->vendor_service_id;
            VendorService::updateSubTransferService($data);
        }
        
        return $this->ok($service, "Transfer service updated");
    }

    public function delete($id)
    {

        $type = TransferService::find($id);

        if($type) {
            VendorService::deleteSubTransferService($type->vendor_service_id);
            $type->delete();
            return $this->ok( $type, "Transfer service deleted ");
        }
        return $this->ok($type, "Transfer service was not deleted, some problem happened");
    }
}