<?php
namespace Columbus\Contracts\Transfers\Http\Controllers;

use Auth;
use App\Http\Controllers\ApiController;
use Columbus\Contracts\Transfers\Http\Requests\VehicleDisposalPricingCreateRequest;
use Columbus\Contracts\Transfers\Models\VehicleDisposalPricing;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class VehicleDisposalPricingController extends ApiController
{
    public function all($vd_id, $vendor_id, $vehicle_id, $month, $year, Request $request) {
        Auth::user()->hasPermission('vendors');
        $fromDate = $year . '-' . $month . '-' . '1';
        $toDate = $year . '-' . $month . '-' . '31';
        $records = VehicleDisposalPricing::where('vehicle_disposal_id', $vd_id)
                ->where('vendor_id', $vendor_id)
                ->where('vehicle_id', $vehicle_id)
                ->whereBetween("date", [$fromDate, $toDate])
                ->orderBy('date', 'asc')
                ->get();
        return $this->ok(
            $records,
            "Fetching Vehicle Disposal Pricings is Successful"
        );
    }

    public function save(Request $request)
    {
        $dates = $this->getDatesFromRequest($request);
        $this->removeOldRecords($request, $dates);
        return $this->ok($this->addRecords($request, $dates), "Vehicle Disposal Pricing details added successfully");
    }

    protected function removeOldRecords(Request $request, $dates)
    {
        VehicleDisposalPricing::where([
                ['vehicle_disposal_id', '=', $request['vehicle_disposal_id']],
                ['vendor_id', '=', $request['vendor_id']],
                ['vehicle_id', '=', $request['vehicle_id']],
            ])->whereIn('date', $dates)->delete();
    }

    protected function addRecords(Request $request, $dates)
    {
        $records = [];
        //while($currentDate <= $toDate) {
        foreach ($dates as $date) {
            $data = [
                'date' => $date,
                'vehicle_disposal_id' => $request['vehicle_disposal_id'],
                'vendor_id' => $request['vendor_id'],
                'vehicle_id' => $request['vehicle_id'],
                'quantity' => $request['quantity'],
                'price' => $request['price'],
                'extra_km_price' => $request['extra_km_price'],
            ];
            array_push($records, $data);
        }
        DB::table('vehicle_disposal_pricings')->insert($records);

        return $records;
    }

    public function update($id, VehicleDisposalPricingCreateRequest $request)
    {
        $data = $request->all();
        $vehicleDisposalPricing = VehicleDisposalPricing::find($id);
        $vehicleDisposalPricing->update($data);

        return $this->ok($vehicleDisposalPricing, "Vehicle Disposal Pricing details Updated successfully");
    }

    public function delete($id)
    {
        $vehicleDisposalPricing = VehicleDisposalPricing::find($id);
        $vehicleDisposalPricing->delete();
        return $this->ok($vehicleDisposalPricing, "Vehicle Disposal Pricing deleted successfully");
    }
}
