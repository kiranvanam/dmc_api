<?php
namespace Columbus\Contracts\Transfers\Http\Controllers;

use Auth;
use App\Http\Controllers\ApiController;
use Columbus\Contracts\Transfers\Http\Requests\TransferPointTypeCreateRequest;
use Columbus\Contracts\Transfers\Models\TransferPointType;
use Illuminate\Http\Request;

class TransferPointTypeController extends ApiController
{
    public function all() {
        Auth::user()->hasPermission('vendors');
        return $this->ok(TransferPointType::all(), "Fetching Transfer Point Types is Successful");
    }

    public function save(TransferPointTypeCreateRequest $request)
    {
        return $this->ok(TransferPointType::create($request->all()), "Point Type created");
    }

    public function update($id, TransferPointTypeCreateRequest $request)
    {
        $data = $request->all();
        $transferPointType = TransferPointType::find($id);
        $transferPointType->update($data);

        return $this->ok($transferPointType, "Point Type Updated");
    }

    public function delete($id)
    {
        $transferPointType = TransferPointType::find($id);
        $transferPointType->delete();
        return $this->ok($transferPointType, "Point Type deleted");
    }
}
