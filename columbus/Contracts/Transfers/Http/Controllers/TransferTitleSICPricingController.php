<?php

namespace Columbus\Contracts\Transfers\Http\Controllers;

use Columbus\Utilities\DateUtil;
use App\Http\Controllers\ApiController;
use Columbus\Contracts\Transfers\Models\TransferTitleSICPricing;

class TransferTitleSICPricingController  extends ApiController
{
    public function pricingBetweenDates($title_id )
    {
        
        $from_date = DateUtil::createCarbonIfNotCarbon(request('from_date'))->toDateString();
        $to_date = DateUtil::createCarbonIfNotCarbon(request('to_date'))->toDateString();

        $pricing = TransferTitleSICPricing::
                             where('transfer_title_id', $title_id)
                            ->where('date', '>=', $from_date)
                            ->where('date','<=', $to_date)
                            ->orderBy('date')
                            ->get();

        return $this->ok( $pricing, "sic pricing details for given ranges");
    }

    public function update($title_id)
    {
        $request = request();
        $this->weekDaysPricingDetails = $request->all();
        $this->weekDaysPricingDetails['transfer_title_id'] = $title_id;

        $dates = DateUtil::datesForRange(
                                $request->get("from_date"), 
                                $request->get('to_date'), 
                                $this->callBackFunction()
                            );
        return $this->ok($dates , "Transfer Title SIC Pricing updated for given ranges"); 
    }

    public function callBackFunction()
    {
        return function($date) {
            $dayDetails = $this->getDayDetails($date);
            
            if(empty($dayDetails) == false) {
                $routePricing = TransferTitleSICPricing::
                                        where('transfer_title_id', $dayDetails['transfer_title_id'])
                                      ->where('date', $dayDetails['date'])
                                      ->first();

                if(empty($routePricing)) {
                   $routePricing = TransferTitleSICPricing::create($dayDetails);                
                }else {
                    $routePricing->update($dayDetails);
                }
                return $routePricing;
            }
            return null;
        };
    }

    public function getDayDetails($date)
    {
        $week_day_index = $date->dayOfWeek;
        
        //$allSlotsTotalAllocationsForDay = 0;

        foreach ($this->weekDaysPricingDetails['operating_days'] as  $dayDetails) {
             if($dayDetails['week_day_index'] == $week_day_index) {
                $dayDetails['currency'] = $this->weekDaysPricingDetails['currency'];
                $dayDetails['date'] = $date->toDateString();
                $dayDetails['transfer_title_id'] = $this->weekDaysPricingDetails['transfer_title_id'];
                
                //$dayDetails['time_slots_with_pricing_details'];                
                
                //$dayData['allocation'] = $dayDetails['allocation'];               
                
                //$dayDetails['is_available'] = true;
                //$dayDetails['is_on_request'] = ;
                
                return $dayDetails;
             }   
        }

        return null;
    }
}