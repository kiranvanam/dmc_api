<?php

namespace Columbus\Contracts\Transfers\Http\Controllers;

use Columbus\Utilities\DateUtil;
use App\Http\Controllers\ApiController;
use Columbus\Contracts\Transfers\Models\TransferTitlePrivateVehiclePricing;

class TransferTitlePrivateVehiclePricingController extends ApiController
{
    public function pricingBetweenDates($transfer_title_id )
    {
        $from_date = DateUtil::createCarbonIfNotCarbon(request('from_date'))->toDateString();
        $to_date = DateUtil::createCarbonIfNotCarbon(request('to_date'))->toDateString();

        $pricing = TransferTitlePrivateVehiclePricing::                            
                             where('transfer_title_id', $transfer_title_id)
                            ->where('vehicle_id', request('vehicle_id'))
                            ->where('date', '>=', $from_date)
                            ->where('date','<=', $to_date)
                            ->orderBy('date')
                            ->get();

        return $this->ok( $pricing, "Vehicle Pricing details for given date ranges");
    }

    public function update($transfer_title_id)
    {
        $request = request();
        $this->weekDaysPricingDetails = $request->all();
        $this->weekDaysPricingDetails['transfer_title_id'] = $transfer_title_id;
        
        $dates = DateUtil::datesForRange(
                                $request->get("from_date"), 
                                $request->get('to_date'), 
                                $this->callBackFunction()
                            );
        return $this->ok($dates , " Vehicle Pricing updated for given date ranges"); 
    }

    public function callBackFunction()
    {
        return function($date) {
            $dayDetails = $this->getDayDetails($date);
            
            if(empty($dayDetails) == false) {
                $routePricing = TransferTitlePrivateVehiclePricing::
                                        where('transfer_title_id', $dayDetails['transfer_title_id'])
                                      ->where('vehicle_id', $dayDetails['vehicle_id'])
                                      ->where('date', $dayDetails['date'])
                                      ->first();

                if(empty($routePricing)) {
                   $routePricing = TransferTitlePrivateVehiclePricing::create($dayDetails);                
                }else {
                    $routePricing->update($dayDetails);
                }
                return $routePricing;
            }
            return null;
        };
    }

    public function getDayDetails($date)
    {
        $week_day_index = $date->dayOfWeek;
        
        //$allSlotsTotalAllocationsForDay = 0;

        foreach ($this->weekDaysPricingDetails['operating_days'] as  $dayDetails) {
             if($dayDetails['week_day_index'] == $week_day_index) {
                $dayDetails['currency'] = $this->weekDaysPricingDetails['currency'];
                $dayDetails['date'] = $date->toDateString();
                $dayDetails['transfer_title_id'] = $this->weekDaysPricingDetails['transfer_title_id'];
                $dayDetails['vehicle_id'] = $this->weekDaysPricingDetails['vehicle_id'];                
                
                return $dayDetails;
             }   
        }

        return null;
    }
}