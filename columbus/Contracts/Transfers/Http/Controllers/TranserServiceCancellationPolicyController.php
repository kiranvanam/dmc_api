<?php

namespace Columbus\Contracts\Transfers\Http\Controllers;

use App\Http\Controllers\ApiController;
use Columbus\Contracts\Transfers\Models\TransferServiceCancellationPolicy as CancellationPolicy;

class TranserServiceCancellationPolicyController extends ApiController
{
    function index($transfer_service_id)
    {
        $policies = CancellationPolicy::where('transfer_service_id', $transfer_service_id)->get();        
        return $this->ok($policies);
    }

    function create($transfer_service_id)
    {
        $data = request()->all();
        $policy = CancellationPolicy::create($data);
        return $this->ok($policy, "Cancellation Policy created");
    }

    function update($transfer_service_id, $cancellation_policy_id)
    {
        $data = request()->all();
        $policy = CancellationPolicy::findOrFail($cancellation_policy_id);
        $policy->update($data);
        return $this->ok($policy, "Policy Details are updated");
    }

    function delete($transfer_service_id, $cancellation_policy_id)
    {
        $policy = CancellationPolicy::findOrFail($cancellation_policy_id);
        $policy->delete();
        return $this->ok($policy, "Cancellation Policy has been Deleted");
    }
}