<?php

namespace Columbus\Contracts\Transfers\Http\Controllers;

use App\Http\Controllers\ApiController;
use Columbus\Contracts\Transfers\Helpers\TitleBasedSearch;
use Columbus\Contracts\Transfers\Helpers\Point2PointSearch;

class SearchController extends ApiController
{
    
    public function search()
    {    
        $searchCriteria = request()->all(); 
        /* $searchCriteria = [   "transfer_type" => "point-2-point",
                                    "onward" => [
                                        "pick_up_area_id" => 2,
                                        "drop_off_area_id" => 1,
                                        "travelling_date" => "2018-05-16"
                                    ],
                                    "is_return_journy" => true,
                                    "return" => [
                                        "pick_up_area_id" => 1,
                                        "drop_off_area_id" => 2,
                                        "travelling_date" => "2018-05-19"
                                    ],
                                ]; */
        $transferServiceCategory = array_get($searchCriteria, 'transfer_service_category');
        
        if( $transferServiceCategory == "point-2-point") {
            return  $this->ok((new Point2PointSearch())->search($searchCriteria));
        } else if($transferServiceCategory == "assign-to-title") {
            return $this->ok( (new TitleBasedSearch())->search($searchCriteria));
        }

        return $this->ok("Unkonwn transfer type is being searched");
    }
    
    public function vehicleAvailability($transfer_service_id = null)
    {
        $transfer_service_id = request('transfer_service_id');
        $vehicle_id = request('vehicle_id');
        $travel_date = request('travel_date');
    }
}