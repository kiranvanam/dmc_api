<?php

namespace Columbus\Contracts\Transfers\Http\Controllers;

use App\Http\Controllers\ApiController;
use Columbus\SystemSettings\Models\Code;
use Columbus\Contracts\Transfers\Models\TransferTitle;

class TransferTitleController extends ApiController
{
    public function index()
    {
        $titles = TransferTitle::
                    where('city_id', request('city_id'))
                    ->where('transfer_service_id', request('transfer_service_id'))
                    ->get();

        return $this->ok( $titles , "Transfer titles");
    }

    public function create()
    {
        $data = request()->all();
        $data['slug'] = str_slug( $data['name']);
        $data['code'] =  Code::getNextCodeForTransfers();
        return $this->ok( TransferTitle::create( $data ), "Transfer Title Created" );
    }

    public function update($transfer_title_id)
    {
        $data = request()->all();
        $title = TransferTitle::findOrFail($transfer_title_id);
        if( empty($title->code) )
            $data['code'] =  Code::getNextCodeForTransfers();
        
        $title->update( $data);

        return $this->ok($title, "Transfer title details updated successfully");
    }
}