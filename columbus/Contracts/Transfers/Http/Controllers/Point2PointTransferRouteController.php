<?php

namespace Columbus\Contracts\Transfers\Http\Controllers;

use App\Http\Controllers\ApiController;
use Columbus\Contracts\Transfers\Models\Point2PointTransferRoute;

class Point2PointTransferRouteController extends ApiController
{
    public function index()
    {
        $routes = Point2PointTransferRoute::
                    where('city_id', request('city_id'))
                    ->where('transfer_service_id', request('transfer_service_id'))
                    ->get();

        return $this->ok( $routes , "Point-2-Point Transfer routes");
    }

    public function create()
    {
        return $this->ok( Point2PointTransferRoute::create(request()->all()), "Point-2-Point transfer route is created");
    }

    public function update($id)
    {
        $routeDetails = Point2PointTransferRoute::findOrFail($id);

        $routeDetails->update( request()->all());

        return $this->ok( $routeDetails, "Transfer route details updated" );
    }
}