<?php
namespace Columbus\Contracts\Transfers\Http\Controllers;

use App\Http\Controllers\ApiController;
use Columbus\Contracts\Transfers\Models\AreaTransfer;
use Columbus\Contracts\Transfers\Http\Requests\AreaTransferCreateRequest;
use Illuminate\Http\Request;

class AreaTransferController extends ApiController {
    public function all() {
        return $this->ok(AreaTransfer::all(), " Successful");
    }

    public function save(AreaTransferCreateRequest $request) {
        $areaTransfer = AreaTransfer::firstOrCreate($request->all());
        return $this->ok($areaTransfer, "Area Transfer Added Successfully");
    }
}
