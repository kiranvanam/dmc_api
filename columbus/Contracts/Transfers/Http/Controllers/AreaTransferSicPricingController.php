<?php
namespace Columbus\Contracts\Transfers\Http\Controllers;

use Auth;
use App\Http\Controllers\ApiController;
use Columbus\Contracts\Transfers\Http\Requests\AreaTransferSicPricingCreateRequest;
use Columbus\Contracts\Transfers\Models\AreaTransferSicPricing;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AreaTransferSicPricingController extends ApiController
{
    public function all($area_transfer_id, $month, $year, Request $request) {
        Auth::user()->hasPermission('vendors');
        $fromDate = $year . '-' . $month . '-' . '1';
        $toDate = $year . '-' . $month . '-' . '31';
        $records = AreaTransferSicPricing::where('area_transfer_id', $area_transfer_id)
                ->whereBetween("date", [$fromDate, $toDate])
                ->orderBy('date', 'asc')
                ->get();
        return $this->ok(
            $records,
            "Fetching Area Transfer SIC Pricings is Successful"
        );
    }

    public function save(Request $request)
    {
        $dates = $this->getDatesFromRequest($request);
        $this->removeOldRecords($request, $dates);
        return $this->ok($this->addRecords($request, $dates), "Area Transfer SIC Pricing details added successfully");
    }

    protected function removeOldRecords(Request $request, $dates)
    {
        AreaTransferSicPricing::where([
                ['area_transfer_id', '=', $request['area_transfer_id']],
            ])->whereIn('date', $dates)->delete();
    }

    protected function addRecords(Request $request, $dates)
    {
        $records = [];
        foreach ($dates as $date) {
            $data = [
                'date' => $date,
                'area_transfer_id' => $request['area_transfer_id'],
                'quantity' => $request['quantity'],
                'price' => $request['price'],
            ];
            array_push($records, $data);
        }
        DB::table('area_transfer_sic_pricings')->insert($records);
        return $records;
    }
}
