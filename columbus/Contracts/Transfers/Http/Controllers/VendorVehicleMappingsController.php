<?php
namespace Columbus\Contracts\Transfers\Http\Controllers;

use App\Http\Controllers\ApiController;
use Columbus\Contracts\Transfers\Models\VendorVehiclePricing;
use Illuminate\Http\Request;

class VendorVehicleMappingsController extends ApiController
{

    public function forVendor($vendor_id, Request $request)
    {
        return $this->ok(VendorVehiclePricing::where('vendor_id', $vendor_id)
                                    ->where('transfer_type_id', $request->get('transfer_type_id'))
                                    ->where('city_id', $request->get('city_id'))
                                    ->get(),
                        "Vendor vehicles details");

    }
    
    public function attach($vehicle_id, $vendor_id, Request $request)
    {
        $vendorVehicleData = $request->only(['vehicle_id','vendor_id','city_id', 'brand_id',  'transfer_type_id']);

        // need to maintain pricing details for 12 months
        $months = collect([0,1,2,3,4,5,6,7,8,9,10,11]);
        $vendorVehicles = [];
        $months->each(function($month)  use($vendorVehicleData, &$vendorVehicles) {
            $vendorVehicleData['month'] = $month;
             $vendorVehicles[] = VendorVehiclePricing::create($vendorVehicleData);
        });

        return $this->ok($vendorVehicles, "Vehicle attached to vendor");
    }

    public function detach($vehicl_id, $vendor_id, Request $request)
    {
        $vendorVehicles = VendorVehiclePricing::where('vendor_id', $vendor_id)
                                    ->where('transfer_type_id', $request->get('transfer_type_id'))
                                    ->where('brand_id', $request->get('brand_id'))
                                    ->where('vehicle_id', $vehicle_id)
                                    ->where('city_id', $request->get('city_id'))
                                    ->get();

        $vendorVehicles->each(function($mapping)
        {
            $mapping->delete(); 
        });

        return $this->ok(true, "Vehicle detached from vendor");
    }
}
