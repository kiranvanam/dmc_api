<?php

namespace Columbus\Contracts\Transfers\Models;

use Columbus\Contracts\Transfers\Models\VehicleDisposal;
use Illuminate\Database\Eloquent\Model;

class VehicleDisposalPricing extends Model
{
    public $timestamps = false;
    protected $fillable = ['date', 'vehicle_disposal_id', 'vendor_id', 'vehicle_id', 'quantity', 'price', 'extra_km_price'];
    protected $dates = ['date'];
    protected $dateFormat = 'Y-m-d';
    public function vehicleDisposal() {
        return $this->belongsTo(VehicleDisposal::class);
    }
}
