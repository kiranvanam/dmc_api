<?php

namespace Columbus\Contracts\Transfers\Models;

use Illuminate\Database\Eloquent\Model;

class TransferPoint extends Model
{
    public $timestamps = false;
    protected $fillable = ['name', 'slug', 'code', 'country_id', 'region_id', 'city_id', 'area_id', 'lat', 'lng', 'transfer_point_type_id'];
}
