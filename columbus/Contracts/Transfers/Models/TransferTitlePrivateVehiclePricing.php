<?php

namespace Columbus\Contracts\Transfers\Models;

use Illuminate\Database\Eloquent\Model;

class TransferTitlePrivateVehiclePricing extends Model
{
    public $table = "transfer_title_private_vehicle_pricing";
    
    public $timestamps = false;
        
    protected $dates = ['date'];
    
    
    protected $fillable = ['transfer_title_id', 'vehicle_id', 
                            'date',
                            'inventory', 'utilized', 
                            'price', 'duration', 'duration_type', 
                            'extra_charge', 'extra_duration', 'extra_duration_type',
                            'currency',
                            'is_available', 'is_on_request', 
                        ];
    
    public function vehicle()
    {
        return $this->belongsTo(\Columbus\Contracts\Vehicles\Models\Vehicle::class, 'vehicle_id')->with('brand');
    }
}