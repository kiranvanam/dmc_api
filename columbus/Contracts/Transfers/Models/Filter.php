<?php

namespace Columbus\Contracts\Transfers\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Filter extends Model
{
    use SoftDeletes;

    public $table = "transfer_filters";
    
    protected $dates = ['deleted_at'];

    protected $fillable = ['country_id', 'groupby', 'name'];
}