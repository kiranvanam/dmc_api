<?php

namespace Columbus\Contracts\Transfers\Models;

use Illuminate\Database\Eloquent\Model;

class TransferServiceCancellationPolicy extends Model
{    
    protected $fillable = ['transfer_service_id', 'transfer_service_type',
                            'booking_start_date', 'booking_end_date',
                            'travelling_start_date', 'travelling_end_date',
                            'is_non_refundable',
                            'policy_rules'
                        ];

    protected $casts = [
        'policy_rules' => 'array'
    ];
}