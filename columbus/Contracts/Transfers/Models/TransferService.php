<?php

namespace Columbus\Contracts\Transfers\Models;

use App\CancellationPolicy;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TransferService extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $fillable= ['country_id', 'vendor_service_id',
                    'name', 'description', 'category',  // category - point-2-point/title-based
                    'is_sic', 'is_private', 'is_show_luggage_info',
                    'sic_details', 'private_details'
                ];
    protected $casts =[
        'sic_details' => 'array',
        'private_details' => 'array'
    ];

    public function cancellationPolicies()
    {
        //return $this->hasMany(TransferServiceCancellationPolicy::class, 'transfer_service_id');
        return $this->hasOne(CancellationPolicy::class, 'service_id')->where('service', 'transfer-service');
    }

    public static function toggleByVendorServiceId($vendor_service_id)
    {
        $service = static::withTrashed()
                        ->where('vendor_service_id', $vendor_service_id)
                        ->first();
        if( !empty($service) ) {
            if ($service->trashed()) {
                $service->restore();
            } else {
                $service->delete();
            }
        }
    }

    public static function updateByVendorServiceId($vendor_service_id, $data = [])
    {
        $service = static::withTrashed()
                        ->where('vendor_service_id', $vendor_service_id)
                        ->first();

        if( !empty($service)  && !empty($data) ) {
            $service->update(['name' => $data['name']]);
        } else {
            $data['vendor_service_id'] = $vendor_service_id;
            static::createWith($data);
        }
    }

    public static function createWith($data = [])
    {
        static::create($data);
    }

}
