<?php

namespace Columbus\Contracts\Transfers\Models;

use Illuminate\Database\Eloquent\Model;

class TransferTitleSICPricing extends Model
{
    public $table = "transfer_title_sic_pricing";
    
    public $timestamps = false;
        
    protected $dates = ['date'];

    protected $fillable = ['transfer_title_id',
                            'date',
                            'inventory', 'utilized', 
                            'adult_price', 'child_price', 'infant_price', 'currency',
                            'is_available', 'is_on_request',
                        ];
    
}