<?php

namespace Columbus\Contracts\Transfers\Models;

use Illuminate\Database\Eloquent\Model;

class Point2PointTransferRoutePrivateVehiclePricing extends Model
{
    public $table = "point_2_point_transfer_route_private_vehicle_pricing";
    
    public $timestamps = false;
        
    protected $dates = ['date'];
    
    
    protected $fillable = ['transfer_route_id', 'vehicle_id', 
                            'date',
                            'inventory', 'utilized', 'price', 'currency',
                            'is_available', 'is_on_request', 
                        ];

    public function vehicle()
    {
        return $this->belongsTo(\Columbus\Contracts\Vehicles\Models\Vehicle::class, 'vehicle_id');
    }
    
}