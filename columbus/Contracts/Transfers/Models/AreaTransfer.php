<?php

namespace Columbus\Contracts\Transfers\Models;

use Illuminate\Database\Eloquent\Model;

class AreaTransfer extends Model
{
    public $timestamps = false;
    protected $fillable = ['vendor_id', 'from_country_id', 'to_country_id', 'from_city_id', 'to_city_id', 'from_area_id', 'to_area_id'];
}
