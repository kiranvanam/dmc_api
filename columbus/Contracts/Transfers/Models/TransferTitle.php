<?php

namespace Columbus\Contracts\Transfers\Models;

use Illuminate\Database\Eloquent\Model;

class TransferTitle extends Model
{
    public $timestamps = false;

    protected $fillable = ['city_id', 'transfer_service_id', 'name', 'slug', 
                            'code', 'is_sic', 'is_private', 
                            'description', 'filters'
                        ];

    protected $casts = [
        "filters" => "array"
    ];

    public function privatePricing() {
        return $this->hasMany(TransferTitlePrivateVehiclePricing::class)->with('vehicle');
    }

    public function sicPricing() {
        return $this->hasOne(TransferTitleSICPricing::class);
    }
}