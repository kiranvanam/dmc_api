<?php

namespace Columbus\Contracts\Transfers\Models;

use Illuminate\Database\Eloquent\Model;

class AreaTransferSicPricing extends Model
{
    public $timestamps = false;
    protected $fillable = ['area_transfer_id', 'price', 'quantity'];
}
