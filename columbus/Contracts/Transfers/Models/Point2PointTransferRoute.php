<?php

namespace Columbus\Contracts\Transfers\Models;

use Illuminate\Database\Eloquent\Model;

class Point2PointTransferRoute extends Model
{
    public $table  = "point_2_point_transfer_routes";
    public $timestamps = false;
    
    protected $fillable = ['city_id', 'transfer_service_id', 
                        'from_area_id', 'to_area_ids', 
                        'is_sic', 'is_private', 'is_same_price_for_return_travel'
                    ];
    
    protected $casts = [
        'to_area_ids' => 'array'
    ];
}