<?php

namespace Columbus\Contracts\Transfers\Models;

use Columbus\Contracts\Transfers\Models\VehicleDisposalPricing;
use Illuminate\Database\Eloquent\Model;

class VehicleDisposal extends Model
{
    public $timestamps = false;
    protected $fillable = ['name', 'slug', 'code', 'country_id', 'city_id', 'hours'];

    public function prices() {
        return $this->hasMany(VehicleDisposalPricing::class);
    }
}
