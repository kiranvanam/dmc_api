<?php

namespace Columbus\Contracts\Transfers\Models;

use Illuminate\Database\Eloquent\Model;

class Point2PointTransferRouteSICPricing extends Model
{
    public $table = "point_2_point_transfer_route_sic_pricing";
    
    public $timestamps = false;
        
    protected $dates = ['date'];

    protected $fillable = ['transfer_route_id',
                            'date',
                            'inventory', 'utilized', 
                            'adult_price', 'child_price', 'infant_price', 'currency',
                            'is_available', 'is_on_request',
                        ];
    
}