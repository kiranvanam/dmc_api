<?php
namespace Columbus\Contracts\Transfers\Models;

use Illuminate\Database\Eloquent\Model;

class VendorVehiclePricing extends Model
{
    public $timestamps = false;

    protected $table = "vendor_vehicle_pricing";

    protected $fillable = ['vendor_id','city_id', 'transfer_type_id', 'vehicle_id', 'brand_id', 'month', 'pricing_details'];

    protected $casts = [
        'pricing_details' => 'array',
    ];
}
