<?php

namespace Columbus\Contracts\Transfers\Models;

use Illuminate\Database\Eloquent\Model;

class TransferPointType extends Model
{
    public $timestamps = false;
    protected $fillable = ['name', 'slug'];
}
