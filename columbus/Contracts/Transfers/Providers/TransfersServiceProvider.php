<?php

namespace Columbus\Contracts\Transfers\Providers;

use Illuminate\Support\Facades\Route;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class TransfersServiceProvider extends ServiceProvider
{
    protected $namespace = "Columbus\Contracts\Transfers\Http\Controllers";

    public function boot()
    {
        $this->app->router->group(['namespace' => $this->namespace], function(){
            if (! $this->app->routesAreCached()) {
                require __DIR__.'/../routes.php';
            }
        });
        $this->loadViewsFrom(__DIR__.'/../resources/views', 'acl');

        $this->loadMigrationsFrom(__DIR__.'/../database/migrations');
    }
}
