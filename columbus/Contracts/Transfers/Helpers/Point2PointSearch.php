<?php

namespace Columbus\Contracts\Transfers\Helpers;


use Columbus\Contracts\Vehicles\Models\Vehicle;
use Columbus\Contracts\Transfers\Models\TransferService;
use Columbus\Contracts\Transfers\Models\Point2PointTransferRoute;
use Columbus\Contracts\Transfers\Models\Point2PointTransferRouteSICPricing;
use Columbus\Contracts\Transfers\Models\Point2PointTransferRoutePrivateVehiclePricing;

class Point2PointSearch
{

    public function search($searchCriteria)
    {

        $routePricingDetails = [];

        $onWardSearchCriteria = $searchCriteria['onward'];

        $transferRoute = $this->findTransferRoute( $onWardSearchCriteria);

        if( !empty($transferRoute) ) {
            $travel_date = array_get($searchCriteria,'onward.travel_date');
            $routePricingDetails['on_ward'] = $this->findPricingDetailsForRouteOnDate( $transferRoute, $travel_date);
        }

        if( $searchCriteria['is_return_journy']) {

            $returnSearchCriteria = $searchCriteria['return'];

            $transferRoute = $this->findTransferRoute( $returnSearchCriteria);

            if( !empty($transferRoute) ) {
                $travel_date = array_get($searchCriteria,'return.travel_date');
                $routePricingDetails['return'] = $this->findPricingDetailsForRouteOnDate( $transferRoute, $travel_date);
            }
        }

        // only if onward and/or return journy details are filled
        $transfer_service_id = array_get($searchCriteria, "transfer_service_id");
        if( !empty($routePricingDetails) || !empty($transfer_service_id) ) {
            $routePricingDetails['transfer_service_details'] = TransferService::where('id', $transfer_service_id)
                                                                        ->with(['cancellationPolicies' => function($query) use($transfer_service_id) {
                                                                            $query->where('service_id', $transfer_service_id);
                                                                        }])
                                                                        ->first();
        }

        return $routePricingDetails;
    }

    protected function findTransferRoute($routeSearchCriteria)
    {
        // step1: find direct route
        $pick_up_area_id = $routeSearchCriteria['pick_up_area_id'];
        $drop_off_area_id = $routeSearchCriteria['drop_off_area_id'];

        $transferRoute = $this->fetchTransferRouteFromArea2ToArea( $pick_up_area_id, $drop_off_area_id);

        //step 2: find indirect route if no route found in step 1.
        if( empty($transferRoute)) {
            $transferRoute = $this->fetchTransferRouteFromArea2ToArea( $drop_off_area_id, $pick_up_area_id, true);
        }

        return $transferRoute;
    }

    protected function fetchTransferRouteFromArea2ToArea($pick_up_area_id, $drop_off_area_id, $is_reverse_route = false)
    {
        $transferRoutes = Point2PointTransferRoute::where('from_area_id', $pick_up_area_id);

        if($is_reverse_route){
            $transferRoutes->where('is_same_price_for_return_travel', true);
        }

        return  $this->findAreaIdInToAreaIds($transferRoutes->get(), $drop_off_area_id);
    }

    protected function findPricingDetailsForRouteOnDate($transferRoute, $travel_date)
    {
        $routePricingDetails = [];

        $routePricingDetails['sic'] = $this->sicPricing($transferRoute->id, $travel_date);
        $routePricingDetails['private'] = $this->privateVehiclePricing($transferRoute->id, $travel_date);

        return $routePricingDetails;
    }

    protected function sicPricing($route_id, $travel_date)
    {
        return Point2PointTransferRouteSICPricing::where('transfer_route_id', $route_id)
                                                ->where('date', $travel_date)
                                                ->first();
    }

    protected function privateVehiclePricing($route_id, $travel_date)
    {
        $pricings = Point2PointTransferRoutePrivateVehiclePricing::
                        where('transfer_route_id', $route_id)
                        ->where('date', $travel_date)
                        ->with('vehicle.brand')
                        ->get();

        return $pricings;
    }

    protected function findAreaIdInToAreaIds($transferRoutes, $area_id)
    {
        if(empty($area_id))
            return null;

        // check if `area_id` is found in `to_area_ids` column in point_2_point_transfer_routes
        return $transferRoutes->first(
                                function($routeDetails) use($area_id) {
                                    return array_search(
                                            $area_id,
                                            $routeDetails->to_area_ids
                                        ) !== false;
                                }
                            );
    }

    protected function prepareVehicles( $routePricingDetails)
    {
        $vehicle_ids = [];
        $privatePricing = array_get( $routePricingDetails, 'on_ward.private', collect([]) );
        if( $privatePricing->isNotEmpty()) {
            $vehicle_ids = $privatePricing->pluck('vehicle_id')->toArray();
        }

        $privatePricing = array_get( $routePricingDetails, 'return.private', collect([]) );
        if( $privatePricing->isNotEmpty()) {
            $vehicle_ids = array_merge( $vehicle_ids, $privatePricing->pluck( 'vehicle_id')->toArray());
        }

        return Vehicle::withIds($vehicle_ids);
    }
}