<?php

namespace Columbus\Contracts\Transfers\Helpers;

use Columbus\Contracts\Transfers\Models\TransferTitle;
use Columbus\Contracts\Transfers\Models\TransferService;

class TitleBasedSearch
{
    public function search($searchCriteria)
    {
        $transfer_service_id = array_get($searchCriteria, 'transfer_service_id', null);

        $results = [];

        $travel_date = array_get($searchCriteria, 'travel_date', null);

        $results['titles_pricing'] = TransferTitle::where('city_id', array_get($searchCriteria, 'pick_up_city_id', null))
                            ->where('transfer_service_id', $transfer_service_id)
                            ->with([ 'privatePricing' => function($query) use($travel_date) {
                                    $query->where('date', $travel_date);
                                }]
                            )
                            ->with(['sicPricing' => function($query) use($travel_date) {
                                    $query->where('date', $travel_date);
                                }]
                            )->get();

        // only if onward and/or return journy details are filled
        if( !empty($routePricingDetails) || !empty($transfer_service_id) ) {
            $results['transfer_service_details'] = TransferService::where('id', $transfer_service_id)
                                                                        ->with('cancellationPolicies')
                                                                        ->first();
        }
        return $results;
    }

}