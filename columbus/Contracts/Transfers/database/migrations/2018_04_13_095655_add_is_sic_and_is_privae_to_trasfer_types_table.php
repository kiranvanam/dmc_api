<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIsSicAndIsPrivaeToTrasferTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('transfer_types', function (Blueprint $table) {
            $table->boolean('is_sic')->default(true);
            $table->boolean('is_private')->default(true);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('transfer_types', function (Blueprint $table) {
            $table->dropColumn('is_sic');
            $table->dropColumn('is_private');
        });
    }
}
