<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenameTransferTypeIdColumnToTransferServiceIdForPoint2PointTransferRoutesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('point_2_point_transfer_routes', function (Blueprint $table) {
            if (Schema::hasColumn('point_2_point_transfer_routes', 'transfer_type_id')) {
                $table->renameColumn('transfer_type_id', 'transfer_service_id');
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('point_2_point_transfer_routes', function (Blueprint $table) {
            //$table->renameColumn('transfer_service_id', 'transfer_type_id');
        });
    }
}
