<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIsSicAndIsPrivateColumnsToTransferTitlesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('transfer_titles', function (Blueprint $table) {
            $table->boolean('is_sic')->default(false)->after('code');
            $table->boolean('is_private')->default(false)->after('is_sic');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('transfer_titles', function (Blueprint $table) {
            $table->dropColumn('is_sic');
            $table->dropColumn('is_private');
        });
    }
}
