<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenameTransferTypeIdColumnToTransferServiceIdForTransferTitlesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('transfer_titles', function (Blueprint $table) {
            if (Schema::hasColumn('transfer_titles', 'transfer_type_id')) {
                $table->renameColumn('transfer_type_id', 'transfer_service_id');
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('transfer_titles', function (Blueprint $table) {
            //
        });
    }
}
