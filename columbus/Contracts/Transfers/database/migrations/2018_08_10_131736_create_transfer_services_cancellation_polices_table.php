<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransferServicesCancellationPolicesTable extends Migration
{
    /**
     * Run the migrations.
     * @return void
     */
    public function up()
    {
        Schema::create('transfer_service_cancellation_policies', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('transfer_service_id')->index();
            $table->string('transfer_service_type', 20);
            $table->date('booking_start_date')->nullable();
            $table->date('booking_end_date')->nullable();
            $table->date('travelling_start_date')->nullable();
            $table->date('travelling_end_date')->nullable();            
            $table->boolean('is_non_refundable')->default(false);
            $table->text('policy_rules')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transfer_service_cancellation_policies');
    }
}
