<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVehicleDisposalPricingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vehicle_disposal_pricings', function (Blueprint $table) {
            $table->increments('id');
            $table->date('date')->index();
            $table->integer('vehicle_disposal_id')->index();
            $table->integer('vendor_id')->index();
            $table->integer('vehicle_id')->index();
            $table->integer('quantity');
            $table->integer('utilized')->default(0);
            $table->decimal('price');
            $table->decimal('extra_km_price')->default(0);
            $table->smallInteger('status')->default(1);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vehicle_disposal_pricings');
    }
}
