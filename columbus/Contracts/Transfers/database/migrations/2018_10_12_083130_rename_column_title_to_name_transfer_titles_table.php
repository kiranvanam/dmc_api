<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenameColumnTitleToNameTransferTitlesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('transfer_titles', function (Blueprint $table) {
            if(Schema::hasColumn('transfer_titles', 'title') ) {
                $table->renameColumn('title', 'name');
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('transfer_titles', function (Blueprint $table) {
            //
        });
    }
}
