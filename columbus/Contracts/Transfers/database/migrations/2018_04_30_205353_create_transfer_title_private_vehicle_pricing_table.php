<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransferTitlePrivateVehiclePricingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transfer_title_private_vehicle_pricing', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('transfer_title_id');
            $table->integer('vehicle_id');
            $table->date('date');
            $table->decimal('price', 8,2);
            $table->string('currency',6);
            $table->integer('duration');
            $table->string('duration_type', 10);
            $table->decimal('extra_charge', 8,2);
            $table->integer('extra_duration')->nullable();        
            $table->string('extra_duration_type', 10)->nullable();
            $table->integer('allocation');
            $table->integer('utilized')->default(0);
            $table->boolean('is_available');
            $table->boolean('is_on_request');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transfer_title_private_vehicle_pricing');
    }
}
