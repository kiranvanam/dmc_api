<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSicAndPrivateDetailsColumnsToTransferServicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('transfer_services', function (Blueprint $table) {
            $table->boolean('is_show_luggage_info')->default(false);
            $table->text('sic_details')->nullable();
            $table->text('private_details')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('transfer_services', function (Blueprint $table) {
            $table->dropColumn('is_show_luggage_info');
            $table->dropColumn('sic_details');
            $table->dropColumn('private_details');
        });
    }
}
