<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAreaTransferSicPricingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('area_transfer_sic_pricings', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('area_transfer_id')->index();
            $table->date('date')->index();
            $table->integer('quantity');
            $table->decimal('price');
            $table->decimal('utilized')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('area_transfer_sic_pricings');
    }
}
