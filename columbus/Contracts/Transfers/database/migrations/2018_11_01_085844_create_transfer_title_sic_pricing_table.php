<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransferTitleSicPricingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transfer_title_sic_pricing', function (Blueprint $table) {
            $table->increments('id');            
            $table->integer('transfer_title_id');
            $table->date('date');
            $table->decimal('adult_price', 8,2);
            $table->decimal('child_price', 8,2);
            $table->decimal('infant_price', 8,2);
            $table->string('currency',6);
            $table->integer('inventory');
            $table->integer('utilized')->default(0);
            $table->boolean('is_available');
            $table->boolean('is_on_request');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transfer_title_sic_pricing');
    }
}
