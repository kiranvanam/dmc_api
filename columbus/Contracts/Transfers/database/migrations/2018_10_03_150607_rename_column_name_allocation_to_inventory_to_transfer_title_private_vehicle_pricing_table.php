<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenameColumnNameAllocationToInventoryToTransferTitlePrivateVehiclePricingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('transfer_title_private_vehicle_pricing', function (Blueprint $table) {
            if(Schema::hasColumn('transfer_title_private_vehicle_pricing', 'allocation')) 
            {
                $table->renameColumn('allocation', 'inventory');
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('transfer_title_private_vehicle_pricing', function (Blueprint $table) {
            //
        });
    }
}
