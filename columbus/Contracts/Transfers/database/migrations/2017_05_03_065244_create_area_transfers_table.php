<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAreaTransfersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('area_transfers', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('vendor_id')->index();
            $table->integer('from_country_id')->index();
            $table->integer('to_country_id');
            $table->integer('from_city_id')->index();
            $table->integer('to_city_id');
            $table->integer('from_area_id')->index();
            $table->integer('to_area_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('area_transfers');
    }
}
