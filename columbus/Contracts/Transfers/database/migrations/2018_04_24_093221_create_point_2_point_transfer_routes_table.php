<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePoint2PointTransferRoutesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('point_2_point_transfer_routes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('city_id');
            $table->integer('transfer_type_id');
            $table->integer('from_area_id');
            $table->string('to_area_ids');
            $table->boolean('is_sic');
            $table->boolean('is_private');
            $table->boolean('is_same_price_for_return_travel');            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('point_2_point_transfer_routes');
    }
}
