<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenameColumnAllocationToInventoryPoint2PointTransferRouteSicPricingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('point_2_point_transfer_route_sic_pricing', function (Blueprint $table) {
            $table->renameColumn('allocation', 'inventory');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('point_2_point_transfer_route_sic_pricing', function (Blueprint $table) {
            //
        });
    }
}
