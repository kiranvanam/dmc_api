<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenamneTransferTypesTableNameToTransferServicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(Schema::hasTable('transfer_types')){
            Schema::rename('transfer_types', 'transfer_services');
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if(Schema::hasTable('transfer_services')){
            Schema::rename('transfer_services', 'transfer_types');
        }
    }
}
