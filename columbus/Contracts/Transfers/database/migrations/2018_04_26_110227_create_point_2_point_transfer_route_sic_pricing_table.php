<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePoint2PointTransferRouteSicPricingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('point_2_point_transfer_route_sic_pricing', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('transfer_route_id');
            $table->date('date');
            $table->decimal('adult_price', 8,2);
            $table->decimal('child_price', 8,2);
            $table->string('currency',6);
            $table->integer('allocation');
            $table->integer('utilized')->default(0);
            $table->boolean('is_available');
            $table->boolean('is_on_request');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('point_2_point_transfer_route_sic_pricing');
    }
}
