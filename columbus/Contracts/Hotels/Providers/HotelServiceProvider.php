<?php

namespace Columbus\Contracts\Hotels\Providers;

use Illuminate\Support\Facades\Route;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class HotelServiceProvider extends ServiceProvider
{
    protected $namespace = "Columbus\Contracts\Hotels\Http\Controllers";

    public function boot()
    {
        $this->app->router->group(['namespace' => $this->namespace], function(){
            if (! $this->app->routesAreCached()) {
                require __DIR__.'/../routes.php';
            }
        });
        $this->loadViewsFrom(__DIR__.'/../resources/views', 'meals');

        $this->loadMigrationsFrom(__DIR__.'/../database/migrations');
    }
}
