<?php

Route::group(['prefix'=>'/api', 'middleware'=>['auth:api','cors']], function() {

    Route::group(["prefix"=> "hotels"], function() {
        Route::get("", 'HotelController@index');
        Route::post("", 'HotelController@create');
        Route::put("{hotel_id}", 'HotelController@update');
        Route::put("{hotel_id}/thumbnail", 'HotelController@updateThumbnail');

        Route::group(['prefix'=>'/{hotel_id}/rooms'], function() {
            Route::get("", 'RoomController@index');
            Route::post("", 'RoomController@create');
            Route::put("{room_id}", 'RoomController@update');
            Route::get("{room_id}/pricing", 'RoomPricingController@pricingForRange');
            Route::post("{room_id}/pricing", 'RoomPricingController@update');
        });
    });

});