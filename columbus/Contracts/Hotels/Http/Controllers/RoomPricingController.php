<?php

namespace Columbus\Contracts\Hotels\Http\Controllers;

use App\Http\Controllers\ApiController;
use Columbus\Contracts\Hotels\Models\RoomPricing;
use Columbus\Utilities\DateUtil;
use Illuminate\Http\Request;

class RoomPricingController extends ApiController
{
    protected $weekDaysPricingDetails = [];

    public function pricingForRange($hotel_id, $room_id, Request $request)
    {
        $from_date = DateUtil::createCarbonIfNotCarbon($request->from_date)->toDateString();
        $to_date = DateUtil::createCarbonIfNotCarbon($request->to_date)->toDateString();
        $pricing = RoomPricing::where('room_id', $room_id)
                            ->where('date', '>=', $from_date)
                            ->where('date','<=', $to_date)
                            ->orderBy('date')
                            ->get();
        return $this->ok($pricing, "Pricing Details for the given range");
    }

    public function update($hotel_id, $room_id, Request $request)
    {
        $this->weekDaysPricingDetails = $request->all();
        $this->weekDaysPricingDetails['room_id'] = $room_id;

        //return $this->ok($this->weekDaysPricingDetails);

        $dates = DateUtil::datesForRange(
                                $request->get("from_date"),
                                $request->get('to_date'),
                                $this->callBackFunction()
                            );
        return $this->ok($dates , "Pricing updated for given ranges");
    }

    //'room_id', 'date', 'time_slots_with_pricing_details', 'allocation', 'is_available', 'is_on_request'
    public function callBackFunction()
    {
        return function($date) {
            $dayDetails = $this->getDayDetails($date);

            if(empty($dayDetails) == false) {
                $guide_vendor_pricing = RoomPricing::where('room_id', $dayDetails['room_id'])
                                      ->where('date', $dayDetails['date'])
                                      ->first();

                if(empty($guide_vendor_pricing)) {
                   $guide_vendor_pricing = RoomPricing::create($dayDetails);
                }else {
                    $guide_vendor_pricing->update($dayDetails);
                }
                return $guide_vendor_pricing;
            }
            return null;
        };
    }
    /* the data format

    room_id: ''
    operating_days {
                    index_for_source_to_copy_day_details:-1,

                    is_available:true,
                    is_on_request:false,

                    week_day_name: this.calendar.weekDays()[week_day_index],
                    week_day_index,

                    time_slots:[
                        {
                            start_time:'',
                            end_time:'',
                            allocation: '',
                            is_available: true,
                            is_on_request: false,
                            adult_price:'',
                            child_price:'',
                            infant_price:''
                        }
                    ],
                    allocation:0
    }
    */
    public function getDayDetails($date)
    {
        $week_day_index = $date->dayOfWeek;

        //$allSlotsTotalAllocationsForDay = 0;

        foreach ($this->weekDaysPricingDetails['operating_days'] as  $dayDetails) {
            if($dayDetails['week_day_index'] == $week_day_index) {
                $dayDetails['currency'] = $this->weekDaysPricingDetails['currency'];
                $dayDetails['date'] = $date->toDateString();
                $dayDetails['room_id'] = $this->weekDaysPricingDetails['room_id'];


                return $dayDetails;
            }
        }

        return null;
    }
}