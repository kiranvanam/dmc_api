<?php

namespace Columbus\Contracts\Hotels\Http\Controllers;

use App\Http\Controllers\ApiController;
use Columbus\Contracts\Hotels\Models\Hotel;
use Columbus\Contracts\Hotels\Http\Requests\HotelRequest;

class HotelController extends ApiController
{
    function index()
    {
        $city_id = request('city_id');
        $vendor_id = request('vendor_id');

        $hotels = Hotel::where('city_id', $city_id)
                    ->where('vendor_id', $vendor_id)
                    ->with('rooms')
                    ->get();
        return $this->ok($hotels, "Hotels for selected Vendor");
    }


    public function create(HotelRequest $request)
    {
        return $this->ok( $request->save(), "Hotel created");
    }

    public function update( $hotel_id, HotelRequest $request)
    {
        return $this->ok( $request->update($hotel_id), "Hotel details updated");
    }

    public function updateThumbnail($hotel_id)
    {
        $thumbnail = request('thumbnail');
        $hotel = Hotel::findOrFail($hotel_id);

        $hotel->update(['thumbnail'=> $thumbnail]);

        return $this->ok($hotel, "Thumbnail updated");
    }

    public function updateFilters($hotel_id)
    {
        $filters = request('filters');
        $hotel = Hotel::findOrFail($hotel_id)->update(['filters'=> "filters"]);
    }

}