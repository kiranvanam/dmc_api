<?php

namespace Columbus\Contracts\Hotels\Http\Controllers;

use App\Http\Controllers\ApiController;
use Columbus\Contracts\Hotels\Models\Room;
use Columbus\Contracts\Hotels\Http\Requests\RoomRequest;

class RoomController extends ApiController
{
    function index($hotel_id)
    {
        $hotel_id = $hotel_id;
        
        $hotels = Room::where('hotel_id', $hotel_id)->get();
        return $this->ok($hotels, "Room for Hotel");
    }


    public function create($hotel_id, RoomRequest $request)
    {
        return $this->ok( $request->save(), "Room created"); 
    }

    public function update( $hotel_id, $room_id, RoomRequest $request)
    {
        return $this->ok( $request->update($room_id), "Room details updated");
    }

    public function updateThumbnail($hotel_id, $room_id)
    {
        $thumbnail = request('thumbnail');
        $room = Room::findOrFail($room_id);

        $room->update(['thumbnail'=> $thumbnail]);
        
        return $this->ok($room, "Thumbnail updated");
    }

}