<?php

namespace Columbus\Contracts\Hotels\Http\Requests;

use Columbus\SystemSettings\Models\Code;
use Columbus\Contracts\Hotels\Models\Hotel;
use Illuminate\Foundation\Http\FormRequest;

class HotelRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }


    public function rules()
    {
        return [
            'city_id' => 'required',
            'vendor_id' => 'required',
            'vendor_code' => 'required',
            'name' => 'required',
            'category'=> 'required',
            'check_in' => 'required',
            'check_out'=> 'required'

        ];
    }

    protected function prepareForValidation()
    {
    }


    public function messages()
    {
        return [
            'city_id.required' => "City is not selected",
            'vendor_id.required' => 'Vendor must be selected',
            'name.required' => "Ticket title/name must be provided",
            'category' => 'Category must be choosen',
            'check_in' => 'Check-in time must be provided',
            'check_out' => 'Check-out time must be provided'
        ];
    }

    public function save()
    {
        $data = $this->all();
        $data['code'] = Code::getNextCodeForModule("accommodation");
        $hotel = Hotel::create($data);
        return $hotel;
    }

    public function update($hotel_id)
    {
        $data = $this->all();
        unset($data['code']);
        logger($data);
        $hotel = Hotel::findOrFail($hotel_id);
        $hotel->update($data);
        return $hotel;
    }
}