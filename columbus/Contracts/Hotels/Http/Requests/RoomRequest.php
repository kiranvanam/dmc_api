<?php

namespace Columbus\Contracts\Hotels\Http\Requests;

use Columbus\SystemSettings\Models\Code;
use Columbus\Contracts\Hotels\Models\Room;
use Illuminate\Foundation\Http\FormRequest;

class RoomRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }


    public function rules()
    {
        return [
            'hotel_id' => 'required',
            'name' => 'required',
            //'code' => 'required',
            'meal_plans' => 'required',
            'total_pax_without_extra_beds' => 'required',
            'total_extra_beds' => 'required'
        ];
    }

    protected function prepareForValidation()
    {
    }


    public function messages()
    {
        return [
            'hotel_id.required' => "Hotel is not selected",
            'name.required' => "Ticket title/name must be provided",
            'meal_plans.required' => 'Please select Meal plans available for this room.',
            'total_pax_without_extra_beds.required'=> 'Please Provide Total Pax withour Extra beds',
            'total_extra_beds.required' => "Please Provide Total exta beds"
        ];
    }

    public function save()
    {
        $data = $this->all();
        //$data['code'] = Code::getNextCodeForModule("accommodation");
        $hotel = Room::create($data);
        return $hotel;
    }

    public function update($room_id)
    {
        $data = $this->all();
        $room = Room::findOrFail($room_id);
        $room->update($data);
        return $room;
    }
}