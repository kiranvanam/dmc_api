<?php

namespace Columbus\Contracts\Hotels\Models;

use App\CancellationPolicy;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Hotel extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $fillable = ['city_id', 'vendor_id', 'vendor_code',
                            'name', 'code', 'category', 'child_min_age', 'child_max_age', 'status',
                            'check_in', 'check_out',
                            'thumbnail', 'description',
                            'filters'
                        ];

    protected $casts = [
        "filters" => "array"
    ];

    public function rooms()
    {
        return $this->hasMany( Room::class, 'hotel_id');
    }

    function cancellationPolicies()
    {
        return $this->hasOne(CancellationPolicy::class, 'service_id')->where('service', 'hotels');
    }
}