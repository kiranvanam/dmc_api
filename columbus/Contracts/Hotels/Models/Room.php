<?php

namespace Columbus\Contracts\Hotels\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Room extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    public $table = "hotel_room_details";

    protected $fillable = ['hotel_id', 'name', 'status',
                            'total_pax_without_extra_beds', 'total_extra_beds',
                            'meal_plans', 'occupancy_types',
                            'description',
                        ];

    protected $casts = [
        "meal_plans" => "array",
        "occupancy_types" => "array"
    ];

    function pricing()
    {
        return $this->hasMany(RoomPricing::class, 'room_id');
    }
}