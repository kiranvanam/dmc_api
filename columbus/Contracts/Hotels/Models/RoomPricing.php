<?php

namespace Columbus\Contracts\Hotels\Models;

use Illuminate\Database\Eloquent\Model;

class RoomPricing extends Model
{
    public $table = "hotel_room_pricing";

    protected $fillable = [ "room_id", "date",
                "pricing", "currency",
                'inventory', 'utilized',
                'is_available', 'is_on_request'
            ];

    protected $casts = [
        "pricing" => 'array'
    ];
}