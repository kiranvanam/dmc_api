<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenameTotalAlllowedPaxColumnNamesInRoomsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('hotel_room_details', function (Blueprint $table) {
            if(Schema::hasColumn('hotel_room_details', 'total_adults_allowed'))
                $table->renameColumn('total_adults_allowed', 'total_pax_without_extra_beds');

            if(Schema::hasColumn('hotel_room_details', 'total_children_allowed'))
                $table->renameColumn('total_children_allowed', 'total_extra_beds');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('hotel_room_details', function (Blueprint $table) {
        });
    }
}
