<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHotelRoomDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hotel_room_details', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('hotel_id')->index();
            $table->string('name');
            $table->string('code',30)->nullable();
            $table->string('area', 100);
            $table->integer('no_of_persons_allowed');
            $table->integer('no_of_extra_beds_allowed');
            $table->string('meal_plans');
            $table->string('description', 2000);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hotel_room_details');
    }
}
