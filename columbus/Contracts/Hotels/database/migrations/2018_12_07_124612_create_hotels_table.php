<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHotelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hotels', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('city_id');
            $table->integer('vendor_id');
            $table->string('vendor_code', 25);
            $table->string('name');
            $table->string('code');
            $table->string('category', 10);
            $table->string('status', 50)->nullable();
            $table->string('check_in', 20);
            $table->string('check_out', 20);
            $table->string('thumbnail')->nullable();
            $table->string('description', 2000)->nullable();
            $table->string('filters', 1000)->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hotels');
    }
}
