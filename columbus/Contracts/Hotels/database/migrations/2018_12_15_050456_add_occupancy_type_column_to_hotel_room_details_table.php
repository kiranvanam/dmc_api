<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddOccupancyTypeColumnToHotelRoomDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('hotel_room_details', function (Blueprint $table) {
            $table->string('occupancy_types', 50)->nullable()->after('meal_plans');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('hotel_room_details', function (Blueprint $table) {
            $table->dropColumn('occupancy_types');
        });
    }
}
