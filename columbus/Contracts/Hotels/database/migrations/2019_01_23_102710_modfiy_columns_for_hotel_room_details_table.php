<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModfiyColumnsForHotelRoomDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('hotel_room_details', function (Blueprint $table) {
            $table->string('occupancy_types', 100)->default()->change();

            if( Schema::hasColumn('hotel_room_details', 'no_of_persons_allowed')) {
                $table->renameColumn('no_of_persons_allowed', 'total_adults_allowed');
            }
            if( Schema::hasColumn('hotel_room_details', 'no_of_extra_beds_allowed')) {
                $table->renameColumn('no_of_extra_beds_allowed', 'total_children_allowed');
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('hotel_room_details', function (Blueprint $table) {
            //
        });
    }
}
