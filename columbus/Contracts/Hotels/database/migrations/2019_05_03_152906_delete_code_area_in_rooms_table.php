<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DeleteCodeAreaInRoomsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('hotel_room_details', function (Blueprint $table) {
            if(Schema::hasColumn('hotel_room_details', 'code'))
                $table->dropColumn('code');

            if(Schema::hasColumn('hotel_room_details', 'area'))
                $table->dropColumn('area');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('hotel_room_details', function (Blueprint $table) {
            //
        });
    }
}
