<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHotelRoomPricingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hotel_room_pricing', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('room_id')->index();
            $table->date('date');
            $table->string('pricing', 500);
            $table->string('currency', 6);
            $table->integer('inventory');
            $table->integer('utilized')->default(0);
            $table->boolean('is_available');
            $table->boolean('is_on_request');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hotel_room_pricing');
    }
}
