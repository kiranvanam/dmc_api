<?php

namespace Columbus\Contracts\Visas\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class VisaAllotmentCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'visa_id' => 'required',
            'vendor_id' => 'required',
            'quantity' => 'required',
        ];
    }

    protected function prepareForValidation()
    {
    }

    /**
     * customizing error messages for (field,rule)
     */
    public function messages()
    {
        return [
            'visa_id.required' => "Visa ID Is Incorrectly Specified",
            'vendor_id.required' => "Invalid Vendor Selection",
            'quantity.required' => "Quantity must be provided",
        ];
    }

}
