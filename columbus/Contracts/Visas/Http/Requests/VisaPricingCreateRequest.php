<?php

namespace Columbus\Contracts\Visas\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class VisaPricingCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'vendor_id' => 'required',
            'vendor_code' => 'required',
            'adult_price' => 'required',
            'child_price' => "required|numeric",
            "infant_price" => "required|numeric",
            'inventory' => 'required|numeric',
            //'utilized' => 'required',
            'visa_id' => 'required',
        ];
    }

    protected function prepareForValidation()
    {
    }

    /**
     * customizing error messages for (field,rule)
     */
    public function messages()
    {
        return [
            'vendor_id.required' => "Please Select A Vendor Before Adding The Price",
            'vendor_code.required' => "Please select A Vendor Before adding the price",
            'adult_price.required' => "Please Provide Adult Price",
            'child_price.required' => "Please Provide Child Price",
            'infant_price.required' => "Please Provide Infant price",
            'inventory.required' => "Please provide inventory details",
            //'utilized.required' => "Please specify utilized field",
            'visa_id.required' => "Invalid visa selection",
        ];
    }

}
