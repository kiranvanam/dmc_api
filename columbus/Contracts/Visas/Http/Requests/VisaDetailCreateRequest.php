<?php

namespace Columbus\Contracts\Visas\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class VisaDetailCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'min_days' => 'required',
            'max_days' => 'required',
            'processing_min_days' => 'required',
            'processing_max_days' => 'required',
            'validity' => 'required',
            'validity_type' => 'required'
        ];
    }

    protected function prepareForValidation()
    {
    }

    /**
     * customizing error messages for (field,rule)
     */
    public function messages()
    {
        return [
            'min_days.required' => "Please specify min days required to submit the visa",
            'max_days.required' => "Please specify max days required to submit the visa",
            'processing_min_days.required' => "Please specify the min time required to process the visa",
            'processing_max_days.required' => "Please specify the max time required to process the visa",
            'validity.required' => "Please specify the visa validity period",
            'validity_type.required' => "Please specify the visa validity type"
        ];
    }

}
