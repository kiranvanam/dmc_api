<?php

namespace Columbus\Contracts\Visas\Http\Controllers;

use App\Http\Controllers\ApiController;
use Columbus\Contracts\Visas\Models\Visa;
use Columbus\Contracts\Visas\Models\VisaPricing;

class VisaSearchController extends ApiController
{
    public function search()
    {
        //\DB::enableQueryLog();

        $travel_date = request('travel_date');
        $visa_type_id = request('visa_type_id');
        $visa_id = request('visa_id');
        //visa name, pricing, details
        $visas = Visa::where('visa_type_id', $visa_type_id)
                    ->with(['details' => function($query) use($visa_id){
                        if(!empty($visa_id))
                            $query->where('visa_id', $visa_id);
                    }])
                    ->with(['pricing' => function($query) use($travel_date) {
                        $query->where('date', $travel_date)
                              ->orderBy('adult_price');
                    }])
                    ->get();

        //logger( \DB::getQueryLog());
        return $this->ok($visas, $travel_date);
    }

    public function detailsByVisaType($visa_type_id)
    {
        return $this->ok(Visa::with('details')->get(), "Fetching Visa is Successful");

    }
}