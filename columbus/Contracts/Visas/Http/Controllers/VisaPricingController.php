<?php

namespace Columbus\Contracts\Visas\Http\Controllers;

use Auth;
use Illuminate\Http\Request;
use Columbus\Utilities\DateUtil;
use App\Http\Controllers\ApiController;
use Columbus\Contracts\Visas\Models\VisaPricing;
use Columbus\Contracts\Visas\Http\Requests\VisaPricingCreateRequest;

class VisaPricingController extends ApiController
{
    protected $weekDaysPricingDetails = [];

    public function all($visa_id, $vendor_id, $month, $year, Request $request) {
        Auth::user()->hasPermission('vendors');
        $fromDate = $year . '-' . $month . '-' . '1';
        $toDate = $year . '-' . $month . '-' . '31';
        $records = VisaPricing::where([
                    ['visa_id', '=', $visa_id],
                    ['vendor_id', '=', $vendor_id],
                ])->whereBetween("date", [$fromDate, $toDate])
                ->orderBy('date', 'asc')
                ->get();
        return $this->ok(
            $records,
            "Fetching Visa Pricings is Successful"
        );
    }

    public function update(VisaPricingCreateRequest $request)
    {

        $this->weekDaysPricingDetails = $request->all();

        $dates = DateUtil::datesForRange(
                                $request->get("from_date"),
                                $request->get('to_date'),
                                $this->callBackFunction()
                            );
        return $this->ok($dates , "Visa Pricing details updated Successfully");
    }

    public function callBackFunction()
    {
        return function($date) {

            $dayDetails = $this->getDayDetails($date);

            if(empty($dayDetails) == false) {
                $routePricing = VisaPricing::where('visa_id', $dayDetails['visa_id'])
                                    ->where('vendor_id', $dayDetails['vendor_id'])
                                    ->where('date', $dayDetails['date'])
                                    ->first();

                if(empty($routePricing)) {
                   $routePricing = VisaPricing::create($dayDetails);
                }else {
                    $routePricing->update($dayDetails);
                }
                return $routePricing;
            }

            return null;
        };
    }

    public function getDayDetails($date)
    {
        $week_day_index = $date->dayOfWeek;
        $request = $this->weekDaysPricingDetails;
        //$allSlotsTotalAllocationsForDay = 0;
        return [
                'date' => $date->toDateString(),
                'visa_id' => $request['visa_id'],
                'vendor_id' => $request['vendor_id'],
                'vendor_code' => $request['vendor_code'],
                'inventory' => $request['inventory'],
                'adult_price' => $request['adult_price'],
                'child_price' => $request['child_price'],
                'infant_price' => $request['infant_price'],
                //'utilized' => $request['utilized'],
                'child_details' => $request['child_details'],
                'currency' => $request['currency']
            ];
        /* foreach ($this->weekDaysPricingDetails['operating_days'] as  $dayDetails) {
             if($dayDetails['week_day_index'] == $week_day_index) {
                $dayDetails['currency'] = $this->weekDaysPricingDetails['currency'];
                $dayDetails['date'] = $date->toDateString();
                $dayDetails['transfer_route_id'] = $this->weekDaysPricingDetails['transfer_route_id'];
                $dayDetails['vehicle_id'] = $this->weekDaysPricingDetails['vehicle_id'];

                //$dayDetails['time_slots_with_pricing_details'];

                //$dayData['allocation'] = $dayDetails['allocation'];

                //$dayDetails['is_available'] = true;
                //$dayDetails['is_on_request'] = ;

                return $dayDetails;
             }
        } */

        return null;
    }
}
