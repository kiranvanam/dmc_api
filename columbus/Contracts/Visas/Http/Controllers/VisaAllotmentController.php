<?php

namespace Columbus\Contracts\Visas\Http\Controllers;

use Auth;
use App\Http\Controllers\ApiController;
use Columbus\Contracts\Visas\Http\Requests\VisaAllotmentCreateRequest;
use Columbus\Contracts\Visas\Models\VisaAllotment;
use Illuminate\Http\Request;

class VisaAllotmentController extends ApiController
{
    public function save(VisaAllotmentCreateRequest $request)
    {
        return $this->ok(VisaAllotment::create($request->all()), "Visa Allotment details added successfully");
    }

    public function update($id, VisaAllotmentCreateRequest $request)
    {
        $data = $request->all();
        $visaAllotment = VisaAllotment::find($id);
        $visaAllotment->update($data);

        return $this->ok($visaAllotment, "Visa Allotment details Updated successfully");
    }

    public function delete($id)
    {
        $visaAllotment = VisaAllotment::find($id);
        $visaAllotment->delete();
        return $this->ok($visaAllotment, "Visa Allotment deleted successfully");
    }
}
