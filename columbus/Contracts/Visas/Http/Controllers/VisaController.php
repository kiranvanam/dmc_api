<?php

namespace Columbus\Contracts\Visas\Http\Controllers;

use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\ApiController;
use Columbus\SystemSettings\Models\Code;
use Columbus\Contracts\Visas\Models\Visa;
use Columbus\Contracts\Visas\Models\VisaType;
use Columbus\Contracts\Visas\Http\Requests\VisaCreateRequest;

class VisaController extends ApiController
{
    public function all() {
        //Auth::user()->hasPermission('vendors');
        $visa_type_id = request('visa_type_id');
        $query = Visa::query();

        if(!empty($visa_type_id))
            $query->where('visa_type_id', $visa_type_id);
        
        return $this->ok($query->with('details')->get(), "Fetching Visa is Successful");
    }

    public function save(VisaCreateRequest $request)
    {
        $data = $request->all();
        $data['code']= Code::getNextCodeForModule("visas");
        return $this->ok(Visa::create($data));
    }

    public function update($visa_id, VisaCreateRequest $request)
    {
        $data = $request->all();
        $visa = Visa::find($visa_id);

        if( empty($visa->code) ) {
            $data['code']= Code::getNextCodeForModule("visas");
        }

        $visa->update($data);

        return $this->ok($visa, "Visa details Updated successfully");
    }

    public function delete($visa_id)
    {
        $visa = Visa::find($visa_id);
        $visa->delete();
        return $this->ok($visa, "Visa deleted successfully");
    }

    public function updateEligibility(Request $request)
    {
        $visa_id = $request->get('visa_id');
        DB::table('country_visa')->where([
            ['visa_id', '=', $visa_id],
            ['eligibility_type', '=', $request->eligibility_type]
        ])->delete();

        $data = array();
        foreach($request->country_ids as $country_id) {
            array_push($data, ['country_id' => $country_id, 'visa_id' => $visa_id, 'eligibility_type' => $request->eligibility_type]);
        }

        return $this->ok(DB::table('country_visa')->insert($data));
    }

    public function getEligibility($visa_id, Request $request)
    {
        return DB::table('country_visa')->where('visa_id', $visa_id)->get();
    }
}
