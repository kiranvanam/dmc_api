<?php

namespace Columbus\Contracts\Visas\Http\Controllers;

use Auth;
use App\Http\Controllers\ApiController;
use Columbus\Contracts\Visas\Models\VisaCountryInfo;
use Illuminate\Http\Request;

class VisaCountryInfoController extends ApiController
{
    public function all() {
        Auth::user()->hasPermission('vendors');
        return $this->ok(VisaSubType::all(), "Fetching Visa is Successful");
    }

    public function save(VisaSubTypeCreateRequest $request)
    {
        return $this->ok(VisaSubType::create($request->all()));
    }

    public function update($visasubtype_id, VisaSubTypeCreateRequest $request)
    {
        $data = $request->all();
        $visasubtype = VisaSubType::find($visasubtype_id);
        $visasubtype->update($data);

        return $this->ok($visasubtype, "Visa details Updated successfully");
    }

    public function delete($visasubtype_id)
    {
        $visasubtype = VisaSubType::find($visasubtype_id);
        $visasubtype->delete();
        return $this->ok($visasubtype, "Visa deleted successfully");
    }
}
