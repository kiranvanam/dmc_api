<?php

namespace Columbus\Contracts\Visas\Http\Controllers;

use Auth;
use App\Http\Controllers\ApiController;
use Columbus\Contracts\Visas\Http\Requests\VisaDetailCreateRequest;
use Columbus\Contracts\Visas\Models\VisaDetail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class VisaDetailController extends ApiController
{
    public function save(VisaDetailCreateRequest $request)
    {
        return $this->ok(VisaDetail::create($request->all()));
    }

    public function update($id, VisaDetailCreateRequest $request)
    {
        $data = $request->all();
        $visaDetail = VisaDetail::find($id);
        $visaDetail->update($data);
        return $this->ok($visaDetail, "Visa Details Updated successfully");
    }
}
