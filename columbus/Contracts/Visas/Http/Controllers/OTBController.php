<?php

namespace Columbus\Contracts\Visas\Http\Controllers;

use App\Http\Controllers\ApiController;
use Columbus\Contracts\Visas\Models\OTB;

class OTBController extends ApiController
{
    
    function index()
    {
        return $this->ok(  OTB::all(), "Otb detils");
    }

    function create()
    {
        $data = request(['country_id', 'airlines_name', 'adult_price', 'child_price', 'currency', 'remarks']);
        return $this->ok( OTB::create($data), "OTB details created");
    }

    function update($otb_id)
    {
        $data = request(['country_id', 'airlines_name', 'adult_price', 'child_price', 'currency', 'remarks']);
        $otb = OTB::findOrFail($otb_id);
        $otb->update($data);
        return $this->ok($otb, "OTB details updated");
    }

    function delete($otb_id)
    {
        $otb = OTB::findOrFail($otb_id);
        $otb->delete();
        return $this->ok($otb, "OTB deleted");
    }
}