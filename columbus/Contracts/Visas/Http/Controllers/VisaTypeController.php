<?php

namespace Columbus\Contracts\Visas\Http\Controllers;

use Auth;
use App\Http\Controllers\ApiController;
use Columbus\Contracts\Visas\Http\Requests\VisaTypeCreateRequest;
use Columbus\Contracts\Visas\Models\VisaType;
use Illuminate\Http\Request;

class VisaTypeController extends ApiController
{
    public function all() {        
        return $this->ok(VisaType::all(), "Fetching Visa Types is Successful");
    }

    public function save(VisaTypeCreateRequest $request)
    {
        return $this->ok(VisaType::create($request->all()));
    }

    public function update($visatype_id, VisaTypeCreateRequest $request)
    {
        $data = $request->all();
        $visatype = VisaType::find($visatype_id);
        $visatype->update($data);

        return $this->ok($visatype, "Visa Type details Updated  successfully");
    }

    public function delete($visatype_id)
    {
        $visatype = VisaType::find($visatype_id);
        $visatype->delete();
        return $this->ok($visatype, "Visa Type deleted successfully");
    }
}
