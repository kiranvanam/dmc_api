<?php

namespace Columbus\Contracts\Visas\Models;

use Illuminate\Database\Eloquent\Model;

class VisaType extends Model
{
    public $timestamps = false;
    protected $fillable = ['name', 'slug', 'country_id'];
}
