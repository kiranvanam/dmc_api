<?php

namespace Columbus\Contracts\Visas\Models;

use Columbus\Destination\Models\Country;
use Columbus\Contracts\Visas\Models\VisaPricing;
use Columbus\Contracts\Visas\Models\VisaDetail;
use Columbus\Contracts\Visas\Models\VisaAllotment;
use Illuminate\Database\Eloquent\Model;

class Visa extends Model
{
    public $timestamps = false;
    protected $fillable = ['country_id', 'visa_type_id', 'name', 'slug', 'code'];

    public function countries() {
        return $this->belongsToMany(Country::class, 'country_visa', 'visa_id', 'country_id')->withPivot('eligibility_type');
    }

    public function details() {
        return $this->hasOne(VisaDetail::class);
    }
    
    public function pricing()
    {
        return $this->hasMany(VisaPricing::class);
    }
}
