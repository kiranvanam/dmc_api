<?php

namespace Columbus\Contracts\Visas\Models;

use Illuminate\Database\Eloquent\Model;

class OTB extends Model
{
    public $table = "otb_details";

    protected $fillable = ['country_id', 'airlines_name', 'adult_price', 'child_price', 'currency', 'remarks'];
}