<?php

namespace Columbus\Contracts\Visas\Models;

use Illuminate\Database\Eloquent\Model;

class VisaPricing extends Model
{
    public $timestamps = false;
    protected $fillable = [ 'visa_id','date', 'vendor_id', 'vendor_code', 'country_id',
                            'inventory', 'utilized', 'adult_price', 'child_price', 'infant_price', 'child_details', 'currency'];

    protected $dates = ['date'];

    protected $casts = [
       'child_details' => 'array',
   ];

}
