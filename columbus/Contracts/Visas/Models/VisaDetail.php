<?php

namespace Columbus\Contracts\Visas\Models;

use Illuminate\Database\Eloquent\Model;

class VisaDetail extends Model
{
    public $timestamps = false;
    protected $fillable = ['visa_id', 'min_days', 'max_days', 'processing_min_days', 'processing_max_days',
                    'validity', 'validity_type', 'description',  'docs_required', 'terms_and_conditions', 'remarks'];
    
}
