<?php

namespace Columbus\Contracts\Visas\Models;

use Illuminate\Database\Eloquent\Model;

class VisaCountryInfo extends Model
{
    public $timestamps = false;
    protected $fillable = ['country_id', 'visa_id', 'eligibility_type'];
}
