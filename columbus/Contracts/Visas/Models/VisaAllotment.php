<?php

namespace Columbus\Contracts\Visas\Models;

use Illuminate\Database\Eloquent\Model;

class VisaAllotment extends Model
{
    public $timestamps = false;
    protected $fillable = ['visa_id', 'vendor_id', 'quantity'];
}
