<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddInfantPriceToVisaPricingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('visa_pricings', function (Blueprint $table) {
            $table->decimal('child_price', 8,2)->default(0)->after('adult_price');
            $table->decimal('infant_price', 8,2)->default(0)->after('child_price');
            $table->decimal('adult_price')->change();
            $table->renameColumn('quantity', 'inventory');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('visa_pricings', function (Blueprint $table) {
            $table->dropColumn('child_price');
            $table->dropColumn('infant_price');
        });
    }
}
