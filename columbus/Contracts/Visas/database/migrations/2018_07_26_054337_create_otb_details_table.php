<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOtbDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('otb_details', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('country_id');
            $table->string('airlines_name');
            $table->decimal('adult_price');
            $table->decimal('child_price');
            $table->string('currency',10);
            $table->text('remarks')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('otb_details');
    }
}
