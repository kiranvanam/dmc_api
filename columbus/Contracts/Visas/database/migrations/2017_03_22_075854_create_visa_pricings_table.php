<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVisaPricingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('visa_pricings', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('visa_id')->index();
            $table->integer('vendor_id');
            $table->float('adult_price', 8, 2);
            $table->json('child_details');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('visa_pricings');
    }
}
