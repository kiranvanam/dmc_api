<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVisaDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('visa_details', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('visa_id')->unique();
            $table->integer('min_days');
            $table->integer('max_days');
            $table->integer('processing_min_days');
            $table->integer('processing_max_days');
            $table->integer('validity');
            $table->string('validity_type');
            $table->text('docs_required')->nullable();
            $table->text('tcs')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('visa_details');
    }
}
