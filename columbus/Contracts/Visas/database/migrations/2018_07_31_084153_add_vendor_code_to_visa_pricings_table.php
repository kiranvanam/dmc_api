<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddVendorCodeToVisaPricingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('visa_pricings', function (Blueprint $table) {
            $table->string('vendor_code', 50)->nullable()->after('vendor_id');
        });
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('visa_pricings', function (Blueprint $table) {
            $table->dropColumn('vendor_code');
        });
    }
}
