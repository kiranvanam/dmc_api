<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRemarksToVisaDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('visa_details', function (Blueprint $table) {
            $table->text('remarks')->nullable();
            $table->renameColumn('tcs', 'terms_and_conditions');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('visa_details', function (Blueprint $table) {
            $table->dropColumn('remarks');            
        });
    }
}
