<?php

Route::group(['middleware'=>['auth:api','cors']], function(){

    Route::post("api/search/visas", 'VisaSearchController@search');

    Route::group(['prefix'=>'/api/visas'] , function() {

        Route::get('/', 'VisaController@all');
        Route::get('/eligibility/{id}', 'VisaController@getEligibility');
        Route::post('/', 'VisaController@save');
        Route::post('eligibility', 'VisaController@updateEligibility');
        Route::put('/{id}', 'VisaController@update');
        Route::delete('/{id}', 'VisaController@delete');

        Route::group(['prefix' => 'type'], function () {
            Route::get('/', 'VisaTypeController@all');
            Route::post('/', 'VisaTypeController@save');
            Route::put('/{id}', 'VisaTypeController@update');
            Route::delete('/{id}', 'VisaTypeController@delete');
        });

        Route::group(['prefix' => 'pricing'], function() {
            Route::get('/{visa_id}/vendor/{vendor_id}/month/{month}/year/{year}', 'VisaPricingController@all');
            //Route::post('/', 'VisaPricingController@save');
            Route::post('/', 'VisaPricingController@update');
        });

        Route::group(['prefix' => 'details'], function() {
            Route::post('/', 'VisaDetailController@save');
            Route::put('/{id}', 'VisaDetailController@update');
        });

        Route::group(['prefix' => 'allotment'], function() {
            Route::post('/', 'VisaAllotmentController@save');
            Route::put('/{id}', 'VisaAllotmentController@update');
            Route::delete('/{id}', 'VisaAllotmentController@delete');
        });
    });

    Route::group(['prefix' => "api/otb"], function() {
        Route::get("", 'OTBController@index');
        Route::post("", 'OTBController@create');
        Route::put("{otb_id}", 'OTBController@update');
        Route::delete("{otb_id}", "OTBController@delete");
    });
});

