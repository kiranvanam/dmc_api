<?php

namespace Columbus\Contracts\Visas\Providers;

use Illuminate\Support\Facades\Route;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class VisasServiceProvider extends ServiceProvider
{
    protected $namespace = "Columbus\Contracts\Visas\Http\Controllers";

    public function boot()
    {
        $this->app->router->group(['namespace' => $this->namespace], function(){
            if (! $this->app->routesAreCached()) {
                require __DIR__.'/../routes.php';
            }
        });
        $this->loadViewsFrom(__DIR__.'/../resources/views', 'acl');

        $this->loadMigrationsFrom(__DIR__.'/../database/migrations');
    }
}
