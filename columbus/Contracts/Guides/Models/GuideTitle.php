<?php

namespace Columbus\Contracts\Guides\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Columbus\Contracts\Guides\Models\GuideTitleVendorDetail;

class GuideTitle extends Model
{
    use SoftDeletes;

    protected $fillable = ['country_id', 'city_id', 'name', 'code', 'is_available'];

    protected $dates = ['deleted_at'];


    public static function isDuplicate($data)
    {        
        // check if <tour,type> is created 
        $tourTitle = GuideTitle::withTrashed()
                    ->where('name', $data['name'])
                    ->where('city_id', $data['city_id']);

        if( isset($data['id'])) 
            $tourTitle = $tourTitle->where('id', '!=', $data['id']);
            
        $tourTitle = $tourTitle->first();
        
        if( empty($tourTitle))
        return null;
        
        logger("Trying to create duplicate Guide: ", $data);

        return  [ 'Guide Duplicaiton' => 
                    $data['name'] . " Guide with this name is already created for this city"
                ];            

    }

    public function vendors()
    {
        return $this->hasMany(GuideTitleVendorDetail::class, 'guide_title_id')
                    ->with('pricing')
                    ->with('cancellationPolicies');
    }
    public function vendor()
    {
        return $this->hasOne(GuideTitleVendorDetail::class, 'guide_title_id');
    }

}