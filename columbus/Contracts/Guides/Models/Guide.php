<?php
namespace Columbus\Contracts\Guides\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Guide extends Model
{
    protected $table = "vendor_guides";

    use SoftDeletes;
    protected $dates = ['deleted_at'];

    protected $fillable = ['city_id', 'vendor_id','name', 'slug', 'code', 'language_ids', 'is_active', 'pricing'];

    protected $casts = [
        "language_ids" => "array",
        "pricing" => "array"
    ];
}
