<?php

namespace Columbus\Contracts\Guides\Models;

use App\CancellationPolicy;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Columbus\Contracts\Guides\Models\GuideTitleVendorDetailPricing;

class GuideTitleVendorDetail extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $fillable = ['guide_title_id', 'vendor_id', 'vendor_code', 
                          'itinerary', 'inclusions', 'exclusions', 'terms_and_conditions', 'remarks',
                         'filters', 'gallery'
                        ];
    protected $casts = [
        'filters' => 'array', 
        'gallery' => 'array'
    ];

    public function pricing()
    {
        return $this->hasOne(GuideTitleVendorDetailPricing::class, 'guide_title_vendor_detail_id');
    }

    function cancellationPolicies()
    {
        return $this->hasOne(CancellationPolicy::class, 'service_id')->where('service', 'guide-vendor');
    }
}