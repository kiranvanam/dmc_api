<?php

namespace Columbus\Contracts\Guides\Models;

use Illuminate\Database\Eloquent\Model;


class GuideTitleAvailability extends Model
{
    protected $table = "vendor_guide_title_availability";

    public $timestamps = false;

    protected $fillable = ['city_id', 'vendor_id' , 'guide_title_id', 'date', 'allocation', 'price', 'is_available', 'is_on_request'];

    protected $dates = ['date'];

}
