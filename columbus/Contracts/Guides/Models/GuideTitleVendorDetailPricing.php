<?php

namespace Columbus\Contracts\Guides\Models;

use Illuminate\Database\Eloquent\Model;

class GuideTitleVendorDetailPricing extends Model
{
    public $table = "guide_title_vendor_detail_pricing";
    
    public $timestamps = false;
        
    protected $dates = ['date'];
    
    
    protected $fillable = ['guide_title_vendor_detail_id', 
                            'date',
                            'inventory', 'utilized', 
                            'price', 'duration', 'duration_type', 
                            'extra_charge', 'extra_duration', 'extra_duration_type',
                            'currency',
                            'is_available', 'is_on_request', 
                        ];
}