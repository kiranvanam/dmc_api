<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGuideTitleVendorDetailPricingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('guide_title_vendor_detail_pricing', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('guide_title_vendor_detail_id');
            $table->date('date');
            $table->decimal('price', 8,2);
            $table->string('currency',6);
            $table->integer('duration');
            $table->string('duration_type', 10);
            $table->decimal('extra_charge', 8,2);
            $table->integer('extra_duration')->nullable();        
            $table->string('extra_duration_type', 10)->nullable();
            $table->integer('inventory');
            $table->integer('utilized')->default(0);
            $table->boolean('is_available');
            $table->boolean('is_on_request');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('guide_title_vendor_detail_pricing');
    }
}
