<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVendorGuideTitleAvailabilityTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vendor_guide_title_availability', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('city_id')->index();
            $table->integer('vendor_id')->index();
            $table->integer('guide_title_id')->index();
            $table->date('date')->index();
            $table->integer('allocation');
            $table->decimal('price');
            $table->boolean('is_available')->default(false);
            $table->boolean('is_on_request')->default(false);

            $table->index(['vendor_id', 'guide_title_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vendor_guide_title_availability');
    }
}
