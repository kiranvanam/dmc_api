<?php

namespace Columbus\Contracts\Guides\Http\Controllers;

use App\Http\Controllers\ApiController;
use Illuminate\Database\Eloquent\SoftDeletes;
use Columbus\Contracts\Guides\Models\GuideTitleVendorDetail;

class GuideTitleVendorDetailController extends ApiController
{

    public function index($vendor_id)
    {
        $vendorGuidesList = GuideTitleVendorDetail::where('vendor_id', $vendor_id)->get();

        return $this->ok($vendorGuidesList, "Vendor Guides list");
    }

    public function create($vendor_id)
    {
        $vendorGuideDetails = GuideTitleVendorDetail::create(request()->all());

        return $this->ok($vendorGuideDetails, "Guide Details created");
    }

    public function update($vendor_id, $Guide_title_vendor_detail_id)
    {
        $vendorGuideDetails = GuideTitleVendorDetail::findOrFail($Guide_title_vendor_detail_id);

        $vendorGuideDetails->update(request()->all());

        return $this->ok($vendorGuideDetails, "Guide details updated");
    }

    public function updateFilters( $vendor_id, $Guide_title_vendor_detail_id)
    {
        $vendorGuideDetails = GuideTitleVendorDetail::findOrFail($Guide_title_vendor_detail_id);

        $data = [];
        $data['filters'] = request('filters');

        $vendorGuideDetails->update($data);

        return $this->ok($vendorGuideDetails, "Guide Filters updated");
    }

}