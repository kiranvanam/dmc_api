<?php

namespace Columbus\Contracts\Guides\Http\Controllers;

use App\Http\Controllers\ApiController;
use Columbus\Contracts\Guides\Models\Guide;
use Illuminate\Http\Request;


class GuideController extends ApiController
{
    
    function guidesForVendor()
    {
        return $this->ok(Guide::where('vendor_id', request()->get('vendor_id'))->get(), 
                        "Guides available for this vendor"
                        );
    }

    public function create(Request $request)
    {
        $data = $this->addSlug($request);
        return $this->ok(Guide::create($data), "Guide Created");
    }

    public function update($guide_id, Request $request)
    {
        $guide = Guide::find($guide_id);

        if($guide) {
            $data = $this->addSlug($request);
            $guide->update($data);
        }

        return $this->ok($guide, "Guide details updated");
    }

    public function delete($guide_id)
    {
        $guide = Guide::find($guide_id);

        if($guide) {
           $guide->delete(); 
        }

        return $this->ok($guide, "Guide deleted");
     }

     public function addSlug(Request $request)
     {
        $data = $request->all();
        $data['slug'] = str_slug($data['name']);

        return $data;

     }
}