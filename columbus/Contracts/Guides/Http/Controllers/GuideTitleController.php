<?php

namespace Columbus\Contracts\Guides\Http\Controllers;

use App\Http\Controllers\ApiController;
use Columbus\Contracts\Guides\Models\GuideTitle;
use Columbus\Contracts\Guides\Http\Requests\GuideTitleRequest;

class GuideTitleController extends ApiController
{
    function index()
    {
        $city_id = request()->get('city_id');
        $GuideTitles = GuideTitle::withTrashed()
                            ->where('city_id', $city_id)
                            ->get();
        return $this->ok( $GuideTitles , "Guide titles list");  
    }

    public function create(GuideTitleRequest $request)
    {   
        $data = $request->all();
        
        $errorMessages = [];
         
        //check if duplicate Guide is  being created
        if($errorMessage=GuideTitle::isDuplicate($data))
                $errorMessages[] = $errorMessage;
    
        if(empty($errorMessages))
            return $this->ok($request->save(), "Guide title(s) Created");
            
        return $this->validationErrors($errorMessages, "Duplicate Guide creation is not allowed");
    }

    public function update($title_id, GuideTitleRequest $request)
    {
        $data = $request->all();
        
        $errorMessages = [];
            
        if($errorMessage=GuideTitle::isDuplicate($data))
            $errorMessages[] = $errorMessage;
        
        if(empty($errorMessages))
            return $this->ok($request->update($title_id) , "Guide title updated");
        
        return $this->validationErrors($errorMessages, "Duplicate Guide creation is not allowed");
        
    }

    public function delete($title_id)
    {
        $Guide = GuideTitle::findOrFail($title_id);

        $Guide->delete();
        
        return $this->ok($Guide, "Guide title deleted");
    }

    public function restore($title_id)
    {
        $GuideTitle = GuideTitle::onlyTrashed()
                            ->where('id', $title_id)
                            ->first();

        if(empty($GuideTitle)) {
            return $this->notFound("Guide title not found");
        }

        $GuideTitle->restore();
        return $this->ok($GuideTitle, "Guide title restored");
    }
}