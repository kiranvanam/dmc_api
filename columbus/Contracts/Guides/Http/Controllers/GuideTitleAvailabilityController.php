<?php
namespace Columbus\Contracts\Guides\Http\Controllers;

use App\Http\Controllers\ApiController;
use Columbus\Contracts\Guides\Models\GuideTitleAvailability;
use Columbus\Utilities\DateUtil;
use Illuminate\Http\Request;

class GuideTitleAvailabilityController extends ApiController
{
    protected $weekDaysPricingDetails = [];


    public function pricingForRange( Request $request)
    {
        $vendor_id = $request->get('vendor_id'); 
        $guide_title_id = $request->get('guide_title_id');

        $from_date = DateUtil::createCarbonIfNotCarbon($request->from_date)->toDateString();
        $to_date = DateUtil::createCarbonIfNotCarbon($request->to_date)->toDateString();

        $pricing = GuideTitleAvailability::where('vendor_id', $vendor_id)
                            ->where('guide_title_id', $guide_title_id)
                            ->where('date', '>=', $from_date)
                            ->where('date','<=', $to_date)
                            ->orderBy('date')
                            ->get();
        return $this->ok($pricing, "Vendor Pricing Details for Guide Title");
    }

    public function update(  Request $request)
    {
        $vendor_id = $request->get('vendor_id'); 
        $guide_title_id = $request->get('guide_title_id');
        $this->weekDaysPricingDetails = $request->all();
        $this->weekDaysPricingDetails['vendor_id'] = $vendor_id;
        $this->weekDaysPricingDetails['guide_title_id'] = $guide_title_id;
        $this->weekDaysPricingDetails['city_id'] = $request->get('city_id');
        //return $this->ok($this->weekDaysPricingDetails);
        
        $dates = DateUtil::datesForRange(
                                $request->get("from_date"), 
                                $request->get('to_date'), 
                                $this->callBackFunction()
                            );
        return $this->ok($dates , "Pricing updated for given ranges");
    }

    //'meal_vendor_plan_id', 'date', 'time_slots_with_pricing_details', 'allocation', 'is_available', 'is_on_request'
    public function callBackFunction()
    {
        return function($date) {
            $dayDetails = $this->getDayDetails($date);
            $guide_title_id = $this->weekDaysPricingDetails['guide_title_id'];
            if(empty($dayDetails) == false) {
                $meal_vendor_plan_pricing = GuideTitleAvailability::where('vendor_id', $dayDetails['vendor_id'])
                                      ->where('guide_title_id', $guide_title_id)
                                      ->where('date', $dayDetails['date'])
                                      ->first();

                if(empty($meal_vendor_plan_pricing)) {
                   $meal_vendor_plan_pricing = GuideTitleAvailability::create($dayDetails);                
                }else {
                    $meal_vendor_plan_pricing->update($dayDetails);
                }
                return $meal_vendor_plan_pricing;
            }
            return null;
        };
    }
    /* the data format

    meal_vendor_plan_id: ''
    operating_days {
                    index_for_source_to_copy_day_details:-1,

                    is_available:true,
                    is_on_request:false,
                    
                    week_day_name: this.calendar.weekDays()[week_day_index],
                    week_day_index, 

                    allocation:0
    }
    */
    public function getDayDetails($date)
    {
        $week_day_index = $date->dayOfWeek;
        
        //$allSlotsTotalAllocationsForDay = 0;

        foreach ($this->weekDaysPricingDetails['operating_days'] as  $dayDetails) {
             if($dayDetails['week_day_index'] == $week_day_index) {

                $dayDetails['date'] = $date->toDateString();
                $dayDetails['vendor_id'] = $this->weekDaysPricingDetails['vendor_id'];
                $dayDetails['guide_title_id'] = $this->weekDaysPricingDetails['guide_title_id'];
                $dayDetails['city_id'] = $this->weekDaysPricingDetails['city_id'];                
                //$dayDetails['slots']
                //$dayDetails['time_slots_with_pricing_details'];                
                
                //$dayData['allocation'] = $dayDetails['allocation'];               
                
                //$dayDetails['is_available'] = true;
                //$dayDetails['is_on_request'] = ;
                
                return $dayDetails;
             }   
        }

        return null;
    }
}