<?php

namespace Columbus\Contracts\Guides\Http\Requests;

use Columbus\SystemSettings\Models\Code;
use Illuminate\Foundation\Http\FormRequest;
use Columbus\Contracts\Guides\Models\GuideTitle;

class GuideTitleRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    
    public function rules()
    {
        return [
            'city_id' => 'required',
            'name' => 'required'
        ];
    }

    protected function prepareForValidation()
    {
    }

    
    public function messages()
    {
        return [
            'city_id.required' => "City is not selected",
            'name.required' => "Guide title/name must be provided",            
        ];
    }

    public function save()
    {
        $data = $this->all();
        $GuideTitles =[];
        $data['code'] = Code::getNextCodeForModule("guides");
        $GuideTitle = GuideTitle::create($data);
        return $GuideTitle;
    }


    public function update($title_id)
    {
        $GuideTitle = GuideTitle::find($title_id);
        if($GuideTitle) {
            $GuideTitle->update($this->all());
        }
        return $GuideTitle;
    }

}