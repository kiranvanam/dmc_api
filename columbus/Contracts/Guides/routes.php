<?php

Route::group(['prefix'=>'/api', 'middleware'=>[/*'auth:api','cors'*/]], function() {

    Route::group(["prefix" => "vendor-guides",], function(){
        Route::get("", "GuideController@guidesForVendor");
        Route::post("",'GuideController@create');
        Route::put("{guide_id}",'GuideController@update');
        Route::delete("{guide_id}",'GuideController@delete');
        //Route::post("{guid_id}/pricing", 'GuidePricingController@pricingForRange');
        //Route::put("{guide_id}/pricing", 'GuidePricingController@update');

    });

    /* Route::group(["prefix" => "guide-titles",], function(){
        Route::get("", "GuideTitleController@index");
        Route::post("",'GuideTitleController@create');
        Route::put("{title_id}",'GuideTitleController@update');
        Route::delete("{title_id}",'GuideTitleController@delete');

    }); */

    Route::group(['prefix'=>'guide-titles'], function() {
        Route::get("", 'GuideTitleController@index');
        Route::post("", 'GuideTitleController@create');
        Route::put("{title_id}", 'GuideTitleController@update');
        Route::delete("{title_id}", "GuideTitleController@delete");
        Route::put("{title_id}/restore", 'GuideTitleController@restore');
    });

    Route::group(['prefix'=>"vendors/{vendor_id}/guide-titles"], function() {
        Route::get("", 'GuideTitleVendorDetailController@index');

        Route::post("", "GuideTitleVendorDetailController@create");
        Route::group(["prefix"=>"{guide_title_vendor_detail_id}"], function() {
            Route::put("", "GuideTitleVendorDetailController@update");
            Route::put("filters", "GuideTitleVendorDetailController@updateFilters");
            Route::post("gallery", "GuideTitleVendorDetailController@updateGallery");

            Route::group(['prefix' => "cancellation-policies"], function(){
                Route::get("", 'GuideTitleVendorDetailCancellationPolicyController@index');
                Route::post("", "GuideTitleVendorDetailCancellationPolicyController@create");
                Route::put("{cancellation_policy_id}", 'GuideTitleVendorDetailCancellationPolicyController@update');
                Route::delete("{cancellation_policy_id}", "GuideTitleVendorDetailCancellationPolicyController@delete");
            });

            Route::post("pricing", "GuideTitleVendorDetailPricingController@pricingForRange");
            Route::put("pricing", "GuideTitleVendorDetailPricingController@update");
        });
    });

    Route::group(['prefix'=>'vendor-guide-title-availability'], function() {
        Route::post("", 'GuideTitleAvailabilityController@pricingForRange');
        Route::put("", 'GuideTitleAvailabilityController@update');
    });

});

//Route::get('/tour-types', 'TourSettingsController@index');