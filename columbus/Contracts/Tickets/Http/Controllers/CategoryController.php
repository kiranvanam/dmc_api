<?php

namespace Columbus\Contracts\Tickets\Http\Controllers;

use App\Http\Controllers\ApiController;
use Columbus\Contracts\Tickets\Models\TicketCategory;
use Illuminate\Http\Request;


class CategoryController extends ApiController
{   

    public function create(Request $request)
    {
        return $this->ok(TicketCategory::create($request->all()), "Ticket Category Created");
    }

    public function update($type_id, Request $request)
    {
        $type = TicketCategory::find($type_id);

        if($type) {
            $type->update($request->all());
        }
        return $this->ok($type, "Ticket Category updated");
    } 

    public function delete($type_id)
    {
        $type = TicketCategory::find($type_id);

        if($type) {
            $type->delete();
        }
        return $this->ok($type, "Ticket Category deleted");
    }
}