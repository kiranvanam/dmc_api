<?php
namespace Columbus\Contracts\Tickets\Http\Controllers;

use App\Http\Controllers\ApiController;
use Columbus\Contracts\Tickets\Models\TicketTitleVendorDetailPricing;
use Columbus\Utilities\DateUtil;
use Illuminate\Http\Request;

class TicketTitleVendorDetailPricingController extends ApiController
{
    protected $weekDaysPricingDetails = [];

    public function pricingForRange($vendor_id, $ticket_title_vendor_detail_id, Request $request)
    {
        $from_date = DateUtil::createCarbonIfNotCarbon($request->from_date)->toDateString();
        $to_date = DateUtil::createCarbonIfNotCarbon($request->to_date)->toDateString();
        $pricing = TicketTitleVendorDetailPricing::where('ticket_title_vendor_detail_id', $ticket_title_vendor_detail_id)
                            ->where('date', '>=', $from_date)
                            ->where('date','<=', $to_date)
                            ->orderBy('date')
                            ->get();
        return $this->ok($pricing, "Pricing Details for the given range");
    }

    public function update($vendor_id, $ticket_title_vendor_detail_id, Request $request)
    {
        $this->weekDaysPricingDetails = $request->all();
        $this->weekDaysPricingDetails['ticket_title_vendor_detail_id'] = $ticket_title_vendor_detail_id;

        //return $this->ok($this->weekDaysPricingDetails);
        
        $dates = DateUtil::datesForRange(
                                $request->get("from_date"), 
                                $request->get('to_date'), 
                                $this->callBackFunction()
                            );
        return $this->ok($dates , "Pricing updated for given ranges");
    }

    //'ticket_title_vendor_detail_id', 'date', 'time_slots_with_pricing_details', 'allocation', 'is_available', 'is_on_request'
    public function callBackFunction()
    {
        return function($date) {
            $dayDetails = $this->getDayDetails($date);

            if(empty($dayDetails) == false) {
                $meal_vendor_plan_pricing = TicketTitleVendorDetailPricing::where('ticket_title_vendor_detail_id', $dayDetails['ticket_title_vendor_detail_id'])
                                      ->where('date', $dayDetails['date'])
                                      ->first();

                if(empty($meal_vendor_plan_pricing)) {
                   $meal_vendor_plan_pricing = TicketTitleVendorDetailPricing::create($dayDetails);                
                }else {
                    $meal_vendor_plan_pricing->update($dayDetails);
                }
                return $meal_vendor_plan_pricing;
            }
            return null;
        };
    }
    /* the data format

    ticket_title_vendor_detail_id: ''
    operating_days {
                    index_for_source_to_copy_day_details:-1,

                    is_available:true,
                    is_on_request:false,
                    
                    week_day_name: this.calendar.weekDays()[week_day_index],
                    week_day_index, 

                    time_slots:[ 
                        {
                            start_time:'', 
                            end_time:'',
                            allocation: '',
                            is_available: true,
                            is_on_request: false,
                            adult_price:'',
                            child_price:'',
                            infant_price:''
                        }
                    ], 
                    allocation:0
    }
    */
    public function getDayDetails($date)
    {
        $week_day_index = $date->dayOfWeek;
        
        //$allSlotsTotalAllocationsForDay = 0;

        foreach ($this->weekDaysPricingDetails['operating_days'] as  $dayDetails) {
             if($dayDetails['week_day_index'] == $week_day_index) {

                $dayDetails['date'] = $date->toDateString();
                $dayDetails['ticket_title_vendor_detail_id'] = $this->weekDaysPricingDetails['ticket_title_vendor_detail_id'];
                $dayDetails['currency'] = $this->weekDaysPricingDetails['currency'];
                            
                return $dayDetails;
             }   
        }

        return null;
    }
}