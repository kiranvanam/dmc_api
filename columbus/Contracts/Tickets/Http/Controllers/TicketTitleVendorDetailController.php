<?php

namespace Columbus\Contracts\Tickets\Http\Controllers;

use App\Http\Controllers\ApiController;
use Illuminate\Database\Eloquent\SoftDeletes;
use Columbus\Contracts\Tickets\Models\TicketTitleVendorDetail;

class TicketTitleVendorDetailController extends ApiController
{
    
    public function index($vendor_id)
    {
        $vendortitcketsList = TicketTitleVendorDetail::where('vendor_id', $vendor_id)->get();

        return $this->ok($vendortitcketsList, "Vendor titckets list");
    }

    public function create($vendor_id)
    {
        $vendorTicketDetails = TicketTitleVendorDetail::create(request()->all());
        
        return $this->ok($vendorTicketDetails, "titcket Details created");
    }

    public function update($vendor_id, $titcket_title_vendor_detail_id)
    {
        $vendorTicketDetails = TicketTitleVendorDetail::findOrFail($titcket_title_vendor_detail_id);

        $vendorTicketDetails->update(request()->all());

        return $this->ok($vendorTicketDetails, "titcket details updated");
    }

    public function updateFilters( $vendor_id, $titcket_title_vendor_detail_id)
    {
        $vendorTicketDetails = TicketTitleVendorDetail::findOrFail($titcket_title_vendor_detail_id);

        $data = [];
        $data['filters'] = request('filters');

        $vendorTicketDetails->update($data);

        return $this->ok($vendorTicketDetails, "titcket Filters updated");
    }

}