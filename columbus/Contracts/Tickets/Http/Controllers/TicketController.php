<?php

namespace Columbus\Contracts\Tickets\Http\Controllers;

use App\Http\Controllers\ApiController;
use Columbus\Contracts\Tickets\Models\Ticket;
use Columbus\Utilities\FileManager;
use Illuminate\Http\Request;


class TicketController extends ApiController
{   
    function index()
    {
        return $this->ok(Ticket::forCity( request()->get('city_id')) , "Tickers for the selected city");
    }

    public function create(Request $request)
    {
        return $this->ok(Ticket::create($request->all()), "Ticket created");
    }

    public function update($ticket_id, Request $request)
    {
        $ticket = Ticket::find($ticket_id);
        if($ticket) {
            $ticket->update($request->all());
        }

        return $this->ok($ticket, "Ticket details are updated" );
    }

    public function uploadGallery($ticket_id, Request $request, FileManager $fileManager)
    {
        $ticket = Ticket::find($ticket_id);
        
        $gallery = [];

        if($ticket && $ticket->gallery)  {
            $gallery = $ticket->gallery;
        }
        
        if ($request->hasFile('images')) {
            $country_id = $request->get('country_id');
            $vendor_id = $request->get('vendor_id');
            
            $path = "uploads/images/countries/$country_id/tickets/$ticket->id";

            $gallery = array_merge($gallery, $fileManager->savePhotos($path , $request->file('images')));

        }
        
        $ticket->update(['gallery'=> $gallery]);
        

        return $this->ok($ticket, "Tickets gallery updated");
    }
}