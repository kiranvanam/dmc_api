<?php

namespace Columbus\Contracts\Tickets\Http\Controllers;

use App\Http\Controllers\ApiController;
use Columbus\Contracts\Tickets\Models\TicketCategory;
use Columbus\Utilities\FileManager;
use Illuminate\Http\Request;


class TicketCategoryController extends ApiController
{   

    public function create($ticket_id, Request $request)
    {
        return $this->ok(TicketCategory::create($request->all()), "Ticket Category created");
    }

    public function update($ticket_id, $category_id, Request $request)
    {
        $ticket = TicketCategory::find($category_id);
        if($ticket) {
            $ticket->update($request->all());
        }
        
        return $this->ok($ticket, "Category details are updated" );
    }

    public function uploadGallery($ticket_id, $category_id, Request $request, FileManager $fileManager)
    {
        $ticketCategory = TicketCategory::find($category_id);
        
        $gallery = [];

        if($ticketCategory && $ticketCategory->gallery)  {
            $gallery = $ticketCategory->gallery;
        }
        
        if ($request->hasFile('images')) {
            $country_id = $request->get('country_id');
            $vendor_id = $request->get('vendor_id');
            
            $path = "uploads/images/countries/$country_id/tickets/$ticket_id";

            $gallery = array_merge($gallery, $fileManager->savePhotos($path , $request->file('images')));

        }
        
        $ticketCategory->update(['gallery'=> $gallery]);
        

        return $this->ok($ticketCategory, "Category gallery updated");
    }
}