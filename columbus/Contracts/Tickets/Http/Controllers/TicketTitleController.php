<?php

namespace Columbus\Contracts\Tickets\Http\Controllers;

use App\Http\Controllers\ApiController;
use Columbus\Contracts\Tickets\Models\TicketTitle;
use Columbus\Contracts\Tickets\Http\Requests\TicketTitleRequest;

class TicketTitleController extends ApiController
{
    function index()
    {
        $city_id = request()->get('city_id');
        $ticketTitles = TicketTitle::withTrashed()
                            ->where('city_id', $city_id)
                            ->get();
        return $this->ok( $ticketTitles , "Ticket titles list");  
    }

    public function create(TicketTitleRequest $request)
    {   
        $data = $request->all();
        
        $errorMessages = [];
         
        //check if duplicate ticket is  being created
        if($errorMessage=TicketTitle::isDuplicate($data))
                $errorMessages[] = $errorMessage;
    
        if(empty($errorMessages))
            return $this->ok($request->save(), "Ticket title(s) Created");
            
        return $this->validationErrors($errorMessages, "Duplicate Ticket creation is not allowed");
    }

    public function update($title_id, TicketTitleRequest $request)
    {
        $data = $request->all();
        
        $errorMessages = [];
            
        if($errorMessage=TicketTitle::isDuplicate($data))
            $errorMessages[] = $errorMessage;
        
        if(empty($errorMessages))
            return $this->ok($request->update($title_id) , "Ticket title updated");
        
        return $this->validationErrors($errorMessages, "Duplicate Ticket creation is not allowed");
        
    }

    public function updateThumbnail($title_id)
    {
        $ticket = TicketTitle::findOrFail($title_id);
        $ticket->update(['thumbnail' => request('thumbnail')]);
        return $this->ok($ticket, "Thumbnail is updated");
    }

    public function delete($title_id)
    {
        $Ticket = TicketTitle::findOrFail($title_id);

        $Ticket->delete();
        
        return $this->ok($Ticket, "Ticket title deleted");
    }

    public function restore($title_id)
    {
        $ticketTitle = TicketTitle::onlyTrashed()
                            ->where('id', $title_id)
                            ->first();

        if(empty($ticketTitle)) {
            return $this->notFound("Ticket title not found");
        }

        $ticketTitle->restore();
        return $this->ok($ticketTitle, "Ticket title restored");
    }
}