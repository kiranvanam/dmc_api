<?php

namespace Columbus\Contracts\Tickets\Http\Controllers;

use App\Http\Controllers\ApiController;
use Columbus\Contracts\Tickets\Models\Filter;

class TicketFilterController extends ApiController
{
    public function index()
    {
        return $this->ok( Filter::withTrashed()->get(), "Tour filters list");
    }

    public function create()
    {    
        $data = request()->all(); //only(['groupby', 'name']);
        return $this->ok(Filter::create($data), "Filter created");
    }

    public function update($filter_id)
    {
        $filter = Filter::findOrFail($filter_id);
        $data = request()->all(); //only([ 'groupby', 'name']);
        $filter->update($data);
        return $this->ok($filter, "Filter updated");
    }

    public function delete($filter_id)
    {
        $filter = Filter::findOrFail($filter_id);
        $filter->forceDelete();
        return $this->ok($filter, "Filter deleted");
    }

    public function restore($filter_id)
    {
        $filter = Filter::onlyTrashed()->where('id', $filter_id)->first();

        $filter->restore();

        return $this->ok($filter, "Filter restored");
    }

    public function updateFilterGroupLabel()
    {
        Filter::where('groupby', request('old'))->update(['groupby' => request('updated')]);
        return $this->index();
    }
}