<?php

namespace Columbus\Contracts\Tickets\Http\Controllers;

use App\Http\Controllers\ApiController;
use Columbus\Contracts\Tickets\Models\VendorTicketDetail;
use Columbus\Utilities\FileManager;
use Illuminate\Http\Request;


class VendorTicketDetailController extends ApiController
{   
    function index($ticker_vendor_id)
    {
        return $this->ok(VendorTicketDetail::where('ticket_vendor_id', $ticket_vendor_id)->get(), "Ticket details for Vendor" );
    }

    public function create($ticket_vendor_id, Request $request)
    {
        return $this->ok(VendorTicketDetail::create($request->all()), "Ticket added for Vendor");
    }

    public function update($ticket_vendor_id, $ticket_detail_id, Request $request)
    {
        $vendorTicketDetail = VendorTicketDetail::find($ticket_detail_id);

        if($vendorTicketDetail) {
            $vendorTicketDetail->update($request->all());
        }
        return $this->ok($vendorTicketDetail, "Ticket details are updated");
    }

    public function uploadGallery($ticket_vendor_id, $ticket_detail_id, Request $request, FileManager $fileManager)
    {
        $ticketVendor = TicketVendor::find($ticket_vendor_id);
        
        $gallery = [];

        if($ticketVendor && $ticketVendor->gallery)  {
            $gallery = $ticketVendor->gallery;
        }
        
        if ($request->hasFile('images')) {
            $country_id = $request->get('country_id');
                        
            $path = "uploads/images/countries/$country_id/tickets/vendors/$ticketVendor->id";

            $gallery = array_merge($gallery, $fileManager->savePhotos($path , $request->file('images')));

        }
        
        $ticketVendor->update(['gallery'=> $gallery]);
        

        return $this->ok($ticketVendor, "Tickets gallery updated");
    }

}