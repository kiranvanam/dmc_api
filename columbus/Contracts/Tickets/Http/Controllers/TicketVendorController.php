<?php

namespace Columbus\Contracts\Tickets\Http\Controllers;

use App\Http\Controllers\ApiController;
use Columbus\Contracts\Tickets\Models\TicketVendor;
use Columbus\Utilities\FileManager;
use Illuminate\Http\Request;


class TicketVendorController extends ApiController
{   
    function index()
    {
        $ticketVendor = TicketVendor::with('tickets')
                      ->where('vendor_id', request()->get('vendor_id'))->first();
        return $this->ok($ticketVendor, "Vendor details");
    }

    public function create(Request $request)
    {
        return $this->ok(TicketVendor::create($request->all()), "Ticket Vendor created");
    }

    public function update($ticket_vendor_id, Request $request) 
    {
        $ticketVendor = TicketVendor::find($ticket_vendor_id);

        if($ticketVendor) {
            $ticketVendor->update($request->all());
        }
        return $this->ok($ticketVendor, "Vendor Details updated");
    }

    public function uploadGallery($ticket_vendor_id, Request $request, FileManager $fileManager)
    {
        $ticketVendor = TicketVendor::find($ticket_vendor_id);
        
        $gallery = [];

        if($ticketVendor && $ticketVendor->gallery)  {
            $gallery = $ticketVendor->gallery;
        }
        
        if ($request->hasFile('images')) {
            $country_id = $request->get('country_id');
                        
            $path = "uploads/images/countries/$country_id/tickets/vendors/$ticketVendor->id";

            $gallery = array_merge($gallery, $fileManager->savePhotos($path , $request->file('images')));

        }
        
        $ticketVendor->update(['gallery'=> $gallery]);
        

        return $this->ok($ticketVendor, "Tickets gallery updated");
    }
}