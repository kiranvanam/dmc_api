<?php

namespace Columbus\Contracts\Tickets\Http\Controllers;

use App\Http\Controllers\ApiController;
use Columbus\Contracts\Tickets\Models\TicketCategory;
use Columbus\Contracts\Tickets\Models\TicketType;
use Illuminate\Http\Request;


class TypeController extends ApiController
{   
    function index()
    {
        return $this->ok([
               "ticket_types" => TicketType::all(),
               "ticket_categories" => TicketCategory::all()
            ] , "Tickers for the selected city");
    }

    public function create(Request $request)
    {
        return $this->ok(TicketType::create($request->all()), "Ticket Type Created");
    }

    public function update($type_id, Request $request)
    {
        $type = TicketType::find($type_id);

        if($type) {
            $type->update($request->all());
        }
        return $this->ok($type, "Ticket Type updated");
    } 

    public function delete($type_id)
    {
        $type = TicketType::find($type_id);

        if($type) {
            $type->delete();
        }
        return $this->ok($type, "Ticket Type deleted");
    }
}