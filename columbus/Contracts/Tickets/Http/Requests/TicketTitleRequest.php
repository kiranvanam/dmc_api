<?php

namespace Columbus\Contracts\Tickets\Http\Requests;

use Columbus\SystemSettings\Models\Code;
use Illuminate\Foundation\Http\FormRequest;
use Columbus\Contracts\Tickets\Models\TicketTitle;

class TicketTitleRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    
    public function rules()
    {
        return [
            'city_id' => 'required',
            'name' => 'required'
        ];
    }

    protected function prepareForValidation()
    {
    }

    
    public function messages()
    {
        return [
            'city_id.required' => "City is not selected",
            'name.required' => "Ticket title/name must be provided",            
        ];
    }

    public function save()
    {
        $data = $this->all();
        $ticketTitles =[];
        $data['code'] = Code::getNextCodeForModule("tickets");
        $ticketTitle = TicketTitle::create($data);        
        return $ticketTitle;
    }


    public function update($title_id)
    {
        $ticketTitle = TicketTitle::find($title_id);
        if($ticketTitle) {
            $ticketTitle->update($this->all());
        }
        return $ticketTitle;
    }

}