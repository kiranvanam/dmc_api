<?php

namespace Columbus\Contracts\Tickets\Models;

use Illuminate\Database\Eloquent\Model;

class Ticket extends Model
{
    public $timestamps = false;

    protected $fillable = ['city_id', 'name', 'ticket_category_id' ,'ticket_type_ids', 'child_age_from', 'child_age_to', 'description', 'gallery'];

    protected $casts = [
        'gallery' => 'array',
        'ticket_type_ids' => 'array'
    ];

    //helper methods
    public static function forCity($city_id)
    {
        return self::where('city_id', $city_id)->get();
    }

    //relations
    public function categories()
    {
        return $this->hasMany(TicketCategory::class);
    }

}