<?php

namespace Columbus\Contracts\Tickets\Models;

use Illuminate\Database\Eloquent\Model;

class TicketVendor extends Model
{
    protected $table = "ticket_vendors";

    public $timestamps = false;

    protected $fillable = ['vendor_id', 'ticket_category_ids', 'description', 'gallery'];

    protected $casts = [
        "ticket_category_ids" => 'array',
        "gallery" => "array"
    ];

    public function tickets()
    {
        return $this->hasMany(VendorTicketDetail::class);
    }
}