<?php

namespace Columbus\Contracts\Tickets\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Columbus\Contracts\Tickets\Models\TicketTitleVendorDetail;

class TicketTitle extends Model
{
    use SoftDeletes;

    protected $fillable = ['country_id', 'city_id', 'name', 'code', 'is_available',
                        'description', 'thumbnail'];

    protected $dates = ['deleted_at'];


    public static function isDuplicate($data)
    {
        // check if <tour,type> is created
        $tourTitle = TicketTitle::withTrashed()
                    ->where('name', $data['name'])
                    ->where('city_id', $data['city_id']);

        if( isset($data['id']))
            $tourTitle = $tourTitle->where('id', '!=', $data['id']);

        $tourTitle = $tourTitle->first();

        if( empty($tourTitle))
        return null;

        logger("Trying to create duplicate Ticket: ", $data);

        return  [ 'Ticket Duplicaiton' =>
                    $data['name'] . " Ticket with this name is already created for this city"
                ];

    }

    /* used while fetching pricing details */
    public function vendors()
    {
        return $this->hasMany(TicketTitleVendorDetail::class, 'ticket_title_id')->with('pricing')->with('cancellationPolicies');
    }

    /* it's used while retriving booking details */
    public function vendor()
    {
        return $this->hasOne(TicketTitleVendorDetail::class, 'ticket_title_id');
    }
}