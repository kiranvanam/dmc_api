<?php

namespace Columbus\Contracts\Tickets\Models;

use Illuminate\Database\Eloquent\Model;


class TicketCategory extends Model
{
    public $timestamps = false;

    protected $fillable = ['name'];
}