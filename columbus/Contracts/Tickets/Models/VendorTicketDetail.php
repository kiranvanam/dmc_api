<?php

namespace Columbus\Contracts\Tickets\Models;

use Illuminate\Database\Eloquent\Model;


class VendorTicketDetail extends Model
{
    protected $table = "ticket_vendor_ticket_details";

    public $timestamps = false;

    protected $fillable = ['ticket_vendor_id', 'ticket_id', 'code', 'inclusions', 'exclusions', 'remarks', 'terms_and_conditions' ,'description', 'cancellation_policies', 'cancellation_policy_rules', 'gallery'];

    protected $casts = [
        'gallery' => 'array',
        'cancellation_policy_rules' => 'array'
    ];
    
}