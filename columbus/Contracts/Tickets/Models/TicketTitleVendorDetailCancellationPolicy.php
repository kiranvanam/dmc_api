<?php

namespace Columbus\Contracts\Tickets\Models;

use Illuminate\Database\Eloquent\Model;

class TicketTitleVendorDetailCancellationPolicy extends Model
{   
    
    protected $fillable = ['ticket_title_vendor_detail_id',
                            'booking_start_date', 'booking_end_date',
                            'travelling_start_date', 'travelling_end_date',
                            'is_non_refundable',
                            'policy_rules'
                        ];

    protected $casts = [
        'policy_rules' => 'array'
    ];
}