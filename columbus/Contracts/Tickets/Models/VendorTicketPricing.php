<?php

namespace Columbus\Contracts\Tickets\Models;

use Illuminate\Database\Eloquent\Model;


class VendorTicketPricing extends Model
{   
    protected $table = "ticket_vendor_ticket_pricing";
    public  $timestamps = false;

    protected $fillable = ['vendor_ticket_id', 'date', 'time_slots_with_pricing_details', 'is_available', 
                            'is_on_request'];

    protected $dates = ['date'];

    protected $casts = [
        "time_slots_with_pricing_details" => "array"
    ];
}
