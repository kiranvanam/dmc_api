<?php

namespace Columbus\Contracts\Tickets\Models;

use Illuminate\Database\Eloquent\Model;


class TicketType extends Model
{
    public $timestamps = false;

    protected $fillable = [ 'name'];
    
}