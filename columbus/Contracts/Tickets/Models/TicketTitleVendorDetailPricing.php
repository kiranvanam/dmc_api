<?php

namespace Columbus\Contracts\Tickets\Models;

use Illuminate\Database\Eloquent\Model;

class TicketTitleVendorDetailPricing extends Model
{
    public $table = "ticket_title_vendor_detail_pricing";
    
    protected $fillable = ['ticket_title_vendor_detail_id', 'date', 
                            'time_slots_with_pricing_details', 'currency',
                            'is_available', 'is_on_request'];

    protected $dates = ['date'];

    protected $casts = [
        "time_slots_with_pricing_details" => "array"
    ];

}