<?php

Route::group(['prefix'=>'/api', 'middleware'=>['auth:api','cors']], function() {

    Route::get("ticket-settings", 'TypeController@index');

    Route::group(['prefix' => "ticket-filters"], function() {
        Route::get("", "TicketFilterController@index");
        Route::post("", "TicketFilterController@create");
        Route::put("{filter_id}", "TicketFilterController@update");
        Route::delete("{filter_id}", "TicketFilterController@delete");
        Route::put("{filter_id}/restore", "TicketFilterController@restore");

            Route::post("update-filter-group-heading", 'TicketFilterController@updateFilterGroupLabel');
    });


    Route::group(['prefix'=>'ticket-titles'], function() {
        Route::get("", 'TicketTitleController@index');
        Route::post("", 'TicketTitleController@create');
        Route::put("{title_id}", 'TicketTitleController@update');
        Route::delete("{title_id}", "TicketTitleController@delete");
        Route::put("{title_id}/restore", 'TicketTitleController@restore');
        Route::put('/{id}/thumbnail', 'TicketTitleController@updateThumbnail');
    });

    Route::group(['prefix'=>"vendors/{vendor_id}/ticket-titles"], function() {        
        Route::get("", 'TicketTitleVendorDetailController@index');

        Route::post("", "TicketTitleVendorDetailController@create");
        Route::group(["prefix"=>"{ticket_title_vendor_detail_id}"], function() {
            Route::put("", "TicketTitleVendorDetailController@update");
            Route::put("filters", "TicketTitleVendorDetailController@updateFilters");
            Route::post("gallery", "TicketTitleVendorDetailController@updateGallery");

            Route::group(['prefix' => "cancellation-policies"], function(){
                Route::get("", 'TicketTitleVendorDetailCancellationPolicyController@index');
                Route::post("", "TicketTitleVendorDetailCancellationPolicyController@create");
                Route::put("{cancellation_policy_id}", 'TicketTitleVendorDetailCancellationPolicyController@update');
                Route::delete("{cancellation_policy_id}", "TicketTitleVendorDetailCancellationPolicyController@delete");
            });
            
            Route::post("pricing", "TicketTitleVendorDetailPricingController@pricingForRange");
            Route::put("pricing", "TicketTitleVendorDetailPricingController@update");
        });
    });

    Route::group(['prefix'=> 'ticket-types',], function (){
        Route::post("", 'TypeController@create');
        Route::put("{type_id}", 'TypeController@update');
        Route::delete("{type_id}", 'TypeController@delete');
    });

    Route::group(['prefix'=> 'ticket-categories',], function (){
        Route::post("", 'CategoryController@create');
        Route::put("{category_id}", 'CategoryController@update');
        Route::delete("{category_id}", 'CategoryController@delete');
    });

    Route::group(['prefix'=> 'tickets',], function (){
        Route::get('', 'TicketController@index');
        Route::post('', 'TicketController@create');
        Route::put('/{ticket_id}', 'TicketController@update');
        Route::post('{ticket_id}/gallery', 'TicketController@uploadGallery');

        Route::group(["prefix"=> "{ticket_id}/categories"], function(){
            Route::post("", 'TicketCategoryController@create');
            Route::put("/{category_id}", 'TicketCategoryController@update');
            Route::post('{category_id}/gallery', 'TicketCategoryController@uploadGallery');
        });
    });

    Route::group(['prefix'=> 'ticket-vendors'], function (){
        Route::get("", 'TicketVendorController@index');
        Route::post("",'TicketVendorController@create');
        Route::put("/{ticket_vendor_id}",'TicketVendorController@update');
        Route::post("/{ticket_vendor_id}/gallery", 'TicketVendorController@uploadGallery');


        //vendor ticket details
        Route::group(['prefix'=>"{ticket_vendor_id}/ticket-details"], function(){
            Route::get("",'VendorTicketDetailController@index');
            Route::post("", "VendorTicketDetailController@create");
            Route::put("{ticket_detail_id}", "VendorTicketDetailController@update");
            Route::post('{ticket_detail_id}/gallery', 'VendorTicketDetailController@uploadGallery');


            Route::group(['prefix'=>'{ticket_detail_id}/pricing'], function(){
                Route::post("", 'VendorTicketPricingController@pricingForRange');
                Route::put("", 'VendorTicketPricingController@update');
            });
        });

    });

});
