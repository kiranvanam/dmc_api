<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTicketTitleVendorDetailPricingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ticket_title_vendor_detail_pricing', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ticket_title_vendor_detail_id');
            $table->date('date');
            $table->text('time_slots_with_pricing_details');
            $table->string('currency', 6);
            $table->boolean('is_available');
            $table->boolean('is_on_request');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ticket_title_vendor_detail_pricing');
    }
}
