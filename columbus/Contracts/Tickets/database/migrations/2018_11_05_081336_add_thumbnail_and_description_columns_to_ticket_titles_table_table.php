<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddThumbnailAndDescriptionColumnsToTicketTitlesTableTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ticket_titles', function (Blueprint $table) {
            if(Schema::hasColumn('ticket_titles', 'type')) {
                $table->dropColumn('type');
            }
            $table->string('description', 1000)->nullable()->after('is_available');
            $table->string('thumbnail')->nullable()->after('description');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ticket_titles', function (Blueprint $table) {
            $table->dropColumn('description');
            $table->dropColumn('thumbnail');
        });
    }
}
