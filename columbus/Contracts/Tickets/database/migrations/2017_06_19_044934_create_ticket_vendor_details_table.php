<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTicketVendorDetailsTable extends Migration
{
    
    public function up()
    {
        Schema::create('ticket_vendors', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('vendor_id');
            $table->string('ticket_category_ids',50);
            $table->string('description')->nullable()->default("");
            $table->string("gallery")->nullable()->default("");
            
            $table->index('vendor_id');
        });

        Schema::create('ticket_vendor_ticket_details', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ticket_vendor_id');
            $table->integer('ticket_id');
            $table->string('code',100);
            $table->string('description')->nullable()->default("");
            $table->string('inclusions')->nullable()->default("");
            $table->string("exclusions")->nullable()->default("");
            $table->string("remarks")->nullable()->default("");
            $table->string("terms_and_conditions")->nullable()->default("");
            $table->string("cancellation_policies")->nullable()->default("");
            $table->string("cancellation_policy_rules")->nullable()->default("");
            $table->string('gallery')->nullable()->default("");

            $table->index(['ticket_vendor_id','ticket_id'], 'ticket_vendor_id_index');
            $table->index('code');
        });

    }

    
    public function down()
    {
        Schema::dropIfExists('ticket_vendor_ticket_details');
        Schema::dropIfExists('ticket_vendors');
    }
}
