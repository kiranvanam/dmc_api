<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTicketTitleVendorDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ticket_title_vendor_details', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ticket_title_id')->index();
            $table->integer('vendor_id')->index();
            $table->string('vendor_code', 25);
            $table->text('itinerary')->nullable();
            $table->text('inclusions')->nullable();
            $table->text('exclusions')->nullable();
            $table->text('terms_and_conditions')->nullable();
            $table->text('remarks')->nullable();
            $table->string('filters', 1000)->nullable();
            $table->string('gallery', 1500)->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ticket_title_vendor_details');
    }
}
