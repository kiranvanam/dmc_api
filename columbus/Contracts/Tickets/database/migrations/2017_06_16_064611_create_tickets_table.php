<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTicketsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tickets', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('city_id');
            $table->string('name',250);
            $table->integer('ticket_category_id')->nullable();
            $table->string('ticket_type_ids',50)->default("[]");
            $table->smallInteger('child_age_from')->default(0);
            $table->smallInteger('child_age_to')->default(0);
            $table->string('description')->nullable();
            $table->string('gallery')->nullable();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tickets');
    }
}
