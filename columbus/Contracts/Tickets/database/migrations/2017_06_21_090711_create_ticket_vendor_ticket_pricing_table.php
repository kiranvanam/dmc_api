<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTicketVendorTicketPricingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ticket_vendor_ticket_pricing', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('vendor_ticket_id');
            $table->date('date');
            $table->text('time_slots_with_pricing_details')->nullable();
            $table->boolean('is_available')->default(1);
            $table->boolean('is_on_request')->default(false);

            $table->index('vendor_ticket_id');
            $table->index('date');
            $table->index(['vendor_ticket_id', 'date']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ticket_vendor_ticket_pricing');
    }
}
