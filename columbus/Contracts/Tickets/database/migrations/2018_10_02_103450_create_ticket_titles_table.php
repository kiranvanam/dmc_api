<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTicketTitlesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ticket_titles', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('country_id')->index();
            $table->integer('city_id')->index();
            $table->string('name', 500)->index();
            $table->string('code',25)->index();
            $table->boolean('is_available')->default(true)->index();
            $table->timestamps();
            $table->softDeletes();

            $table->unique(['city_id', 'name'], 'one_ticket_title_per_city_unique_index');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ticket_titles');
    }
}
