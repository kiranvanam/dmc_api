<?php

namespace Columbus\Contracts\Tours\Http\Controllers;

use App\Http\Controllers\ApiController;
use Columbus\Contracts\Tours\Http\Requests\ChildAgeCreateRequest;
use Columbus\Contracts\Tours\Models\Category;
use Columbus\Contracts\Tours\Models\DurationFilter;
use Columbus\Contracts\Tours\Models\GuideLanguage;
use Columbus\Contracts\Tours\Models\Service;
use Columbus\Contracts\Tours\Models\Theme;
use Columbus\Contracts\Tours\Models\TravelerType;
use Columbus\Contracts\Tours\Models\Type;
use Illuminate\Http\Request;


class TourSettingsController extends ApiController
{
    
    function index($destination_id = null)
    {
        return $this->ok( [
               "types" =>  Type::fetchWithSubTypes(),
               "categories" => Category::all(),
               "themes" => Theme::all(),
               "travler_types" => TravelerType::all(),
               "guide_languages" => GuideLanguage::all(),
               "duration_filters" => DurationFilter::all(),
               "services" => Service::all()
               ], 
               "Tour child ages");  
    }

    
}