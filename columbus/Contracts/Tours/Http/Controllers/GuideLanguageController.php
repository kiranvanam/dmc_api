<?php

namespace Columbus\Contracts\Tours\Http\Controllers;

use App\Http\Controllers\ApiController;
use Columbus\Contracts\Tours\Models\GuideLanguage;
use Illuminate\Http\Request;


class GuideLanguageController extends ApiController
{
    
    
    public function create(Request $request)
    {
        $language = GuideLanguage::create($request->all());
        return $this->ok($language, "Language added");
    }

    public function update($language_id, Request $request)
    {
        $language = GuideLanguage::find($language_id);

        if($language) {
            $language->update($request->all());
        }

        return $this->ok($language, "Language updated");
    }

    public function delete($language)
    {
        $language = GuideLanguage::find($language);
        if($language) {
            $language->delete();
        }
        return $this->ok($language, "Language deleted");
    }
}