<?php

namespace Columbus\Contracts\Tours\Http\Controllers;

use App\Http\Controllers\ApiController;

class TourTitlePrivateVendorServiceMappingController extends ApiController
{
    // it doesn't return by service type but only based on tour_title_vendor_detail_id
    public function index($vendor_id, $tour_title_vendor_detail_id)
    {
        $mappings = \DB::table('tour_title_private_vendor_detail_service_mappings')
                        ->where('tour_title_vendor_detail_id', $tour_title_vendor_detail_id)
                        ->get();
        return $this->ok($mappings, "All service Mappings for this private tour");
    }

    public function create($vendor_id, $tour_title_vendor_detail_id)
    {
        $service = request('service');

        // step 1: delete all old mappings for this service
        \DB::table('tour_title_private_vendor_detail_service_mappings')
            ->where('tour_title_vendor_detail_id', $tour_title_vendor_detail_id)
            ->where('service', $service)
            ->delete();
        
        // step 2: create new mappings for this service
        
        $mappings = [];
        foreach (request('service_ids') as $service_id)
            $mappings[] = [ 'service'=>$service, 
                            'tour_title_vendor_detail_id' => $tour_title_vendor_detail_id,
                            'service_id' => $service_id
                        ]; 
        
        return $this->ok(\DB::table('tour_title_private_vendor_detail_service_mappings')->insert($mappings), "Mapping updated"); 
                            
        
    }
}