<?php

namespace Columbus\Contracts\Tours\Http\Controllers;

use App\Http\Controllers\ApiController;
use Columbus\Contracts\Tours\Models\VendorTour;
use Columbus\Utilities\FileManager;
use Illuminate\Http\Request;


class VendorTourController extends ApiController
{
    
    function index(Request $request)
    {
        $tourVendor = VendorTour::where('vendor_id', $request->get('vendor_id'))
                        /*->where('city_id', $request->get('city_id'))*/
                        ->get();
        return $this->ok($tourVendor , "Tour child ages");  
    }

    public function create(Request $request)
    {
        return $this->ok(VendorTour::create($request->all()), "Tour Vendor created");
    }

    public function update($tour_vendor_id, Request $request) 
    {
        $tourVendor = VendorTour::find($tour_vendor_id);

        if($tourVendor) {
            $tourVendor->update($request->all());
        }
        return $this->ok($tourVendor, "Vendor Details updated");
    }

    public function uploadGallery($tour_vendor_id, Request $request, FileManager $fileManager)
    {
        $tourVendor = VendorTour::find($tour_vendor_id);
        
        $gallery = [];

        if($tourVendor && $tourVendor->gallery)  {
            $gallery = $tourVendor->gallery;
        }
        
        if ($request->hasFile('images')) {
            $country_id = $request->get('country_id');
                        
            $path = "uploads/images/countries/$country_id/tours/vendors/$tourVendor->id";

            $gallery = array_merge($gallery, $fileManager->savePhotos($path , $request->file('images')));

        }
        
        $tourVendor->update(['gallery'=> $gallery]);
        

        return $this->ok($tourVendor, "Tour Vendor gallery updated");
    }
}