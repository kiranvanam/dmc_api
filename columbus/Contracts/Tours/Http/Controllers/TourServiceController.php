<?php

namespace Columbus\Contracts\Tours\Http\Controllers;

use App\Http\Controllers\ApiController;
use Columbus\Contracts\Tours\Models\Service;
use Illuminate\Http\Request;


class TourServiceController extends ApiController
{
    
    
    public function create(Request $request)
    {
        $service = Service::create($request->all());
        return $this->ok($service, "Service added");
    }

    public function update($service_id, Request $request)
    {
        $service = Service::find($service_id);

        if($service) {
            $service->update($request->all());
        }

        return $this->ok($service, "Service updated");
    }

    public function delete($service_id)
    {
        $service = Service::find($service_id);
        if($service) {
            $service->delete();
        }
        return $this->ok($service, "Service deleted");
    }
}