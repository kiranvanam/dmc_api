<?php

namespace Columbus\Contracts\Tours\Http\Controllers;

use App\Http\Controllers\ApiController;
use Illuminate\Database\Eloquent\SoftDeletes;
use Columbus\Contracts\Tours\Models\TourTitleVendorDetail;

class TourTitleVendorDetailController extends ApiController
{

    public function index($vendor_id)
    {
        $vendorToursList = TourTitleVendorDetail::where('vendor_id', $vendor_id)->get();

        return $this->ok($vendorToursList, "Vendor tours list");
    }

    public function create($vendor_id)
    {
        $vendorTourDetails = TourTitleVendorDetail::create(request()->all());

        return $this->ok($vendorTourDetails, "Tour Details created");
    }

    public function update($vendor_id, $tour_title_vendor_detail_id)
    {
        $vendorTourDetails = TourTitleVendorDetail::findOrFail($tour_title_vendor_detail_id);

        $vendorTourDetails->update(request()->all());

        return $this->ok($vendorTourDetails, "Tour details updated");
    }

    public function updateRequiredServices($vendor_id, $tour_title_vendor_detail_id)
    {
        $vendorTourDetails = TourTitleVendorDetail::findOrFail($tour_title_vendor_detail_id);

        $vendorTourDetails->update(['booking_required_private_services' => request('booking_required_private_services')]);

        return $this->ok($vendorTourDetails, "Required Booking Services updated");
    }

    public function updateFilters( $vendor_id, $tour_title_vendor_detail_id)
    {
        $vendorTourDetails = TourTitleVendorDetail::findOrFail($tour_title_vendor_detail_id);

        $data = [];
        $data['filters'] = request('filters');

        $vendorTourDetails->update($data);

        return $this->ok($vendorTourDetails, "Tour Filters updated");
    }

    public function updateSlotDetails($vendor_id, $tour_title_vendor_detail_id)
    {
        $vendorTourDetails = TourTitleVendorDetail::findOrFail($tour_title_vendor_detail_id);

        $data = [];
        $data['slot_details'] = request('slot_details');

        $vendorTourDetails->update($data);

        return $this->ok($vendorTourDetails, "Slot Details updated for Tour");
    }

    public function updateAddons($vendor_id, $tour_title_vendor_detail_id)
    {
        $vendorTourDetails = TourTitleVendorDetail::findOrFail($tour_title_vendor_detail_id);

        $data = [];
        $data['add_ons'] = request('add_ons');

        $vendorTourDetails->update($data);

        return $this->ok($vendorTourDetails, "Tour add-ons updated");
    }

}