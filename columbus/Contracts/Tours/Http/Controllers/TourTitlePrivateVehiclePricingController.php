<?php

namespace Columbus\Contracts\Tours\Http\Controllers;

use App\Http\Controllers\ApiController;
use Columbus\Contracts\Tours\Models\TourTitlePrivateVehiclePricing;
use Columbus\Utilities\DateUtil;
use Illuminate\Http\Request;

class TourTitlePrivateVehiclePricingController extends ApiController
{
    protected $weekDaysPricingDetails = [];

    public function pricingForRange( Request $request)
    {
        $tour_title_private_service_mapping_id = 
                            $request->tour_title_private_service_mapping_id;

        $from_date = DateUtil::createCarbonIfNotCarbon($request->from_date)->toDateString();
        $to_date = DateUtil::createCarbonIfNotCarbon($request->to_date)->toDateString();

        $pricing = TourTitlePrivateVehiclePricing
                            ::where('tour_title_private_service_mapping_id', $tour_title_private_service_mapping_id)
                            ->where('date', '>=', $from_date)
                            ->where('date','<=', $to_date)
                            ->orderBy('date')
                            ->get();
        return $this->ok($pricing, "Pricing Details for the given range");
    }

    public function update( Request $request)
    {
        $tour_title_private_service_mapping_id = 
                            $request->tour_title_private_service_mapping_id;
        $this->weekDaysPricingDetails = $request->all();
        $this->weekDaysPricingDetails['tour_title_private_service_mapping_id'] = $tour_title_private_service_mapping_id;        
        //return $this->ok($this->weekDaysPricingDetails);
        
        $dates = DateUtil::datesForRange(
                                $request->get("from_date"), 
                                $request->get('to_date'), 
                                $this->callBackFunction()
                            );
        return $this->ok($dates , "Pricing updated for given ranges");
    }

    //'meal_vendor_plan_id', 'date', 'time_slots_with_pricing_details', 'allocation', 'is_available', 'is_on_request'
    public function callBackFunction()
    {
        return function($date) {
            $dayDetails = $this->getDayDetails($date);
            //$vehicle_id = $this->weekDaysPricingDetails['vehicle_id'];
            if(empty($dayDetails) == false) {
                $meal_vendor_plan_pricing = TourTitlePrivateVehiclePricing
                                    ::where('tour_title_private_service_mapping_id', 
                                            $dayDetails['tour_title_private_service_mapping_id'])
                                      ->where('date', $dayDetails['date'])
                                      ->first();

                if(empty($meal_vendor_plan_pricing)) {
                   $meal_vendor_plan_pricing = TourTitlePrivateVehiclePricing::create($dayDetails);                
                }else {
                    $meal_vendor_plan_pricing->update($dayDetails);
                }
                return $meal_vendor_plan_pricing;
            }
            return null;
        };
    }
    /* the data format

    meal_vendor_plan_id: ''
    operating_days {
                    index_for_source_to_copy_day_details:-1,

                    is_available:true,
                    is_on_request:false,
                    
                    week_day_name: this.calendar.weekDays()[week_day_index],
                    week_day_index, 

                    time_slots:[ 
                        {
                            start_time:'', 
                            end_time:'',
                            allocation: '',
                            is_available: true,
                            is_on_request: false,
                            adult_price:'',
                            child_price:'',
                            infant_price:''
                        }
                    ], 
                    allocation:0
    }
    */
    public function getDayDetails($date)
    {
        $week_day_index = $date->dayOfWeek;
        
        //$allSlotsTotalAllocationsForDay = 0;

        foreach ($this->weekDaysPricingDetails['operating_days'] as  $dayDetails) {
             if($dayDetails['week_day_index'] == $week_day_index) {

                $dayDetails['date'] = $date->toDateString();
                $dayDetails['tour_title_private_service_mapping_id'] = $this->weekDaysPricingDetails['tour_title_private_service_mapping_id'];
                $dayDetails['currency'] = $this->weekDaysPricingDetails['currency'];                
                return $dayDetails;
             }   
        }

        return null;
    }
}