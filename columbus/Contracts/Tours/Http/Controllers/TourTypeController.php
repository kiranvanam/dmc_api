<?php

namespace Columbus\Contracts\Tours\Http\Controllers;

use App\Http\Controllers\ApiController;
use Columbus\Contracts\Tours\Models\Type;
use Illuminate\Http\Request;


class TourTypeController extends ApiController
{
    
    function index($destination_id = null)
    {
        return $this->ok(Type::all() , "Tour child ages");  
    }

    public function create(Request $request)
    {
        $data = $this->addSlug($request->all());
        $type = Type::create($data);
        return $this->ok(Type::fetchWithSubTypes(), "Type added");
    }

    public function update($type_id, Request $request)
    {
        $type = Type::find($type_id);

        if($type) {
            $data = $this->addSlug($request->all());
            $type->update($data);
        }

        return $this->ok(Type::fetchWithSubTypes(), "Type updated");
    }

    public function delete($type_id)
    {
        $type = Type::find($type_id);
        if($type) {
            Type::where('parent_id', $type->id)->get()->each(function($subType){
                $subType->delete();
            });
            $type->delete();
        }
        return $this->ok(Type::fetchWithSubTypes(), "Type deleted");
    }

    public function addSlug(Request $request)
    {
        $data = $request->all();
        if(isset($data['name']) && !empty($data['name']) )
            $data['slug'] = str_slug($data['name']);

        return $data;
    }
}