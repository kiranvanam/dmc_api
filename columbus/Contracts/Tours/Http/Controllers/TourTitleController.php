<?php

namespace Columbus\Contracts\Tours\Http\Controllers;

use App\Http\Controllers\ApiController;
use Columbus\Contracts\Tours\Models\TourTitle;
use Columbus\Contracts\Tours\Http\Requests\TourTitleRequest;

class TourTitleController extends ApiController
{
    function index()
    {
        $city_id = request()->get('city_id');
        $tourTitles = TourTitle::withTrashed()
                            ->where('city_id', $city_id)
                            ->get();
        return $this->ok( $tourTitles , "Tour titles list");  
    }

    public function create(TourTitleRequest $request)
    {   
        $data = $request->all();
        
        $errorMessages = [];
        foreach($data['types'] as $type) {
            $data['type'] = $type;
            
            if($errorMessage=TourTitle::isDuplicate($data))
                    $errorMessages[] = $errorMessage;
        }
        if(empty($errorMessages))
            return $this->ok($request->save(), "Tour title(s) Created");
            
        return $this->validationErrors($errorMessages, "Duplicate Tour creation is not allowed");
    }

    public function update($title_id, TourTitleRequest $request)
    {
        $data = $request->all();
        
        $errorMessages = [];
        foreach($data['types'] as $type) {
            $data['type'] = $type;
            
            if($errorMessage=TourTitle::isDuplicate($data))
                    $errorMessages[] = $errorMessage;
        }
        if(empty($errorMessages))
            return $this->ok($request->update($title_id) , "Tour title updated");
        
        return $this->validationErrors($errorMessages, "Duplicate Tour creation is not allowed");
        
    }

    public function updateThumbnail($title_id)
    {
        $ticket = TourTitle::findOrFail($title_id);
        $ticket->update(['thumbnail' => request('thumbnail')]);
        return $this->ok($ticket, "Thumbnail is updated");
    }

    public function delete($title_id)
    {
        $tour = TourTitle::findOrFail($title_id);

        $tour->delete();
        
        return $this->ok($tour, "Tour title deleted");
    }

    public function restore($title_id)
    {
        $tourTitle = TourTitle::onlyTrashed()
                            ->where('id', $title_id)
                            ->first();

        if(empty($tourTitle)) {
            return $this->notFound("Tour title not found");
        }

        $tourTitle->restore();
        return $this->ok($tourTitle, "Tour title restored");
    }
}