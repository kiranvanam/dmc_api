<?php

namespace Columbus\Contracts\Tours\Http\Controllers;

use App\Http\Controllers\ApiController;
use Columbus\Contracts\Tours\Models\Theme;
use Illuminate\Http\Request;


class ThemeController extends ApiController
{
    
    function index()
    {
        return $this->ok(Theme::all() , " Tour themes list");  
    }

    public function create(Request $request)
    {
        $theme = Theme::create($request->all());
        return $this->ok($theme, "Theme added");
    }

    public function update($theme_id, Request $request)
    {
        $theme = Theme::find($theme_id);

        if($theme) {
            $theme->update($request->all());
        }

        return $this->ok($theme, "Theme updated");
    }

    public function delete($theme_id)
    {
        $theme = Theme::find($theme_id);

        if($theme) {
            $theme->delete();
        }
        return $this->ok($theme, "Theme deleted");
    }
}