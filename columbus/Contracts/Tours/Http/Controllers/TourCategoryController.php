<?php

namespace Columbus\Contracts\Tours\Http\Controllers;

use App\Http\Controllers\ApiController;
use Columbus\Contracts\Tours\Models\Category;
use Illuminate\Http\Request;


class TourCategoryController extends ApiController
{
    
    function index($destination_id = null)
    {
        return $this->ok(Category::all() , "Tour child ages");  
    }

    public function create(Request $request)
    {
        $type = Category::create($request->all());
        return $this->ok($type, "Category added");
    }

    public function update($type_id, Request $request)
    {
        $type = Category::find($type_id);

        if($type) {
            $type->update($request->all());
        }

        return $this->ok($type, "Category updated");
    }

    public function delete($category_id)
    {
        $type = Category::find($category_id);
        if($type) {
            $type->delete();
        }
        return $this->ok($type, "Category deleted");
    }
}