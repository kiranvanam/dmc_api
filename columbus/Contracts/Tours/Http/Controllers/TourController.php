<?php

namespace Columbus\Contracts\Tours\Http\Controllers;

use App\Http\Controllers\ApiController;
use Columbus\Contracts\Tours\Models\Tour;
use Columbus\Utilities\FileManager;
use Illuminate\Http\Request;


class TourController extends ApiController
{
    
    function index()
    {
        $city_id = request()->get('city_id');
        return $this->ok( Tour::where('city_id', $city_id)->get() , "Tour list");  
    }

    public function create(Request $request)
    {
        $data = $this->addSlug($request);
        
        return $this->ok(Tour::create($data), "Tour Created");
    }

    public function update($tour_id, Request $request)
    {
        
        $tour = Tour::find($tour_id);
        
        if($tour) {
            $data = $this->addSlug($request);
            $tour->update($data);
        }

        return $this->ok($tour, "Tour updated");
    }

    public function updateSic($tour_id)
    {
        $tour = Tour::find($tour_id);
        
        if($tour) {
            $tour->is_sic = !$tour->is_sic;
            $tour->save();
        }
        return $this->ok($tour, "Tour Sic status updated");
    }

    public function updatePrivate($tour_id)
    {
        $tour = Tour::find($tour_id);
        
        if($tour) {
            $tour->is_private = !$tour->is_private;
            $tour->save();
        }
        return $this->ok($tour, "Tour Private status updated");
    }

    public function delete($tour_id, Request $request)
    {
        $tour = Tour::find($tour_id);

        if($tour) {
            $tour->delete();
        }
        return $this->ok($tour, "Tour deleted");
    }

    public function uploadGallery($tour_id, Request $request, FileManager $fileManager)
    {
        $tour = Tour::find($tour_id);
        
        $gallery = [];

        if($tour && $tour->gallery)  {
            $gallery = $tour->gallery;
        }
        
        if ($request->hasFile('images')) {
            $country_id = $request->get('country_id');
            $vendor_id = $request->get('vendor_id');
            
            $path = "uploads/images/countries/$country_id/tours/$tour->id";

            $gallery = array_merge($gallery, $fileManager->savePhotos($path , $request->file('images')));

        }
        
        $tour->update(['gallery'=> $gallery]);
        

        return $this->ok($tour, "Tickets gallery updated");
    }

    public function addSlug(Request $request)
    {
        $data = $request->all();
        if(isset($data['name']) && !empty($data['name']) )
            $data['slug'] = str_slug($data['name']);

        return $data;
    }
}