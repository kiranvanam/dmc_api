<?php

namespace Columbus\Contracts\Tours\Http\Controllers;

use App\Http\Controllers\ApiController;


class TypeController extends ApiController
{
    
    function index($destination_id = null)
    {
        return $this->ok( "tours", "Tour list");  
    }
}