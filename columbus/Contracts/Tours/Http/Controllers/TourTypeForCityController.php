<?php

namespace Columbus\Contracts\Tours\Http\Controllers;

use App\Http\Controllers\ApiController;
use Columbus\Contracts\Tours\Models\TourTypeForCity;
use Illuminate\Http\Request;


class TourTypeForCityController extends ApiController
{
    
    function index()
    {
        $city_id = request()->get('city_id');
        return $this->ok(
                TourTypeForCity::fetchWithSubTypesForCity($city_id), 
                "Tour types");  
    }

    public function create(Request $request)
    {
        $data = $this->addSlug($request);
        $type = TourTypeForCity::create($data);
        return $this->ok(TourTypeForCity::fetchWithSubTypesForCity($type->city_id), 
                         "Tour Type added"
                         );
    }

    public function update($type_id, Request $request)
    {
        $data = $this->addSlug($request);
        $type = TourTypeForCity::find($type_id);

        if($type) {
            $type->update($data);
        }

        return $this->ok(TourTypeForCity::fetchWithSubTypesForCity($type->city_id), "Tour  Type updated");
    }

    public function delete($type_id)
    {
        $type = TourTypeForCity::find($type_id);
        if($type) {
            TourTypeForCity::where('parent_id', $type->id)
                            ->get()
                            ->each(function($subType){
                                $subType->delete();
                            });            
            $type->delete();
        }
        return $this->ok(TourTypeForCity::fetchWithSubTypesForCity($type->city_id), "Tour Type deleted");
    }

    public function addSlug(Request $request)
    {
        $data = $request->all();
        if(isset($data['name']) && !empty($data['name']) )
            $data['slug'] = str_slug($data['name']);

        return $data;
    }
}