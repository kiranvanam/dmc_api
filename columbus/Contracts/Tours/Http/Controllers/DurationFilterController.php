<?php

namespace Columbus\Contracts\Tours\Http\Controllers;

use App\Http\Controllers\ApiController;
use Columbus\Contracts\Tours\Models\DurationFilter;
use Illuminate\Http\Request;


class DurationFilterController extends ApiController
{
    

    public function create(Request $request)
    {
        $durationFilter = DurationFilter::create($request->all());
        return $this->ok($durationFilter, "Duration Filter added");
    }

    public function update($filter_id, Request $request)
    {
        $durationFilter = DurationFilter::find($filter_id);

        if($durationFilter) {
            $durationFilter->update($request->all());
        }

        return $this->ok($durationFilter, "Duration Filter updated");
    }

    public function delete($filter_id)
    {
        $durationFilter = DurationFilter::find($filter_id);
        if($durationFilter) {
            $durationFilter->delete();
        }
        return $this->ok($durationFilter, "Duration Filter deleted");
    }
}