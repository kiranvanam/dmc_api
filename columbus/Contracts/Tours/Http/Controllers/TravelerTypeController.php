<?php

namespace Columbus\Contracts\Tours\Http\Controllers;

use App\Http\Controllers\ApiController;
use Columbus\Contracts\Tours\Models\TravelerType;
use Illuminate\Http\Request;


class TravelerTypeController extends ApiController
{
    
    function index()
    {
        return $this->ok(TravelerType::all() , " Tour Traveler types list");  
    }

    public function create(Request $request)
    {
        $t_type = TravelerType::create($request->all());
        return $this->ok($t_type, "TravelerType added");
    }

    public function update($t_type_id, Request $request)
    {
        $t_type = TravelerType::find($t_type_id);

        if($t_type) {
            $t_type->update($request->all());
        }

        return $this->ok($t_type, "TravelerType updated");
    }

    public function delete($t_type_id)
    {
        $t_type = TravelerType::find($t_type_id);

        if($t_type) {
            $t_type->delete();
        }
        return $this->ok($t_type, "TravelerType deleted");
    }
}