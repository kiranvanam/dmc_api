<?php

namespace Columbus\Contracts\Tours\Http\Requests;

use Columbus\Contracts\Tours\Models\ChildAge;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class ChildAgeCreateRequest extends FormRequest
{
    
    public function authorize()
    {
        return true;
    }

    
    public function rules()
    {
        return [
            'country_id' => 'required',
            'min_age' => 'required',
            'max_age' => 'required'
        ];
    }

    protected function prepareForValidation()
    {
    }

    
    public function messages()
    {
        return [
            'country_id.required' => "Country is not selected",
            'min_age.required' => "From Age is not selected",
            'max_age.required' => "To Age is not selected",
        ];
    }

    public function save()
    {
        return ChildAge::create($this->all());
    }

    public function update($child_age_id)
    {
        $childAge = ChildAge::find($child_age_id);
        if($childAge) {
            $childAge->update($this->all());
        }
        return $childAge;
    }


}
