<?php

namespace Columbus\Contracts\Tours\Http\Requests;

use Columbus\SystemSettings\Models\Code;
use Illuminate\Foundation\Http\FormRequest;
use Columbus\Contracts\Tours\Models\TourTitle;

class TourTitleRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    
    public function rules()
    {
        return [
            'city_id' => 'required',
            'name' => 'required',
            'types' => 'required|array|min:1',            
        ];
    }

    protected function prepareForValidation()
    {
    }

    
    public function messages()
    {
        return [
            'city_id.required' => "City is not selected",
            'name.required' => "Tour title/name must be provided",
            'types.required' => "Tour type(SIC/Private) must be provided",
            "types.array" => "Tour type(SIC/Private) must be provided",
            "types.min"=> "Tour type(SIC/Private) must be provided"
        ];
    }

    public function save()
    {
        $data = $this->all();
        $tourTitles =[];
        foreach( $data['types'] as $type ) {
            $data['code'] = Code::getNextCodeForModule("tours");
            $data['type'] = $type;
            $tourTitles[] = TourTitle::create($data);
        }
        return $tourTitles;
    }


    public function update($title_id)
    {
        $tourTitle = TourTitle::find($title_id);
        if($tourTitle) {
            $tourTitle->update($this->all());
        }
        return $tourTitle;
    }

}