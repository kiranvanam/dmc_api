<?php

namespace Columbus\Contracts\Tours\Models;

use Illuminate\Database\Eloquent\Model;


class TourTitlePrivateVehiclePricing extends Model
{
    protected $table = "tour_title_private_vehicle_pricing";

    protected $fillable = [ 'tour_title_private_service_mapping_id',
                            'date', 'inventory', 'price', 'currency', 'is_available', 'is_on_request'];

    protected $dates = ['date'];

}
