<?php

namespace Columbus\Contracts\Tours\Models;

use Illuminate\Database\Eloquent\Model;


class Category extends Model
{
    public $table = "tour_categories";
    public $timestamps = false;

    protected $fillable = ['name'];
}
