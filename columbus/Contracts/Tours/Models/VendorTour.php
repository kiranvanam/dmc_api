<?php

namespace Columbus\Contracts\Tours\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class VendorTour extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $fillable = ['city_id','vendor_id', 'tour_id', 'code', 'is_sic', 'is_private' ,'itinerary_details' , 'inclusions', 'exclusions', 'terms_and_conditions', 'cancellation_policies', 'cancellation_policy_rules', 'gallery', 'filters'];

    protected $casts = [
        'gallery' => 'array',
        "filters" => "array",
        'cancellation_policy_rules' => 'array'
    ];    
}
