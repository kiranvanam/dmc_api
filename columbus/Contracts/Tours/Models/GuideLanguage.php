<?php

namespace Columbus\Contracts\Tours\Models;

use Illuminate\Database\Eloquent\Model;


class GuideLanguage extends Model
{
    protected $table = "tour_guide_languages";

    public $timestamps = false;

    protected $fillable = ['name'];   
}
