<?php

namespace Columbus\Contracts\Tours\Models;

use Illuminate\Database\Eloquent\Model;


class Tour extends Model
{
   

    protected $fillable = ['city_id', 'name', 'slug', 'is_sic', 'is_private', 'private_tour_vendor_service_ids',
                        'private_tour_guide_title_ids', 'private_tour_ticket_ids', 'description', 'gallery'];

    protected $casts = [
        "gallery" => "array",
        "private_tour_vendor_service_ids" => "array",
        "private_tour_guide_title_ids" => "array",
        "private_tour_ticket_ids" => "array"
    ];

}
