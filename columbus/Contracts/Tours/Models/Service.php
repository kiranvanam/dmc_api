<?php

namespace Columbus\Contracts\Tours\Models;

use Illuminate\Database\Eloquent\Model;


class Service extends Model
{
    protected $table = "tour_services";

    public $timestamps = false;

    protected $fillable = ['vendor_service_id', 'name'];   
}
