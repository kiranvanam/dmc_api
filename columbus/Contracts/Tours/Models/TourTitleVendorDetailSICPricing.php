<?php

namespace Columbus\Contracts\Tours\Models;

use Illuminate\Database\Eloquent\Model;

class TourTitleVendorDetailSICPricing extends Model
{
    public $table = "tour_title_vendor_detail_sic_pricing";

    protected $fillable = ['tour_title_vendor_detail_id', 'date',
                            'time_slots_with_pricing_details', 'currency',
                            'is_available', 'is_on_request'];

    protected $dates = ['date'];

    protected $casts = [
        "time_slots_with_pricing_details" => "array"
    ];

}