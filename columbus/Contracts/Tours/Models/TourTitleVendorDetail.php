<?php

namespace Columbus\Contracts\Tours\Models;

use App\CancellationPolicy;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TourTitleVendorDetail extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $fillable = ['tour_title_id', 'vendor_id', 'vendor_code',
                            'booking_required_private_services',
                          'itinerary', 'inclusions', 'exclusions', 'terms_and_conditions', 'remarks',
                         'filters', 'add_ons',
                         'slot_details'
                        ];
    protected $casts = [
        "booking_required_private_services" => "array",
        'filters' => 'array',
        'add_ons' => 'array',
        'slot_details' => 'array'
    ];

    /* START- used in Tour search  */

        /* for SIC  */
    function pricing()
    {
        return $this->hasOne(TourTitleVendorDetailSICPricing::class, 'tour_title_vendor_detail_id');
    }

        /* START- for Private  */
    function daySlots()
    {
        return $this->hasOne(TourTitlePrivateAvailability::class, 'tour_title_vendor_detail_id');
    }

    function availableServices()
    {
        return $this->hasMany(TourTitlePrivateServiceMapping::class, 'tour_title_vendor_detail_id');
    }
        /* END - for Private */


    /* END - used in Tour search  */

    function cancellationPolicies()
    {
        return $this->hasOne(CancellationPolicy::class, 'service_id')->where('service', 'tour-title-vendor');
    }

}