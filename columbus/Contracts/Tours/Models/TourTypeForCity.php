<?php

namespace Columbus\Contracts\Tours\Models;

use Illuminate\Database\Eloquent\Model;


class TourTypeForCity extends Model
{
    public $table = "tour_types_for_city";
    public $timestamps = false;

    protected $fillable = ['city_id', 'name', 'slug',  'parent_id'];

    public static function fetchWithSubTypesForCity($city_id)
    {
        return self::where('city_id', $city_id)
                    ->where('parent_id', null)
                    ->with('subTypes')
                    ->get();
    }

    public function subTypes()
    {
        return $this->hasMany(self::class, 'parent_id')->with('subTypes');
    }
}
