<?php

namespace Columbus\Contracts\Tours\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TourTitle extends Model
{
    use SoftDeletes;

    protected $fillable = ['country_id', 'city_id', 'name', 'code', 'type', 'is_available', 'description', 'thumbnail'];

    protected $dates = ['deleted_at'];

    /* only for SIC useful, as there are no inclusons, excluins, t_and_c and remarks for Private tour */
    public function vendor()
    {
        return $this->hasOne(TourTitleVendorDetail::class, 'tour_title_id');
    }
    
    public static function isDuplicate($data)
    {        
        // check if <tour,type> is created 
        $tourTitle = TourTitle::withTrashed()
                    ->where('name', $data['name'])
                    ->where('city_id', $data['city_id'])
                    ->where('type', $data['type']);
        if( isset($data['id'])) 
            $tourTitle = $tourTitle->where('id', '!=', $data['id']);
            
        $tourTitle = $tourTitle->first();
        
        if( empty($tourTitle))
        return null;
        
        logger("Trying to create duplicate Tour: ", $data);

        return  [ 'Tour Duplicaiton' => 
                    $data['name'] . " of ". $data['type'] ."-type is already created for this city"
                ];            

    }

    public function isSIC()
    {
        return $this->type == "sic";        
    }

    public function isPrivate()
    {
        return $this->type == "private";
    }

    public static function byCity($city_id, $types = [ "sic", "private"])
    {
        return static::where('city_id', $city_id)->whereIn('type', $types)->get();
    }  

}