<?php

namespace Columbus\Contracts\Tours\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Filter extends Model
{
    use SoftDeletes;

    public $table = "tour_filters";
    
    protected $dates = ['deleted_at'];

    protected $fillable = ['country_id', 'groupby', 'name'];
}