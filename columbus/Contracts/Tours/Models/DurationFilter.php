<?php

namespace Columbus\Contracts\Tours\Models;

use Illuminate\Database\Eloquent\Model;


class DurationFilter extends Model
{
    public $table = "tour_duration_filters";
    public $timestamps = false;

    protected $fillable = ['name'];
}
