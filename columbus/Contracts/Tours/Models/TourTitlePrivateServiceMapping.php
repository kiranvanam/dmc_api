<?php

namespace Columbus\Contracts\Tours\Models;

use Illuminate\Database\Eloquent\Model;

class TourTitlePrivateServiceMapping extends Model
{
    protected $table = "tour_title_private_vendor_detail_service_mappings";
    
    public $timestrmps = false;
    
    protected $fillable = ['tour_title_vendor_detail_id', 'service_id', 'service'];
}