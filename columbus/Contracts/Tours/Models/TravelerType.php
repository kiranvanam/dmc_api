<?php

namespace Columbus\Contracts\Tours\Models;

use Illuminate\Database\Eloquent\Model;


class TravelerType extends Model
{
    public $table = "tour_traveler_types";
    public $timestamps = false;

    protected $fillable = ['name'];
}
