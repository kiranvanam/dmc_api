<?php

namespace Columbus\Contracts\Tours\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class VendorTourPricing extends Model
{   
    use SoftDeletes;
    
    protected $fillable = ['vendor_tour_id', 'date', 'time_slots_with_pricing_details', 'is_available', 
                            'is_on_request'];

    protected $dates = ['date', 'deleted_at'];

    protected $casts = [
        "time_slots_with_pricing_details" => "array"
    ];
}
