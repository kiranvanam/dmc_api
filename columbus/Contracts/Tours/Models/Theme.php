<?php

namespace Columbus\Contracts\Tours\Models;

use Illuminate\Database\Eloquent\Model;


class Theme extends Model
{
    public $table = "tour_themes";
    public $timestamps = false;

    protected $fillable = ['name'];
}
