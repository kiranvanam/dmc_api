<?php

namespace Columbus\Contracts\Tours\Models;

use Illuminate\Database\Eloquent\Model;


class TourTitlePrivateAvailability extends Model
{
    protected $table = "tour_title_private_vendor_detail_availability";

    //public $timestamps = false;

    protected $fillable = ['tour_title_vendor_detail_id', 
                    'date', 'is_available', 'is_on_request', 'time_slots'
                ];

    protected $dates = ['date'];

    protected $casts = [
        "time_slots" => "array"
    ];

}
