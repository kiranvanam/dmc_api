<?php

namespace Columbus\Contracts\Tours\Models;

use Illuminate\Database\Eloquent\Model;


class Type extends Model
{
    public $table = "tour_types";
    public $timestamps = false;

    protected $fillable = ['name', 'parent_id'];

    public static function fetchWithSubTypes()
    {
        return self::where('parent_id',null)->with('subTypes')->get();
    }

    public function subTypes()
    {
        return $this->hasMany(self::class, 'parent_id')->with('subTypes');
    }
}
