<?php
namespace Columbus\Contracts\Tours\Models;

use Columbus\Contracts\Tours\Models\Tour;
use Illuminate\Database\Eloquent\Model;

class TourMainType extends Model
{

    protected $fillable = ['city_id', 'name', 'slug'];

    public function tours()
    {
        return $this->hasMany(Tour::class, 'tour_main_type_id');
    }
}
