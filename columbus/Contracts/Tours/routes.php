<?php

Route::group(['prefix'=>'/api', 'middleware'=>['auth:api','cors']], function() {

    Route::get('/tour-settings', 'TourSettingsController@index');

    Route::group(['prefix'=> 'tour-services'], function(){
        Route::post('/', 'TourServiceController@create');
        Route::put('/{category_id}', 'TourServiceController@update');
        Route::delete('/{category_id}', 'TourServiceController@delete');
    });

    Route::group(['prefix'=>'tour-categories'], function (){
        Route::post('/', 'TourCategoryController@create');
        Route::put('/{category_id}', 'TourCategoryController@update');
        Route::delete('/{category_id}', 'TourCategoryController@delete');
    });
    Route::group(['prefix'=> 'tour-guide-languages'], function(){
        Route::post('/', 'GuideLanguageController@create');
        Route::put('/{category_id}', 'GuideLanguageController@update');
        Route::delete('/{category_id}', 'GuideLanguageController@delete');
    });

    Route::group(['prefix'=> 'tour-duration-filters'], function(){
        Route::post('/', 'DurationFilterController@create');
        Route::put('/{category_id}', 'DurationFilterController@update');
        Route::delete('/{category_id}', 'DurationFilterController@delete');
    });

    Route::group(['prefix'=>'tour-types'], function() {
        //Route::get('/', 'TourTypeController@index');
        Route::post('/', 'TourTypeController@create');
        Route::put('/{id}', 'TourTypeController@update');
        Route::delete("/{id}", 'TourTypeController@delete');
    });
    Route::group(['prefix'=>'tour-types-for-city'], function() {
        Route::get('/', 'TourTypeForCityController@index');
        Route::post('/', 'TourTypeForCityController@create');
        Route::put('/{id}', 'TourTypeForCityController@update');
        Route::delete("/{id}", 'TourTypeForCityController@delete');
    });

    Route::group(['prefix'=>'tour-themes'], function() {
        //Route::get('/', 'ThemeController@index');
        Route::post('/', 'ThemeController@create');
        Route::put('/{id}', 'ThemeController@update');
        Route::delete("/{id}", 'ThemeController@delete');
    });

    Route::group(['prefix'=>'tour-traveler-types'], function() {
        //Route::get('/', 'TravelerTypeController@index');
        Route::post('/', 'TravelerTypeController@create');
        Route::put('/{id}', 'TravelerTypeController@update');
        Route::delete("/{id}", 'TravelerTypeController@delete');
    });

    //Tour filters
    Route::group(['prefix' => "tour-filters"], function() {
        Route::get("", "TourFilterController@index");
        Route::post("", "TourFilterController@create");
        Route::put("{filter_id}", "TourFilterController@update");
        Route::delete("{filter_id}", "TourFilterController@delete");
        Route::put("{filter_id}/restore", "TourFilterController@restore");

        Route::post("update-filter-group-heading", 'TourFilterController@updateFilterGroupLabel');
    });

    Route::group(['prefix'=>'tour-titles'], function() {
        Route::get("", 'TourTitleController@index');
        Route::post("", 'TourTitleController@create');
        Route::put("{title_id}", 'TourTitleController@update');
        Route::delete("{title_id}", "TourTitleController@delete");
        Route::put("{title_id}/restore", 'TourTitleController@restore');

        Route::put('/{id}/thumbnail', 'TourTitleController@updateThumbnail');

    });
    Route::group(['prefix'=>"vendors/{vendor_id}/tour-titles"], function() {
        Route::get("", 'TourTitleVendorDetailController@index');

        Route::post("", "TourTitleVendorDetailController@create");
        Route::group(["prefix"=>"{tour_title_vendor_detail_id}"], function() {
            Route::put("", "TourTitleVendorDetailController@update");
            Route::put("filters", "TourTitleVendorDetailController@updateFilters");
            Route::put("slot-details", "TourTitleVendorDetailController@updateSlotDetails");
            Route::put("required-services", "TourTitleVendorDetailController@updateRequiredServices");
            Route::put("add-ons", "TourTitleVendorDetailController@updateAddons");
            Route::post("gallery", "TourTitleVendorDetailController@updateGallery");


            Route::group(['prefix' => "cancellation-policies"], function(){
                Route::get("", 'TourTitleVendorDetailCancellationPolicyController@index');
                Route::post("", "TourTitleVendorDetailCancellationPolicyController@create");
                Route::put("{cancellation_policy_id}", 'TourTitleVendorDetailCancellationPolicyController@update');
                Route::delete("{cancellation_policy_id}", "TourTitleVendorDetailCancellationPolicyController@delete");
            });

            Route::post("pricing", "TourTitleVendorDetailSICPricingController@pricingForRange");
            Route::put("pricing", "TourTitleVendorDetailSICPricingController@update");

            /* start- Private tours */
            Route::post("availability", 'TourTitlePrivateAvailabilityController@pricingForRange');
            Route::put("availability", 'TourTitlePrivateAvailabilityController@update');

            Route::get("service-mappings", 'TourTitlePrivateVendorServiceMappingController@index');
            Route::post("service-mappings", 'TourTitlePrivateVendorServiceMappingController@create');

            Route::group(['prefix'=>'service-mappings/{mapping_id}'], function(){
                Route::post("vehicle-pricing", 'TourTitlePrivateVehiclePricingController@pricingForRange');
                Route::put("vehicle-pricing", 'TourTitlePrivateVehiclePricingController@update');
            });
            /* END - Private tours */
        });
    });

    /*Route::group(['prefix'=>'tours'], function() {
        Route::get('', 'TourController@index');
        Route::post("", 'TourController@create');
        Route::put("/{tour_id}", 'TourController@update');
        Route::post("/{tour_id}/update-sic-status", 'TourController@updateSic');
        Route::post("/{tour_id}/update-private-status",'TourController@updatePrivate');
        Route::delete("/{tour_id}", "TourController@delete");
        Route::post("{tour_id}/gallery", 'TourController@uploadGallery');

        Route::group(['namespace'=>'PrivateTours'], function(){

            Route::post("/{tour_id}/private-tour-availability", 'TourAvailabiltiyController@pricingForRange');
            Route::put("/{tour_id}/private-tour-availability", 'TourAvailabiltiyController@update');

            Route::post("/{tour_id}/vehicle-availability/{vehicle_id}", 'VehicleAvailabilityController@pricingForRange');
            Route::put("/{tour_id}/vehicle-availability/{vehicle_id}", 'VehicleAvailabilityController@update');

        });
    });*/


    Route::group(["prefix" => "vendor-tours",], function(){
        Route::get("", "VendorTourController@index");
        Route::post("",'VendorTourController@create');
        Route::put("{tour_vendor_id}",'VendorTourController@update');

        Route::post("{vendor_tour_id}/pricing", 'VendorTourPricingController@pricingForRange');
        Route::put("{vendor_tour_id}/pricing", 'VendorTourPricingController@update');

        Route::post("{tour_vendor_id}/gallery",'VendorTourController@uploadGallery');

    });

});

Route::get('/tour-types', 'TourSettingsController@index');
