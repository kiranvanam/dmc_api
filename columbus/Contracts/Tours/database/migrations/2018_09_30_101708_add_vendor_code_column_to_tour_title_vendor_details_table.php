<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddVendorCodeColumnToTourTitleVendorDetailsTable extends Migration
{
    public function up()
    {
        Schema::table('tour_title_vendor_details', function (Blueprint $table) {
            $table->string('vendor_code', 25)->default("")->after('vendor_id');
        });
    }
 
    public function down()
    {
        Schema::table('tour_title_vendor_details', function (Blueprint $table) {
            $table->dropColumn('vendor_code');
        });
    }
}
