<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTourTitleVendorDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tour_title_vendor_details', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('tour_title_id')->index();
            $table->integer('vendor_id')->index();
            $table->text('itinerary')->nullable();
            $table->text('inclusions')->nullable();
            $table->text('exclusions')->nullable();
            $table->text('terms_and_conditions')->nullable();
            $table->text('remarks')->nullable();
            $table->string('filters', 1000)->nullable();
            $table->string('gallery', 1500)->nullable();
            $table->timestamps();
            $table->softDeletes();            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tour_title_vendor_details');
    }
}
