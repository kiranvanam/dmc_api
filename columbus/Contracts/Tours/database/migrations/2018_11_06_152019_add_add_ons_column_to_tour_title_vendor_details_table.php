<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAddOnsColumnToTourTitleVendorDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tour_title_vendor_details', function (Blueprint $table) {
            $table->string('add_ons')->default("")->after('filters');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tour_title_vendor_details', function (Blueprint $table) {
            $table->dropColumn('add_ons');
        });
    }
}
