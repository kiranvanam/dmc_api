<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateToursTable extends Migration
{    
    public function up()
    {

        Schema::create('tours', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('city_id')->index();
            $table->string('name', 500);
            $table->string('slug', 500)->index();
            $table->boolean('is_sic')->default(true);
            $table->boolean('is_private')->default(true);
            $table->string('description',1500)->nullable();
            $table->string('gallery')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tours');
        //Schema::dropIfExists('tours');
    }
}
