<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTourTitlePrivateVehiclePricingAvailabilityTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tour_title_private_vehicle_pricing', function (Blueprint $table) {
            $table->increments('id');
            
            $table->integer('tour_title_private_service_mapping_id')
                   ->index('tour_title_private_sm_id_index');

            $table->date('date')->index();
            $table->boolean('is_available')->default(false);
            $table->boolean('is_on_request')->default(false);
            $table->integer('inventory');
            $table->decimal('price', 8,2);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tour_title_private_vehicle_pricing');
    }
}
