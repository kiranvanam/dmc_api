<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVendorTourPricingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vendor_tour_pricings', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('vendor_tour_id');
            $table->date('date');
            $table->text('time_slots_with_pricing_details')->null();
            $table->boolean('is_available')->default(1);
            $table->boolean('is_on_request')->default(false);

            $table->timestamps();
            $table->softDeletes();

            $table->index('vendor_tour_id');
            $table->index('date');
            $table->index(['vendor_tour_id', 'date']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vendor_tour_pricings');
    }
}
