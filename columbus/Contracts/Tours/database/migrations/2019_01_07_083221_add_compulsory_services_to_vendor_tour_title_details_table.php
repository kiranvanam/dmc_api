<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCompulsoryServicesToVendorTourTitleDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tour_title_vendor_details', function (Blueprint $table) {
            $table->string('booking_required_private_services')->nullable()->after('vendor_code');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tour_title_vendor_details', function (Blueprint $table) {
            $table->dropColumn('booking_required_private_services');
        });
    }
}
