<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCurrencyColumnToTourTitlePrivateVehiclePricingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tour_title_private_vehicle_pricing', function (Blueprint $table) {
            $table->string('currency', 6)->default('USD')->after('price');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tour_title_private_vehicle_pricing', function (Blueprint $table) {
            $table->dropColumn('currency');
        });
    }
}
