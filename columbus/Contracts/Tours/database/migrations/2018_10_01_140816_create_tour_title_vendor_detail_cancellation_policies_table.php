<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTourTitleVendorDetailCancellationPoliciesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tour_title_vendor_detail_cancellation_policies', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('tour_title_vendor_detail_id')->index('tour_title_vendor_detail_id_cance_policy_index');
            $table->date('booking_start_date')->nullable();
            $table->date('booking_end_date')->nullable();
            $table->date('travelling_start_date')->nullable();
            $table->date('travelling_end_date')->nullable();            
            $table->boolean('is_non_refundable')->default(false);
            $table->text('policy_rules')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tour_title_vendor_detail_cancellation_policies');
    }
}
