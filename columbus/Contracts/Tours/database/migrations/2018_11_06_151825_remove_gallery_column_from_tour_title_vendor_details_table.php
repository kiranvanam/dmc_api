<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveGalleryColumnFromTourTitleVendorDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tour_title_vendor_details', function (Blueprint $table) {
            if(Schema::hasColumn('tour_title_vendor_details', 'gallery') ) {
                $table->dropColumn('gallery');
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tour_title_vendor_details', function (Blueprint $table) {
            //
        });
    }
}
