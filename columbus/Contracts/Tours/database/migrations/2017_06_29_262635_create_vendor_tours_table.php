<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVendorToursTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vendor_tours', function (Blueprint $table) {
            $table->increments('id')->index();
            $table->integer('city_id')->index();
            $table->integer('vendor_id')->index();
            
            $table->integer('tour_id')->index();
            $table->string('code',30);
            
            $table->boolean('is_sic');
            $table->boolean('is_private');
            
            $table->string('itinerary_details')->nullable()->default("");
            $table->string('inclusions')->nullable()->default("");
            $table->string('exclusions')->nullable()->default("");
            $table->string('remarks')->nullable()->default("");
            $table->string('terms_and_conditions')->nullable()->default("");
            $table->string('cancellation_policies')->nullable()->default("");
            $table->string('cancellation_policy_rules')->nullable()->default("");
            $table->string('gallery')->nullable()->default("");
            $table->string('filters', 1000)->nullable();
            
            $table->timestamps();
            $table->softDeletes();
            
            $table->index(['city_id', 'vendor_id', 'tour_id'], 'city_vendor_tour_index');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vendor_tours');
    }
}
