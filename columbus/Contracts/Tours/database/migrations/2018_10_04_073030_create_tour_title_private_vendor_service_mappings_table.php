<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTourTitlePrivateVendorServiceMappingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tour_title_private_vendor_detail_service_mappings', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('tour_title_vendor_detail_id');
            $table->integer('service_id');
            $table->string('service');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tour_title_private_vendor_detail_service_mappings');
    }
}
