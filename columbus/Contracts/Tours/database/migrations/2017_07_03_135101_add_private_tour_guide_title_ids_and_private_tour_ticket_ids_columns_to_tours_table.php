<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPrivateTourGuideTitleIdsAndPrivateTourTicketIdsColumnsToToursTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tours', function (Blueprint $table) {
            $table->string('private_tour_guide_title_ids', 200)->after('private_tour_vendor_service_ids')->nullable();
            $table->string('private_tour_ticket_ids', 200)->after('private_tour_guide_title_ids')->nullable();
            
            if (Schema::hasColumn('tours', 'deleted_at') == false) 
            {
                $table->softDeletes();
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tours', function (Blueprint $table) {
            $table->dropColumn('private_tour_guide_title_ids');
            $table->dropColumn('private_tour_ticket_ids');
        });
    }
}
