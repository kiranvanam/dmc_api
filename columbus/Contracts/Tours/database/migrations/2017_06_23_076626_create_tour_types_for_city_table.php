<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTourTypesForCityTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tour_types_for_city', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('city_id')->index();
            $table->string('name',150);
            $table->string('slug',150)->index();
            $table->integer('parent_id')->nullable();

            $table->timestamps();            

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tour_types_for_city');
    }
}
