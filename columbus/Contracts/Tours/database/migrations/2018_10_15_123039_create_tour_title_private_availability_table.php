<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTourTitlePrivateAvailabilityTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tour_title_private_vendor_detail_availability', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('tour_title_vendor_detail_id')->index('tour_title_vendor_detail_id_index'); // service mapping id
            $table->date('date')->index();
            $table->boolean('is_available')->default(false);
            $table->boolean('is_on_request')->default(false);
            $table->string('time_slots');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tour_title_private_vendor_detail_availability');
    }
}
