<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVehiclesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vehicles', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('slug');
            $table->integer('country_id');
            $table->integer('vehicle_class_id');
            $table->integer('vehicle_brand_id');
            $table->integer('vehicle_type_id');
            $table->integer('seater');
            $table->integer('small_bags');
            $table->integer('small_bag_weight');
            $table->integer('large_bags');
            $table->integer('large_bag_weight');
            $table->integer('seater_baggage')->nullable();
            $table->integer('bags')->nullable();
            $table->integer('per_bag_weight')->nullable();
            $table->text('seater_information')->nullable();
            $table->text('description')->nullable();
            // IMAGE HERE
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vehicles');
    }
}
