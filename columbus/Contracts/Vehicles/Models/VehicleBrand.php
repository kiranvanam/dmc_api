<?php

namespace Columbus\Contracts\Vehicles\Models;

use Illuminate\Database\Eloquent\Model;

class VehicleBrand extends Model
{
    public $timestamps = false;
    protected $fillable = ['name', 'slug'];
}
