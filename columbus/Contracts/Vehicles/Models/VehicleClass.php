<?php

namespace Columbus\Contracts\Vehicles\Models;

use Illuminate\Database\Eloquent\Model;

class VehicleClass extends Model
{
    public $timestamps = false;
    protected $fillable = ['name', 'slug'];
}
