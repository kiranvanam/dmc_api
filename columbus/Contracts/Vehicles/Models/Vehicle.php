<?php

namespace Columbus\Contracts\Vehicles\Models;

use App\Image;
use Illuminate\Database\Eloquent\Model;

class Vehicle extends Model
{
    public $timestamps = false;
    protected $fillable = ['name', 'slug', 'country_id', 'vehicle_class_id', 'vehicle_brand_id', 'vehicle_type_id',
                    'seater', 'passengers_only', 'passengers_with_luggage', 
                    'small_bags', 'small_bag_weight', 'large_bags', 'large_bag_weight', 'seater_baggage',
                    'bags', 'per_bag_weight', 'passengers_with_luggage_notes', 'description', 'thumbnail'];

    public function images()
    {
        return $this->morphMany( Image::class , 'imageable');
    }

    public static function withIds($vehicle_ids)
    {
        return self::whereIn('id', $vehicle_ids)->with('brand')->get();
    }

    public function brand()
    {
        return $this->belongsTo(VehicleBrand::class, 'vehicle_brand_id');
    }
}
