<?php

namespace Columbus\Contracts\Vehicles\Models;

use Illuminate\Database\Eloquent\Model;

class VehicleChildAge extends Model
{
    public $timestamps = false;
    protected $fillable = ['country_id', 'min_age', 'description'];
}
