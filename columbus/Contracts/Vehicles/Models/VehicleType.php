<?php

namespace Columbus\Contracts\Vehicles\Models;

use Illuminate\Database\Eloquent\Model;

class VehicleType extends Model
{
    public $timestamps = false;
    protected $fillable = ['name', 'slug'];
}
