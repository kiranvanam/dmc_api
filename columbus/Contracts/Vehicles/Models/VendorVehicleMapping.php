<?php

namespace Columbus\Contracts\Vehicles\Models;

use Illuminate\Database\Eloquent\Model;

class VendorVehicleMapping extends Model
{
    public $timestamps = false;
    protected $fillable = [ 'vendor_id' ,'vehicle_ids'];

     protected $casts = [
        'vehicle_ids' => 'array',
    ];   
}
