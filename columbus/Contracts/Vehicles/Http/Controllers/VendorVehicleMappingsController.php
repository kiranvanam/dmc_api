<?php

namespace Columbus\Contracts\Vehicles\Http\Controllers;

use App\Http\Controllers\ApiController;
use Columbus\Contracts\Vehicles\Http\Requests\VehicleTypeCreateRequest;
use Columbus\Contracts\Vehicles\Models\VehicleType;
use Columbus\Contracts\Vehicles\Models\VendorVehicleMapping;
use Illuminate\Http\Request;

class VendorVehicleMappingsController extends ApiController
{
    public function index() {
        return $this->ok( VendorVehicleMapping::all() , "Fetching Vehicle Types is Successful");
    }

    public function update($vendor_id)
    {   

        return $this->ok("");
    }

}