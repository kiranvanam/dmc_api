<?php

namespace Columbus\Contracts\Vehicles\Http\Controllers;

use Auth;
use App\Http\Controllers\ApiController;
use Columbus\Contracts\Vehicles\Http\Requests\VehicleBrandCreateRequest;
use Columbus\Contracts\Vehicles\Models\VehicleBrand;
use Illuminate\Http\Request;

class VehicleBrandController extends ApiController
{
    public function all() {
        Auth::user()->hasPermission('vendors');
        return $this->ok(VehicleBrand::all(), "Fetching Vehicle Brands is Successful");
    }

    public function save(VehicleBrandCreateRequest $request)
    {
        return $this->ok(VehicleBrand::create($request->all()), "Vehicle Brand details added successfully");
    }

    public function update($id, VehicleBrandCreateRequest $request)
    {
        $data = $request->all();
        $vehicleBrand = VehicleBrand::find($id);
        $vehicleBrand->update($data);

        return $this->ok($vehicleBrand, "Vehicle Brand details Updated successfully");
    }

    public function delete($id)
    {
        $vehicleBrand = VehicleBrand::find($id);
        $vehicleBrand->delete();
        return $this->ok($vehicleBrand, "Vehicle Brand deleted successfully");
    }
}
