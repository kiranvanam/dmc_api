<?php

namespace Columbus\Contracts\Vehicles\Http\Controllers;

use Auth;
use App\Http\Controllers\ApiController;
use Columbus\Contracts\Vehicles\Http\Requests\VehicleClassCreateRequest;
use Columbus\Contracts\Vehicles\Models\VehicleClass;
use Illuminate\Http\Request;

class VehicleClassController extends ApiController
{
    public function all() {
        Auth::user()->hasPermission('vendors');
        return $this->ok(VehicleClass::all(), "Fetching Vehicle Classes is Successful");
    }

    public function save(VehicleClassCreateRequest $request)
    {
        return $this->ok(VehicleClass::create($request->all()), "Vehicle Class details added successfully");
    }

    public function update($id, VehicleClassCreateRequest $request)
    {
        $data = $request->all();
        $vehicleClass = VehicleClass::find($id);
        $vehicleClass->update($data);

        return $this->ok($vehicleClass, "Vehicle Class details Updated successfully");
    }

    public function delete($id)
    {
        $vehicleClass = VehicleClass::find($id);
        $vehicleClass->delete();
        return $this->ok($vehicleClass, "Vehicle Class deleted successfully");
    }
}
