<?php

namespace Columbus\Contracts\Vehicles\Http\Controllers;

use Auth;
use App\Http\Controllers\ApiController;
use Columbus\Contracts\Vehicles\Http\Requests\VehicleCreateRequest;
use Columbus\Contracts\Vehicles\Models\Vehicle;
use Columbus\Contracts\Vehicles\Models\VehicleClass;
use Columbus\Contracts\Vehicles\Models\VehicleType;
use Columbus\Contracts\Vehicles\Models\VehicleBrand;
use Columbus\Contracts\Vehicles\Models\VehicleChildAge;
use Illuminate\Http\Request;

class VehicleController extends ApiController
{
    public function settings() {
        Auth::user()->hasPermission('vendors');

        return $this->ok([
                'classes' => VehicleClass::all(),
                'types' => VehicleType::all(),
                'brands' => VehicleBrand::all(),
                'ages' => VehicleChildAge::all()
            ], "Fetching Vehicle Settings is Successful");
    }

    public function all() {
        Auth::user()->hasPermission('vendors');
        return $this->ok(Vehicle::with('images')->get(), "Vehicles lists");
    }

    public function save(VehicleCreateRequest $request)
    {
        return $this->ok(Vehicle::create($request->all()), "Vehicle created");
    }

    public function update($id, VehicleCreateRequest $request)
    {
        $data = $request->all();
        $vehicle = Vehicle::find($id);
        $vehicle->update($data);

        return $this->ok($vehicle, "Vehicle details Updated");
    }

    public function updateThumbnail($id)
    {
        $vehicle = Vehicle::findOrFail($id);
        $vehicle->update(['thumbnail' => request('thumbnail')]);
        return $this->ok($vehicle, "Thumbnail is updated");
    }

    public function delete($id)
    {
        $vehicle = Vehicle::find($id);
        $vehicle->delete();
        return $this->ok($vehicle, "Vehicle deleted");
    }
}
