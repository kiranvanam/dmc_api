<?php
namespace Columbus\Contracts\Vehicles\Http\Controllers;

use App\Http\Controllers\ApiController;
use App\Image;
use Columbus\Contracts\Vehicles\Models\Vehicle;
use Columbus\Utilities\FileManager;
use Illuminate\Http\Request;

class ImageController extends ApiController
{
   
   protected  $fileManager = null;

   function __construct(FileManager $fileManager)
   {
       $this->fileManager = $fileManager;
   }

   public function save($vehicle_id, Request $request)
   {
       $vehicle = Vehicle::find($vehicle_id);
       
       $images = [];

       if ($request->hasFile('images')) {
            $path = "images/countries/$vehicle->country_id/vehicles/$vehicle->id" ;

            $images = $this->fileManager->savePhotos($path , $request->file('images'));

            foreach ($images as $image) {
                $vehicle->images()->save(new Image(['path'=> $image ]));
            }

        }

       return $this->ok($vehicle->images, "Vehicle Images are updated");
   }

   public function delete($vehicle_id, $image_id)
   {
        $image = Image::find($image_id);

        if($image){
            $image->delete();
            return $this->ok(true, "Image deleted");
        }

        return $this->ok(false , "Image not deleted");

   }

}
