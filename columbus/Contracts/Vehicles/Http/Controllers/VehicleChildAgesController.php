<?php

namespace Columbus\Contracts\Vehicles\Http\Controllers;

use Auth;
use App\Http\Controllers\ApiController;
use Columbus\Contracts\Vehicles\Http\Requests\VehicleChildAgeCreateRequest;
use Columbus\Contracts\Vehicles\Models\VehicleChildAge;
use Illuminate\Http\Request;

class VehicleChildAgesController extends ApiController
{
    public function all() {
        Auth::user()->hasPermission('vendors');
        return $this->ok(VehicleChildAge::all(), "Fetching Vehicle ChildAges is Successful");
    }

    public function save(VehicleChildAgeCreateRequest $request)
    {
        return $this->ok(VehicleChildAge::create($request->all()), "Vehicle ChildAge details added successfully");
    }

    public function update($id, VehicleChildAgeCreateRequest $request)
    {
        $data = $request->all();
        $vehicleChildAge = VehicleChildAge::find($id);
        $vehicleChildAge->update($data);

        return $this->ok($vehicleChildAge, "Vehicle ChildAge details Updated successfully");
    }

    public function delete($id)
    {
        $vehicleChildAge = VehicleChildAge::find($id);
        $vehicleChildAge->delete();
        return $this->ok($vehicleChildAge, "Vehicle ChildAge deleted successfully");
    }
}
