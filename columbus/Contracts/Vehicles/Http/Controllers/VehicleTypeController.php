<?php

namespace Columbus\Contracts\Vehicles\Http\Controllers;

use Auth;
use App\Http\Controllers\ApiController;
use Columbus\Contracts\Vehicles\Http\Requests\VehicleTypeCreateRequest;
use Columbus\Contracts\Vehicles\Models\VehicleType;
use Illuminate\Http\Request;

class VehicleTypeController extends ApiController
{
    public function all() {
        Auth::user()->hasPermission('vendors');
        return $this->ok(VehicleType::all(), "Fetching Vehicle Types is Successful");
    }

    public function save(VehicleTypeCreateRequest $request)
    {
        return $this->ok(VehicleType::create($request->all()), "Vehicle Type details added successfully");
    }

    public function update($id, VehicleTypeCreateRequest $request)
    {
        $data = $request->all();
        $vehicleType = VehicleType::find($id);
        $vehicleType->update($data);

        return $this->ok($vehicleType, "Vehicle Type details Updated successfully");
    }

    public function delete($id)
    {
        $vehicleType = VehicleType::find($id);
        $vehicleType->delete();
        return $this->ok($vehicleType, "Vehicle Type deleted successfully");
    }
}
