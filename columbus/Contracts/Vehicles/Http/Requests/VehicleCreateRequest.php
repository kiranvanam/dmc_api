<?php

namespace Columbus\Contracts\Vehicles\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class VehicleCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'vehicle_type_id' => 'required',
            'vehicle_brand_id' => 'required',
            'vehicle_class_id' => 'required',
            'country_id' => 'required',
            'seater' => 'required',
            'small_bags' => 'required',
            'small_bag_weight' => 'required',
            'large_bags' => 'required',
            'large_bag_weight' => 'required',
        ];
    }

    protected function prepareForValidation()
    {
        $this->merge([
            'slug' => str_slug($this->get('name'))
        ]);
    }

    /**
     * customizing error messages for (field,rule)
     */
    public function messages()
    {
        return [
            'name.required' => "Please specify a valid name",
            'vehicle_type_id.required' => "Please select a valid vehicle type",
            'vehicle_class_id.required' => "Please select a valid class type",
            'vehicle_brand_id.required' => "Please select a valid brand type",
            'country_id.required' => "Please select a valid country",
            'seater.required' => "Please select a valid seater",
            'small_bags.required' => "Please select a valid number of small bags",
            'large_bags.required' => "Please select a valid number of large bags",
            'small_bag_weight.required' => "Please enter a valid weight of small bags",
            'large_bag_weight.required' => "Please enter a valid weight of large bags",
        ];
    }

}
