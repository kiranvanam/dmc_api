<?php

namespace Columbus\Contracts\Vehicles\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class VehicleChildAgeCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'country_id' => 'required',
            'min_age' => 'required'
        ];
    }

    protected function prepareForValidation()
    {
    }

    /**
     * customizing error messages for (field,rule)
     */
    public function messages()
    {
        return [
            'country_id.required' => 'Please select a country',
            'min_age.required' => "Please specify a valid age"
        ];
    }

}
