<?php

use Columbus\Contracts\Vehicles\Models\Vehicle;
use Columbus\Contracts\Vehicles\Models\VehicleBrand;
use Columbus\Contracts\Vehicles\Models\VehicleClass;
use Columbus\Contracts\Vehicles\Models\VehicleType;

Route::group(['prefix'=>'/api/contracts/vehicles', 'middleware'=>['auth:api','cors']], function() {
    Route::get('/', 'VehicleController@all');
    Route::post('/', 'VehicleController@save');
    Route::put('/{id}', 'VehicleController@update');
    Route::put('/{id}/thumbnail', 'VehicleController@updateThumbnail');
    Route::delete('/{id}', 'VehicleController@delete');

    Route::post('/{id}/images', 'ImageController@save');
    Route::delete('/{vehicle_id}/images/{image_id}', 'ImageController@delete');

    Route::group(['prefix' => 'settings'], function () {
        Route::get('/', 'VehicleController@settings');
        Route::group(['prefix' => 'child-ages'], function () {
            Route::get('/', 'VehicleChildAgesController@all');
            Route::post('/', 'VehicleChildAgesController@save');
            Route::put('/{id}', 'VehicleChildAgesController@update');
            Route::delete('/{id}', 'VehicleChildAgesController@delete');
        });
    });

    Route::group(['prefix' => 'class'], function () {
        Route::get('/', 'VehicleClassController@all');
        Route::post('/', 'VehicleClassController@save');
        Route::put('/{id}', 'VehicleClassController@update');
        Route::delete('/{id}', 'VehicleClassController@delete');
    });
    Route::group(['prefix' => 'type'], function () {
        Route::get('/', 'VehicleTypeController@all');
        Route::post('/', 'VehicleTypeController@save');
        Route::put('/{id}', 'VehicleTypeController@update');
        Route::delete('/{id}', 'VehicleTypeController@delete');
    });
    Route::group(['prefix' => 'brand'], function () {
        Route::get('/', 'VehicleBrandController@all');
        Route::post('/', 'VehicleBrandController@save');
        Route::put('/{id}', 'VehicleBrandController@update');
        Route::delete('/{id}', 'VehicleBrandController@delete');
    });
});
