<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCodesTable extends Migration
{

    public function up()
    {
        Schema::create('codes', function (Blueprint $table) {
            $table->increments('id');

            $table->string('module_name',100);

            $table->string('code_prefix',30);
            $table->integer('start_code');
            $table->integer('last_used_code');

            $table->timestamps();
            $table->softDeletes();
        });
    }


    public function down()
    {
        Schema::dropIfExists('codes');
    }
}
