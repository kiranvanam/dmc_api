<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDmcBankAccountsTable extends Migration
{

    public function up()
    {
        Schema::create('dmc_bank_accounts', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('country_id');
            $table->string('city', 50);
            $table->string('currency', 6);
            $table->string('in_favour_off', 150);
            $table->string('bank_name', 150);
            $table->string('branch_name', 150);
            $table->string('account_number', 50);
            $table->string('ifsc_code', 50)->nullable();
            $table->string('swift_code', 50)->nullable();
            $table->string('iban_code', 50)->nullable();
            $table->string('address', 500)->default(null);
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('dmc_bank_accounts');
    }
}
