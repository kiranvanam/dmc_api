<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvoiceSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invoice_settings', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('operating_country_id')->index();
            $table->string('selling_country_ids', 1500);
            $table->integer('taxes_country_id');
            $table->string('currency', 5);
            $table->text('service_invoice_settings');            
            /* $table->string('gross_total');
            $table->string('dmc_details', 1000);
            $table->string('invoice_notes', 1000)->nullable(); */
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invoice_settings');
    }
}
