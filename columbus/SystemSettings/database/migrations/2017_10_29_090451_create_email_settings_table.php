<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmailSettingsTable extends Migration
{    
    public function up()
    {
        Schema::create('email_sending_settings', function (Blueprint $table) {
            $table->increments('id');
            $table->string('action', 200);
            $table->string('primary_email_id');
            $table->string('cc_email_ids')->nullable();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('email_sending_settings');
    }
}
