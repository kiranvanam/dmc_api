<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTaxesTable extends Migration
{
    
    public function up()
    {
        Schema::create('taxes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('country_id');
            $table->string('name');
            $table->string('slug');
            $table->string('agency_name')->default('');
            $table->decimal('amount');
            $table->string('amount_type', 20)->default('percent'); // ['percent', 'value']
            $table->string('tax_type', 30);
        });
    }

    
    public function down()
    {
        Schema::dropIfExists('taxes');
    }
}
