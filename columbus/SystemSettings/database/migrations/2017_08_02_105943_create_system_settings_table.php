<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSystemSettingsTable extends Migration
{    
    public function up()
    {
        Schema::create('system_settings', function (Blueprint $table) {
            $table->increments('id');
            $table->string('type',50);
            $table->text('setting_values');
        });
    }

    
    public function down()
    {
        Schema::dropIfExists('system_settings');
    }
}
