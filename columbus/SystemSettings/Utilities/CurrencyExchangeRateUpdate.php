<?php

namespace Columbus\SystemSettings\Utilities;

use Columbus\SystemSettings\Models\Currency;
use Columbus\SystemSettings\Models\CurrencyExchangeRate;


class CurrencyExchangeRateUpdate
{
    protected $exchangeService;

    function __construct()
    {
        $this->exchangeService = new Exchanger();
    }

    function update($exchange_rates)
    {
        $self = $this;

        // temporary variable to keep updated values for every conversion
        $updated_exchnage_rates = $exchange_rates;

        collect($exchange_rates)->each( function( $fromCurrencyExchangeRates, $from_currency) use($self, &$updated_exchnage_rates) {

                collect($fromCurrencyExchangeRates)->each( function($values, $to_currency) use($from_currency, $self, &$updated_exchnage_rates) {

                    if( $from_currency != $to_currency ) {

                        $convertedRate =  $self->exchangeService->convert($from_currency, $to_currency);
                        $roundedRate = $self->roundCurrency($convertedRate);
                        $updated_exchnage_rates[$from_currency][$to_currency]['exchange_rate'] = $roundedRate;
                    }
                });
            });
        
        //update with latest conversions
        return $updated_exchnage_rates;
    }

    protected function roundCurrency($value)
    {
        return roundUp($value, 3);
    }
}