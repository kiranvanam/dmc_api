<?php

namespace Columbus\SystemSettings\Utilities;

use Swap\Builder;

class Exchanger
{
    protected $exchangeProviders = [];
    
    function __construct()
    {
        $this->exchangeProviders = collect([]); 
        $this->addBuilder('fixer', ['access_key' => '78ebe146e175a055e505128a2d73292c']);
        $this->addBuilder('currency_layer', ['access_key' => 'd62d08ef4c6a99421e74e0a62292656f', 'enterprise' => false]);
        $this->addBuilder('forge', ['api_key' => 'jGxXqScFqTt057yBo5L4fwv9qpGZ10oy']);
        $this->addBuilder('open_exchange_rates', ['app_id'=> '0bbebc92d6a74cf5b0fd27be72b333a0', 'enterprise' => false]);
    }

    protected function addBuilder( $service, $options = []) 
    {
        $builder = (new Builder())
                    ->add($service, $options)    
                    ->build();
        $this->exchangeProviders->push($builder);
    }

    public function convert($from, $to, $amount=1)
    {
        $convertedRate = 0;

        if( $this->areCurrenciesNotSupported([$from, $to])) {
            $this->exchangeProviders->first( function($builder) use(&$convertedRate, $from, $to) {
                                        try {
                                            $rate = $builder->latest("$from/$to");
                                            $convertedRate = $rate->getValue();
                                        } catch (\Exception $e) {
                                            $convertedRate = 0;
                                        }
                                        return $convertedRate;
                                    });
        }

        if( empty($convertedRate)) {
            $convertedRate = convertCurrency($from, $to, 1);
        }
        // return 0 becuase the price conversions will be shown as 0 in all price dispalys.so easy to report for system users.
        return $convertedRate;  
    }

    protected function areCurrenciesNotSupported( $currencies = [] )
    {
        $notSupported = collect(['AED', 'INR']);

        return $notSupported->intersect($currencies)->isEmpty();
    }
}