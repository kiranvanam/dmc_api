<?php

Route::get("/api/currencies", 'SystemSettingsController@currencies');

Route::group(['prefix'=> 'api', 'middleware' => ['auth:api','cors']], function() {

    Route::get('system-settings', 'SystemSettingsController@index');

    Route::post('/currencies/{currency_id}/toggle', 'CurrencyToggleController@toggleActiveFlag');

    //Route::get('currency-conversion-buffers', 'CurrencyBufferController@index');

    Route::group(['prefix' => 'bank-accounts'], function () {
        Route::get('/', 'BankAccountsController@all');
        Route::get('/country/{country_id}', 'BankAccountsController@fetchByCountry');
        Route::post('/', 'BankAccountsController@save');
        Route::put('/{id}', 'BankAccountsController@update');
        Route::delete('/{id}', 'BankAccountsController@delete');
    });

    Route::group(['prefix' => 'taxes'], function () {
        Route::get('/', 'TaxController@all');
        Route::get('/country/{country_id}', 'TaxController@fetchByCountry');
        Route::post('/', 'TaxController@save');
        Route::put('/{id}', 'TaxController@update');
        Route::delete('/{id}', 'TaxController@delete');
    });

    Route::group(['prefix'=> 'dmc-bank-accounts'], function() {
        Route::get('/', 'DMCBankAccountDetailsController@index');
        Route::post('/', 'DMCBankAccountDetailsController@create');
        Route::put('/{bank_account_id}', 'DMCBankAccountDetailsController@update');
        Route::delete('/{bank_account_id}', 'DMCBankAccountDetailsController@delete');
    });

    Route::group(['prefix'=> 'country-service-markups'], function() {
        Route::get('/', 'CountryServiceMarkupsController@index');
        Route::post('/', 'CountryServiceMarkupsController@create');
        Route::put('/{markup_id}', 'CountryServiceMarkupsController@update');
        Route::delete('/{markup_id}', 'CountryServiceMarkupsController@delete');
    });

    Route::group(['prefix'=> 'country-service-taxes'], function() {
        Route::get('/', 'CountryServiceTaxesController@index');
        Route::post('/', 'CountryServiceTaxesController@create');
        Route::put('/{markup_id}', 'CountryServiceTaxesController@update');
        Route::delete('/{markup_id}', 'CountryServiceTaxesController@delete');
    });


    //Route::put('/currency-exchange-rates', 'CurrencyExchangeRateController@updateExchnageRates');
    Route::put('/currency-exchange-rate-buffers', 'CurrencyExchangeRateController@updateBuffers');

    Route::group(['prefix'=>'system-settings/invoices'], function(){
        Route::get("/", 'InvoiceSettingsController@index');
        Route::post("/", 'InvoiceSettingsController@create');
        Route::put("/{invoice_setting_id}", 'InvoiceSettingsController@update');
    });

    Route::group(['prefix' => "email-settings"], function() {
        Route::get("", 'EmailSettingController@index');
        Route::post("", 'EmailSettingController@create');
        Route::put("{id}", 'EmailSettingController@updat');
        Route::delete("{id}", 'EmailSettingController@delete');
    });
});

Route::group(['prefix'=>"api/codes", 'middleware'=>['auth:api','cors']], function() {
    Route::get("/", 'CodeGenerationController@index');
    Route::post("/",'CodeGenerationController@create');
    Route::get("/{module_name}", 'CodeGenerationController@nextCodeForModule');

    Route::post("vendors", 'CodeGenerationController@generateForvendors');
    Route::get("vendors", 'CodeGenerationController@vendorsNextCode');

    Route::post("tours", 'CodeGenerationController@generateForTours');
    Route::get("tours", 'CodeGenerationController@toursNextCode');

    Route::post("vehicles", 'CodeGenerationController@generateForVehicles');
    Route::get("vehicles", 'CodeGenerationController@vehiclesNextCode');

    Route::post("guides", 'CodeGenerationController@generateForGuides');
    Route::get("guides", 'CodeGenerationController@guidesNextCode');
});

Route::group(['prefix' => "api/filters"], function(){
    Route::get("", "FilterController@index");
    Route::post("", "FilterController@create");
    Route::put("{filter_id}", "FilterController@update");
    Route::delete("{filter_id}", "FilterController@delete");
    Route::put("{filter_id}/restore", "FilterController@restore");
    // not used PUT as above put("{filter_id}) is over-riding the below route, so keep as POST
    Route::post("update-filter-group-heading", 'FilterController@updateFilterGroupLabel');

});


