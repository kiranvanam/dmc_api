<?php

namespace Columbus\SystemSettings\Http\Controllers;

use App\Http\Controllers\ApiController;
use Columbus\SystemSettings\Models\CountryServiceTax;
use Columbus\SystemSettings\Http\Requests\CountryServiceTaxRequest;

class CountryServiceTaxesController extends ApiController
{
    public function index()
    {
        return $this->ok(CountryServiceTax::all());
    }

    public function create(CountryServiceTaxRequest $request)
    {
        return $this->ok($request->save(), "Service Taxes added");
    }

    public function update($servieTax_id, CountryServiceTaxRequest $request)
    {
        return $this->ok( $request->update($servieTax_id), "Service taxes updted" );
    }

    public function delete($servieTax_id)
    {
        $serviceTax = CountryServiceTax::findOrFail($servieTax_id);

        $serviceTax->delete();

        return $this->ok($serviceTax, "Service tax deleted");
    }

    
}