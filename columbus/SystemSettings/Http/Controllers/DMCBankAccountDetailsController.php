<?php

namespace Columbus\SystemSettings\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;
use Columbus\SystemSettings\Models\DMCBankAccount;
use Columbus\SystemSettings\Http\Requests\DMCBankAccountRequest;

class DMCBankAccountDetailsController extends ApiController
{
    function index() {
        return $this->ok(DMCBankAccount::all());
    }

    function create(DMCBankAccountRequest $request) {
        return $this->ok( $request->save(), "Bank account added" );
    }

    public function update($account_id, DMCBankAccountRequest $request)
    {
        return $this->ok( $request->update($account_id), "Bank account updated" );
    }

    public function delete($account_id)
    {
        $bankAccount = DMCBankAccount::findOrFail($account_id);
        
        $bankAccount->delete();

        return $this->ok($bankAccount, "Bank Account deleted");
    }
}