<?php

namespace Columbus\SystemSettings\Http\Controllers;

use App\Http\Controllers\ApiController;
use Columbus\SystemSettings\Models\EmailSetting;

class EmailSettingController extends ApiController
{
    function index()
    {
        return $this->ok(EmailSetting::all(), "Email Settings");
    }

    function create()
    {
        return $this->ok( EmailSetting::create( request()->all() ), "Email setting created");
    }

    function update($id)
    {
        $email = EmailSetting::findOrFail($id);

        $email->update(request()->all());

        return $this->ok($email, "Email setting updated");
    }

    function delete($id)
    {
        $email = EmailSetting::findOrFail($id);
        $email->delete($id);

        return $this->ok($email, "Eamil setting deleted");
    }
}