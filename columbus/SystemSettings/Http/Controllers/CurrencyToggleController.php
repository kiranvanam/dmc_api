<?php

namespace Columbus\SystemSettings\Http\Controllers;

use App\Http\Controllers\ApiController;
use Columbus\SystemSettings\Models\Currency;
use Illuminate\Support\Facades\DB;


class CurrencyToggleController extends ApiController
{
    
    public function toggleActiveFlag($currency_id)
    {
        $currency = Currency::find($currency_id);

        if($currency) {
            $currency->is_active = !$currency->is_active;
            $currency->save();
        }else{
            return $this->notFound("Currency not found");
        }

        return $this->ok($currency, "Status toggle for currency -" . $currency->symbol );
    }
}
