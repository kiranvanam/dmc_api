<?php

namespace Columbus\SystemSettings\Http\Controllers;

use App\Http\Controllers\ApiController;
use Columbus\SystemSettings\Utilities\Exchanger;
use Columbus\SystemSettings\Models\CurrencyExchangeRate;
use Columbus\SystemSettings\Utilities\CurrencyExchangeRateUpdate;

class CurrencyExchangeRateController extends ApiController
{
    public function index()
    {

        $cer = new CurrencyExchangeRateUpdate();

        return $cer->update();
    }

    public function updateExchnageRates()
    {
        $cer = new CurrencyExchangeRateUpdate();
        $cer->update();
        return $this->ok( CurrencyExchangeRate::getExchangeRates(), "Currency Exchange rates are updated");
    }

    public function updateBuffers()
    {
        // always create new one, so we have history of currency values
        $currency_exchage_rate_settings = request()->all();
        $newSettings = [
            'exchange_rate_buffer_amount' => $currency_exchage_rate_settings['exchange_rate_buffer_amount'],
            'exchange_rate_buffer_amount_type' => $currency_exchage_rate_settings['exchange_rate_buffer_amount_type'],
            'exchange_rates' => $currency_exchage_rate_settings['exchange_rates']
        ];

        $currency_exchage_rate = CurrencyExchangeRate::create(['exchange_rate_settings' => $newSettings]);

        if( array_has($currency_exchage_rate_settings, 'update_only_buffers')) {
            return $this->ok( CurrencyExchangeRate::getExchangeRates() , "Currency Settings updated successfully");
        }

        $currency_exchage_rate =  CurrencyExchangeRate::latest()->first();
        $newSettings = $currency_exchage_rate->exchange_rate_settings;
        //dd($currency_exchage_rate->exchange_rate_settings);
        $utility = new  CurrencyExchangeRateUpdate();

        $newSettings['exchange_rates'] = $utility->update( $newSettings['exchange_rates']);

        $currency_exchage_rate->update(['exchange_rate_settings'=> $newSettings]);

        return $this->ok( CurrencyExchangeRate::getExchangeRates() , "Currency Settings updated successfully");
    }

}
