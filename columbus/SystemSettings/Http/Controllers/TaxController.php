<?php

namespace Columbus\SystemSettings\Http\Controllers;

use App\Http\Controllers\ApiController;
use Columbus\SystemSettings\Http\Requests\TaxCreateRequest;
use Columbus\Destination\Models\Country;
use Columbus\SystemSettings\Models\Tax;
use Illuminate\Http\Request;

class TaxController extends ApiController
{
    public function all(Request $request) {
        return $this->ok(Tax::all());
    }

    public function save(TaxCreateRequest $request)
    {
        return $this->ok(Tax::create($request->all()), "Tax Created Successfully");
    }

    public function fetchByCountry($country_id, Request $request) {
        $country = Country::find($country_id);
        return $this->ok($country->taxes, "Fetching Taxes By Country");
    }

    public function update($id, TaxCreateRequest $request)
    {
        $tax = Tax::find($id);
        $tax->update($request->all());
        return $this->ok($tax , "Tax Details Updated Successfully");
    }

    public function delete($id)
    {
        $tax = Tax::find($id);

        // Delete The Tax
        $tax->delete();

        return $this->ok($tax, "Tax Deleted Successfully");
    }
}
