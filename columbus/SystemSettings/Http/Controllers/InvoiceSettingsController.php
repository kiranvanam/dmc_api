<?php

namespace Columbus\SystemSettings\Http\Controllers;

use App\Http\Controllers\ApiController;
use Columbus\SystemSettings\Models\InvoiceSetting;

class InvoiceSettingsController extends ApiController
{

    public function index()
    {
        return $this->ok( InvoiceSetting::all(), "Invoice settings");
    }

    public function create()
    {
        $data = $this->prepareData();
        $data['service_invoice_settings'] = [ $data['service_invoice_settings'] ];

        $invoice = InvoiceSetting::create($data);

        return $this->ok( $invoice, "Invoice setting is added");
    }

    public function update($invocieSettingId)
    {
        $data = $this->prepareData();

        $invoiceSetting = InvoiceSetting::findOrFail($invocieSettingId);

        $data['service_invoice_settings'] = $this->refillInvoiceSettingswithNewData($invoiceSetting, $data['service_invoice_settings']);

        $invoiceSetting->update($data);


        return $this->ok($invoiceSetting, "Invoice setting details updated");
    }

    protected function refillInvoiceSettingswithNewData($invoice, $serviceSettings)
    {
        $isNewServiceSettings = true;
        $updated = collect($invoice->service_invoice_settings)
                    ->map( function($settings) use($serviceSettings, &$isNewServiceSettings) {
                        if($settings['service_slug'] == $serviceSettings['service_slug']) {
                            $isNewServiceSettings = false;
                            return $serviceSettings;
                        }
                        return $settings;
                    });

        if($isNewServiceSettings) {
            $updated->push($serviceSettings);
        }

        return $updated->toArray();
    }

    protected function prepareData() {
        $data = request()->only('currency', 'selling_country_ids', 'taxes_country_id');

        $data['service_invoice_settings'] =  [
                                            'service_slug' => request('service_slug'),
                                            'settings' => request()->only('dmc_details', 'gross_total', 'invoice_notes')
                                        ];
        return $data;
    }
}