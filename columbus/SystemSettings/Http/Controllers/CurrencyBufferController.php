<?php

namespace Columbus\SystemSettings\Http\Controllers;

use App\Http\Controllers\ApiController;
use Columbus\SystemSettings\Models\SystemSetting;

class CurrencyBufferController extends ApiController
{
    public function index()
    {
        return $this->ok(SystemSetting::where('type', 'currency_conversions')->first(), "");
    }

}