<?php

namespace Columbus\SystemSettings\Http\Controllers;

use App\Http\Controllers\ApiController;
use Columbus\SystemSettings\Models\Filter;

class FilterController extends ApiController
{
    public function index()
    {
        $service = request('service');
        $qeury = Filter::withTrashed();
        if(!empty($service))
            $qeury->where('service', $service);

        return $this->ok( $qeury->get(), "Tour filters list");
    }

    public function create()
    {
        $data = request()->all(); //only(['service' 'groupby', 'name']); and optional [country_id]
        if( array_get($data, 'service'))
            return $this->ok(Filter::create($data), "Filter created");

        return $this->invalidRequest([ 'service' => "Service Must be given"], "You can't create Filter without specifing service");
    }

    public function update($filter_id)
    {
        $filter = Filter::findOrFail($filter_id);
        $data = request()->all(); //only([ 'groupby', 'name']);
        $filter->update($data);
        return $this->ok($filter, "Filter updated");
    }

    public function delete($filter_id)
    {
        $filter = Filter::withTrashed()
                        ->where('id', $filter_id)
                        ->first();
        $filter->forceDelete();
        return $this->ok($filter, "Filter deleted");
    }

    public function restore($filter_id)
    {
        $filter = Filter::onlyTrashed()->where('id', $filter_id)->first();

        $filter->restore();

        return $this->ok($filter, "Filter restored");
    }

    public function updateFilterGroupLabel()
    {
        $service = request('service');
        if(!empty($service)) {
            Filter::where('groupby', request('old'))->update(['groupby' => request('updated')]);
            return $this->index();
        }
        return $this->invalidRequest("Service Must be given", "You can't update Filter Group Heating/Title without specifing service");
    }
}