<?php

namespace Columbus\SystemSettings\Http\Controllers;

use App\Http\Controllers\ApiController;
use Columbus\SystemSettings\Models\CountryServiceMarkup;
use Columbus\SystemSettings\Http\Requests\CountryServiceMarkupRequest;

class CountryServiceMarkupsController extends ApiController
{
    public function index()
    {
        return $this->ok( CountryServiceMarkup::all() );
    }

    public function create(CountryServiceMarkupRequest $request)
    {
        return $this->ok($request->save(), "Service markups created");
    }

    public function update($markup_id, CountryServiceMarkupRequest $request)
    {
        return $this->ok($request->update($markup_id), "Service markups updated ");
    }

    public function delete($markup_id)
    {
        $markup = CountryServiceMarkup::findOrFail($markup_id);

        $markup->delete();

        return $this->ok($markup, "Service markups deleted");
    }
}