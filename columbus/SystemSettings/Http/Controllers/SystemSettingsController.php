<?php

namespace Columbus\SystemSettings\Http\Controllers;

use App\Http\Controllers\ApiController;
use Columbus\Constants\AmountValueTypes;
use Columbus\SystemSettings\Models\Code;
use Columbus\SystemSettings\Models\Currency;
use Columbus\SystemSettings\Models\SystemSetting;
use Columbus\SystemSettings\Models\InvoiceSetting;
use Columbus\Contracts\Vendors\Models\VendorService;
use Columbus\SystemSettings\Models\CurrencyExchangeRate;

class SystemSettingsController extends ApiController
{

    public function index()
    {
        return $this->ok( [
                'currencies' => Currency::currencies(),
                'currency_conversions' => CurrencyExchangeRate::getExchangeRates(),
                'emails' =>  SystemSetting::emails(),
                'product_codes' => Code::all(),
                'vendor_services' => VendorService::fetchAll(),
                'amountValueTypes'=> AmountValueTypes::values(),
                'invoice_settings' => InvoiceSetting::all()
            ],
            "System settings"
        );
    }

    public function currencies()
    {
        return $this->ok(Currency::currencies(), "Currencies");
    }
}
