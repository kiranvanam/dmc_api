<?php

namespace Columbus\SystemSettings\Http\Controllers;

use App\Http\Controllers\ApiController;
use Columbus\SystemSettings\Http\Requests\BankAccountCreateRequest;
use Columbus\Destination\Models\Country;
use Columbus\SystemSettings\Models\BankAccount;
use Illuminate\Http\Request;

class BankAccountsController extends ApiController
{
    public function all(Request $request) {
        return $this->ok(BankAccount::all());
    }

    public function save(BankAccountCreateRequest $request)
    {
        return $this->ok(BankAccount::create($request->all()), "Bank Account Created Successfully");
    }

    public function fetchByCountry($country_id, Request $request) {
        $country = Country::find($country_id);
        return $this->ok($country->bankAccounts, "Fetching Bank Accounts By Country");
    }

    public function update($id, BankAccountCreateRequest $request)
    {
        $bankAccount = BankAccount::find($id);
        $bankAccount->update($request->all());
        return $this->ok($bankAccount , "Bank Account Details Updated Successfully");
    }

    public function delete($id)
    {
        $bankAccount = BankAccount::find($id);

        // Delete The Bank Account
        $bankAccount->delete();

        return $this->ok($bankAccount, "Bank Account Deleted Successfully");
    }
}
