<?php

namespace Columbus\SystemSettings\Http\Controllers;

use App\Http\Controllers\ApiController;
use Columbus\SystemSettings\Models\Code;

class CodeGenerationController extends ApiController
{
    public function index()
    {
        return $this->ok(Code::all(), "codes listing");
    }
    public function create()
    {
         $module = Code::createCodeRecordWithData( request()->all());

         return $this->ok($module, "Code created for module - ". $module['module_name']);
    }
    public function nextCodeForModule($module_name)
    {
        return $this->ok(Code::nextCodeForModule($module_name), "Next Code generated");
    }
    public function generateForvendors()
    {
        return $this->ok(Code::generateForVendors(request()->all()), "Code created for vendor module" );
    }

    public function vendorsNextCode()
    {
        return $this->ok(Code::vendorsNextCode(), "Next Vendor Code details");
    }

    public function generateForTours()
    {
        return $this->ok(Code::generateForTours(request()->all()), "Code created for Tour Product module" );   
    }

    public function toursNextCode()
    {
        return $this->ok(Code::toursNextCode(), "Next Tour Code details");
    }

    public function generateForVehicles()
    {
        return $this->ok(Code::generateForVehicles(request()->all()), "Code created for Vehicle Product module" );   
    }

    public function vehiclesNextCode()
    {
        return $this->ok(Code::vehiclesNextCode(), "Next Vehicle Code details");
    }
    
    public function generateForGuides()
    {
        return $this->ok(Code::generateForGuides(request()->all()), "Code created for Guides Product module" );   
    }

    public function guidesNextCode()
    {
        return $this->ok(Code::guidesNextCode(), "Next Guides Code details");
    }
}