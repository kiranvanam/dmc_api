<?php

namespace Columbus\SystemSettings\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Columbus\SystemSettings\Models\CountryServiceMarkup;

class CountryServiceMarkupRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    
    public function rules()
    {
        return [
            'country_id' => 'required',
            'label'=> 'required|string', 
            'service_markups' => 'required',             
        ];
    }

    protected function prepareForValidation()
    {
    }

    
    public function messages()
    {
        return [
            'country_id.required' => "Country must be selected",
            'label.required' => "Markup label field must be provided",            
        ];
    }

    public function save()
    {
        return  CountryServiceMarkup::create($this->all());
    }

    public function update($markup_id)
    {
        $markup = CountryServiceMarkup::findOrFail($markup_id);

        $markup->update($this->all());
        
        return $markup;
    }
}