<?php

namespace Columbus\SystemSettings\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Columbus\SystemSettings\Models\DMCBankAccount;

class DMCBankAccountRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    
    public function rules()
    {
        return [
            'country_id' => 'required',
            'city'=> 'required|string', 
            'in_favour_off' => 'required|string', 
            'bank_name' => 'required|string',
            'branch_name' => 'required|string', 
            'account_number' => 'required|string',
            //'ifsc_code' => 'string',
            //'swift_code' => 'string', 
            //'iban_code' => 'string',
            //'address' => 'required|string'
        ];
    }

    protected function prepareForValidation()
    {
    }

    
    public function messages()
    {
        return [
            'country_id.required' => "Country must be selected",
            'city.required' => "City name field must be filled",
            'in_favour_off.required' => "in facour off field must be filled",
            'bank_name.required' => "Bank name field must be filled",
            'branch_name.required' => " Branch name field must be filled",
            'account_number.required' => "Account number field must be filled",
        ];
    }

    public function save()
    {
        return  DMCBankAccount::create($this->all());
    }

    public function update($account_id)
    {
        $bankAccount = DMCBankAccount::findOrFail($account_id);

        $bankAccount->update($this->all());
        
        return $bankAccount;
    }
}