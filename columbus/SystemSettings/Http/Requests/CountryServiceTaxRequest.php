<?php

namespace Columbus\SystemSettings\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Columbus\SystemSettings\Models\CountryServiceTax;

class CountryServiceTaxRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    
    public function rules()
    {
        return [
            'country_id' => 'required|integer',
            'service_slug'=> 'required|string',
            'tax_type' => "required|string", 
            'service_taxes' => 'required',             
        ];
    }

    protected function prepareForValidation()
    {
    }

    
    public function messages()
    {
        return [
            'country_id.required' => "Country must be selected",
            'service_slug.required' => "Service type must be selected",
            'tax_type.string' => "Tax type must be a valid string, if not working please report it",            
            'tax_type.required' => "Tax type must be sent in data",
        ];
    }

    public function save()
    {
        return  CountryServiceTax::create($this->all());
    }

    public function update($serviceTax_id)
    {
        $tax = CountryServiceTax::findOrFail($serviceTax_id);

        $tax->update($this->all());
        
        return $tax;
    }
}