<?php

namespace Columbus\SystemSettings\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class BankAccountCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'country_id' => 'required',
            'company_name' => 'required',
            'slug' => 'required',
            'bank_name' => 'required',
            'branch_name' => 'required',
            'swift_code' => 'required'
        ];
    }

    protected function prepareForValidation()
    {
        $this->merge([
                'slug' => str_slug($this->get('company_name')),
            ]
        );
    }

    /**
     * customizing error messages for (field,rule)
     */
    public function messages()
    {
        return [
            'country_id.required' => 'Please Select A Country',
            'company_name.required' => 'Please Enter Valid Company Name',
            'bank_name.required' => 'Please Enter Bank Name',
            'branch_name.required' => 'Please Enter Branch Name',
            'swift_code.required' => 'Please Enter Valid Swift Code Of The Bank',
        ];
    }

}
