<?php

namespace Columbus\SystemSettings\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class TaxCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'country_id' => 'required',
            'name' => 'required',
            'amount_type' => 'required',
            'amount' => 'required',
            'tax_type' => 'required'
        ];
    }

    protected function prepareForValidation()
    {
        $this->merge([
                'slug' => str_slug($this->get('name')),
            ]
        );
    }

    /**
     * customizing error messages for (field,rule)
     */
    public function messages()
    {
        return [
            'country_id.required' => 'Please Select A Country',
            'name.required' => 'Please Enter A Valid Name',
            'amount_type.required' => 'Please Select A Valid Amount Type',
            'amount.required' => 'Please Enter A Valid Amount',
            'tax_type.required' => 'Oops...!!! Tax Type was incorrectly specified',
        ];
    }

}
