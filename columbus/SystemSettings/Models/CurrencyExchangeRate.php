<?php

namespace Columbus\SystemSettings\Models;

use Illuminate\Database\Eloquent\Model;

class CurrencyExchangeRate extends Model
{
    
    protected $fillable = ['exchange_rate_settings', 'created_at'];

    protected $casts = [
                'exchange_rate_settings' => 'array'
            ];

    public $timestamps = false;

    public static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            $model->created_at = $model->freshTimestamp();
        });
    }

    static function getExchangeRates()
    {
        $currencyExchangeRates = self::latest()->first();

        if($currencyExchangeRates) {
            return $currencyExchangeRates->exchange_rate_settings;
        }

        return [
                'exchange_rate_buffer_amount' => 1, 
                'exchange_rate_buffer_amount_type' => 'PERCENT',
                'exchange_rates' => []
            ];
}

    static function updateExchangeRates($currency_exchage_rate_settings)
    {
        $currencyExchangeRates = static::create( ['exchange_rate_settings'=> $currency_exchage_rate_settings]);
        return $currencyExchangeRates;
    }
}