<?php

namespace Columbus\SystemSettings\Models;

use Illuminate\Database\Eloquent\Model;

class CountryServiceTax extends Model
{
    public $timestamps = false;
    protected $fillable = ['country_id', 'service_slug', 'tax_type', 'service_taxes'];

    protected $casts = [ 'service_taxes' => 'array' ];
}