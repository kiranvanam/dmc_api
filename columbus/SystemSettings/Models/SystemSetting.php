<?php

namespace Columbus\SystemSettings\Models;

use Illuminate\Database\Eloquent\Model;

class SystemSetting extends Model
{
    // setting type keys
    const EMAILS = "emails";

    protected $table = "system_settings";

    public $timestamps = false;

    protected $fillable = [ 'type', 'setting_values'];

    protected $casts = [
        'setting_values' => 'array'
    ];

    
    public static function emails()
    {
        return  static::where('type', self::EMAILS)->first();
    }
}
