<?php

namespace Columbus\SystemSettings\Models;

use Illuminate\Database\Eloquent\Model;

class Currency extends Model
{
  public $timestamps = false;

  protected  $fillable = ['symbol', 'code', 'name', 'is_active'];

  public static function currencies()
  {
      return Currency::where('name', '!=', "")->orderBy("symbol")->get();
  }
}
