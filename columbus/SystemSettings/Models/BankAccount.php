<?php

namespace Columbus\SystemSettings\Models;

use Illuminate\Database\Eloquent\Model;

class BankAccount extends Model
{
    public $timestamps = false;
    protected $fillable = ['country_id', 'company_name', 'slug', 'bank_name', 'branch_name', 'swift_code', 'address', 'remittance_charge'];
}
