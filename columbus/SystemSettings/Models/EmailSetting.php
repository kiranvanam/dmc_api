<?php

namespace Columbus\SystemSettings\Models;

use Illuminate\Database\Eloquent\Model;

class EmailSetting extends Model
{
    protected $fillable = [ 'from', 'actions', 'password',  'cc'];

    protected $casts = [ "cc" => 'array', 'actions' => 'array' ];


    public static function enableEmailSenderForAction($action) 
    {
        
        $settings = EmailSetting::all();
        
        $email = $settings->first( 
                                function($emailSetting) use($action) {
                                    return in_array($action, $emailSetting->actions);
                                }
                            );
        // if specific one not found, search for "default" one
        if(empty($email)) {
            $email = $settings->first( 
                                    function($emailSetting) use($action) {
                                        return in_array("default", $emailSetting->actions);
                                    }
                                );
        }             

        if(empty($email)) 
            return $email;

        
        /*'mail.host' => 'smtp.yandex.ru',
        'mail.port' => 465,
        'mail.encryption' =>'ssl',
        'mail.username' => 'username',
        'mail.password' => 'password' 
        */
        \Config::set('mail.username', $email->from);
        \Config::set('mail.password', $email->password);
        \Config::set('mail.encryption', 'tls');
        \Config::set('mail.port', '587');
        \Config::set('mail.host', 'smtp.gmail.com');
        $provider = new \Columbus\SystemSettings\Providers\DynamicMailerServiceProvider();
        $provider->register();
        /* $host = config('mail.host');
        $port = config('mail.port');
        $encryption = config('mail.encryption');
        logger(" $host - $port -  ");
        $transport = (new \Swift_SmtpTransport($host, $port))
                       ->setUsername($email->from)
                       ->setPassword($email->password);
                       //->setEncryption($encryption);

	    \Mail::setSwiftMailer(new \Swift_Mailer($transport)); */

        return $email;
    }
}