<?php

namespace Columbus\SystemSettings\Models;

use Illuminate\Database\Eloquent\Model;

class DMCBankAccount extends Model
{
    public $table = "dmc_bank_accounts";

    protected  $fillable = ['country_id', 'city', 'in_favour_off', 'bank_name','branch_name', 'account_number',
            'ifsc_code','swift_code', 'currency', 'iban_code','address'];

}