<?php

namespace Columbus\SystemSettings\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class InvoiceSetting extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];
     
    protected $fillable = ['selling_country_ids', 'currency', 'taxes_country_id'  ,'service_invoice_settings'];

    protected $casts = [
        'selling_country_ids'=> 'array',
        'service_invoice_settings' => 'array'
    ];
}