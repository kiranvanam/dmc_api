<?php

namespace Columbus\SystemSettings\Models;

use Illuminate\Database\Eloquent\Model;

class Tax extends Model
{
    public $timestamps = false;
    protected $fillable = ['country_id', 'name', 'slug', 'agency_name', 'amount_type', 'amount', 'tax_type'];
}
