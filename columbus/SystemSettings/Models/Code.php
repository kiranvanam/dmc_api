<?php

namespace Columbus\SystemSettings\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Code extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];

    protected $fillable = ['module_name', 'code_prefix', 'start_code', 'last_used_code'];

    protected $modules = [ "vendors" ];


    protected static function getCodeRecordForModule($data=[])
    {
        return Code::where('module_name', $data['module_name'])->first();
    }

    public static function createCodeRecordWithData($data)
    {

        $code = self::getCodeRecordForModule($data);

        if(!$code) {
            //initially start code & last used code are both same
            $data['last_used_code'] = $data['start_code'];
            $code = Code::create($data);
        }

        return $code;
    }

    public static function nextCodeForModule($module_name) {
        return self::updateLastUsedCode(self::getCodeRecordForModule( ['module_name' => $module_name]));
    }

    public static function getNextCodeForModule($module_name)
    {
        $code = self::nextCodeForModule($module_name);
        return $code->code_prefix . "-" . $code->last_used_code;
    }
    protected static function getNextCodeForTransfers() {
        return static::getNextCodeForModule("transfers");
    }

    public static function getNextCodeForMeal()
    {
        return static::getNextCodeForModule("meals");
    }


    public static function updateLastUsedCode($code)
    {
        if($code) {
            $last_used_code = $code->last_used_code;
            if( empty( $last_used_code) ) {
                $last_used_code = $code->start_code;
            }else {
                $last_used_code = ++$code->last_used_code;
            }
            $code->last_used_code = str_pad($last_used_code, strlen($code->start_code), 0, STR_PAD_LEFT);
            $code->save();
        }

        return $code;
    }


    public static function generateForVendors($data)
    {
        $data['module_name'] = "vendors";

        return self::createCodeRecordWithData($data);
    }

    public static function vendorsNextCode()
    {
       $code = self::getCodeRecordForModule(['module_name'=> "vendors" ]);

       return self::updateLastUsedCode($code);
    }

    public static function generateForTours($data)
    {
        $data['module_name'] = "tours";

        return self::createCodeRecordWithData($data);
    }

    public static function toursNextCode()
    {
       $code = self::getCodeRecordForModule(['module_name'=> "tours" ]);

       return self::updateLastUsedCode($code);
    }

    public static function generateForVehicles($data)
    {
        $data['module_name'] = "vehicles";

        return self::createCodeRecordWithData($data);
    }

    public static function vehiclesNextCode()
    {
       $code = self::getCodeRecordForModule(['module_name'=> "vehicles" ]);

       return self::updateLastUsedCode($code);
    }

    public static function generateForGuides($data)
    {
        $data['module_name'] = "guides";

        return self::createCodeRecordWithData($data);
    }

    public static function guidesNextCode()
    {
       $code = self::getCodeRecordForModule(['module_name'=> "guides" ]);

       return self::updateLastUsedCode($code);
    }
}
