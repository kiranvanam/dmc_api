<?php

namespace Columbus\SystemSettings\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Filter extends Model
{
    use SoftDeletes;

    public $table = "filters";
    
    protected $dates = ['deleted_at'];

    protected $fillable = ['country_id', 'service', 'groupby', 'name'];
}