<?php

namespace Columbus\SystemSettings\Models;

use Illuminate\Database\Eloquent\Model;

class CountryServiceMarkup extends Model
{
    public $timestamps = false;
    protected $fillable = ['country_id', 'label', 'service_markups'];

    protected $casts = [ 'service_markups' => 'array' ];
}