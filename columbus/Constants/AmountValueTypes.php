<?php
namespace Columbus\Constants;


class AmountValueTypes
{
    use HasEnums;

    const PERCENT = 'percent';
    const VALUE = 'value';
}
