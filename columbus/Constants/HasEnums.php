<?php
namespace Columbus\Constants;


use ReflectionClass;

trait HasEnums
{
    /**
     * The array of enumerators of a given group.
     *
     * @param  null|string $group
     * @return  array
     */
    static function enums($group = null)
    {
        $constants = (new ReflectionClass(get_called_class()))->getConstants();

        if ($group) {
            return array_filter($constants, function ($key) use ($group) {
                return strpos($key, $group.'_') === 0;
            }, ARRAY_FILTER_USE_KEY);
        }

        return $constants;
    }

    public static function values()
    {
      $values = static::enums();
      $final = [];
      
      foreach ($values as $key => $value) {
        $status = [ 'id'=>$value ,'name' => $key ];
        $final[] = $status;
      }
      return $final;
    }

    /**
     * Check if the given value is valid within the given group.
     *
     * @param  mixed $value
     * @param  null|string $group
     * @return  bool
     */
    static function isValidEnumValue($value, $group = null)
    {
        $constants = static::enums($group);

        return in_array($value, $constants);
    }

    /**
     * Check if the given key exists.
     *
     * @param  mixed $key
     * @return  bool
     */
    static function isValidEnumKey($key)
    {
        return array_key_exists($key, static::enums());
    }
}