<?php

namespace Columbus\Constants;

use Columbus\Constants\HasEnums;


class TransferPointTypes
{
    use HasEnums;

    const AIRPORT = 1;
    const ACCOMMODATION = 2;
    const COACH_STATION = 3;
    const CRUISE_TERMINAL = 4;
    const OTHERS = 5;
}
