<?php

namespace Columbus\Constants;


class LocationLevel
{
    use HasEnums;

    const REGION = 1;
    const CITY = 2;
    const AREA = 3;
}
