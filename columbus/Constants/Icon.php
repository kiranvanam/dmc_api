<?php

namespace Columbus\Constants;

class Icon
{
   protected static $icons = [ 
            ["icon" =>"soap-icon-fridge", 'name' => "Fridge"],
            ["icon" => "soap-icon-fireplace", "name" => "Fireplace"],
            ["icon" => "soap-icon-television", "name" => "TV" ],
            ["icon" => "soap-icon-fmstereo", "name" => "Stereo"],
            ["icon" => "soap-icon-beach", "name" =>"Beach"],
            ["icon" => "soap-icon-food", "name" => "Food"],
            ["icon" => "soap-icon-fueltank", "name" => "Fuel Tank"],
            ["icon" => "soap-icon-breakfast", "name" => "Breakfast"],
            ["icon" => "soap-icon-coffee", "name" => "Coffe"],
            ["icon" => "soap-icon-party", "name" => "Party"],
            ["icon" => "soap-icon-savings", "name" =>"Savings"],
            ["icon" => "soap-icon-address", "name" => "Address"],
            ["icon" => "soap-icon-horn", "name" =>"Horn"],
            ["icon" => "soap-icon-conference", "name" => "Conference"],
            ["icon" => "soap-icon-support", "name" => "Support" ],
            ["icon" => "soap-icon-tree", "name" => "Tree"],
            ["icon" => "soap-icon-friends", "name" => "Friends"],
            ["icon" => "soap-icon-winebar", "name" => "Winebar"],
            ["icon" => "soap-icon-plans", "name" =>"Plans"],
            ["icon" => "soap-icon-guideline","name" =>"Guideline"],
            ["icon" => "soap-icon-wifi","name" =>"Wifi"],
            ["icon" => "soap-icon-binoculars", "name" =>"Binoculars"],
            ["icon" => "soap-icon-key", "name" =>"Key"],
            ["icon" => "soap-icon-fork", 'name' => "Fork"],
            ["icon" => "soap-icon-locations", "name" => "Locaions"],
            ["icon" => "soap-icon-couples", "name" => "Couples"],
            ["icon" => "soap-icon-card" , "name" => "Card"],
            ["icon" => "soap-icon-fitnessfacility", "name" => "Fitness facility" ],
            ["icon" => "soap-icon-flexible", "name" => "Flixible"],
            ["icon" => "soap-icon-phone", "name" => "Phone"],
            ["icon" => "soap-icon-cruise", "name" => "Cruise" ],
            ["icon" => "soap-icon-joystick", "name" => "Joystick" ],
            ["icon" => "soap-icon-lost-found", "name" => "Lost found"],
    ];

    public static function icons()
    {
      return static::$icons;
    }

}