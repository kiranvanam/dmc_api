<?php

namespace Columbus\Constants;

use Columbus\Constants\HasEnums;


class MealPreferences
{
    use HasEnums;

    const JAIN_VEG = 1;
    const VEG = 2;
    const NON_VEG = 3;

}
