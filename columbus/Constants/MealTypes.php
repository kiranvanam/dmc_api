<?php

namespace Columbus\Constants;

use Columbus\Constants\HasEnums;


class MealTypes
{
    use HasEnums;

    const BREAKFAST = 1;
    const LUNCH = 2;
    const DINNER = 3;
}
