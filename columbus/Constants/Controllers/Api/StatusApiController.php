<?php

namespace App\Http\Controllers\Api;

use App\Constants\Status;
use App\Http\Controllers\ApiController;
use App\Http\Requests;
use Illuminate\Http\Request;

class StatusApiController extends ApiController
{

    public function index()
    {
      return $this->ok(Status::values(),"Status Listing");
    }
}