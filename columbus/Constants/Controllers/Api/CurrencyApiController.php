<?php

namespace Columbus\Constants\Http\Controllers\Api;

use App\Http\Controllers\ApiController;
use App\Http\Requests;
use Columbus\Constants\Currency;
use Illuminate\Http\Request;

class CurrencyApiController extends ApiController
{
    
    public function index()
    {
      return $this->ok(Currency::values(),"Currency Listing");
    }
}