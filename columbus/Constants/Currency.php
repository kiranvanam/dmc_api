<?php

namespace Columbus\Constants;


class Currency
{
    use HasEnums;

    const USD = 1;
    const EUR = 2;
    const GBP = 3;
    const AUD = 4;
    const SGD = 5;
    const INR = 6;
}