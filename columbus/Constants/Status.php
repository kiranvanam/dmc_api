<?php
namespace Columbus\Constants;


class Status
{
    use HasEnums;

    const ACTIVE = 1;
    const INACTIVE = 2;
    const HOLD = 3;
    const SUSPENDED = 4;

}