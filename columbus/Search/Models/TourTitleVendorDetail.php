<?php

namespace Columbus\Search\Models;

use Columbus\Contracts\Tours\Models\TourTitleVendorDetail as BaseTourTitleVendorDetail;
use Columbus\Contracts\Tours\Models\TourTitlePrivateServiceMapping;

class TourTitleVendorDetail extends BaseTourTitleVendorDetail
{
    
    public function vehicles()
    {
        return $this->hasManyThrough(TourTitlePrivateServiceMapping::class);
    }

    public function tickets()
    {
        return $this->hasManyTickets();
    }

}