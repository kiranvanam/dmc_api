<?php

namespace Columbus\Search\Models;

use Columbus\Contracts\Vehicles\Models\Vehicle as BaseVehicle;
use Columbus\Contracts\Tours\Models\TourTitlePrivateVehiclePricing;

class Vehicle extends BaseVehicle
{
    
    function pricing()
    {
        return $this->hasOne( TourTitlePrivateVehiclePricing::class, 'tour_title_private_service_mapping_id');
    }

}