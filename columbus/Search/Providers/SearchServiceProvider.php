<?php

namespace Columbus\Search\Providers;

use Illuminate\Support\Facades\Route;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class SearchServiceProvider extends ServiceProvider
{
    protected $namespace = "Columbus\Search\Http\Controllers";

    public function boot()
    {
        $this->app->router->group(['namespace' => $this->namespace], function() {
            if (! $this->app->routesAreCached()) {
                require __DIR__.'/../routes.php';
            }
        });        
    }
}
