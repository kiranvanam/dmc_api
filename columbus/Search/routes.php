<?php

Route::group(['prefix'=>'api/search', 'middleware'=>['auth:api','cors']], function() {
    Route::group(['prefix' => 'tours'], function() {
        Route::post("", 'TourController@search');
        Route::post("add-ons", 'AddOnController@search');
        Route::post("vehicle-availability", 'VehicleAvailabilityCheckController@check');
    });
    Route::group(['prefix'=> 'tickets'], function() {
        Route::post("", 'TicketController@search');
    });

    Route::group(['prefix'=> 'meals'], function() {
        Route::post("", 'RestaurantController@search');
    });
    Route::group(['prefix'=> 'guides'], function() {
        Route::post("", 'GuideController@search');
    });
    Route::group(['prefix'=> 'hotels'], function() {
        Route::post("", 'HotelController@index');
    });
    Route::group(["prefix" => "hotels"] , function() {
        Route::post("/", "HotelController@search");

        Route::group(['prefix'=> "grn/{hotel_code}"], function() {
            Route::post("/refetch", "GrnHotelController@refetchHotelRates");
            Route::get("/gallery", "GrnHotelController@fetchGallery");
            Route::post("cancellation-policies", 'GrnHotelController@fetchCancellationPolicies');
            Route::post("rate-recheck", "GrnHotelController@rateRecheck");
        });
    });

    Route::get("mapped-cities", "CityMappingController@index");
});

/* Route::get("/search-hotels", "HotelController@searchFromBrowser");

Route::get("/grn/hotels", function(){
    $result = \Storage::disk('public')->get("/grn-search-hotels-comprehensive.json");
    return  json_decode($result, true);
});
Route::get("php.ini", function(){
    return phpinfo();
}); */

/* Route::get("change-tickets", function() {
    $services = \Columbus\Contracts\Vendors\Models\VendorService::where('slug', 'excursion-tickets')->get();

    $services->each( function($service) {
        $service->update(['slug' => 'tickets', 'name' => 'Tickets']);
    });

    $settings = \Columbus\SystemSettings\Models\InvoiceSetting::all();
    $settings->each( function($setting) {
        $serviceWiseInvoiceSettings = $setting->service_invoice_settings;
        foreach ($serviceWiseInvoiceSettings as $key => $serviceSettings) {
            if( array_get( $serviceSettings, 'service_slug') == 'excursion-tickets' ) {
                $serviceSettings['service_slug'] = "tickets";
                $serviceWiseInvoiceSettings[$key] = $serviceSettings;
                $setting->update( ['service_invoice_settings' => $serviceWiseInvoiceSettings ]);
            }
            //return true;
        };
    });
    $agencies = \Columbus\Agents\Models\TravelAgency::all();
    $agencies->each( function($agency) {
        $settings = array_get($agency->markups,'service_markups');
        if(!empty( $settings ))
        foreach ($settings as $key => $serviceMarkups) {
            if( array_get($serviceMarkups, 'service_slug') == 'excursion-tickets' ) {
                $serviceMarkups['service_slug'] = "tickets";
                $settings[$key] = $serviceMarkups;
                $agency->update( ['markups' => $settings]);
            }
        }
    });
    $taxes = \Columbus\SystemSettings\Models\CountryServiceTax::all();

    $taxes->each( function($tax) {
        if( $tax->service_slug == 'excursion-tickets') {
            $tax->update(['service_slug'=> "tickets"]);
        }
    });
    // tour title private vendor service mappings - service column
    //$mappings = \DB::table('tour_title_private_vendor_detail_service_mappings')->get();

}); */


