<?php

namespace Columbus\Search\Http\Controllers;

use App\Http\Controllers\ApiController;
use Columbus\Contracts\Guides\Models\GuideTitle;

class GuideController extends ApiController
{
    public function search()
    {
        $travel_date = request('travel_date'); // 'Y-m-d'
        $city_id = request('city_id');

        $guides =  GuideTitle::where('city_id', $city_id)
                            ->with(['vendors.pricing' => 
                                    function($query) use ($travel_date) {
                                        $query->where('date', $travel_date); 
                                    }
                                ]
                            )
                            ->get();
        return $this->ok($guides, "guide results");
    }
}