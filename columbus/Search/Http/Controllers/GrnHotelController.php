<?php

namespace Columbus\Search\Http\Controllers;

use App\Http\Controllers\ApiController;
use Columbus\ThirdParty\Utilities\GRN\Accommodation;
use Columbus\Search\Http\Requests\GRNRefetchHotelRatesRequest;

class GrnHotelController extends ApiController
{
    public function refetchHotelRates($hotel_code, GRNRefetchHotelRatesRequest $request)
    {
        $accommodation = new Accommodation();
        $result = $accommodation->refetchRatesForHotel($request->all());
        return $this->ok($result, "Hotel Rates are fetched");
    }

    public function fetchGallery($hotel_code, Accommodation $accommodation)
    {
        return $this->ok( $accommodation->fetchHotelGallery($hotel_code));
    }

    public function fetchCancellationPolicies($hotel_code, Accommodation $accommodation)
    {
        return $this->ok($accommodation->fetchCancellationPolicies( request()->all()));
    }

    public function rateRecheck($hotel_code, Accommodation $accommodation)
    {
        return $this->ok($accommodation->rateRecheck(request()->all()));
    }
}