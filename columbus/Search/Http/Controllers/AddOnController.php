<?php

namespace Columbus\Search\Http\Controllers;

use Columbus\Utilities\DateUtil;
use App\Http\Controllers\ApiController;
use Columbus\Contracts\Tickets\Models\TicketTitle;

class AddOnController extends ApiController
{
    public function search()
    {
        $travel_date = request('travel_date', null);
        $city_id = request('city_id', null);
        $ticket_ids = request('add_ons', null);

        $tickets = TicketTitle::whereIn('id', $ticket_ids)
                            ->where('city_id', $city_id)
                            ->with(['vendors.pricing' =>
                                    function($query) use ($travel_date) {
                                        $query->where('date', $travel_date);
                                    }
                                ]
                            )
                            ->get();

        return $this->ok( $tickets , "Addons available for this tour" );
    }
}