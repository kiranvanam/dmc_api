<?php

namespace Columbus\Search\Http\Controllers;

use Columbus\Utilities\DateUtil;
use Columbus\Search\Models\Vehicle;
use App\Http\Controllers\ApiController;
use Columbus\Contracts\Tours\Models\TourTitle;
use Columbus\Contracts\Meals\Models\MealVendor;
use Columbus\Contracts\Guides\Models\GuideTitle;
use Columbus\Contracts\Tickets\Models\TicketTitle;
use Columbus\Contracts\Tours\Models\TourTitleVendorDetail;
use Columbus\Contracts\Tours\Models\TourTitlePrivateAvailability;
use Columbus\Contracts\Tours\Models\TourTitleVendorDetailSICPricing;

class TourController extends ApiController
{
    public function search()
    {
        $check_in = request('check_in');
        $check_out = request('check_out');


        $check_in = $check_in ? $check_in : request('travel_date');
        $check_out = $check_out ? $check_out : request('travel_date');

        //$travel_date = request('travel_date'); // 'Y-m-d'
        $city_id = request('city_id');
        $types = request('types', ['sic', 'private']);
        $tourTitles = TourTitle::where('city_id', $city_id)->whereIn('type', $types)->get();

        $tourTitles->each( function($tourTitle) use($check_in, $check_out) {
            if( $tourTitle->isSIC()) {
                $tourTitle->vendors = TourTitleVendorDetail::where('tour_title_id', $tourTitle->id)
                                                            ->with(['pricing'=> function($query) use($check_in, $check_out)  {
                                                                                        $query->whereBetween('date', [$check_in, $check_out]);
                                                                                    }
                                                                    ])
                                                            ->with('cancellationPolicies')
                                                            ->get();

            }elseif( $tourTitle->isPrivate()) {
                // operating days &  services (attached to this service)
                $tourTitle->vendor_details = TourTitleVendorDetail::where('tour_title_id', $tourTitle->id)
                                                                ->with('availableServices')
                                                                ->first();

                if( empty( $tourTitle->vendor_details) ||  empty( $tourTitle->vendor_details->availableServices ) ) {
                    $tourTitle->vendor_details = NULL;
                    return;
                }
                //service detils for
                $vehicleIds = $tourTitle->vendor_details->availableServices->where('service', 'transfers')->pluck('service_id')->all();
                $serviceMappingIds = $tourTitle->vendor_details->availableServices->where('service', 'transfers')->pluck('id')->all();
                if(!empty($vehicleIds) ) {

                    $tourTitle->vendor_details->vehicles = [
                                                "vehicles" => Vehicle::with('brand')
                                                                    ->whereIn('id', $vehicleIds)
                                                                    ->with([ 'pricing' =>
                                                                                function($query) use($check_in, $check_out, $serviceMappingIds) {
                                                                                    $query->whereBetween('date', [$check_in, $check_out]);
                                                                                    $query->whereIn('tour_title_private_service_mapping_id', $serviceMappingIds);
                                                                                }
                                                                        ]
                                                                    )
                                                                    ->get(),
                                                "cancellation_policies" => \App\CancellationPolicy::where( 'service_id', $tourTitle->vendor_details->id)
                                                                                                ->where('service', 'tour-title-vendor-transfers')
                                                                                                ->first()
                                            ];
                }
                $ticketIds = $tourTitle->vendor_details->availableServices->where('service', 'tickets')->pluck('service_id')->all();

                if( !empty($ticketIds) ) {

                    $tourTitle->vendor_details->tickets = TicketTitle::whereIn('id', $ticketIds)
                                                                    ->with(['vendors.pricing' =>
                                                                            function($query) use ($check_in, $check_out) {
                                                                                $query->whereBetween('date', [$check_in, $check_out]);
                                                                            }
                                                                        ]
                                                                    )
                                                                    ->get();
                }

                $guideIds = $tourTitle->vendor_details->availableServices->where('service', 'guides')->pluck('service_id')->all();
                if( !empty($guideIds) ) {

                    $tourTitle->vendor_details->guides = GuideTitle::whereIn('id', $guideIds)
                                                                    ->with(['vendors.pricing' =>
                                                                            function($query) use ($check_in, $check_out) {
                                                                                $query->whereBetween('date', [$check_in, $check_out]);
                                                                            }
                                                                        ]
                                                                    )
                                                                    ->get();
                }

                $mealVendorIds = $tourTitle->vendor_details->availableServices->where('service', 'meals')->pluck('service_id')->all();

                if( !empty($mealVendorIds) ) {

                    $tourTitle->vendor_details->restaurants = MealVendor::where('id', $mealVendorIds)
                                                                    ->with(['mealPlans.pricing' =>
                                                                                function($query) use($check_in, $check_out) {
                                                                                    $query->whereBetween('date', [$check_in, $check_out]);
                                                                                }
                                                                        ]
                                                                    )
                                                                    ->with('cancellationPolicies')
                                                                    ->get();
                }
            }
        });

        return $this->ok($tourTitles, "Tour Serch form");
    }
}