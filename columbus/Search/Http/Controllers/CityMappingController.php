<?php

namespace Columbus\Search\Http\Controllers;

use Columbus\Destination\Models\City;
use App\Http\Controllers\ApiController;
use Columbus\ThirdParty\Accommodation\MasterData\GRN\Models\GrnDestination;

class CityMappingController extends ApiController
{
    public function index()
    {

        $cities = City::all();

        $finalCities = [];

        $cities->each( function ($city) use (&$finalCities) {

            $grnCities = GrnDestination::allMatchedDestinationsForName($city->name);
            array_walk($grnCities, function($grnCity) use (&$finalCities, $city) {
                $grnCity['id'] = $city->id;
                $grnCity['name'] = $city->name;
                $finalCities[] = $grnCity;
            });

        });

        return $this->ok($finalCities, "Mapped Ctities");

    }
}