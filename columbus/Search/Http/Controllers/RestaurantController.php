<?php

namespace Columbus\Search\Http\Controllers;

use App\Http\Controllers\ApiController;
use Columbus\Contracts\Meals\Models\MealVendor;

class RestaurantController extends ApiController
{
    
    public function search()
    {
        $travel_date = request('travel_date'); // 'Y-m-d'
        $city_id = request('city_id');

        $resturants = MealVendor::where('city_id', $city_id)
                            ->with(['mealPlans.pricing' => function($query) use($travel_date) {
                                            $query->where('date', $travel_date);
                                        }
                                ]
                            )
                            ->with('cancellationPolicies')
                            ->get();

        return $this->ok( $resturants , "Restaurant results");
    }
}