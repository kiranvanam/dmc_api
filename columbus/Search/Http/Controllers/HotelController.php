<?php

namespace Columbus\Search\Http\Controllers;

use Carbon\Carbon;
use App\Http\Controllers\ApiController;
use Columbus\Contracts\Hotels\Models\Hotel;
use Columbus\ThirdParty\Utilities\GRN\Accommodation as GRNHotelSearch;

class HotelController extends ApiController
{
    public function search()
    {

        $requestSearchData =  request()->all();
        logger("Request Details", $requestSearchData);

        $check_in = request('check_in');
        $check_out = request('check_out');
        $nationality = request('nationality', "IN");
        $city_id = request('city_id');
        $service_souce = request('service_source');
        $results = [];
        $d1 = Carbon::now();
        if($service_souce == "grn") {

            $searchCriteria = [
                    "hotel_codes" => [],
                    "checkin" => $check_in,
                    "checkout" => $check_out, // YYYY-MM-DD
                    "client_nationality" => $nationality,
                    "grn_code" => request('grn_code', 'D!000160'),
                    "rooms" => request('rooms')
                ];
            $accommodation =  new GRNHotelSearch();
            //results is array with "no_of_hotels" and hotels keys
            $results = $accommodation->search($searchCriteria);

        }elseif($service_souce == "contracting") {
            $hotels = Hotel::with(['rooms.pricing' => function($query) use($check_in, $check_out) {
                                    $query->whereBetween('date', [$check_in, $check_out]);
                            }])
                            ->where('city_id', $city_id)
                            ->with('cancellationPolicies')
                            ->get();
            $results['hotels'] = $hotels;
            $results['no_of_hotels'] = $hotels->count();
        }

        $d2 = Carbon::now();
        $total_processing_time = $d2->diffInSeconds($d1);
        $results['time_taken'] = $total_processing_time;
        $totlaHotels = array_get($results, 'no_of_hotels', 0);

        return $this->ok(
            $results,
            $totlaHotels ? " Total $totlaHotels Hotels found" : "No Hotel is available for your search criteria"
        );
    }

    /*
     * it's fake request , just change check-in and check-out dates
     */
    function searchFromBrowser() {
        request()->merge([
            "booking_date" => "2019-11-21",
            "check_in" => "2019-10-21",
            "check_out" => "2019-10-23",
            "city_id" => 1,
            "city_name" => "Dubai",
            "country_cca3_code" => "ARE",
            "country_id" => 8,
            "grn_code" => "D!000160",
            "hotel_category" => "2,5",
            "nationality" => "IN",
            "no_of_nights" => 1,
            "no_of_rooms" => 1,
            "rooms" => [[ "adults" => 2, "no_of_children" => 0, "children_ages" => [] ]],
            "search_source" => "grn",
            "service" => "hotels",
            "service_source" => "grn"
        ]);
        return $this->search();
    }
}