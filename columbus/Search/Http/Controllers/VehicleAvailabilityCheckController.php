<?php

namespace Columbus\Search\Http\Controllers;

use \App\Http\Controllers\ApiController; 
use Columbus\Contracts\Tours\Models\TourTitleVendorDetail;
use Columbus\Contracts\Tours\Models\TourTitlePrivateVehiclePricing;

class VehicleAvailabilityCheckController extends ApiController
{
    public function check()
    {
        $tour_title_id = request('tour_title_id');
        $vendor_id = request('vendor_id');
        $vehicle_id = request('vehicle_id');
        $travel_date = request('travel_date');
        $no_of_units = request('no_of_units');

        $vendorDetail = TourTitleVendorDetail::where('tour_title_id', $tour_title_id)
                                                        ->where('vendor_id', $vendor_id)
                                                        ->first();

        if( empty($vendorDetail) ) 
            return $this->ok( NULL , "Not matching tour vendor found");

        $serviceMapping = \DB::table('tour_title_private_vendor_detail_service_mappings')
                        ->where('tour_title_vendor_detail_id', $vendorDetail->id)
                        ->where('service_id', $vehicle_id)
                        ->where('service', 'transfers')
                        ->first();
        // check if no_of_booked vehicles are allowed
        if( empty($serviceMapping) )
            return $this->ok( NULL , "Vehilce not mapped/allowed for this  Service/Tour");

        $pricing = TourTitlePrivateVehiclePricing
                            ::where('tour_title_private_service_mapping_id', $serviceMapping->id)
                            ->where('date', $travel_date)
                            ->first();

        // check if no_of_booked vehicles are allowed
        if( empty($pricing) )
            return $this->ok( NULL , "Vehilce not Available for this  Service/Tour");

        // if already on request, then no need to update with the new pricing in front end
        // if requestd no of vehicles are not available then
        if( $pricing->is_on_request || $pricing->inventory < $no_of_units ) {
            $pricing->is_on_available =  FALSE;
            $pricing->is_on_request = TRUE;
        }
        
        $pricing->no_of_units = $no_of_units;
        
        return $this->ok($pricing, "Vehicle Pricings details");                                                     
    }
    
}