<?php

namespace Columbus\Search\Http\Controllers;

use Columbus\Utilities\DateUtil;
use App\Http\Controllers\ApiController;
use Columbus\Contracts\Tickets\Models\TicketTitle;

class TicketController extends ApiController
{
    public function search()
    {
        $check_in = request('check_in');
        $check_out = request('check_out');

        $check_in = $check_in ? $check_in : request('travel_date');
        $check_out = $check_out ? $check_out : request('travel_date');

        $travelDates = [$check_in, $check_out]; //DateUtil::allDatesBetween($check_in, $check_out);

        $city_id = request('city_id');

        $tickets = TicketTitle::where('city_id', $city_id)
                                ->with(['vendors.pricing' =>
                                        function($query) use ($travelDates) {
                                            $query->whereBetween('date', $travelDates);
                                        }
                                    ]
                                )
                                ->get();

        return $this->ok($tickets);
    }
}