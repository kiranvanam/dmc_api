<?php

namespace Columbus\Search\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class GRNRefetchHotelRatesRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "search_id" => "required|string",
            "hotel_code" => "required|string",
        ];
    }

    public function messaages()
    {
        return [
            "search_id.required" => "Search-id is required to fetch Hotel rates",
            "hotel_code.required" => "Hotel code is required to fetch hotel rates"


        ];
    }
}