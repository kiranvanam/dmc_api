<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRegionIdToTranasferPointsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('transfer_points', function (Blueprint $table) {
            $table->renameColumn('tp_type_id', 'transfer_point_type_id');
            $table->integer('region_id')->after('country_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('transfer_points', function (Blueprint $table) {
            $table->renameColumn('transfer_point_type_id','tp_type_id');
            $table->dropColumn('region_id');
        });
    }
}
