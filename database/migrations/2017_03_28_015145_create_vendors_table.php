<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVendorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vendors', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('country_id')->index();
            $table->string('name');
            $table->string('slug');
            $table->string('description',500)->default("");
            $table->string('address', 500)->default("");
            $table->text('contact_details')->nullable();
        });

        Schema::create('vendor_service_mappings', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('vendor_id');
            $table->integer('vendor_service_id');
            $table->string('values')->default("");
            //
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vendor_service_mappings');
        Schema::dropIfExists('vendors');
    }
}
