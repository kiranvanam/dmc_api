<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCountryDefaultCurrenciesTable extends Migration
{

    public function up()
    {
        Schema::create('country_default_currencies', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('country_id');
            $table->integer('currency_id')->default(0);
            $table->string('module',50)->default('vendors');
        });
    }

    public function down()
    {
        Schema::dropIfExists('country_default_currencies');
    }
}
