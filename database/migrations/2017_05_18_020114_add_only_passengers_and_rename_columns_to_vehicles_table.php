<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddOnlyPassengersAndRenameColumnsToVehiclesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('vehicles', function (Blueprint $table) {
            $table->integer('passengers_only')->after('seater');
            $table->integer('passengers_with_luggage')->after('passengers_only');
            $table->renameColumn('seater_information', 'passengers_with_luggage_notes');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('vehicles', function (Blueprint $table) {
            $table->dropColumn('passengers_only');
            $table->dropColumn('passengers_with_luggage');
            $table->renameColumn('passengers_with_luggage_notes', 'seater_information');
        });
    }
}
