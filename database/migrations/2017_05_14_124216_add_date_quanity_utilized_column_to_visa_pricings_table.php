<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDateQuanityUtilizedColumnToVisaPricingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('visa_pricings', function (Blueprint $table) {
            $table->date('date')->index()->after('visa_id');
            $table->integer('quantity')->default(0)->after('vendor_id');
            $table->integer('utilized')->default(0)->after('quantity');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('visa_pricings', function (Blueprint $table) {
            $table->dropColumn('date');
            $table->dropColumn('quantity');
            $table->dropColumn('utilized');
        });
    }
}
