<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddModuuleApiCredentialsCountryCodeMappingsColumnsToVendorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('vendors', function (Blueprint $table) {
            $table->string('module', 50)->default('contracts');
            $table->text('api_credentials',1000)->nullable();
            $table->text('api_vendor_countries')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('vendors', function (Blueprint $table) {
            $table->dropColumn('module');
            $table->dropColumn('api_credentials');
            $table->dropColumn('api_vendor_countries');
        });
    }
}
