<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddOwnerIdToRolesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('roles', function (Blueprint $table) {
            $table->integer('owner_id')->default(0);
        });
    }

    
    public function down()
    {
        Schema::table('roles', function (Blueprint $table) {
            $table->dropColumn('owner_id');
        });
    }
}
