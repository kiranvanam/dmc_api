<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVendorContractsApiDataMappingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vendor_contracts_api_data_mappings', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('vendor_id');
            $table->integer('country_id');
            $table->text('mappings')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vendor_contracts_api_data_mappings');
    }
}
