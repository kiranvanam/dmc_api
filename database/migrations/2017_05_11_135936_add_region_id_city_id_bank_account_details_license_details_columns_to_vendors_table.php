<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRegionIdCityIdBankAccountDetailsLicenseDetailsColumnsToVendorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('vendors', function (Blueprint $table) {
            $table->string('code', 20)->default("")->after('name');
            $table->integer('region_id')->default(0)->after('country_id');
            $table->integer('city_id')->default(0)->after('region_id');
            $table->text('bank_account_details')->after('contact_details')->nullable();
            $table->string('license_details',1000)->after('bank_account_details')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('vendors', function (Blueprint $table) {
            $table->dropColumn('code');
            $table->dropColumn('region_id');
            $table->dropColumn('city_id');
            $table->dropColumn('bank_account_details');
            $table->dropColumn('license_details');
        });
    }
}
