<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCountryWiseServiceTaxesTable extends Migration
{
    
    public function up()
    {
        Schema::create("country_wise_service_taxes", function (Blueprint $table) {
            $table->increments('id');
            $table->integer('country_id');
            $table->integer('vendor_service_id');
            $table->integer('tax_id');
            $table->string('tax_type', 30)->default('purchase');
        });
    }

    public function down()
    {
        Schema::dropIfExists('country_wise_service_taxes');
    }
}
