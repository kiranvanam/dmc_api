<?php

use App\Currency;
use App\Dmc;
use Columbus\Destination\Models\Location;
use Illuminate\Database\Seeder;

class GDTSeeder extends Seeder
{
    public function run()
    {
        $gdt = [ 'name' => 'Go Dubai Tourism',
                 'code' => 'GDT', 
                 'description' => "UAE based tour operator company" 
                ];

        $dmc = Dmc::firstOrCreate($gdt);

        echo "DMC created" . PHP_EOL;


        $agent = "";

        //dmc countries
        $uae =Location::where('code', 'AE')->first();
        $dmc->countries()->sync([$uae->id]);

        echo "DMC-Countries are mapped " . PHP_EOL;

        $destinations = [
          ["name" => "Fujairah","code" => "D!000609"],
          ["name" => "Ras al-Khaimah","code"=>"D!000608"],
          ["name" => "Umm al-Quwain", "code"=>"D!000607"],
          ["name" => "Sharjah & Ajman", "code" => "D!000605"],
          ["name" => "Abu Dhabi", "code"=>"D!000604"],
          ["name"=> "Dubai","code"=>"D!000160"]
        ];

        foreach ($destinations as $destination) {
          $destination['slug'] = str_slug($destination['name']);
          $destination['parent_id'] = $uae->id;
          $destnFound = Location::where('parent_id', $uae->id)
                      ->where('name',$destination['name'])
                      ->first();
          if(!$destnFound) {
            Location::create($destination);
          }
          
        }

        echo "DMC-Destinations data is seeded". PHP_EOL;

        //dmc currencies
        $currencies = Currency::whereIn('symbol', ['USD', 'AED', 'INR'])->get();

        $ids = $currencies->pluck('id')->all();
        
        $dmc->currencies()->sync($ids);

        echo "DMC-Currencies are mapped " . PHP_EOL;


    }
}
