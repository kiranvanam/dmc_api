<?php

use App\User;
use Columbus\Auth\Models\Permission;
use Columbus\Auth\Models\Resource;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class ResourceTableSeeder extends Seeder
{

    public function run()
    {

        $resourceConfigDir = '/resources';

        $files = Storage::disk('config')->files( $resourceConfigDir);


        foreach ($files as $file_name) {
            $file_name = Str::replaceFirst("/" , ".",  Str::substr($file_name, 0 , strlen($file_name) - 4));
            $this->createResource(config($file_name));
        }

        echo "Resource-Permission Data is seeded" . PHP_EOL;
    }


    function createResource($resourceAndPermissions, Resource $parent = null) {

        $record = $resourceAndPermissions['resource'];

        // if parent is there, make this resource as child
        if($parent)
            $record['parent_resource_id'] = $parent['id'];

        $resource = Resource::where('slug', $record['slug'])->whereIn('parent_resource_id', [$parent['id'],0])->first();
        if($resource) {
            $resource->update($record);
        } else {
            $resource = Resource::create($record);
        }

        //insert permissions
        $permissions = $resourceAndPermissions['permissions'];

        foreach ($permissions as $permission) {
            $permissionObj = Permission::where('slug', $permission['slug'])->where( 'resource_id', $resource->id)->first();
            $permission['resource_id'] = $resource->id;
            if($permissionObj) {
                $permissionObj->update($permission);
            }else{
                Permission::create($permission);
            }
        }

        // create sub resources
        if(isset($resourceAndPermissions['sub'])) {

            $subResources = $resourceAndPermissions['sub'];

            if($subResources) {
                foreach ($subResources as $subResource) {
                    $this->createResource($subResource, $resource);
                }
            }
        }
    }

}
