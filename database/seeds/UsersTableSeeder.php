<?php

use App\User;
use Columbus\Auth\Models\Permission;
use Columbus\Auth\Models\Resource;
use Columbus\Auth\Models\Role;
use Columbus\Auth\Models\Team;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{   
    protected $dmcOwnerAccount = [];

    protected $super_admin = [];
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {   
        $this->super_admin = [
                                "name" => "LexyCode",
                                "email" => "code@lexi-code.com",
                                "password" => Hash::make("lexi-code@123") 
                            ];
        $users[] = $this->super_admin;

        if(\App::environment() == "production") {
            $this->dmcOwnerAccount = [
              'name' => 'Dmc owner',
              'email' => 'admin@royaleagledubai.com',
              'password' => Hash::make("Admin@RetDubai")
            ];
        } else {
            $this->dmcOwnerAccount = [
                'name' => 'Dmc owner',
                'email' => 'dmc@domain.com',
                'password' => Hash::make("dmc@123")
            ];
        }

        $users [] = $this->dmcOwnerAccount;

        foreach ($users as $user) {
           $user = $this->createUserWith($user);
        }

        $this->createTeams();

        echo "User data seeded " . PHP_EOL;
    }

    protected function createUserWith($data)
    {
        if($user = User::where('email', $data['email'])->first() ) {
            $user->update($data);
        } else {
            $user = User::create($data);
        }
        return $user;
    }


    protected function createTeamWith($data)
    {   
        //$data['slug'] = str_slug($data['slug']);

        if($team = Team::where('name', $data['name'])->first() ) {
            $team->update($data);
        } else {
            $team = Team::create($data);
        }
        return $team;
    }

    public function createTeams()
    {
        $user = User::where('email', $this->super_admin['email'])->first();
        
        $this->createSuperAdmin($user);
        $this->createDmcAdmin($user);
    }

    public function createSuperAdmin($owner)
    {
        
        $role = Role::where('slug', 'super-admin')->first();
        //$user = User::where('email', 'nways@nways.com')->first();
        //assign role this user
        $owner->roles()->sync([$role->id]);

        // create team
        $team = [
          'name' => 'Nways Solutions',
          'owner_id' => $owner->id,
          'description' => "This is main team for this product, they have all rights"
        ];

        $team = $this->createTeamWith($team);

    }

    public function createDmcAdmin($owner)
    {
        $role = Role::where('slug', 'dmc-owner')->first();
        //user and  his roles
        $user = User::where('email', $this->dmcOwnerAccount['email'])->first();
        
        $user->roles()->sync([$user->id]);   
        
        // team & users
        $team = [
          'name' => 'Royal Eagle Tourism operations head',
          'owner_id' => $owner->id,
          'description' => "This is DMC company"
        ];

        $team = $this->createTeamWith($team);
    }
}
