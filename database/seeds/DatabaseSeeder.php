<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $this->call(ResourceTableSeeder::class);
      $this->call(RolesTableSeeder::class);
      $this->call(UsersTableSeeder::class);

      $this->call(CountriesTableSeeder::class);
      $this->call(CurrencySeeder::class);
      $this->call(VendorServicesTableSeeder::class);
    }

}
