<?php

use Columbus\Destination\Models\Location;
use GuzzleHttp\Client;
use Illuminate\Database\Seeder;


class CountriesTableSeederOld extends Seeder
{

  public function run()
  {

    $headers = [
                'Content-Type' => 'application/json',
                'Accept' => 'application/json',
                'api-key' => '2609a8afb98f00307a27fe161ec332eb'
              ];
    $client = new Client([
                    'base_uri' => 'http://api-sandbox.grnconnect.com/api/v3/',
                    'headers'=> $headers
                    ]);
    $response = $client->get('countries');
    $response = json_decode($response->getBody(), true);
    if(!$response) {
      $response = [];
    }
    $countries = collect($response['countries']);


    $countries->each(function($country) {
      $country['slug'] = str_slug($country['name']);
      Location::firstOrCreate($country);
    });

    echo "Countries data is seeded" . PHP_EOL;
  }
}
