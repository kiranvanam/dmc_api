<?php

use Columbus\SystemSettings\Models\Currency;
use Illuminate\Database\Seeder;

class CurrencySeeder extends Seeder
{
    protected $defaultCurrencies;

    function __construct()
    {
      $this->defaultCurrencies = collect(['AED', 'EUR', 'GBP', 'INR', 'PKR', 'USD',]);
    }

    public function run()
    {
       foreach (config('currency') as $symbol => $code) {
          $data['symbol'] = $symbol;
          $data['code'] = $code;

          Currency::firstOrCreate($data);
       };

      $filename = database_path('data/currencies.json');
      $file = fopen($filename, "r");
      $currencies = json_decode(fread($file,filesize($filename)));

      array_walk($currencies, function($currency) {
          $currencyRecord  = Currency::where('symbol', $currency->code)->first();
          if($currencyRecord) {
            $is_active =  $this->defaultCurrencies->search($currencyRecord->symbol) !== false ? true : false;
            $currencyRecord->update(['name' => $currency->name, 'is_active'=> $is_active]); 
          }
      });

      echo "Currency data is seeded" . PHP_EOL;
    }
}
