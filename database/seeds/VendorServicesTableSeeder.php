<?php

use Columbus\Contracts\Tours\Models\Service;
use Columbus\Contracts\Transfers\Models\TransferService;
use Columbus\Contracts\Vendors\Models\VendorService;
use Illuminate\Database\Seeder;

class VendorServicesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $modules = [
                /*[
                    "name" => "General",
                    "values" => "This is tax is applicable to any booking"
                ],*/

                [
                    "name" => "Accommodation",
                    "values" => "",
                    "children" => [
                        [
                            "name" => "Hotel",
                            "values" => ['Unrated', 1,2,3,4,5,6],
                            "group_by_label" => "Type",
                        ],
                        [
                            "name" => "Hotel Apartments",
                            "values" => ['Unrated', 1,2,3,4,5,6],
                            "group_by_label" => "Type"
                        ],
                        [
                            "name" => "Villa",
                            "values" => ['Unrated', 1,2,3,4,5,6],
                            "group_by_label" => "Type"
                        ]
                    ]
                ],
                [
                    "name" => "Visas",
                    "values" => ""
                ],
                [
                    "name" => "Transfers",
                    "values" => "",
                    "children" => [
                        [
                            "name" => "Airport Transfers",
                            "values" => "",
                            "group_by_label" => "Type"
                        ],
                        [
                            "name" => "Tour Transfers",
                            "values" => "",
                            "group_by_label" => "Type"
                        ],
                        [
                            "name" => "Meal Transfer",
                            "values" => "",
                            "group_by_label" => "Type"
                        ],
                        [
                            "name" => "Vehicle On Disposal-VOD",
                            "values" => "",
                            "group_by_label" => "Type"
                        ]
                    ]
                ],
                [
                    "name" => "Tours",
                    "values" => "",
                    "children" => [
                        [
                            "name" => "SIC",
                            "values" => "",
                            "group_by_label" => "Type"
                        ],
                        [
                            "name" => "Private",
                            "values" => "",
                            "group_by_label" => "Type"
                        ]
                    ]
                ],
                [
                    "name" => "Excursion Tickets",
                    "values" => ""
                ],
                [
                    "name" => "Restaurants",
                    "values" => ""
                ],
                [
                    "name" => "Guides",
                    "values" => ""
                ],
                /* [
                    "name" => "Packages",
                    "values" => ""
                ] */
            ];

            // first enable the country

            //second set services for this country
            $country = \Columbus\Destination\Models\Country::where('cca3', 'ARE')->first();

            foreach ($modules as $module) {

                $name = $module['name'];
                $module['slug'] = str_slug($name);
                $module['country_id'] = $country->id;
                $service = VendorService::where('name', $module['name'])->where('slug', $module['slug'])->first();
                $children = [];
                if(isset($module['children'])) {
                    $children = $module['children'];
                    unset($module['children']);
                }

                if(!$service) {
                    $service =VendorService::create($module);
                }


                if($children) {
                    foreach ($children as $childData) {
                            $childData['slug'] = str_slug($childData['name']);
                            $childData['country_id'] = $country->id;
                            $childData['parent_id'] = $service->id;
                            $subService = VendorService::where('name', $childData['name'])->where('slug', $childData['slug'])->first();

                            if(!$subService) {
                               $subService = VendorService::create($childData);
                            }else{
                                $subService->update($childData);
                            }
                            $fields = [ 'vendor_service_id' => $subService->id,
                                            'name' => $subService->name];

                            if($service->name == "Transfers") {
                                $fields['description'] = "";
                                $transferType = TransferService::where('vendor_service_id', $fields['vendor_service_id'] )->first();
                                if($transferType)
                                    $transferType->update($fields);
                                else
                                    TransferService::create($fields);
                            }else if($service->name == "Tours") {
                                $tourService = Service::where('vendor_service_id', $fields['vendor_service_id'] )->first();
                                if($tourService)
                                    $tourService->update($fields);
                                else
                                    Service::create($fields);
                            }
                    }
                }

            }

            echo "Vendor Services data is seeded" . PHP_EOL;
    }
}
