<?php

use Columbus\Destination\Models\Country;
use GuzzleHttp\Client;
use Illuminate\Database\Seeder;


class CountriesTableSeeder extends Seeder
{
    public function run() {
        $countries_path = database_path('seeds/data/countries.json');
        $countries_string = file_get_contents($countries_path);
        $countries_obj = json_decode($countries_string, true);
        if(!$countries_obj) {
            $countries_obj = [];
        }

        foreach ($countries_obj as $country_obj) {
            $country = Country::firstOrNew(['name' => $country_obj['name']]);
            $country['name'] = $country_obj['name']['common'];
            $country['slug'] = str_slug($country['name']);
            $country['cca2'] = $country_obj['cca2'];
            $country['cca3'] = $country_obj['cca3'];
            $country['nationality'] = $country_obj['demonym'];
            $country['currency'] = json_encode($country_obj['currency']);
            $country['calling_code'] = json_encode($country_obj['callingCode']);
            $country['region'] = $country_obj['region'];
            $country['subregion'] = $country_obj['subregion'];
            if(sizeof($country_obj['latlng']) == 2) {
                $country['lat'] = $country_obj['latlng'][0];
                $country['lng'] = $country_obj['latlng'][1];
            } else {
                $country['lat'] = 0;
                $country['lng'] = 0;
            }
            $country->save();
        }

        echo "Countries data is seeded" . PHP_EOL;
    }
}
