<?php

use App\User;
use Columbus\Auth\Models\Permission;
use Columbus\Auth\Models\Resource;
use Columbus\Auth\Models\Role;
use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
  
  public function run()
  {
    $roles = [
        [ 
          "name" => "Super Admin",
          "slug" => "super-admin",
          "module" => "staff",
          "description" => "You are Nways owner"
        ],
        [ 
          "name" => "Super User",
          "slug" => "dmc-owner",
          "module" => "staff",
          "description" => "You are DMC owner",
          'owner_id' => 1
        ],/*
        [
          "name" => "Distributer/Representative",
          "slug" => "distributer",
          "module" => "agents",
          "description" => "This is Distributer role allowed to maintain own Agents"
        ],
        [
          "name" => "Agent",
          "slug" => "agents",
          "module" => "agents",
          "description" => "This is Agent allowed to maintain his bookings (Hotels, Tours, Transfers, Meals) & teams"
        ],*/
      ];

      foreach ($roles as $role) {
        
        if($roleObj = Role::where('name', $role['name'])->where('module', $role['module'])->orWhere('slug',$role['slug'])->first()){
            $roleObj->update($role);
          }
        else {
            $role = Role::create($role);
        }
      }


      $this->assignSuperAdminPermissions();
      $this->assignDmcOwnerPermissions();


      echo "Roles & Base Role-Permission  relations are seeded " . PHP_EOL;
  }

  protected function assignDmcOwnerPermissions($value='')
  {
    //$user = User::where('email', 'dmc@domain.com')->first();

    $role = Role::where('slug', 'dmc-owner')->first();

    $excludePermissions = Resource::where('slug','dmc')->first()->permissions->pluck('id')->toArray();
    
    $permissions = Permission::whereNotIn('id', $excludePermissions)->get()->pluck('id');

    $role->permissions()->sync($permissions);
  }

  protected function assignSuperAdminPermissions()
  {
    
    $role = Role::where('slug', 'super-admin')->first();

    // assign permissions to role
    $permissions = Permission::all()->pluck('id');

    $role->permissions()->sync($permissions);
  }

}