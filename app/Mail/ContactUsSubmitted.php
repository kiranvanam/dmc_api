<?php

namespace App\Mail;

use App\ContactUs;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ContactUsSubmitted extends Mailable
{
    use Queueable, SerializesModels;

    public $contact_us;

    public function __construct(ContactUs $contactUs)
    {
        $this->contact_us= $contactUs;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from(config('mail.username'))
                    ->bcc( config('mail.bcc'))
                    ->subject( config('app.name') . " Confirmation of Contact-us Submit")
                    ->view('emails.contact-us.contact-us');
    }
}
