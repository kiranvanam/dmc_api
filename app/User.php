<?php

namespace App;

use Columbus\Auth\Models\Resource;
use Columbus\Auth\Models\Role;
use Columbus\Auth\VendorPermissionHelper;
use Columbus\Auth\PermissionChecker;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable, PermissionChecker, VendorPermissionHelper;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'code', 'name', 'designation', 'email', 'password', 'profile_pic_url', 'is_confirmed', 'is_active','is_suspended',
        'mobile', 'telephone', 'status', 'timezone', 'reason_for_suspending', 'agency_id', 'distributor_id', 'owner_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function roles()
    {
        return $this->belongsToMany(Role::class, 'user_has_roles');
    }

    public function permissions()
    {
        $user = static::with('roles')->find($this->id);
        $permissionIds = [];

        $user->roles->every( function($role) use(&$permissionIds) {
            $permissionIds = array_merge($permissionIds,
                                $role->permissions
                                ->pluck('pivot.permission_id')
                                ->toArray());
        });

        $resources = Resource::with(['permissions'=> function($query) use ($permissionIds) {
                                $query->whereIn('id', $permissionIds);
                            }])
                            ->get();

        return $resources;
    }

    public function hasPermission($permission)
    {
        if(empty($this->getResources()))
            $this->initialize($this->permissions());

        if($this->permissionSearch($permission) == false) {
            throw new AuthorizationException("You are not authorized to perform this action");
        }

        return true;
    }
}
