<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Columbus\SystemSettings\Utilities\CurrencyExchangeRateUpdate;

class DBSeeder extends Command
{

    protected $signature = "columbus:seed {module=All}";

    protected $description = 'Seeding all required Data';

    protected $consoleOutPut = [ "headers" => [ "Service", "Is Seeded"] , "body" => []];

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $this->info("Seeding all required/requested data-- start" . PHP_EOL);
        
        $this ->seedCountries()
            ->seedCurrencies()
            //->seedExchangeRates()
            ->seedVendorServices()            
            ->seedResources()
            ->seedRoles()
            ->seedUsers()
            ->passPortInstall(); // laravel-passport setup

        $this->table($this->consoleOutPut['headers'], $this->consoleOutPut['body'] );

        $this->info("Seeding all required/requested data -- end" . PHP_EOL);
    }

    protected function seedVendorServices()
    {
        $data = ["VendorServices", $this->isCommandToSeedVendorServices()];
        $this->addToConsoleOutPut($data);

        if( $data[1] == false ) 
            return $this;
        
        $this->executeSeederCommandFor('VendorServicesTableSeeder');

        return $this;
    }

    protected function seedCountries()
    {
        $data = ["Countries", $this->isCommandToSeedCountries() ];
        $this->addToConsoleOutPut($data);

        if( $data[1] == false ) 
            return $this;        

        $this->executeSeederCommandFor('CountriesTableSeeder');

        return $this;
    }


    protected function seedCurrencies()
    {
        $data = ["Currencies", $this->isCommandToSeedCurrencies() ];

        $this->addToConsoleOutPut($data);

        if( $data[1] == false )
            return $this;

        $this->executeSeederCommandFor('CurrencySeeder');                
    
        return $this;
    }

    protected function seedExchangeRates()
    {
        $data = ["ExchangeRates", $this->isCommandToSeedCurrencies() ];
        
        $this->addToConsoleOutPut($data);

        if( $data[1] == false ) 
            return $this;
        
        $currencyExchangeRates = new CurrencyExchangeRateUpdate;
        $currencyExchangeRates->update();
        
        $this->line("Exchange Rates are seeded");

        return $this;
    }

    protected function seedResources()
    {
        $data = ["Resources", $this->isCommandToSeedResources() ];
        
        $this->addToConsoleOutPut($data);

        if( $data[1] == false ) 
            return $this;
                
        $this->executeSeederCommandFor('ResourceTableSeeder');

        return $this;
    }

    protected function seedRoles()
    {
        $data = ["Roles", $this->isCommandToSeedRoles() ];
        
        $this->addToConsoleOutPut($data);

        if( $data[1] == false ) 
            return $this;
                
        $this->executeSeederCommandFor('RolesTableSeeder');

        return $this;
    }

    protected function seedUsers()
    {
        $data = ["Users", $this->isCommandToSeedUsers() ];
        
        $this->addToConsoleOutPut($data);

        if( $data[1] == false ) 
            return $this;
        
        $this->executeSeederCommandFor('UsersTableSeeder');

        return $this;
    }

    protected function passPortInstall() 
    {
        $this->call('passport:install');
    }

    //helpers
    protected function addToConsoleOutPut($outPut)
    {
        $this->consoleOutPut['body'][] = $outPut;
    }

    protected function executeSeederCommandFor($seederClassName)
    {
        $this->call('db:seed', [ '--class'=> $seederClassName]);
    }
    
    protected function isCommandToSeedVendorServices()
    {
        return $this->isCommandToSeedAllModules() || $this->argument('module') == 'Services';
    }

    protected function isCommandToSeedAllModules()
    {
        return $this->argument('module') == 'All';
    }

    protected function isCommandToSeedCurrencies()
    {
        return  $this->isCommandToSeedAllModules() || $this->argument('module') == "Currencies";    
    }

    protected function isCommandToSeedCountries()
    {
        return  $this->isCommandToSeedAllModules() || $this->argument('module') == "Countries";    
    }

    protected function isCommandToSeedResources()
    {
        return  $this->isCommandToSeedAllModules() || $this->argument('module') == "Resources";    
    }

    protected function isCommandToSeedRoles()
    {
        return  $this->isCommandToSeedAllModules() || $this->argument('module') == "Roles";    
    }

    protected function isCommandToSeedUsers()
    {
        return  $this->isCommandToSeedAllModules() || $this->argument('module') == "Users";    
    }

}
