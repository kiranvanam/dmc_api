<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\QueryException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Support\Facades\Response;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;
use Illuminate\Support\Facades\App;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
        \Illuminate\Auth\AuthenticationException::class,
        \Illuminate\Auth\Access\AuthorizationException::class,
        \Symfony\Component\HttpKernel\Exception\HttpException::class,
        \Illuminate\Database\Eloquent\ModelNotFoundException::class,
        \Illuminate\Session\TokenMismatchException::class,
        \Illuminate\Validation\ValidationException::class,
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception  $exception
     * @return void
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $exception)
    {

        if($request->expectsJson()) {    
        
            $response = [
                "status" => 520,
                "message" => "Unkown problem happened, please contact support team",
                "body" => [ "details" => "unkown problem happened" ]
            ];

            if ($exception instanceof ModelNotFoundException)
            {
                $response = [
                    "status" => 404,
                    "message" => 'Requested Data is not found',
                    "body"=> [
                        "details" => $exception->getMessage()
                    ]
                ];
            }

            if($exception instanceof QueryException)
            {
                //this is handling PdoException related things
                if(!isset($exception->getPrevious()->errorInfo)){
                    $body= $exception->getPrevious()->getMessage();
                }else{
                    $body = $exception->getPrevious()->errorInfo[2];
                }
                $response = [
                    "status" => 500,
                    "message" => "Enter valid data in fields",
                    "body" => [
                        $body
                    ]
                ];
            }

            if($exception instanceof ValidationException) {
                $response = [
                    'status' => 422,
                    'message' => 'Invalid data is provided',
                    'body' => $exception->validator->messages()
                ];

            }

            if($exception instanceof MethodNotAllowedHttpException) {
                $response = [
                    'status' => 405,
                    'message' => 'Invalid request method',
                    'body' => [ "details" => "This HTTP request method is not allowed on this resource" ]
                ];                
            }
            
            if($exception instanceof AuthorizationException) {
                $response = [
                    'status' => 403,
                    'message' => "You don't have permission to perform this action",
                    'body' => [ "details" => $exception->getMessage()]
                ];   
            }

            if($exception instanceof AuthenticationException) {
                $response = [
                                'status' => 401,
                                'message' => "You aren't logged-in",
                                'body' => [ "details" => "Please Login first" ]
                            ];
            }
            
            // Add debug info only for local environmentss
            if (env('APP_ENV') == "local" )
            {
                $debug = [
                    "request-details" => [
                        "url" => $request->url(),
                        "payload" => $request->all()
                    ],
                    "process-info" => [
                        "sql" => [],
                        "response" => []
                    ],
                    "exception" => [
                        "message" => $exception->getMessage(),
                        "file" =>  $exception->getFile(),
                        "line" => $exception->getLine(),
                        "Trace" => [
                            "body"  => $exception->getTraceAsString(),
                            "line" => $exception->getLine()
                        ]
                    ]
                ];
                $response['debug'] = $debug;
            }
            
            return  Response::json($response,$response['status']);
        }

        if($this->isHttpException($exception))
        {
            switch ($exception->getStatusCode())
            {
                // not found
                case 404:
                return redirect()->route('admin_home');
                break;

                /*// internal error
                case 500:
                return redirect()->route('admin_home');
                break;*/
            }
        }

        return parent::render($request, $exception);
    }

    /**
     * Convert an authentication exception into an unauthenticated response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Illuminate\Auth\AuthenticationException  $exception
     * @return \Illuminate\Http\Response
     */
    protected function unauthenticated($request, AuthenticationException $exception)
    {
        if ($request->expectsJson()) {
            return response()->json([
                                    'status' => 401,
                                    'message' => "You aren't logged-in",
                                    'body' => [ "details" => "Please Login first" ]
                                    ],
                                    401);
        }

        return redirect()->guest('login');
    }
}
