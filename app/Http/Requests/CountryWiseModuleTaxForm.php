<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CountryWiseModuleTaxForm extends FormRequest
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'country_id' => 'required|numeric',
            'vendor_service_id' => 'required|numeric',
            'tax_id' => 'required|numeric'
        ];
    }

    public function save()
    {  
        $data = $this->all();

        $data['id'] = \DB::table('country_wise_service_taxes')->insert($data);
        
        return $data;
    }

    public function update()
    {
        $dbTable = \DB::table('country_wise_service_taxes')->where('id', $this->get('id'));
        $data = $this->except('_method');
        $dbTable->update($data);

        return $data;
    }
}
