<?php

namespace App\Http\Middleware;

use Closure;

class Cors
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $domains = ['http://localhost:8080', 'http://godubaitourism.com', 'b2b.godubaitourism.com', 'http://b2b.godubaitourism.com'];

        $server = $request->server();
        //dd($server);
        if(isset($server['SERVER_NAME'])) {
            $origin = $server['SERVER_NAME'];
            
            if(in_array($origin, $domains)) {

                header('Access-Control-Allow-Origin: '. $origin);
                //header('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS');
                header('Access-Control-Allow-Headers: Origin, Content-Type, Authorization');
            }

        }

        return $next($request);
    }
}
