<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\ApiController;
use App\Http\Requests\CountryWiseModuleTaxForm;


class CountryWiseServiceTaxController extends ApiController
{

    public function index()
    {
        return $this->ok(\DB::table('country_wise_service_taxes')->get());
    }

    public function save(CountryWiseModuleTaxForm $countryWiseModuleTaxForm)
    {
        return $this->ok($countryWiseModuleTaxForm->save());
    }

    public function update(CountryWiseModuleTaxForm $countryWiseModuleTaxForm)
    {
        return $this->ok($countryWiseModuleTaxForm->update());
    }

    public function delete($id)
    {
        $module = \DB::table('country_wise_service_taxes')->where('id', $id)->delete();
        return  $this->ok($module, "Deleted successfully");
    }
}
