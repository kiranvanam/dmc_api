<?php

namespace App\Http\Controllers\Api;

use Illuminate\Support\Facades\DB;
use App\Http\Controllers\ApiController;
use Columbus\SystemSettings\Models\Currency;

class CurrencyController extends ApiController
{
    public function index() {
        return $this->ok(Currency::all(), "Fetching Currency data is Successful");
    }

    public function all()
    {
        return $this->ok(DB::table('country_default_currencies')->get());
    }

    public function save()
    {
        $data = request()->get('country_currencies');
        DB::table('country_default_currencies')
               ->where('module', 'vendors')
               ->delete();

        return $this->ok(DB::table('country_default_currencies')->insert($data), "Updated Successfully"); 
    }

}
