<?php

namespace App\Http\Controllers\Api;

use Columbus\SystemSettings\Models\Currency;
use App\Http\Controllers\ApiController;
use Columbus\Constants\AmountValueTypes;
use Columbus\Constants\TransferPointTypes;
use Columbus\Contracts\Vendors\Models\VendorService;
use Columbus\Utilities\FileManager;
use Columbus\Utilities\TimeZone;
use Illuminate\Http\Request;

class MiscApiController extends ApiController
{
    public function index()
    {
        return $this->ok([
                //'transferPointTypes'=> TransferPointTypes::values(),
                //'currencies' => Currency::currencies(),
                //'vendorServices' => VendorService::fetchAll(),
            ]);
    }

    public function deleteImages(Request $request, FileManager $fileManager)
    {
        $images = $request->get('images');

        $fileManager->removeFiles($images);

        return $this->ok([], "Images are deleted");
    }

    public function timeZones()
    {
        return $this->ok(TimeZone::timeZones(), "TimeZones");
    }
}
