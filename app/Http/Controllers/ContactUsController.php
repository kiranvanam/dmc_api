<?php

namespace App\Http\Controllers;

use App\ContactUs;
use App\Http\Controllers\ApiController;
use App\Mail\ContactUsSubmitted;


class ContactUsController extends ApiController
{
    
    public function create()
    {
        $contactus = ContactUs::create(request()->all());
        
        \Mail::to($contactus->email)
            ->send( new ContactUsSubmitted($contactus) );

        return $this->ok("submitted Successfully");
    }
}