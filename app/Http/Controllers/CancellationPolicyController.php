<?php

namespace App\Http\Controllers;

use App\CancellationPolicy;
use App\Http\Controllers\ApiController;

class CancellationPolicyController extends ApiController
{
    
    function index()
    {
        \DB::connection()->enableQueryLog();

        $query = CancellationPolicy::where('service_id', request('service_id'))
                                    ->where('service', request('service'));

        // add subservice id too if present in request
        $sub_service_id = request('sub_service_id'); 
        if( $sub_service_id !=  "null" && !empty($sub_service_id) ) {
            $query->where('sub_service_id', $sub_service_id);
        }
        $policies = $query->get();
        
        return $this->ok($policies);
    }

    function create()
    {
        $data = request()->all();
        $policy = CancellationPolicy::create($data);
        return $this->ok($policy, "Cancellation Policy created");
    }

    function update($cancellation_policy_id)
    {
        $data = request()->all();
        $policy = CancellationPolicy::findOrFail($cancellation_policy_id);
        $policy->update($data);
        return $this->ok($policy, "Policy Details are updated");
    }

    function delete($cancellation_policy_id)
    {
        $policy = CancellationPolicy::findOrFail($cancellation_policy_id);
        $policy->delete();
        return $this->ok($policy, "Cancellation Policy has been Deleted");
    }
}