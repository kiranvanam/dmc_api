<?php

namespace App\Http\Controllers;

use App\Image;
use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class GalleryUploaderController extends ApiController
{
    public function index()
    {
        $service = request('service', "vehicle");
        $service_id = request('service_id', 1);

        return $this->ok(
                        Image::where('imageable_type', $service)
                            ->where('imageable_id', $service_id)
                            ->get()
                            ->pluck('path')
                    );
    }


    public function save(Request $request)
    {

        if ($request->hasFile('file')) {

            $service = $request->service;
            $service_id = $request->service_id;
            $service_code = $request->service_code;
            $folder =  $request->folder;

            // if folder explicity sepecified, then use that one to created folder, by default folder name same as service
            $folder =  empty($folder) ? $service : $folder;

            $path =  "uploads/$folder/$service_code";

            if( $folder == "bookings" && ( $service == 'visa' || $service == "visas")){
                $path = "$path/$service";
            }

            $filenames = [];
            //image validation rule
            $rules = array('file' => 'required'); //'required|mimes:png,gif,jpeg,txt,pdf,doc'

            $file = $request->file('file');

            $validator = Validator::make(array('file'=> $file), $rules);

            if($validator->passes()){
                $filename = $file->getClientOriginalName();
                $file->move($path, $filename);
                $path = "/$path/$filename";
                if( $request->has('thumbnail') == false ) {
                    Image::create([ "path" => $path, 'imageable_id'=> $service_id, "imageable_type"=> $service ]);
                }
            }

            //'imageable_id', 'imageable_type'
            return $this->ok($path, "Image uploaded");
        }

        return $this->invalidRequest( ["file" => "Requet doesn't have field 'file'."] );
    }

    public function deleteImage()
    {
        $imagePath = request('path');

        Storage::delete( ltrim($imagePath, "/") );

        $image = Image::where('path', $imagePath)->first();

        if(!empty($image)) {
            $image->delete();
            return $this->ok("Image Deleted successfully");
        }
        return $this->ok("Problem happened while deleting image");
    }

}