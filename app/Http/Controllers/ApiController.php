<?php

namespace App\Http\Controllers;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Response as HTTPResponse;
use Illuminate\Support\Facades\Response;

/*
 * 200 – OK – Eyerything is working
   201 – OK – New resource has been created
   204 – OK – The resource was successfully deleted/updated, The server successfully processed the request, but is not returning any content

   304 – Not Modified – The client can use cached data

   400 – Bad Request – The request was invalid or cannot be served. The exact error should be explained in the error payload. E.g. „The JSON is not valid“
   401 – Unauthorized – The request requires an user authentication
   403 – Forbidden – The server understood the request, but is refusing it or the access is not allowed.
   404 – Not found – There is no resource behind the URI.
   422 – Un-processable Entity – Should be used if the server cannot process the enitity, e.g. if an image cannot be formatted or mandatory fields are missing in the payload.

   500 – Internal Server Error – API developers should avoid this error. If an error occurs in the global catch blog, the stracktrace should be logged and not returned as response.
 */
class ApiController extends Controller
{

    protected $status_code = 200;
    protected $message = "";
    protected $body = [];


    protected function getStatusCode()
    {
        return $this->status_code;
    }

    protected function setStatusCode($status_code)
    {
        $this->status_code = $status_code;
        return $this;
    }

    private function getMessage()
    {
        return $this->message;
    }

    private function setMessage($message)
    {
        $this->message = $message;
        return $this;
    }

    private function getBody()
    {
        return $this->body;
    }

    private function setBody($body)
    {
        $this->body = $body;
        return $this;
    }

    protected function sendResponse($headers =[])
    {
        return Response::json($this->prepareResponse(), $this->getStatusCode(), $headers);
    }

    private function prepareResponse()
    {

        return [
          "status" => $this->getStatusCode(),
          "message" => $this->getMessage(),
          "body" => $this->getBody()
        ];
    }

    protected function notFound($message = "Not Found")
    {
        return $this->setStatusCode(HTTPResponse::HTTP_NOT_FOUND)
                    ->setMessage($message)
                    ->sendResponse();
    }

    // 500
    protected function internalErrorStatusCode()
    {
        return $this->setStatusCode(HTTPResponse::HTTP_INTERNAL_SERVER_ERROR);
        return $this;
    }

    protected function internalError($message = "Internal Error, Please try again", $errors=[])
    {

        return $this->internalErrorStatusCode()
                    ->setMessage($message)
                    ->setBody($errors)
                    ->sendResponse();
    }

    protected function invalidRequest($body, $message = "This action can't be success due to insufficient data") 
    {
        return $this->setStatusCode(HTTPResponse::HTTP_BAD_REQUEST)
                    ->setMessage($message)
                    ->setBody($body)
                    ->sendResponse();
    }

    // 200
    protected function ok($body, $message="the request processed successfully")
    {
        return $this->setStatusCode(HTTPResponse::HTTP_OK)
                ->setMessage($message)
                ->setBody($body)
                ->sendResponse();
    }
    
    protected function updated($body, $message="Updated successfully")
    {
        return $this->setStatusCode(HTTPResponse::HTTP_OK)
                ->setMessage($message)
                ->setBody($body)
                ->sendResponse();
    }

    protected  function validationErrors($body, $message="Some invalid data is provided")
    {
        return $this->setStatusCode(HTTPResponse::HTTP_UNPROCESSABLE_ENTITY)
                ->setMessage($message)
                ->setBody($body)
                ->sendResponse();
    }

    //201 – OK – New resource has been created
    protected function created($body,$message="saved successfully")
    {
        return $this->setStatusCode(HTTPResponse::HTTP_CREATED)
            ->setMessage($message)
            ->setBody($body)
            ->sendResponse();
    }

    //422 -- replacing trait method for formatting final response body send for AJAX requests
    protected function formatValidationErrors(Validator $validator)
    {
        return  $this->setStatusCode(HTTPResponse::HTTP_UNPROCESSABLE_ENTITY)
            ->setMessage("Enter valid data in fields")
            ->setBody($validator->errors()->getMessages())
            ->prepareResponse();
    }

    protected function getDatesFromRequest($request)
    {
        if($request['dates'])
            return $request['dates'];

        $fromDate = Carbon::createFromFormat('Y-n-j', $request['fromDate']);
        $toDate = Carbon::createFromFormat('Y-n-j', $request['toDate']);
        $currentDate = $fromDate->copy();
        $dates = [];
        while($currentDate <= $toDate) {
            if(in_array($currentDate->dayOfWeek, $request['days']))
                array_push($dates, $currentDate->copy()->toDateString());
            $currentDate->addDay();
        }
        return $dates;
    }

}
