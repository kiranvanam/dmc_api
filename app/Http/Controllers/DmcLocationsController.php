<?php

namespace App\Http\Controllers;

use Columbus\Destination\Models\Location;
use Illuminate\Http\Request;

class DmcLocationsController extends ApiController
{
    
    public function index()
    {
        return $this->ok(Location::where('code', 'AE')->with('destinations')->first());
    }
}
