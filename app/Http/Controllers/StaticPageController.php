<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class StaticPageController extends Controller
{

    protected $dmc="go-dubai";

    protected $data = [
                    "go-dubai" => [
                        'agentLoginUrl' => "http://b2b.godubaitourism.com/auth/login",
                        'agentRegisterUrl' => "http://b2b.godubaitourism.com/auth/register",
                        "title" => "Go Dubai Tourism"
                    ],
                    "royal-eagle" => [
                        'agentLoginUrl' => "http://b2b.godubaitourism.com/auth/login",
                        'agentRegisterUrl' => "http://b2b.godubaitourism.com/auth/register",
                        "title" => "Royal Eagle Tourism"
                    ]
                ];

    protected function prepareData($viewName)
    {
        $data = $this->data[$this->dmc];
        $data[$viewName] = 'active';
        $data['dmc'] = $this->dmc;
        $data['view'] = "front-end.$this->dmc.$viewName";
        return $data;
    }

    public function home()
    {
        $data = $this->prepareData("home");
        return view($data['view'], [ 'data' => $data]);
    }

    public function about()
    {
        $data = $this->prepareData("about-us");
        return view($data['view'], [ 'data' => $data]);
    }

    public function services()
    {
        $data = $this->prepareData("services");
        return view($data['view'], [ 'data' => $data]);
    }

    public function careers()
    {
        $data = $this->prepareData("careers");
        return view($data['view'], [ 'data' => $data]);
    }

    public function contact()
    {
        $data = $this->prepareData("contact-us");
        return view($data['view'], [ 'data' => $data]);
    }

    public function brochures($value='')
    {
        $data = $this->prepareData("e-brochures");
        return view($data['view'], [ 'data' => $data]);
    }

}
