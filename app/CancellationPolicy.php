<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CancellationPolicy extends Model
{
    protected $fillable = ['service_id', 'sub_service_id', 'service',
                            'booking_start_date', 'booking_end_date',
                            'travelling_start_date', 'travelling_end_date',
                            'is_non_refundable',
                            'policy_rules'
                        ];

    protected $casts = [
        'policy_rules' => 'array'
    ];

    static function policiesFor($service_id, $service)
    {
        return static::where('service_id',$service_id)->where('service', $service)->first();
    }

    static function policiesForPrivateTourVehicle($service_id)
    {
        return static::policiesFor($service_id, "tour-title-vendor-transfers");
    }

    static function policiesForGuide($service_id)
    {
        return static::policiesFor($service_id, "guide-vendor");
    }

    static function policiesForMealVendor($service_id)
    {
        return static::policiesFor($service_id, "meal-vendor");
    }

}