<?php

namespace App\Providers;

use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Support\ServiceProvider;

// To map many to many polymorphic relations ships with customer name instead of complete file name space
class PolyMorphicRelationsShipServiceProvider extends ServiceProvider
{

    public function boot()
    {
        Relation::morphMap([
            'vehicles' =>  'Columbus\Contracts\Vehicles\Models\Vehicle'
        ]);
    }

    public function register()
    {
        
    }
}
