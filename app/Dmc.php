<?php

namespace App;

use Columbus\Destination\Models\Location;
use Illuminate\Database\Eloquent\Model;

class Dmc extends Model
{
  public $timestamps = false;
  protected $fillable = ['name', 'code', 'description'];


  public function countries()
  {
    return $this->belongsToMany(Location::class, 'dmc_countries', 'dmc_id', 'country_id');
  }

  public function currencies()
  {
    return $this->belongsToMany(Currency::class, 'dmc_currencies', 'dmc_id', 'currency_id');
  }
}
