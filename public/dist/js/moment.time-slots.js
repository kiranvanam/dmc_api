if(moment === undefined) {
  throw Error("This package required moment.js as it's dependency");
} else {
  moment.fn.generateTimeslots = function(options) {
    this.validates(options)
    var slot = '';
    var timeslots = [];
    options = this.prepareOptions(options);
    while(slot !== options.endtime) {
      var timeslot = {}
      timeslot.startime = this.format(options.format);
      this.add(options.amount, options.unit);
      slot = this.format(options.format);
      timeslot.endtime = slot;
      timeslots.push(timeslot);
    }
    return timeslots;
  };

  moment.fn.prepareOptions = function(options) {
    var intervalmeta = options.interval.split(" ");
    options.amount = parseInt(intervalmeta[0]);
    options.unit = intervalmeta[1];
    var endtime = moment(options.endtime, options.format);
    endtime.subtract(endtime.milliseconds(), 'milliseconds');
    var timegap = endtime.unix() - this.unix();
    var duration = moment.duration(options.amount, options.unit);
    var diffseconds = timegap % duration.asSeconds();
    if(diffseconds !== 0) {
      endtime.subtract(diffseconds, 'seconds');
      options.endtime = endtime.format(options.format);
    }
    return options;
  };

  moment.fn.validates = function(options) {
    this.validateOptions(options);
    this.validateEndtime(options);
    this.validateFomrat(options);
    this.validateEndtimeParsable(options);
    this.validateEndtimeGreaterThanStartTime(options);
    this.validateInterval(options);
  }
  
  moment.fn.validateOptions = function(options){
    if(typeof options != 'object') {
      throw Error('options must be an object');
    }
  };

  moment.fn.validateEndtime = function(options) {
    if(options.endtime === undefined) {
      throw Error('options must include a endtime property');
    }
  };

  moment.fn.validateFomrat = function(options) {
    if(options.format === undefined) {
      throw Error('options must include a format property');
    }
  };

  moment.fn.validateEndtimeParsable = function(options) {
    var endtime = moment(options.endtime, options.format);
    if(endtime.toString() == 'Invalid date' || endtime.format(options.format) != options.endtime) {
      throw Error("Couldn't parse endtime with the format you have specified");
    }
  };

  moment.fn.validateEndtimeGreaterThanStartTime = function(options) {
    var endtime = moment(options.endtime, options.format);
    if(endtime <= this) {
      throw Error('endtime must be greater than startime');
    }
  }
  moment.fn.validateInterval = function(options) {
    if(options.interval === undefined) {
      throw Error("options must include interval");
    }

    var intervaltypes = ['hour', 'hours', 'minutes', 'h', 'm'];
    var intervalmeta = options.interval.split(" ");
    if(intervalmeta.length !== 2 || parseInt(intervalmeta[0]) == NaN || intervaltypes.indexOf(intervalmeta[1]) < 0) {
      throw Error('Invalid interval');
    }
  };
}