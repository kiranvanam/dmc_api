---
title: API Reference

language_tabs:
- bash
- javascript

includes:

search: true

toc_footers:
- <a href='http://github.com/mpociot/documentarian'>Documentation Powered by Documentarian</a>
---
<!-- START_INFO -->
# Info

Welcome to the generated API reference.
[Get Postman Collection](http://localhost/docs/collection.json)
<!-- END_INFO -->

#general
<!-- START_6fe98d7ced9bf8a790a2e2c2cb69689c -->
## Show the application&#039;s login form.

> Example request:

```bash
curl "http://localhost/api/v1/login" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost/api/v1/login",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
null
```

### HTTP Request
`GET api/v1/login`

`HEAD api/v1/login`


<!-- END_6fe98d7ced9bf8a790a2e2c2cb69689c -->
<!-- START_8c0e48cd8efa861b308fc45872ff0837 -->
## Handle a login request to the application.

> Example request:

```bash
curl "http://localhost/api/v1/login" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost/api/v1/login",
    "method": "POST",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`POST api/v1/login`


<!-- END_8c0e48cd8efa861b308fc45872ff0837 -->
<!-- START_fb2ae43e2e99ff4e90f22ba03801a735 -->
## Log the user out of the application.

> Example request:

```bash
curl "http://localhost/api/v1/logout" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost/api/v1/logout",
    "method": "POST",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`POST api/v1/logout`


<!-- END_fb2ae43e2e99ff4e90f22ba03801a735 -->
<!-- START_82703f5860426190c1aec309db25fa45 -->
## Show the application registration form.

> Example request:

```bash
curl "http://localhost/api/v1/register" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost/api/v1/register",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
null
```

### HTTP Request
`GET api/v1/register`

`HEAD api/v1/register`


<!-- END_82703f5860426190c1aec309db25fa45 -->
<!-- START_8ae5d428da27b2b014dc767c2f19a813 -->
## Handle a registration request for the application.

> Example request:

```bash
curl "http://localhost/api/v1/register" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost/api/v1/register",
    "method": "POST",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`POST api/v1/register`


<!-- END_8ae5d428da27b2b014dc767c2f19a813 -->
<!-- START_f74f5d180f1ce42ff67f887b656ea4d3 -->
## Display the form to request a password reset link.

> Example request:

```bash
curl "http://localhost/api/v1/password/reset" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost/api/v1/password/reset",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
null
```

### HTTP Request
`GET api/v1/password/reset`

`HEAD api/v1/password/reset`


<!-- END_f74f5d180f1ce42ff67f887b656ea4d3 -->
<!-- START_41c40ad08033960fe2cc3dcaf77839de -->
## Send a reset link to the given user.

> Example request:

```bash
curl "http://localhost/api/v1/password/email" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost/api/v1/password/email",
    "method": "POST",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`POST api/v1/password/email`


<!-- END_41c40ad08033960fe2cc3dcaf77839de -->
<!-- START_1da0af59e87a37b059ee0ca68283c2a7 -->
## Display the password reset view for the given token.

If no token is present, display the link request form.

> Example request:

```bash
curl "http://localhost/api/v1/password/reset/{token}" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost/api/v1/password/reset/{token}",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
null
```

### HTTP Request
`GET api/v1/password/reset/{token}`

`HEAD api/v1/password/reset/{token}`


<!-- END_1da0af59e87a37b059ee0ca68283c2a7 -->
<!-- START_a62f1703e9fba891a3e20ff27854aac0 -->
## Reset the given user&#039;s password.

> Example request:

```bash
curl "http://localhost/api/v1/password/reset" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost/api/v1/password/reset",
    "method": "POST",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`POST api/v1/password/reset`


<!-- END_a62f1703e9fba891a3e20ff27854aac0 -->
